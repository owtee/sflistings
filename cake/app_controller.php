<?php
/* SVN FILE: $Id: app_controller.php 6305 2008-01-02 02:33:56Z phpnut $ */
/**
 * Short description for file.
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *								1785 E. Sahara Avenue, Suite 490-204
 *								Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link				http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package			cake
 * @subpackage		cake.cake
 * @since			CakePHP(tm) v 0.2.9
 * @version			$Revision: 6305 $
 * @modifiedby		$LastChangedBy: phpnut $
 * @lastmodified	$Date: 2008-01-01 20:33:56 -0600 (Tue, 01 Jan 2008) $
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		cake
 * @subpackage	cake.cake
 */
 
 
class AppController extends Controller {

function checkSession()
    {
		
		$tr=$this->Session->read('admin_id');
        if(empty($tr))
        {
            $this->redirect('/admin/');
        }
		
    }
	
//the public variables are used for PAYPAL
		public $returnUrl="";
		public $cancelUrl="";
		public $notifyUrl="";
		public $amountPay=NULL;
		public $userEmail=NULL;
		public $eventId=NULL;
		public $userId=NULL;
		public $merchantEmail=NULL;
		public $itemName=NULL;
		public $itemNumber=NULL;
		public $itemQuantity=NULL;
		public $invoice=NULL;
		public $custom=NULL;
		public $paypalUrl='https://www.sandbox.paypal.com/cgi-bin/webscr';
		
		function paypal()
			{
				$this->Paypal->paypal_class();
				$this->Paypal->paypal_url = $this->paypalUrl;   // testing paypal url
			
				$this->Paypal->add_field('business', $this->merchantEmail);
				$this->Paypal->add_field('return', $this->returnUrl);
				//$this->Paypal->add_field('cancel_return', $this->cancelUrl);
				$this->Paypal->add_field('notify_url', $this->notifyUrl);
				$this->Paypal->add_field('item_name', $this->itemName);
				$this->Paypal->add_field('amount', $this->amountPay);
				$this->Paypal->add_field('quantity', $this->itemQuantity);
				$this->Paypal->add_field('custom', $this->custom);
				$this->Paypal->add_field('invoice',$this->invoice);       
				$this->Paypal->add_field('cbt','Return to Xoimages.com to complete your purchase');
				$this->Paypal->submit_paypal_post(); // submit the fields to paypal
			}
			
	
	
	
	
			

}
?>