<?php
/* SVN FILE: $Id: routes.php 6305 2008-01-02 02:33:56Z phpnut $ */
/**
 * Short description for file.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *								1785 E. Sahara Avenue, Suite 490-204
 *								Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link				http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package			cake
 * @subpackage		cake.app.config
 * @since			CakePHP(tm) v 0.2.9
 * @version			$Revision: 6305 $
 * @modifiedby		$LastChangedBy: phpnut $
 * @lastmodified	$Date: 2008-01-01 20:33:56 -0600 (Tue, 01 Jan 2008) $
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.thtml)...
 */
	 $Route->connect('/', array('controller' => 'users', 'action' => 'index'));
	 //$Route->connect('/search', array('controller' => 'searches', 'action' => 'index'));
	 $Route->connect('/search/*', array('controller' => 'searches', 'action' => 'index'));
	 $Route->connect('/city/*', array('controller' => 'users', 'action' => 'index'));
	//$Route->connect('/', array('controller' => 'admins', 'action' => 'login'));
	$Route->connect('/admin', array('controller' => 'admins', 'action' => 'login'));
	$Route->connect('/commercialusers', array('controller' => 'admins', 'action' => 'login'));
	$Route->connect('/content/*', array('controller' => 'contents', 'action' => 'content'));
	$Route->connect('/feedback', array('controller' => 'contents', 'action' => 'feedback'));
	$Route->connect('/listing/*', array('controller' => 'listings', 'action' => 'index'));
	$Route->connect('/mylisting/*', array('controller' => 'listings', 'action' => 'mylisting'));
	$Route->connect('/result/*', array('controller' => 'searches', 'action' => 'result'));
	$Route->connect('/listingdetail/*', array('controller' => 'listings', 'action' => 'listingdetail'));
	$Route->connect('/signup', array('controller' => 'users', 'action' => 'signup'));
	$Route->connect('/editlisting/*', array('controller' => 'listings', 'action' => 'editlisting'));
	$Route->connect('/editimage/*', array('controller' => 'listings', 'action' => 'editimage'));
	$Route->connect('/updatelisting/*', array('controller' => 'listings', 'action' => 'updatelisting'));
	$Route->connect('/signin', array('controller' => 'users', 'action' => 'signin'));
	$Route->connect('/editsearch', array('controller' => 'searches', 'action' => 'editsearchparameter'));
	$Route->connect('/myevent', array('controller' => 'users', 'action' => 'myevent'));
	$Route->connect('/mynewsletter/*', array('controller' => 'users', 'action' => 'mynewsletter'));
	$Route->connect('/unsubscribe/*', array('controller' => 'users', 'action' => 'unsubscribe'));
	$Route->connect('/createevent', array('controller' => 'users', 'action' => 'createevent'));
	$Route->connect('/searchresult/*', array('controller' => 'searches', 'action' => 'searchresult'));
	$Route->connect('/uploadbroker', array('controller' => 'users', 'action' => 'uploadbroker'));
	$Route->connect('/editimage/*', array('controller' => 'listings', 'action' => 'editimage'));
	$Route->connect('/addmailinglist', array('controller' => 'users', 'action' => 'addmailinglist'));
	$Route->connect('/mailaddress/*', array('controller' => 'users', 'action' => 'mailaddress'));
	$Route->connect('/thankyou/*', array('controller' => 'users', 'action' => 'thankyou'));
	$Route->connect('/myaccount/*', array('controller' => 'users', 'action' => 'myaccount'));
	$Route->connect('/myeditaccount/*', array('controller' => 'users', 'action' => 'myeditaccount'));
	$Route->connect('/forgotpassword', array('controller' => 'users', 'action' => 'forgotpassword'));
	$Route->connect('/changepassword', array('controller' => 'users', 'action' => 'changepassword'));
	$Route->connect('/results/*', array('controller' => 'searches', 'action' => 'results'));
	$Route->connect('/editSearch/*', array('controller' => 'searches', 'action' => 'editSearch'));
	$Route->connect('/myinfo/*', array('controller' => 'searches', 'action' => 'myinfo'));
	$Route->connect('/myalllisting/*', array('controller' => 'searches', 'action' => 'myalllisting'));
	$Route->connect('/adminlisting/*', array('controller' => 'listings', 'action' => 'addproperty'));
	$Route->connect('/admineditlisting/*', array('controller' => 'listings', 'action' => 'editproperty'));
        $Route->connect('/copylisting/*', array('controller' => 'listings', 'action' => 'copylisting'));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	$Route->connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
/**
 * Then we connect url '/test' to our test controller. This is helpfull in
 * developement.
 */
	$Route->connect('/tests', array('controller' => 'tests', 'action' => 'index'));
?>