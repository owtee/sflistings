<?php
 
/*
* File: CurrencyConverter.php
* Author: Abhsihek Shaw
* Copyright: NavSoft
* Date: 15/10/09
*
* This program is free software; you can redistribute it and/or 
* modify it under the terms of the GNU General Public License 
* as published by the Free Software Foundation; either version 2 
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNU General Public License for more details: 
* http://www.gnu.org/licenses/gpl.html
*
*/
 
class CurrencyConverter extends Object {
 
   function CurrencyConverter() {
   // Dummy Function Kept for code compatibility
       }
   
   /* Perform the actual conversion, defaults to �1.00 GBP to USD */
   function convert($amount=1,$from="USD",$to="EUR",$decimals=2) {
        if($from == $to)
		{
		   return("$".$amount);
		}
		else
		{
		if(intval($amount) > 0){
		$amount = intval($amount);
		}
		
        $returnHtml = array();
		$page = 'http://www.google.com/search?&q='.$amount.'+' . $from . '+in+' . $to;
		$returnRawHtml = file_get_contents( $page );	
		preg_match_all('/<h2 class=r(.*)\<\/h2\>/Uis',$returnRawHtml,$returnHtml,PREG_PATTERN_ORDER);
		//----------right------//
		//preg_match( '/= (([0-9]|\.|,|\ )*)/',$returnRawHtml,$returnHtml,PREG_OFFSET_CAPTURE, 3);
		//----------right------//
        if (isset($returnHtml[0][0])) 
		{
          $gRate = strip_tags($returnHtml[0][0]);
		  $arr=explode("=",$gRate);
		  $gRate = ereg_replace("[^0-9.]", "", $arr[1]);
		  return(number_format($gRate,$decimals));
        }
        else {
            return "<span style='color:red;'>Sorry Convertion Unavilable!</span>";
       } 
	   }
   }
   
    function downloadExchangeRates() {
	 // Dummy Function Kept for code compatibility
	} 
} 
?>