<?php
/**
* Navigators Software Private Limited
* Name: Abhsihek Shaw
* Date: 21/10/2009
* Date of Modification: 
* Reason of the Controller: To Show the statistics for the most visited page and keyword.
*/
ob_start(); 
class StatisticsController extends AppController
{
  
  var $name = 'Statistics'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Category','Adminmainmenu','Admin','Filter','Categoryassignfilter','Statistics','Listing','Neighborhood','City','Imagelisting','Filterlisting','User','Neighborhoodlisting','Listing_counter','User_ad','Metatag');
  var $components = array('Pagination','GoogleGraph'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function viewstatistics()
  {
  	 //$this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Searches';	 
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 //$this->set('cityStat',$this->Statistics->getCityStat()); 
	 //$this->set('neighborhoodStat',$this->Statistics->getNeighborhoodStat()); 
	 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	

	 //FillingUp the valid dates.
     $sql="SELECT date(MIN(DATE)) AS MIN_DATE,date(MAX(DATE)) AS MAX_DATE FROM statistics"; 
	 $rs = mysql_query($sql) or die(mysql_error().$sql);
     $dateArr=array();
     while($rec=mysql_fetch_assoc($rs))
	{
		array_push($dateArr,$rec['MIN_DATE']);
		array_push($dateArr,$rec['MAX_DATE']);
	} 
     	 
	$this->set('dateArr',$dateArr);	
	
	$dateDefine="";
	$titleDate=" - All Time.";
	
	if(isset($_GET['srchDate']))
	{
	   $exploder=explode("/",$_GET['srchDate']);
	   $dateDefine=" AND date(statistics.DATE)='{$exploder[2]}-{$exploder[0]}-{$exploder[1]}'";
	   $titleDate=" - on {$_GET['srchDate']}.";
    }
	else
	{
	  $exploder=explode("-",$dateArr[1]);
	  $dateDefine=" AND date(statistics.DATE)='{$dateArr[1]}'"; // showing the current or the maximum date
	  $titleDate=" - on {$exploder[1]}/{$exploder[2]}/{$exploder[0]}.";
	}
    
	$this->set("dateDefine",$dateDefine);
	$this->set("titleDate",$titleDate);

//======== Drawing Time Based Statistics =========
  if(isset($_GET['srchDate']))
   {
		  $jsonData="";
		  $sql="SELECT DISTINCT statistics.NEIGHBORHOOD AS NEIGHBORHOOD_ID,neighborhoods.neighborhood_name AS NEIGHBORHOOD FROM neighborhoods,statistics WHERE neighborhoods.id = statistics.NEIGHBORHOOD {$dateDefine} LIMIT 0,10"; 
		  $rs = mysql_query($sql) or die(mysql_error().$sql);
		  $timeNeighborId=array();
		  $timeNeighborName=array();
		  $timeNeighborPrice=array();
		  $jsonData="[";
		  while($rec=mysql_fetch_assoc($rs))
		  {
		  $jsonData.="{";
		  $jsonData.="\"label\":\"{$rec['NEIGHBORHOOD']}\",";
		  $jsonData.="\"data\":[";
		  array_push($timeNeighborId,$rec['NEIGHBORHOOD_ID']);
		  array_push($timeNeighborName,$rec['NEIGHBORHOOD']);
		  
		  $sql1="SELECT statistics.NEIGHBORHOOD,time_format(time(statistics.DATE),'%H') AS HOUR,time_format(time(statistics.DATE),'%i') AS MINUTE,statistics.MAX_PRICE,statistics.MIN_PRICE,statistics.DATE FROM statistics WHERE NEIGHBORHOOD={$rec['NEIGHBORHOOD_ID']} {$dateDefine}"; 
		  $rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
			  while($rec1=mysql_fetch_assoc($rs1))
			  {
			     for($zee=0;$zee<24;$zee++)
				 {
					 if($zee==$rec1['HOUR'])
					 {
					 $hour_fix=(int) $rec1['HOUR']; //strange bug fix for json.
					 $jsonData.="[{$hour_fix}.{$rec1['MINUTE']},{$rec1['MAX_PRICE']}],";
					  array_push($timeNeighborPrice,$rec1['MAX_PRICE']);
					 }
					 else
					 {
					 $jsonData.="[null,null],";
					 }
				 }
			  }
			   $jsonData=substr($jsonData,0,strlen($jsonData)-1); // for removing the extra comma
			   $jsonData.="]},";
		  } 
		  $jsonData=substr($jsonData,0,strlen($jsonData)-1); // for removing the extra comma
		  $jsonData.="];";
		  if(!empty($timeNeighborPrice))
		      $maxPrice=max($timeNeighborPrice)+1000;
		   else
		     $maxPrice=1000;
		$this->set("maxPrice",$maxPrice);
		$this->set("jsonData",$jsonData);
		
		//$sql="SELECT DISTINCT statistics.CITY AS CITY_ID,cities.city_name AS CITY_NAME FROM statistics,cities WHERE cities.id = statistics.CITY {$dateDefine}"; 
		  $sql="SELECT id,city_name from cities ORDER BY ID"; 
		  
		  $rs = mysql_query($sql) or die(mysql_error().$sql);
		  $selData=$_GET['srchDate'];
		  $cityNames="<tr><td><span style='font-size:12px;'>Select City:</span></td><td>
		  <div style='font-size:10px;'>";
		    while($rec=mysql_fetch_assoc($rs))
			{
			   $cityNames.="<a  style='cursor:pointer;' onclick='selectCity({$rec['id']},\"{$rec['city_name']}\");'><img src='http://www.straightforwardlistings.com/img/blueicon.jpg' border='0' alt='{$rec['city_name']}' height='30' width='30'/>{$rec['city_name']}</a> &nbsp;&nbsp;";
			}
			$cityNames.="</div></td></tr>";
			$this->set('citynames',$cityNames);
			
			//----- Filling up the Category  --------------------------------------------/
			
			 $sql="SELECT id,category_name from categories ORDER BY ID"; 
		  
		  $rs = mysql_query($sql) or die(mysql_error().$sql);
		  $selData=$_GET['srchDate'];
		  $categoryNames="<tr><td><span style='font-size:12px;'>Select Category :</span></td><td><select name=\"category\" id=\"category\" style='font-size:12px;'>";
		    while($rec=mysql_fetch_assoc($rs))
			{
			    if(isset($_GET['category']) && $rec['id'] == $_GET['category'])
				$categoryNames.="<option value='{$rec['id']}' SELECTED>{$rec['category_name']}</option>";
				else
				$categoryNames.="<option value='{$rec['id']}'>{$rec['category_name']}</option>";
			}
			$cityNames.="</select></td></tr>";
			$this->set('categorynames',$categoryNames);
			
			
			if(isset($_GET['city']) && isset($_GET['category']) && !empty($_GET['city']) && !empty($_GET['category']))
			{
			
			 $jsonDataCityCat="";
		  $sql="SELECT DISTINCT statistics.NEIGHBORHOOD AS NEIGHBORHOOD_ID,neighborhoods.neighborhood_name AS NEIGHBORHOOD FROM neighborhoods,statistics WHERE neighborhoods.id = statistics.NEIGHBORHOOD AND statistics.CATEGORY={$_GET['category']} AND statistics.CITY={$_GET['city']} {$dateDefine} LIMIT 0,10"; 
		  $rs = mysql_query($sql) or die(mysql_error().$sql);
		  $timeNeighborId=array();
		  $timeNeighborName=array();
		  $timeNeighborPrice=array();
		  $jsonDataCityCat="[";
		  while($rec=mysql_fetch_assoc($rs))
		  {
		  $jsonDataCityCat.="{";
		  $jsonDataCityCat.="\"label\":\"{$rec['NEIGHBORHOOD']}\",";
		  $jsonDataCityCat.="\"data\":[";
		  array_push($timeNeighborId,$rec['NEIGHBORHOOD_ID']);
		  array_push($timeNeighborName,$rec['NEIGHBORHOOD']);
		  
		  $sql1="SELECT statistics.NEIGHBORHOOD,time_format(time(statistics.DATE),'%H') AS HOUR,time_format(time(statistics.DATE),'%i') AS MINUTE,statistics.MAX_PRICE,statistics.MIN_PRICE,statistics.DATE FROM statistics WHERE NEIGHBORHOOD={$rec['NEIGHBORHOOD_ID']} AND statistics.CATEGORY={$_GET['category']} AND statistics.CITY={$_GET['city']}  {$dateDefine}"; 
		  $rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
			  while($rec1=mysql_fetch_assoc($rs1))
			  {
			     for($zee=0;$zee<24;$zee++)
				 {
					 if($zee==$rec1['HOUR'])
					 {
					 $hour_fix=(int) $rec1['HOUR']; //strange bug fix for json.
					 $jsonDataCityCat.="[{$hour_fix}.{$rec1['MINUTE']},{$rec1['MAX_PRICE']}],";
					  array_push($timeNeighborPrice,$rec1['MAX_PRICE']);
					 }
					 else
					 {
					 $jsonDataCityCat.="[null,null],";
					 }
				 }
			  }
			   $jsonDataCityCat=substr($jsonDataCityCat,0,strlen($jsonDataCityCat)-1); // for removing the extra comma
			   $jsonDataCityCat.="]},";
		  } 
		  $jsonDataCityCat=substr($jsonDataCityCat,0,strlen($jsonDataCityCat)-1); // for removing the extra comma
		  $jsonDataCityCat.="];";
			
		   if(!empty($timeNeighborPrice))
		      $maxPriceCityCat=max($timeNeighborPrice)+1000;
		   else
		     $maxPriceCityCat=1000;
			$this->set("maxPriceCityCat",$maxPrice);
			$this->set("jsonDataCityCat",$jsonDataCityCat);
			
			}
			
			
		 
			  
			  //$this->set('dataTable',$data);
		  }
		  
		  
		  //============== Now Showing Filtered Statistics ==============//
		  
		      //$sql="SELECT cities.city_name AS CITY_NAME , categories.category_name AS CATEGORY_NAME,neighborhoods.neighborhood_name AS NEIGHBORHOOD_NAME,MIN_PRICE,MAX_PRICE,DATE,NEIGHBORHOOD  from statistics,cities,categories,neighborhoods where statistics.CITY=cities.id AND statistics.NEIGHBORHOOD=neighborhoods.id AND statistics.CATEGORY=categories.id AND statistics.CATEGORY={$_GET['category']} AND statistics.CITY={$_GET['city']}  {$dateDefine} ORDER BY DATE ASC"; 
			  //$sql="SELECT * from statistics where NEIGHBORHOOD=0 ORDER BY DATE ASC"; 
			  $sql="SELECT cities.city_name AS CITY_NAME , categories.category_name AS CATEGORY_NAME,NEIGHBORHOOD_LIST,MIN_PRICE,MAX_PRICE,BROKER_LANGUAGE,SELECT_TYPE,FILTER_USED,DATE  from statistics,cities,categories where statistics.CITY=cities.id  AND statistics.CATEGORY=categories.id AND statistics.NEIGHBORHOOD=0 {$dateDefine} ORDER BY DATE ASC"; 
			  
			  $rs = mysql_query($sql) or die(mysql_error().$sql);
			  $timeNeighborId=array();
			  $timeNeighborName=array();
			  $timeNeighborPrice=array();
			  $data="";
		  
		      $data.="<table class='statsData1' width='500px'>";
			  $data.="<tr><th>CITY</th><th>CATEGORY</th><th>NEIGHBORHOOD</th><th>MIN PRICE</th><th>MAX PRICE</th><th>Broker Language</th><th>Type</th><th>Filters Used</th><th>DATE</th></tr>";
			  while($rec=mysql_fetch_assoc($rs))
			  {
			     
				 $neighborhoodArray=explode(",",$rec['NEIGHBORHOOD_LIST']);
				 $languageArray=explode(",",$rec['BROKER_LANGUAGE']);
				 $filterArray=explode(",",$rec['FILTER_USED']);
				 
				
				 $neighborhood="";
				 $language="";
				 $filter="";
				 
				 //Getting the each neighborhood name individually
				 if(isset($neighborhoodArray) && !empty($neighborhoodArray))
				 {
				 foreach($neighborhoodArray as $n)
				 {
				 if(isset($n) && !empty($n))
			     {
				 $sql1="SELECT neighborhood_name AS NEIGHBORHOOD_NAME from neighborhoods where id={$n}"; 
			     $rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
				  while($rec1=mysql_fetch_assoc($rs1))
			      {
				     $neighborhood.=$rec1['NEIGHBORHOOD_NAME'].",";
				  }
				  }
				 }
				 $neighborhood=substr($neighborhood,0,-1);
				 }
				 //Getting the each language name individually
				 if(isset($languageArray) && !empty($languageArray))
				 {
					 foreach($languageArray as $l)
					 {
					 if(isset($l) && !empty($l))
					 {
					 $sql1="SELECT language AS LANGUAGE from languages where id={$l}"; 
					 $rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
					  while($rec1=mysql_fetch_assoc($rs1))
					  {
						 $language.=$rec1['LANGUAGE'].",";
					  }
					 }
					 }
					 $language=substr($language,0,-1);
				 }
				 
				 if(isset($filterArray) && !empty($filterArray))
				 {
					  //Getting the each filters name individually
					 foreach($filterArray as $f)
					 {
					 if(isset($f) && !empty($f))
					 {
					 $sql1="SELECT filter_name AS FILTER_NAME from filters where id={$f}"; 
					 $rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
					  while($rec1=mysql_fetch_assoc($rs1))
					  {
						 if(is_numeric($rec1['FILTER_NAME']))
						 {
						 $filter.=$rec1['FILTER_NAME']." Bathroom,";
						 }
						 else
						 {
						 $filter.=$rec1['FILTER_NAME'].",";
						 }
					  }
					  }
					 }
					 $filter=substr($filter,0,-1);
				 }
			     
			  
			    
			    $data.="<tr><td>{$rec['CITY_NAME']}</td><td>{$rec['CATEGORY_NAME']}</td><td>{$neighborhood}</td><td>{$rec['MIN_PRICE']}</td><td>{$rec['MAX_PRICE']}</td><td>{$language}</td><td>{$rec['SELECT_TYPE']}</td><td>{$filter}</td><td>{$rec['DATE']}</td></tr>";  	

			  }
			  $data.="</table>";
			  $this->set("dataSheet",$data);		
		
		
	}

//==================================================================================================

function propertystatistics()
{

 $this->layout = "after_adminlogin";
 $this->pageTitle = 'Direct Hits';
 
     $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 //$this->set('cityStat',$this->Statistics->getCityStat()); 
	 //$this->set('neighborhoodStat',$this->Statistics->getNeighborhoodStat()); 
	 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	
	 
	 	 //FillingUp the valid dates.
     $sql="SELECT date(MIN(DATE)) AS MIN_DATE,date(MAX(DATE)) AS MAX_DATE FROM property_statistics"; 
	 $rs = mysql_query($sql) or die(mysql_error().$sql);
     $dateArr=array();
     while($rec=mysql_fetch_assoc($rs))
	{
		array_push($dateArr,$rec['MIN_DATE']);
		array_push($dateArr,$rec['MAX_DATE']);
	} 
     	 
	$this->set('dateArr',$dateArr);	
	
	$dateDefine="";
	$titleDate=" - All Time.";
	$dateToSend="";
	
	if(isset($_GET['srchDate']))
	{
	   $exploder=explode("/",$_GET['srchDate']);
	   $dateDefine=" AND date(property_statistics.DATE)='{$exploder[2]}-{$exploder[0]}-{$exploder[1]}'";
	   $titleDate=" - on {$_GET['srchDate']}.";
	   $dateToSend="{$exploder[2]}-{$exploder[0]}-{$exploder[1]}";
    }
	else
	{
	  $exploder=explode("-",$dateArr[1]);
	  $dateDefine=" AND date(property_statistics.DATE)='{$dateArr[1]}'"; // showing the current or the maximum date
	  $titleDate=" - on {$exploder[1]}/{$exploder[2]}/{$exploder[0]}.";
	  $dateToSend="{$dateArr[1]}";
	}
    
	$this->set("dateDefine",$dateDefine);
	$this->set("titleDate",$titleDate);
    $sql="SELECT DISTINCT property_statistics.PROPERTY_ID,listings.streetnumber,listings.streetname,listings.state,listings.zip,listings.description,listings.name,count(property_statistics.PROPERTY_ID) AS HITS from property_statistics,listings where listings.id=property_statistics.PROPERTY_ID   {$dateDefine} GROUP BY property_statistics.PROPERTY_ID ORDER BY HITS DESC"; 
			  
			  $rs = mysql_query($sql) or die(mysql_error().$sql);
			 
			  $data="";
		      $totalhits=0;
		      $data.="<table class='statsData1' width='100%'>";
			  $data.="<tr><th>Sr No.</th><th>Property ID</th><th>CITY</th><th>CATEGORY</th><th>NEIGHBORHOOD</th><th>Address </th><th>Description</th><th>Contact Person</th><th>Hits</th></tr>";
			  $c=0;
			 while($rec=mysql_fetch_assoc($rs))
			  {
			    $c++;
				
				$condition = "Listing.id = ".$rec['PROPERTY_ID']." group by Listing.id";
		        $listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
				
				//Now Counting Neighborhood...
				$neighborhoodlist=array();
				/*
				foreach($listingDetail[0]['Neighborhoodlisting'] as $nid)
				{
				print_r($nid);
				$sql1="SELECT neighborhood_name from neighborhoods where id='{$nid['neighborhood_id']}'"; 
				$rs1 = mysql_query($sql1) or die(mysql_error().$sql1);
					while($rec1=mysql_fetch_assoc($rs1))
					{
					array_push($neighborhoodlist,$rec1['neighborhood_name']);
					}
				}*/
				
				for($i=0;$i<sizeof($listingDetail[0]['Neighborhoodlisting']); $i++)
				{
                				
						$condition = "Neighborhood.id = ".$listingDetail[0]['Neighborhoodlisting'][$i]['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						
				array_push($neighborhoodlist,$neighborhoodDetail[0]['Neighborhood']['neighborhood_name']);				
				}
				$neighborhoodlist=array_filter($neighborhoodlist);
				$neighborhoodliststr=implode(",",$neighborhoodlist);
				$hits=$rec['HITS'];
			    $address=$rec['streetnumber']." ".$rec['streetname']." ".$rec['state']." ".$rec['zip'];
				$description=$rec['description'];
				$url="../listingdetail/".$rec['PROPERTY_ID'];
				$detailUrl="'../statistics/individualstatistics?id={$rec['PROPERTY_ID']}&date={$dateToSend}'";
			    $data.="<tr><td>{$c}</td><td><a href='{$url}' title='Click Here to view Property'>{$rec['PROPERTY_ID']}</a></td><td>{$listingDetail[0]['City']['city_name']}</td><td>{$listingDetail[0]['Category']['category_name']}</td><td>{$neighborhoodliststr}</td><td>{$address}</td><td>{$description}</td><td>{$rec['name']}</td><td vlign='middle'><a href=\"javascript:poptastic({$detailUrl})\" title='Click here to view Detail Statistics' style='text-decoration:none;'>&nbsp;{$hits}&nbsp;<img border='0' src='../img/view.gif' /> </a></td></tr>";  	
                $totalhits+=$hits;
			  }
			  $data.="</table>";
		      $this->set("dataSheet",$data);
			  $this->set("totalhits",$totalhits);

}

	function individualstatistics()
	{
	 $this->layout = "blank_template";
	 $this->pageTitle = 'Property Statistics';
	 
	 if(isset($_GET['date']) && isset($_GET['id']))
	 {
	 $dateDefine=" AND date(property_statistics.DATE)='{$_GET['date']}'";
	 $titleDate=" - on {$_GET['date']}.";
	 $sql="SELECT PROPERTY_ID,date(DATE) as DATE,time(DATE) as TIME from property_statistics where PROPERTY_ID={$_GET['id']} {$dateDefine} ORDER BY DATE ASC"; 
			  
			  $rs = mysql_query($sql) or die(mysql_error().$sql);
			 
			  $data="";
		  
		      $data.="<table class='statsData1' width='100%'>";
			  $data.="<tr><th>Sr No.</th><th>Property ID</th><th>Date</th><th>Time</th></tr>";
			  $c=0;
			  while($rec=mysql_fetch_assoc($rs))
			  {
			    $c++;
				$url="../listingdetail/".$rec['PROPERTY_ID'];
			    $data.="<tr><td>{$c}</td><td><a href='{$url}' title='Click Here to view Property'>{$rec['PROPERTY_ID']}</a></td><td>{$rec['DATE']}</td><td>{$rec['TIME']}</td></tr>";  	

			  }
			  $data.="</table>";
              $this->set("dataSheet",$data);
			  $this->set("dateDefine",$dateDefine);
			  $this->set("titleDate",$titleDate);
		}
	 
	 
	 
	}

}
//=====================================   End Manage Content  =====================================  
?>