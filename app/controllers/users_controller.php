<?php
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class UsersController extends AppController
{
	var $name = 'Users'; 
	var $helpers = array('Html','javascript','pagination'); 
	var $uses=array('User','Category','City','Event','Mailinglist','Address','Listing','Advertise','Metatag','Searchneighborhood','Searchfilter','Usersearch','Filter','Language','Eventsendaddress','Neighborhood','Listing','Unsubscribed');/*,'Unsubscribed'*/
	var $components = array('Pagination','Email');
    //var $layout='alluser';
	
	//.......Start page................
   function index($id=NULL)
 	{

               $this->layout = "before_userlogin";
                //$this->pageTitle = 'Straightforwardlistings';

				
		//$metatag = $this->Metatag->findAll();
		//$this->set('metatag',$metatag);
		//print_r($metatag);

                if(isset($id))
               {
                  
                  //find city
                  $citycond = "id='$id'";
                  $cityres = $this->City->find($citycond);
                  $cityname = $cityres['City']['city_name'];
                  //echo "cityname:::".$cityname; exit;
                  $this->set(thetitle,"NYC Apartments | Apartments for sale - $cityname, New York City");
                  $this->set(themetakeyword, "New York City Apartment Rentals,NYC Luxury Apartments for Sale,NYC Short Term Furnished,Single Family Home,Commercial Space,Beds/Youth Hostels,Apartment Share NYC");
                  $this->set(metadesc, "Luxury apartments available for sale and rental in Manhattan, New York. Please choose your apartment rental from.");
               }
               else
              {
                  $this->set(thetitle,"New York City Apartment Rental Listings NYC Commercial Space No Fee");
                  $this->set(themetakeyword, "New York City Apartment Rentals,NYC Luxury Apartments for Sale,NYC Short Term Furnished,Single Family Home,Commercial Space,Beds/Youth Hostels,Apartment Share NYC");
                  $this->set(metadesc, "We provide Apartment Rentals in New York Family Homes Short Term Furnished NYC Commercial Space for Sale Youth Hostels Share Apartments in New York City");
               }
                
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('category_name','id');
		
		$order_by = "id asc";     
		
		$categorydata = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);
		
		
		$admin_message = $this->User->findContent();
		
		$this->set('admin_message',$admin_message);	 
		
		/*if($this->checkLang() != 'en')
		{
                        $categorydata = $this->changeLang('1',"Category","category_name",$categorydata);
		}*/
                
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                         $categorydata = $this->changeLang('1',"Category","category_name",$categorydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        
                        foreach($categorydata as $key=>$value)
                        {
                                   $categorydata[$key]['Category']['category_name'] = $this->changeLang2($categorydata[$key]['Category']['category_name']); 
                        }
                    
                }
                
                
		$this->set('categorydata',$categorydata);	 	
	 	
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";
                
		$citydata = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
                
		if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($citydata as $key=>$value)
                        {
                                   $citydata[$key]['City']['city_name'] = $this->changeLang2($citydata[$key]['City']['city_name']); 
                        }
                    
                }
                
		$this->set('citydata',$citydata);	 	
		if($id==NULL)
		$cityID = 1;
		else
		$cityID = $id;
		$this->set('city_id',$cityID);	 
		$this->set('urlkey','city');
		$this->set('advertise',$this->siteAdd());
			 	
		$this->set('indexContent',$this->translateContent());				
  	}
	
	//.......SignUp start................
  function signup()
  {
	$this->layout = "before_userlogin";
        $this->pageTitle = 'Straightforwardlistings';	
	
	$this->cityData('city','no');	 	
	$this->set('type','add');
	$this->set('advertise',$this->siteAdd());
        
        $error = false;
        /*$order_by = "language asc";
        $filelds = array('language','id');        
        $languagedata = $this->Language->findAllLanguageDetail(NULL, $filelds, $order_by, NULL, NULL);
	$this->set('languagedata',$languagedata);*/
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		}
		//print_r($languagedata);exit;
		$this->set('languagedata',$languagedata);
	//print_r($this->data);exit;	
	if(!empty($this->data))
        {	
			   
			if(!isset($this->data['User']['id']))
			{
				$this->data['User']['id'] = NULL;			
			}
                         
                        if(empty($this->data['User']['email']))
                         {
                                 $this->Session->setFlash('Please enter your email adress !');
                                 $error = true;
                         }
                         
			 
                        if(!empty($this->data['User']['email']))
                         {    
                               
                                if($this->User->availableEmail($this->data['User']['email'],$this->data['User']['id']))
                                {
                                        $this->Session->setFlash('This email is already exists !');
                                        $error = true;
                                }
                                 
                         }
                       
                        if(!empty($this->data['User']['mlsID']))
                         {                                
                                if($this->User->availableBrokerID($this->data['User']['mlsID'],$this->data['User']['id']))
                                {
                                        $this->Session->setFlash('This Broker ID is already exists !');
                                        $error = true;
                                        //$this->addPageRedirect($this->data['User']['pagenum'],$this->data['User']['id']);
                                }
                         }
                         
                       
                       if(!empty($this->data['User']['cellno']))
                         {                               
                                
                                if($this->User->availablecontact($this->data['User']['cellno'],$this->data['User']['id']))
                                {
                                        $this->Session->setFlash('This Cell number is already exists !');
                                        $error = true;
                                        //$this->addPageRedirect($this->data['User']['pagenum'],$this->data['User']['id']);
                                }
                         }
                         
			if($error==false)
                        {
                               
                                $languages = "";
                                foreach($this->data['User']['gender'] as $value)
                                        {
                                              $languages .= $value.','; 
                                        }
                                   if($languages!='')
				   {
					$languages = substr($languages,0,-1);
				   }
				   else
				   {
					$languages = 1;
				   }
                                $this->data['User']['gender'] = $languages; 
                             //print_r($this->data); exit;
				$this->data['User']['username'] = $this->data['User']['email'];
				if(!isset($this->data['User']['id']))
					{					
						$this->data['User']['lastlogindate']=time();
						$this->data['User']['adddate']=time();
						$this->data['User']['password']= md5($this->data['User']['password']);
					}
					
				/*if(empty($this->data['User']['id']) && !empty($this->data['User']['mlsID']))
					{					
						$this->data['User']['isblocked']='1';						
					}*/
				
				 //print_r($this->data); exit;	
				if(isset($this->data['User']['id']))			
				$this->Session->setFlash('Your account is successfully updated');
					
				 if($this->User->save($this->data))
				 {
				 	if(isset($this->data['User']['id']))
					$this->redirect('/myaccount');
					else
					$this->redirect('/thankyou');
				 }
				 
				 
			 
			} 
			
		 
        }
		
	$this->set('indexContent',$this->translateContent('signup'));	
  }
  
 //......SignUp end.............
 
 function signin() 
  {
  	$this->set('indexContent',$this->translateContent());
	$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';	
		
		$this->cityData('city','no');
		$this->set('advertise',$this->siteAdd());		
  	/*if(!isset($_SESSION['userId']))
	{		
		$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';	
		
		$this->cityData('city');	
	}
	else
	$this->redirect('/');*/
  }
 
 //.....Login start.............
	
  function login() 
  {
	$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';
		
		
			if(!empty($this->data) && $this->data['User']['loginusername']!='User Name' && $this->data['User']['loginusername']!='Password')
		 	{
				
					 $username=$this->data['User']['loginusername'];
					 $password=$this->data['User']['loginpassword'];
					 
					 $condition = "email='$username' AND isblocked='0' AND isdelete='0' AND password=md5('$password')";	 
					 $filelds = array('id','username','email','fname','lname','mlsID');			 
					
					 $user_details = $this->User->userLogin($condition, $filelds);					
					
					 if($user_details)
					 {	
					 				
						if($this->User->userUpdate($user_details[0]['User']['id']))
						{
							$this->Session->write('userId',$user_details[0]['User']['id']);
							$this->Session->write('userName',$user_details[0]['User']['username']);
							$this->Session->write('userEmail',$user_details[0]['User']['email']);
							$this->Session->write('fname',$user_details[0]['User']['fname']);
							$this->Session->write('lname',$user_details[0]['User']['lname']);
							
							if(!empty($user_details[0]['User']['mlsID']))
							$this->Session->write('broker','1');
						}				
					
					  }
					  else
					  {
					  		 $this->Session->setFlash('Invalid Login, Try again !');
					  }
					 
			 }
			else
			 {
				  $this->validateErrors($this->User);
			 }
			
			 if(isset($this->params['form']['controller']) && isset($this->params['form']['referer']))
			 $this->redirect('/'.$this->params['form']['referer']);
			 else
			 $this->redirect('/');
			//$this->redirect('/signin');
			//echo 'success';exit;
}
 
 
  //.....Login end and logout start.................. 
  function logout()
  {
  /*$_SESSION['userId'] = '';
  $_SESSION['userName'] = '';
  $_SESSION['userEmail'] = '';
  $_SESSION['fname'] = '';
  $_SESSION['lname'] = '';
   unset($_SESSION['userId']);
   unset($_SESSION['userName']);
   unset($_SESSION['userEmail']);
   unset($_SESSION['fname']);
   unset($_SESSION['lname']);*/
   session_destroy();
   $this->Session->setFlash('You have successfully Logout!');
   $this->redirect('/');
   
  } 
  
   //...logout end...............
   
   
   //.....Login end and logout start.................. 
  function thankyou()
  {
	$this->layout = "before_userlogin";
	$this->pageTitle = 'Straightforwardlistings';
	
		$this->cityData('city','no');
		
		$this->set('advertise',$this->siteAdd());	
		$this->set('indexContent',$this->translateContent());	
  } 
  
   //...logout end...............
   
   
   //........myaccount start...............
 function myaccount()
  {
    $this->checkUser(); 
	$this->layout = "before_userlogin";
	$this->pageTitle = 'Straightforwardlistings';	
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		}
		$this->set('languagedata',$languagedata);
		$this->cityData('city','no');
		
		$this->set('indexContent',$this->translateContent());	
		
		$this->set('type','view');
	
		$logged_user_id=$this->Session->read('userId');  
		
		$this->set('user_id',$logged_user_id);
		
		$this->User->id = $logged_user_id;
		
		$this->set('userdata',$this->User->read());
		
		$this->set('advertise',$this->siteAdd());
		
		$this->render('signup');
	
  }
  //...myaccount end...........................
  
  //........myaccount contactinfo start...............
  function myeditaccount()
  {
	$this->checkUser();
	$this->layout = "before_userlogin";
	$this->pageTitle = 'Straightforwardlistings';	
	$condition = "";
	
	$filelds = array('language','id');
	
	$order_by = "id asc";     
	$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
	
	if(!empty($languagedata) && $this->checkLang() != 'en')
	{
	 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
	}
	//print_r($languagedata);exit;
	$this->set('languagedata',$languagedata);
	$this->cityData('city','no');
	
	$this->set('type','edit');		
	
	$this->set('indexContent',$this->translateContent());	
	
	$logged_user_id=$this->Session->read('userId');
	
	$this->set('advertise',$this->siteAdd());
	
	$this->set('user_id',$logged_user_id);    
	$this->User->id = $logged_user_id;
	$this->data = $this->User->read();
	//print_r($this->data);exit;
	$this->render('signup');
  }
  //........myaccount contactinfo end...............
   
  //........myaccount changepassword start...............
  function myaccount_changepassword()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - Change Password';
	  
	
	if(!empty($this->data))
        {
		
		  $logged_user_id=$this->Session->read('user_id');
		  $cri="id='$logged_user_id'";
	      $logged_user=$this->User->find($cri);
		  $logged_user_password=$logged_user['User']['password'];
		  
		 if($this->data['User']['oldpassword']==$logged_user_password)
		   {
		     $this->User->execute("update users  set password='".$this->data['User']['newpassword']."' where id=".$logged_user['User']['id']);
		     $this->Session->setFlash('The Password is successfully updated!');
			 $this->redirect('/users/myaccount_changepassword/');
		   }
		  else
		   {
		     $this->Session->setFlash('Old Password mismatch. Please enter again!');
			 $this->redirect('/users/myaccount_changepassword/');
		   } 
		} 
  }
  //........myaccount changepassword end............... 
   
  //..................Display Page ....................//
   function manageuser($id = NULL)
   {    
	   $this->checkSession();
	   $this->pageTitle='User Manager';
	   $this->layout="after_adminlogin";  
  
		$cri="isdelete='0' order by User.email asc";
		$params1 = array();
		list($order,$limit,$page) = $this->Pagination->init($cri,$params1);
		//$limit = 30;
		$admindata = $this->User->seletAlluser($cri,$limit,$page);
		//print_r($admindata);exit;
		$this->set('admindata',$admindata); 	 	   
   
	  if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
  }
  //..................................................//
  
  
  //...Add New User.......
  
  function adduser()
  {
	$this->layout = "after_adminlogin";
        $this->pageTitle = 'Add User';	
	
	
	 if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		
	$this->set('type','add');
	  if(!empty($this->data))
        {	
							   
			if(!isset($this->data['User']['id']))
			{
				$this->data['User']['id'] = NULL;			
			}
			     
			if($this->User->availableUsername($this->data['User']['username'],$this->data['User']['id']))
			{
			 	$this->Session->setFlash('This username is already exists !');
			 	$this->addPageRedirect($this->data['User']['pagenum'],$this->data['User']['id']);
			}
			elseif($this->User->availableEmail($this->data['User']['email'],$this->data['User']['id']))
			{
			 	$this->Session->setFlash('This email is already exists !');
			 	$this->addPageRedirect($this->data['User']['pagenum'],$this->data['User']['id']);
			} 
			else
		    {		
			
				if(!isset($this->data['User']['id']))
					{					
						$this->data['User']['lastlogindate']=time();
						$this->data['User']['adddate']=time();
						$this->data['User']['password']= md5($this->data['User']['password']);
					}
				 $this->User->save($this->data);
				 $this->redirect('/Users/manageuser');
				 
				 if(!isset($this->data['User']['id']))			
				 $this->Session->setFlash('New user is successfully added');
				 else
				 $this->Session->setFlash('User is successfully updated');
			 
			} 
			
		 
        }
		
		
  }
 //....End Add New User end..........
 
 function addPageRedirect($pageno,$id = NULL)
 	{
		if(isset($id))
		$this->redirect('/users/edituser/'.$id.'/manageuser?page='.$page);
	}
 
 function edituser($id = null)
	{
	  
	   $this->checkSession();
	   $this->layout='after_adminlogin'; 
	   $this->pageTitle = 'Edit User';
	   $this->User->id=$id;	
	   $this->data = $this->User->read();
	   $this->set('user_id',$id);       
	   $this->set('type','edit');
	   $this->render('adduser');
	}
 
 
  //....Delete User start...........
  function deleteuser($id)
	{
		
		$this->checkSession();  
		
		if($this->User->deleteuser($id))
		{
		       $this->Session->setFlash('The User is successfully Deleted');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }
   }
 //....Delete User end...........
 
 //...............View User...................//
 function viewuser($id=null)
 {
	$this->checkSession();
	$this->layout="after_adminlogin";
	$this->pageTitle='View User';   
	$this->set('type','view');
	$this->set('user_id',$id);    
	$this->User->id = $id;
	$this->set('userdata',$this->User->read());
	//$this->data = $this->User->read();
	 $this->render('adduser');
   } 
 
 //.................End View User.................//
 
 //....Block User start..........
 	function blockuser($id=NULL)
	{
			$this->layout = "after_adminlogin";
			$this->User->blockUser($id);	 
			$this->Session->setFlash('This User is successfully blocked');
			
	      	$controller=$this->params['controller'];
		    $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
 //....Block User end.........
 //....UnBlock User start.....
 	function unblockuser($id=NULL)
	{
	
		$this->layout = "after_adminlogin";
		$this->User->unblockUser($id);	 
		$this->Session->setFlash('This user is successfully Unblocked');
		$controller=$this->params['controller'];
		$action=$this->params['pass'][1];
		if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
		else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
		
   } 
 //.......Unblock User end......
 
 //....Block User start..........
 	function approve($id=NULL)
	{
			$this->layout = "after_adminlogin";
			$this->User->approveUser($id);	 
			$this->Session->setFlash('This User is now approved');
			
	      	$controller=$this->params['controller'];
		    $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
 //....Block User end.........
 //....UnBlock User start.....
 	function disapprove($id=NULL)
	{
	
		$this->layout = "after_adminlogin";
		$this->User->disapproveUser($id);	 
		$this->Session->setFlash('This User is now disapproved');
		$controller=$this->params['controller'];
		$action=$this->params['pass'][1];
		if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
		else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
		
   } 
 //.......Unblock User end......
 
 //........................................................................................BlockAll Admin start
 	function blockall()
	 {
	 	
	   
	 			foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->User->blockUser($id);
				 }
				  $this->Session->setFlash('Record(s) successfully blocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 
			
				 if($this->params['form']['key']!='')
				 {
				 	
				   $this->redirect($controller.'/'.$action.'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	   }	
 //........................................................................................BlockAll Admin end
 //.........................................................................................UnBlockAll start
 	function unblockall()
	 {
	
	 			
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				    $this->User->unblockUser($id);
				 }
				  $this->Session->setFlash('Record(s) successfully unblocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //..........................................................................................UnblockAll end
 //............................................................................................Delete All start
 	function deleteall()
	 {
	  //pr($this);
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->User->execute("DELETE FROM users where id=".$id);
				 }
				  $this->Session->setFlash('User(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //...Delete All end......
 
 
 //...............Search By..................//
 function searchbyname($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'User Management';
		 // print_r($this->params);
		  //exit;		  
		  if(isset($this->params['form']['srchkey'])) 
		  		{ 				    
		             $searchkey=trim($this->params['form']['srchkey']);
					 $this->set('searchkey',$searchkey);
			     }
			
		 
			else
		    {			
				$this->set('searchkey',$searchkey);
				$cri="isdelete='0' order by User.id asc";		
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('admindata',$this->User->seletAlluser($cri,$limit,$page)); 	 
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
			$cri="isdelete='0' AND ".$searchby." LIKE '%".$searchkey."%' order by User.id asc";	
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->User->seletAlluser($cri,$limit,$page)); 	 
		 }
		
		$this->render('manageuser');
		
  }
 //..........................................//
 
 
 //...Active User start......
 	function activeuser($page=1)
 	{ 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active User';	
		  	  
		  $cri="isdelete='0' AND isblocked='0' order by User.id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->User->seletAlluser($cri,NULL, NULL, $limit, $page));   
		  $this->render('manageuser');
	}
	
 //...Inactive USer start....
 	function inactiveuser($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive User';
		  
		   $cri="isdelete='0' AND isblocked='1' order by User.id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->User->seletAlluser($cri,NULL, NULL, $limit, $page));   
		  $this->render('manageuser');
  }
  
 //................................................................................................Search ByName Active start
 
 
 function login2() 
  {
	$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';
		
		//print_r($this->data);exit;
			if(!empty($this->data))
		 	{
					 //if(isset($this->data['User']['checklogin']))
					 //{
						 $username=$this->data['User']['loginusername'];
						 $password=$this->data['User']['loginpassword'];
						 
						 $condition = "email='$username' AND isblocked='0' AND isdelete='0' AND password=md5('$password')";	 
						 $filelds = array('id','username','email','fname','lname','mlsID');			 
						
						 $user_details = $this->User->userLogin($condition, $filelds);					
						
						 if($user_details)
						 {	
										
							if($this->User->userUpdate($user_details[0]['User']['id']))
							{
								
								$this->Session->write('userId',$user_details[0]['User']['id']);
								$this->Session->write('userName',$user_details[0]['User']['username']);
								$this->Session->write('userEmail',$user_details[0]['User']['email']);
								$this->Session->write('fname',$user_details[0]['User']['fname']);
								$this->Session->write('lname',$user_details[0]['User']['lname']);
								
								if(!empty($user_details[0]['User']['mlsID']))
								$this->Session->write('broker','1');
								if(isset($this->params['form']['controller']) && isset($this->params['form']['referer']))
								$this->redirect('/'.$this->params['form']['referer']);
								else
								$this->redirect('/');	
							}				
						  }
						  else
						  {
								 $this->Session->setFlash('Invalid Login, Try again !');
								 $this->redirect('/signin');
						  }
					
			 }
			else
			 {
					   $this->validateErrors($this->User);
					   
			 }
			//$this->redirect('/signin');
}
 
 //...Inactive USer start....
 function myevent()
 	{
		$this->set('indexContent',$this->translateContent());
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
		  
			$condition = "user_id=".$_SESSION['userId'];
			
			$filelds = array('title','description','id','date','stime','etime','created');
			
			$order_by = "id desc";     
			
			$this->set('eventdata',$this->Event->allEvent($condition, $filelds, $order_by, NULL, NULL));		 
		    
		    $this->set('indexContent1',$this->translateContent1());	
                    $this->set('advertise',$this->siteAdd());
		  
  }
  
   function createevent()
 	{
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
                       
			
		if(!empty($this->data))
                {	
			 $error = false;
                         
                        if(!isset($this->data['Event']['id']))
			{
				$this->data['Event']['id'] = NULL;			
			}
                        
                         if(!empty($this->data['Event']['title']))
                         {    
                               
                                if($this->Event->availableEvent($this->data['Event']['title'],$this->data['Event']['id']))
                                {
                                        $this->Session->setFlash('This event title is already exists !');
                                        $error = true;
                                }
                                 
                         }
			if($error==false) {
			$shour = $this->data['Event']['shour'];
			$sminute = $this->data['Event']['sminute'];
			if ($this->data['Event']['sampm'] == 'pm') { $shour += 12; }
			
			$this->data['Event']['stime'] = $shour.":".$sminute.":00";
			
			$ehour = $this->data['Event']['ehour'];
			$eminute = $this->data['Event']['eminute'];
			if ($this->data['Event']['eampm'] == 'pm') { $ehour += 12; }
			
			$this->data['Event']['etime'] = $ehour.":".$eminute.":00";
                        
                        $timediff = (strtotime($this->data['Event']['etime']) - strtotime($this->data['Event']['stime']));
                        
                        list($smonth,$sdate,$syear)= explode('/',$this->data['Event']['date']);
                        
                        $this->data['Event']['date']= $syear.'-'.$smonth.'-'.$sdate;
                        
                        list($emonth,$edate,$eyear)= explode('/',$this->data['Event']['enddate']);
                        
                        $this->data['Event']['enddate']= $eyear.'-'.$emonth.'-'.$edate;
                        
                        $datediff = (strtotime($this->data['Event']['enddate']) - strtotime($this->data['Event']['date']));
                        
                       
                        if($datediff<0)
                        {
                              $this->Session->setFlash('End date should be greater than start date!');  
                        }
                        elseif($timediff<0)
			{
			 	$this->Session->setFlash('End time should be greater than start time!');
			 	
			}
                        else
                        {
			
			//$this->data['Event']['date'] = $this->data['Event']['syear']."-".$this->data['Event']['smonth']."-".$this->data['Event']['sday'];
			
                                $this->data['Event']['created'] = date("Y-m-d H-i-s");
                                $this->data['Event']['user_id'] = $_SESSION['userId'];
                                $this->data['Event']['active'] = 1;
                                //print_r($this->data);exit;
                                $this->Event->save($this->data);
                                if(isset($this->data['Event']['id']))
                                {
                                        $this->Session->setFlash('The Event has successfully been updated');
                                }
                                else
                                {
                                        $this->Session->setFlash('The Event has successfully been created');
                                }
                                $this->redirect('/myevent');
                        }
                        }
		
		}
                $this->set('advertise',$this->siteAdd());
		  
		   $this->set('indexContent',$this->translateContent());		  
  }
  
	function deleteevent($event_id=NULL)
		{
			$this->checkBroker();			
			if($this->Event->deleteevent($event_id))
				{
					$this->Session->setFlash('The Event has successfully been Deleted');
				}	
			$this->redirect('/myevent');
	  }
	  
	function editevent($event_id = NULL,$view_id = NULL)
	{
	
		if(empty($event_id))
		$this->redirect('/');
		
		if(isset($view_id) && $view_id!='')
		{
			$this->set('view_id',$view_id);
		}
		$this->set('indexContent',$this->translateContent());
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		
		$this->cityData('city','no');
			
	   $this->Event->id = $event_id;	
	   $this->data = $this->Event->read();
          
		 if(empty($this->data))
		$this->redirect('/'); 
           
           if(!isset($view_id))
		{
                        list($syear,$smonth,$sdate) = explode('-',$this->data['Event']['date']);
                        $eventStartdate = $smonth.'/'.$sdate.'/'.$syear;
                         $this->set('startdate',$eventStartdate);
                }
           
           if(!isset($view_id))
		{
                        list($eyear,$emonth,$edate) = explode('-',$this->data['Event']['enddate']);
                        $eventEnddate= $emonth.'/'.$edate.'/'.$eyear;
                         $this->set('enddate',$eventEnddate);
                }
           
           
           
          
          
	   $this->set('event_id',$event_id);
           $this->set('advertise',$this->siteAdd());
	   $this->render('createevent');
	  
	}
  
  function mynewsletter($id=NULL)
 	{
		$this->checkUser();
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		
		$condition = "isdelete='0' and isblocked= 0";
		
		$this->cityData('city','no');
		
		$this->set('indexContent',$this->translateContent());		
		$this->set('indexContent1',$this->translateContent1());
		$condition = "user_id='".$_SESSION['userId']."' AND status = '1'";
		
		$filelds = array('name','id','User.email');
		
		$order_by = "id desc";     
		
		$mailingdata = array();
		
		$mailingdata = $this->Mailinglist->findAllmailingDetail($condition, $filelds, $order_by, NULL, NULL);
		
		$this->set('mailingdata',$mailingdata);	 
		$addressdata = array();
		if(sizeof($mailingdata) > 0)
		{
			foreach($mailingdata as $value)
			{ 
				$condition = "mailinglist_id='".$value['Mailinglist']['id']."' AND Address.status = '1'";
				$filelds = array('email','id');
				$order_by = "ORDER BY `email` ASC";
				$addressdata[$value['Mailinglist']['id']] = $this->Address->findAlladdressDetail($condition, $filelds, $order_by, NULL,NULL);    
			    //$addressdata[$value['Mailinglist']['id']] = $this->Address->query("Select DISTINCT email,id from addresses where mailinglist_id='".$value['Mailinglist']['id']."' AND status='1'  GROUP BY email");
			}
		}
		$this->set('key1',$id);	
		$this->set('addressdata',$addressdata);	
		$this->set('advertise',$this->siteAdd());
		
    }
function editemail($id = NULL)
	{
	   //print_r($this->data);exit;
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		
		$this->cityData('city','no');
		
		$this->set('key',$this->data['key']);
		$this->set('mail_id',$this->data['mail_id']);
		if(!empty($this->data) && !isset($this->data['update']))
    	{	
			$id = $this->data['mail_id'];
			$this->Mailinglist->id=$id;	
			$this->data = $this->Mailinglist->read();
			$this->set('userplanid', $this->Mailinglist->read(null, $id));	
		}
		else
		{
			$this->data['Mailinglist']['id'] = $this->data['mail_id'];
			$this->data['Mailinglist']['user_id'] = $_SESSION['userId'];
			$this->data['Mailinglist']['name'] = $this->data['Mailinglist']['name1'];
					//$this->data['Mailinglist']['active'] = 1;
			$this->Mailinglist->save($this->data);
			$this->redirect('/mynewsletter/'.$this->data['mail_id']);	
		
		}
		 $this->set('indexContent',$this->translateContent());	
}
function deletemail($id = NULL)
	{
	   
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		
		$this->cityData('city','no');
		
		$this->Mailinglist->del($id);
		$this->redirect('/mynewsletter');	
}


function mailaddress()
 	{
			//$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			if(isset($_REQUEST['mail_id'])){
			$_SESSION['mail_id'] = $_REQUEST['mail_id'];}
			
			$this->cityData('city','no');
			
			$condition = "mailinglist_id='".$_SESSION['mail_id']."'";
			
			$filelds = array('email','id');
			
			$order_by = "id desc";     
			
			$this->set('addressdata',$this->Address->findAlladdressDetail($condition, $filelds, $order_by, NULL, NULL));	 	
			$this->set('indexContent',$this->translateContent());	
}
  function addmailinglist()
 	{
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			$this->cityData('city','no');
			
		if(!empty($this->data))
		{	
			
			//$this->data['Mailinglist']['created'] = date("Y-m-d H-i-s");
			$this->data['Mailinglist']['user_id'] = $_SESSION['userId'];
			//$this->data['Mailinglist']['active'] = 1;
			$this->Mailinglist->save($this->data);
			$this->redirect('/mynewsletter');	
		
		}
		 $this->set('indexContent',$this->translateContent());		  
  }
  	function frontPagination($criteria,$show)
		{
			
			$count = $this->Listing->findCount($criteria,0);
			$pageCount = ceil($count / $show );
			
			return $pageCount;
			
		}
	
  function preparemail($id=NULL)
 	{
		
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		$this->cityData('city','no');
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
                
		if(isset($this->data) && !empty($this->data))
		{
		 $_SESSION['check_user'] = $this->data;
		}
		else
		{
		   $this->data = $_SESSION['check_user'];		
		}
		
		if(!is_null($this->data['main_id']))
		{
		    // echo 'hello';
		     $main_id = $this->data['main_id'];		
		}
		if(!is_null($this->data['mailing_id']))
		{
		    // echo 'hello';
		     $mailing_id = $this->data['mailing_id'];		
		}
		else
		{
		     $mailing_id = $id;	
		}
		if(!empty($this->data['chkUser_'.$mailing_id]))
		{	
			//print_r($this->data);exit;
			$order_by = "id desc";     
			$filelds1 = array('id DISTINCT','title','description','price');
			$condition1 = "Listing.is_deleted='1' AND Listing.id  IN (SELECT listing_id FROM newsletters WHERE user_id = '".$_SESSION['userId']."')";
			
			$listing1 = array();
			$listing1 = $this->Listing->allListing($condition1, $filelds1, $order_by, NULL, NULL);
			//print_r($listing1);exit;
			$imageArr = array();
			$condition2 = "Advertise.type = 'e'";
			$filelds2 = array('id','file','url','name');
		
			$imageArr = $this->Advertise->findAlladvertiseDetail($condition2, $filelds2, NULL, NULL, NULL);	 	
			//print_r($imageArr);exit;
			$this->set('imageArr', $imageArr);
			
			$allAddress = '';
			foreach($this->data['chkUser_'.$mailing_id] as $value)
			{
				$allAddress .=	$value.',';
			}
			
			$this->set('allAddress',$allAddress);
			$this->set('listingDetail',$listing1);
			$this->set('mailing_id',$mailing_id);
			$this->set('main_id',$main_id);
			$this->set('mailing_id_val',$this->data['chkUser_'.$mailing_id]);
			//print_r($this->data['chkUser_'.$this->data['mailing_id']]);exit;
			if(isset($this->data['preparemail']))
			{
				//print_r($this->data['ads_id']);exit;
				$ads_id = '';
				if(!empty($this->data['ads_id']))
				{
					for($i=0;$i<count($this->data['ads_id']);$i++)
					{
					  $ads_id .= $this->data['ads_id'][$i].",";
					}
					$ads_id = substr($ads_id,0,-1);
				}
				$condition3 = !empty($ads_id)?" AND id IN (".$ads_id.")":"AND id IN ('')";
				$imageArr = array();
				$condition2 = "Advertise.type = 'e' ".$condition3."";
				$filelds2 = array('id','file','url');
				$imageArr = $this->Advertise->findAlladvertiseDetail($condition2, $filelds2, NULL, NULL, NULL);	 	
				$this->set('imageArr', $imageArr);
				//print_r($imageArr);exit;
				$str = '';
				if(!empty($this->data['listing_id']))
				{
					for($i=0;$i<count($this->data['listing_id']);$i++)
					{
					  $str .= $this->data['listing_id'][$i].",";
					}
					$str = substr($str,0,-1);
				}	
				$order_by = "id desc";     
				$filelds1 = array('id DISTINCT','title','description','price');
				$condition1 = "Listing.id IN (SELECT listing_id FROM newsletters WHERE 1 AND listing_id in (".$str.") AND user_id = '".$_SESSION['userId']."')";
				//$order_by = "Listing.id desc";  
				$listing1 = array();
				$listing1 = $this->Listing->allListing($condition1, $filelds1, $order_by, NULL, NULL); 
				$this->set('listing',$listing1);
				//print_r($listing1);exit;
				//print_r($this->data['chkUser_'.$mailing_id]);exit;
				for($i=0;$i<count($this->data['chkUser_'.$mailing_id]);$i++)
				{
					$id_mail = $this->data['chkUser_'.$mailing_id][$i];
					$condition = "id='".$id_mail."'";
					
					$filelds = array('email');
					
					//print_r($listing1);
					//exit;
					$email = $this->Address->findAlladdressDetail($condition, $filelds, $order_by, NULL, NULL);	 	
						
					$this->Email->template = 'email/confirm';
					// You can use customised thmls or the default ones you setup at the start
					$mailContent = '';
					if(isset($this->data['mailContent']))
					{
						$mailContent = $this->data['mailContent'];
					}
					$this->set('mailContent', $mailContent);
					$this->set('id_mail', base64_encode($id_mail));
					//print_r($listing1);exit;
					$this->Email->to = $email[0]['Address']['email'];
					//$this->Email->cc = 'newsletters@straightforwardlistings.com';
					//$this->Email->cc = 'atanu@navsoft.in';
					$this->Email->subject = 'The Straightforwardlistings Broker Newsletter';
					$this->Email->from = $_SESSION['userEmail']; 
					if($_SESSION['fname']!='' ||  $_SESSION['lname']!='')
					{
						$this->Email->fromName = $_SESSION['fname']." ".$_SESSION['lname'];
					}
					else
					{
						$this->Email->fromName = $_SESSION['userEmail'];
					}
					$condition_user = "email = '".$_SESSION['userEmail']."'";
					$userData = $this->User->seletAlluser($condition_user,NULL, NULL);
					//print_r($userData);exit;	 
					$this->set('fromName', $this->Email->fromName);
					$this->set('email', $_SESSION['userEmail']);
					$this->set('telephone', $userData[0]['User']['telephone']);
					$this->set('fax', $userData[0]['User']['fax']);
					//$this->Email->body = $this->data['mailContent'];
					//$fully_qualified_filename = $this->data['mailContent'];
					//$new_name_when_attached = $this->data['mailContent'];
					//$this->Email->attach($fully_qualified_filename, $new_name_when_attached);
					// You can attach as many files as you like.
					$result = $this->Email->send(); 
					//print_r($result);
					//exit;
					$this->Session->setFlash('Mail has been sent successfully');
					$this->redirect('/mynewsletter/'.$main_id);	
				}
			}
			//$this->redirect('/mynewsletter/'.$this->data['mailing_id']);
			$this->set('advertise',$this->siteAdd());
		}
			
  }
  
  
  
   function sendmail()
 	{          
			$condition2 = "Advertise.type = 'e'";
			$filelds2 = array('id','file','url','name');
		
			$imageArr = $this->Advertise->findAlladvertiseDetail($condition2, $filelds2, NULL, NULL, NULL);	 	
			//print_r($imageArr);exit;
			$this->set('imageArr', $imageArr);
			
			
			$mail = '';	
			$mailContent = '';
			if(isset($this->data['Newsletter']['mailContent']))
			{
				$mailContent = $this->data['Newsletter']['mailContent'];
			}
			$this->set('mailContent', $mailContent);
		
			//$this->Email->to = $email[0]['Address']['email'];
			$listing =array();
			
			for($i=0;$i<$this->data['Newsletter']['total'];$i++)
			{
				$listingDetail = array();
				$listingDetail2 = array();
				$listingDetailImageDetail = array();
				$listingDetailTotalImage = array();
				$listingDetail['id']=$this->data['Newsletter']['id'.$i];
				$listingDetail['title']=$this->data['Newsletter']['title'.$i];
				$listingDetail['description'] = $this->data['Newsletter']['description'.$i];
				$listingDetail['price'] = $this->data['Newsletter']['price'.$i];
				$listingDetail2['Listing']=$listingDetail;
				
				if(isset($this->data['Newsletter']['listing_image'.$i])) {
				foreach($this->data['Newsletter']['listing_image'.$i] as $value)
				{
					
					$listingDetailImageDetail['file']= $value;
					array_push($listingDetailTotalImage,$listingDetailImageDetail);
				}
				
				$listingDetail2['Imagelisting']=$listingDetailTotalImage;
				}
				array_push($listing,$listingDetail2);
				
				
			}
			
			//print_r($listing); exit;
			
			$this->set('listing',$listing);
			
			$allAddress = $this->data['Newsletter']['sendaddress'];
			$addressArray = explode(',',$allAddress);
			$counter = 1;
			$tt = 0;
			foreach($addressArray as $value)
			{
			    //print_r($addressArray);
			
				$useraddress = $this->Address->findById($value);
				//print_r($useraddress); exit;
			
			$this->Email->template = 'email/confirm';
			$this->Email->to = $useraddress['Address']['email'];
			/*
			 if($counter==1)
			$this->Email->cc = 'newsletters@straightforwardlistings.com';
			*/
			
				//$this->Email->cc = 'newsletters@straightforwardlistings.com';
				//$this->Email->cc = 'atanu@navsoft.in';
				$this->Email->subject = $this->data['Newsletter']['subject'];
				$this->Email->from = $_SESSION['userEmail']; 
				if($_SESSION['fname']!='' ||  $_SESSION['lname']!='')
				{
					$this->Email->fromName = $_SESSION['fname']." ".$_SESSION['lname'];
				}
				else
				{
					$this->Email->fromName = $_SESSION['userEmail'];
				}
				
				$condition_user = "email = '".$_SESSION['userEmail']."'";
				$userData = $this->User->seletAlluser($condition_user,NULL, NULL);
				//print_r($userData);exit;	 
				$this->set('fromName', $this->Email->fromName);
				$this->set('email', $_SESSION['userEmail']);
				$this->set('telephone', $userData[0]['User']['telephone']);
				$this->set('fax', $userData[0]['User']['fax']);
				$this->set('id_mail', base64_encode($value));
                $this->set('greetings', $this->data['Newsletter']['title']);
                $this->set('signature', $this->data['Newsletter']['signature']);
                $this->set('brokerID',  base64_encode($_SESSION['userId']));
				//echo $this->Email->body(); exit;
				//***********************************16/08/2010**For user*****************************************//
					$st2=0;
					$st3=0;
					$newsletter_condition = "select count(*) as ct from unsubscribeds where email = '".$useraddress['Address']['email']."' and site = 'y'";
					$newsletter_quree = $this->Unsubscribed->query($newsletter_condition);
					if($newsletter_quree[0][0]['ct'] == 0)
					{
					//$result = $this->Email->send('email');
					$st2=1;
					}
					else
					{
						$mail .=$useraddress['Address']['email'].", ";
						$tt++;
					}
					
					$newsletter_condition_broker = "select count(*) as ct from unsubscribeds where email = '".$useraddress['Address']['email']."' and user_id ='".$_SESSION['userId']."' and site = 'n' ";
					$newsletter_quree_broker = $this->Unsubscribed->query($newsletter_condition_broker);
					if($newsletter_quree_broker[0][0]['ct'] == 0)
					{
					//$result = $this->Email->send('email');
					$st3=1;
					}
					else
					{
						$mail .=$useraddress['Address']['email'].", ";
						$tt++;
					}
					
					if($st2==1 && $st3==1)
					$result = $this->Email->send('email');
					//***************************************end********************************************//			
				$this->set('id_mail', base64_encode($value));
				
				$counter++;
				//echo ($result) ? "Mail Sent Sucessfully!!!":"Mail Was not sent!!!";
			}	
			//exit;
			$mail = substr(trim($mail),0,-1);
			$this->Email->template = 'email/confirm';
			$this->Email->to = 'newsletters@straightforwardlistings.com';
			/*
			 if($counter==1)
			$this->Email->cc = 'newsletters@straightforwardlistings.com';
			*/
			
				//$this->Email->cc = 'newsletters@straightforwardlistings.com';
				//$this->Email->cc = 'atanu@navsoft.in';
				$this->Email->subject = $this->data['Newsletter']['subject'];
				$this->Email->from = $_SESSION['userEmail']; 
				if($_SESSION['fname']!='' ||  $_SESSION['lname']!='')
				{
					$this->Email->fromName = $_SESSION['fname']." ".$_SESSION['lname'];
				}
				else
				{
					$this->Email->fromName = $_SESSION['userEmail'];
				}
				
				$condition_user = "email = '".$_SESSION['userEmail']."'";
				$userData = $this->User->seletAlluser($condition_user,NULL, NULL);
				//print_r($userData);exit;	 
				$this->set('fromName', $this->Email->fromName);
				$this->set('email', $_SESSION['userEmail']);
				$this->set('telephone', $userData[0]['User']['telephone']);
				$this->set('fax', $userData[0]['User']['fax']);
				//$this->set('id_mail', base64_encode($value));
                                $this->set('greetings', $this->data['Newsletter']['title']);
                                $this->set('signature', $this->data['Newsletter']['signature']);
                                $this->set('brokerID',  base64_encode($_SESSION['userId']));
				//echo $this->Email->body(); exit;
				
				$result = $this->Email->send('email');
				if($mail != '')
				{
					if($tt != 1)
					{
					$unmess = " But these email id's are unsubscribed ";
					}
					else
					{
					$unmess = " But this email id is unsubscribed ";
					}
					$brop = '(';
					$brcl = ').';
				}
				else
				{
				$unmess = '';
				$brop = '';
				$brcl = '';
				}
				
			$this->Session->setFlash('Mail has been sent successfully.'.$unmess.$brop.$mail.$brcl);
			$this->redirect('/mynewsletter');
			
			
				
			//$this->redirect('/mynewsletter/'.$this->data['mailing_id']);
			//$this->set('advertise',$this->siteAdd());
		
			
  }
  
  
  function shownewsletter()
 	{          
		Configure::write('debug', '0');
			$this->checkBroker();
		$this->layout = "email_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
			//print_r($this->data); exit;
			$condition2 = "Advertise.type = 'e'";
			$filelds2 = array('id','file','url','name');
		
			$imageArr = $this->Advertise->findAlladvertiseDetail($condition2, $filelds2, NULL, NULL, NULL);	 	
			//print_r($imageArr);exit;
			$this->set('imageArr', $imageArr);
			
			//print_r($imageArr);
			//exit(0);
			
			
				
			$mailContent = '';
			if(isset($this->data['Newsletter']['mailContent']))
			{
				$mailContent = $this->data['Newsletter']['mailContent'];
			}
			$this->set('mailContent', $mailContent);
		
			//$this->Email->to = $email[0]['Address']['email'];
			$listing =array();
			
			for($i=0;$i<$this->data['Newsletter']['total'];$i++)
			{
				$listingDetail = array();
				$listingDetail2 = array();
				$listingDetailImageDetail = array();
				$listingDetailTotalImage = array();
				$listingDetail['id']=$this->data['Newsletter']['id'.$i];
				$listingDetail['title']=$this->data['Newsletter']['title'.$i];
				$listingDetail['description'] = $this->data['Newsletter']['description'.$i];
				$listingDetail['price'] = $this->data['Newsletter']['price'.$i];
				$listingDetail2['Listing']=$listingDetail;
				
				if(isset($this->data['Newsletter']['listing_image'.$i])) {
				foreach($this->data['Newsletter']['listing_image'.$i] as $value)
				{
					
					$listingDetailImageDetail['file']= $value;
					array_push($listingDetailTotalImage,$listingDetailImageDetail);
				}
				
				$listingDetail2['Imagelisting']=$listingDetailTotalImage;
				}
				array_push($listing,$listingDetail2);
				
				
			}
			
			//print_r($listing); exit;
			
			$this->set('listing',$listing);
			
			
			$counter = 1;
			
				
				
			/*
			 if($counter==1)
			$this->Email->cc = 'newsletters@straightforwardlistings.com';
			*/
			
				//$this->Email->cc = 'newsletters@straightforwardlistings.com';
				//$this->Email->cc = 'atanu@navsoft.in';
				
				
				$condition_user = "email = '".$_SESSION['userEmail']."'";
				$userData = $this->User->seletAlluser($condition_user,NULL, NULL);
				
				$this->set('email', $_SESSION['userEmail']);
				$this->set('telephone', $userData[0]['User']['telephone']);
				$this->set('fax', $userData[0]['User']['fax']);
				
                                $this->set('greetings', $this->data['Newsletter']['title']);
                                $this->set('signature', $this->data['Newsletter']['signature']);
                               
				//echo $this->Email->body(); exit;
				
				$this->set('advertise',$this->siteAdd());	
		         $this->set('indexContent',$this->translateContent());	
				
				
			
			
		
			
  }
  
  
  function editnewsletter()
 	{
		
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		$this->cityData('city','no');
                
                $condition2 = "Advertise.type = 'e'";
                $filelds2 = array('id','file','url','name');
                
                $imageArr = $this->Advertise->findAlladvertiseDetail($condition2, $filelds2, NULL, NULL, NULL);	 	
                //print_r($imageArr);exit;
                $this->set('imageArr', $imageArr);
                
                $allListing ='';
                foreach($this->data['listing_id'] as $value)
                {
                        $allListing .= $value.',';
                }
                $allListings = substr($allListing,0, -1);
                
                $condition = "is_deleted=1 AND active='1' AND Listing.id IN ($allListings)";		
                      
                $order_by = "Listing.id desc";   
                
                $filelds = array('Listing.id','Listing.price','Listing.description','Listing.title');
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
                $listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, NULL, NULL);
		
		$neighborhood_data = array();
		foreach($listingDetail as $neighborhood)
			{
				
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{						
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                 if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
						
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
                        
                $condition_user = "email = '".$_SESSION['userEmail']."'";
		$userData = $this->User->seletAlluser($condition_user,NULL, NULL);
                 
                $this->set('userData',$userData);               
                $this->set('allAddress',$this->data['allAddress']);
                $this->set('listingDetail',$listingDetail);
	        $this->set('neighborhoodData',$neighborhood_data);
                
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
                $this->set('advertise',$this->siteAdd());
		
			
        }
  function preparemail1()
  {
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
			$this->set('advertise',$this->siteAdd());
			$this->set('key',$this->data['key']);
			$this->set('mail_id',$this->data['mail_id']);
			$this->set('indexContent',$this->translateContent());
			
  }
  function addmailaddress()
 	{
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
                    if(!empty($this->data['Address']['email']))
                        {
                              $mailinglist_id = $this->data['Address']['mailinglist_id'];
                              
                              $allAddress =  explode("\n",$this->data['Address']['email']);
							  $addressArray=array();
                              foreach ($allAddress as $address){
							  $address=trim($address);
							  	if(!in_array($address,$addressArray)){
									array_push($addressArray,$address);
								}
							  }
							  $allAddress=$addressArray;
                     
                              for($i=0;$i<sizeof($allAddress);$i++)
                              {
                                   
                                   if(!empty($allAddress[$i]))
								   {
								   
								   $condition = "`Address`.`mailinglist_id`='".$mailinglist_id."' AND `Address`.`email`='".$allAddress[$i]."' AND `Address`.`status` = '1'";
								  				
								       if($this->Address->findCount($condition) == 0)
                                       {
									      $this->Address->saveAddress($mailinglist_id,$allAddress[$i]);
									   }
									   
                                   }								   
                              }
                                
                        }
                        $this->redirect('/mynewsletter/'.$mailinglist_id);
                       
				  
    }
  function language()
 	{	
		//print_r($this->params); exit;
		$lang = $this->params['url']['language'];
		$this->Session->write('lang',$lang);	
		if($lang == 'ja')
		{
			$charset = 'shift_jis';
		}
		elseif($lang == 'ko')
		{
			$charset = 'euc-kr';
		}
		elseif($lang == 'zh-CN')
		{
			$charset = 'GB2312';
		}
		elseif($lang == 'zh-TW')
		{
			$charset = 'Big5';
		}
		elseif($lang == 'ru')
		{
			$charset = 'KOI8-R';
		}
		else
		{
			$charset = '';
		}
		$this->Session->write('charset',$charset);
		//print_r($this->params);exit;
		//if(isset($this->params[url]))
		//{
			
		//}
		 $add_params = '';
		if(isset($this->params['url']['searchValue']) && !empty($this->params['url']['searchValue']))
		{
		   $add_params = "?searchValue=".$this->params['url']['searchValue'];	
		}
		
		$this->redirect('/'.$this->params['url']['referrer'].$add_params);
  }
  
  
  function uploadbroker()
 	{
		$indexContent = $this->translateContent();
		$this->set('indexContent',$indexContent);
		//print_r($indexContent);exit;
		$this->checkBroker();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		$this->set('advertise',$this->siteAdd());
		$this->cityData('city','no');
		
		//$this->set('indexContent',$this->translateContent());				
		$this->set('indexContent1',$this->translateContent1());	
	
		if(isset($_SESSION['userId']))
		{
			 $path = getcwd()."/uploadimage/broker";
			 $linkpic = array();
			 $linkpic = glob("$path/*.*");
			 
			 $getFileName = '';
			 if(count($linkpic) > 0)
				{
				    foreach($linkpic as $key=>$val)
				    {
					$imageNameExp = explode("_",basename($val));
					//print_r($imageNameExp);
					if($imageNameExp[0] == $_SESSION['userId'])
					{
						$getFileName = basename($val);
						break;
					}
				    }	
				}
				
			 
			 $imgExt = explode(".",$getFileName);
			 $filepath = '';
			 $imagePath = '';
			
			 if(!empty($imgExt))
			 {	
			    if(!empty($imgExt[1]) && $imgExt[1] != 'db')
			    {	 
				$imagePath= URL."uploadimage/broker/".$getFileName ; //Replace Image Path
				$filepath =  getcwd().'/uploadimage/broker/'.$getFileName;
			    }	
			 }
			 $this->set('linkpic',$getFileName);
			 $this->set('imagePath',$imagePath);
			 $this->set('filepath',$filepath);
		}
		//print_r($this->data);exit;
		if($this->data)
		{
			$typeArr = array('0'=>'image/jpeg',
				 '1'=>'image/jpg',
				 '2'=>'image/gif',
				 '3'=>'image/png',
				 '4'=>'image/pjpeg',
				 '5'=>'image/pjpg',
				 '6'=>'image/pgif',
				 '7'=>'image/ppng');
			$date = date("Y-m-d")."-".time();

			//print_r($this->data);exit;	
			
			 
				$imageArr = array();
				$imageArr = explode(".",$this->data['broker']['imagename']['name']);
				//print_r($imageArr);exit;
				$ext = '';
				if(count($imageArr)==2)
				{
				  $ext = $imageArr[1];
				}  
				$fileName = $_SESSION['userId']."_".$date.".".$ext;
				$file_path = getcwd()."/uploadimage/broker";	
				$arrFiles = array();
				$arrFiles = glob("$file_path/*.*");
				//print_r($arrFiles);exit;
				$arr = array();
				$deleteFileName = '';
				if(count($arrFiles) > 0)
				{
				    foreach($arrFiles as $key=>$val)
				    {
					$imageNameExp = explode("_",basename($val));
					if($imageNameExp[0] == $_SESSION['userId'])
					{
						$deleteFileName = basename($val);
						$arr = explode(".",$deleteFileName);
						break;
					}
				    }	
				}	
				//print_r($arr);
				if(count($arr) == 2)
				{
				   $ex = $arr[1];	
				}
				else
				{
				   $ex = '';	
				}
				if(isset($this->data['broker']['PhotoT']) && $this->data['broker']['PhotoT']=='Remove_Photo')
				{
					if(file_exists($file_path."/".$deleteFileName) && $ex!='' && $ex!='db')
					{
						unlink($file_path."/".$deleteFileName);
						$this->Session->setFlash('Broker image has been deleted');
						
					}
					else
					{
						$this->Session->setFlash('There is an error occured. Broker image can not be deleted');
					}
				}
				else if(isset($this->data['broker']['PhotoT']) && $this->data['broker']['PhotoT']=='Change_Photo')
				{
				   if(!empty($this->data['broker']['imagename']['name']) && in_array(strtolower($this->data['broker']['imagename']['type']),$typeArr))
				   {
					if(file_exists($file_path."/".$deleteFileName) && $ex!='' && $ex!='db')
					{
						unlink($file_path."/".$deleteFileName);
						
					}
					$file_temp = $this->data['broker']['imagename']['tmp_name'];
					if(file_exists($file_temp))
					{
					//$file_name = $this->data['broker']['imagename']['name'];				
					$filestatus = move_uploaded_file($file_temp,$file_path."/".$fileName);	
					$this->Session->setFlash('Broker image has been updated');
					}
					else
					{
					    $this->Session->setFlash('There is a problem to upload the image');
					}
				    }	
				    else
				    {
					$this->Session->setFlash('Please select a valid image to upload');
				    }
				}
				else
				{
					$file_temp = $this->data['broker']['imagename']['tmp_name'];
					if(file_exists($file_temp))
					{
					//$file_name = $this->data['broker']['imagename']['name'];				
					$filestatus = move_uploaded_file($file_temp,$file_path."/".$fileName);	
					$this->Session->setFlash('Broker image has been added');
					}
					else
					{
					 $this->Session->setFlash('You have not browsed to upload an image');
					} 
					
				}
				
			echo $this->redirect('/uploadbroker');
			
		}
			
		//
		  
  }
  
   function forgotpassword()
 	{
			
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
			
			if(!empty($this->data))
			{	
							   
						     
			/*if(empty($this->data['User']['username']))
			{
			 	$this->Session->setFlash('Please enter your Username');
			 	
			}*/
			if(empty($this->data['User']['email']))
			{
			 	$this->Session->setFlash('Please enter your Email !');
			 	
			} 
			
			else
		    {	
			
				//$username = $this->data['User']['username'];
				$email = $this->data['User']['email'];
				
				// $condition = "username='$username' AND email='$email'";
				$condition = "email='$email'";
				 $filelds = array('id');			 
					
				 $user_details = $this->User->userLogin($condition, $filelds);					
				
				//print_r($user_details);exit;
				if(!empty($user_details))	
				{		
					$to = $this->data['User']['email'];
					
					$newpassword = $this->createRandomPassword();
					
					if($this->User->userforgotPass($newpassword, $user_details[0]['User']['id']))	
					{
						$subject = "MLS Update 123 Password Request";
						$message = "Your New Password: ".$newpassword."\n";
						$headers  = "MIME-Version: 1.0\r\n";
						$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
						$headers .= "From: admin@straightforwardlistings.com <admin@straightforwardlistings.com>\r\n";
						
					
						if (mail($to,$subject,$message,$headers))
						{
							$this->Session->setFlash('A new Password sent to your e-mail address');	
							$this->redirect('/forgotpassword');	 			
						}
						else
						{
							$this->Session->setFlash('Invalid Usename/E-mail address.  Please try again.');				
						
						}
					}
				}
			
			} 
			
		 
        
			
			}
                $this->set('advertise',$this->siteAdd());        
		
		$this->set('indexContent',$this->translateContent());
				  
  }
  
  
   function changepassword()
 	{
			
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');
			$this->set('indexContent',$this->translateContent());
			$this->set('indexContent1',$this->translateContent1());	
			if(!empty($this->data))
			{						   
						     
				if(empty($this->data['User']['oldpassword']))
				{
					$this->Session->setFlash('Please enter your old password');
					
				}
				elseif(empty($this->data['User']['newpassword']))
				{
					$this->Session->setFlash('Please enter your new password!');
					
				} 
				
				else
				{			
						$oldpassword = $this->data['User']['oldpassword'];
						$newpassword = $this->data['User']['newpassword'];				
						
						 $condition = "password=md5('$oldpassword') AND User.id=". $_SESSION['userId'];	 
						 $filelds = array('id');			
					
						 $user_details = $this->User->userLogin($condition, $filelds);						
						
						if($user_details)	
						{			
							
							if($this->User->userchangePass($newpassword, $_SESSION['userId']))	
							{		
								
									$this->Session->setFlash('Password has been changed');	
									$this->redirect('/changepassword');	 
									
							}
							else
							{
									$this->Session->setFlash('Incorrect your Password');				
								
							}
						} else
						{
							$this->Session->setFlash('Incorrect your Password');
						}
				 }
			
			}
			$this->set('advertise',$this->siteAdd());
							  
  }
  
  
   function createRandomPassword() 
		{    
			$chars = "abcdefghijkmnopqrstuvwxyz023456789";    
			srand((double)microtime()*1000000);   
			$i = 0;    
			$pass = '' ;    
			while ($i <= 5) {        
				$num = rand() % 33;        
				$tmp = substr($chars, $num, 1);        
				$pass = $pass . $tmp;        
				$i++;    
		        }    
		  return $pass;
		}
 
 function unsubscribe($id=NULL,$broker_id=NULL)
 {
		
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';
		
                $id = base64_decode($id);
                if(isset($broker_id) && !empty($broker_id))
                $broker_id = base64_decode($broker_id);
		//$admin_message = $this->User->findContent();
		
		//$this->set('admin_message',$admin_message);	 
		
		$this->cityData('city','no');
		
		$this->set('advertise',$this->siteAdd());
                
		
		if(isset($broker_id) && !empty($broker_id))
		$this->Unsubscribed->saveDataBroker($id,$broker_id);
         else
		$this->Unsubscribed->saveDataSite($id);
                
		$this->set('indexContent',$this->translateContent());
 }
 
 function managebroker()
	{    
		$this->checkSession();
		$this->pageTitle='User Manager';
		$this->layout="after_adminlogin";  
		
                if(isset($this->params['form']['chkUser'])) {
		$this->Session->write('savesearchuser',$this->params['form']['chkUser']);
                }
                
                //$this->params['form']['chkUser']; 
                	 
		$cri="isdelete='0' AND mlsID>0 order by User.email asc";		
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$admindata = $this->User->seletAlluser($cri,$limit,$page);
		
		$this->set('admindata',$admindata);
                
                $adminsavedata = $this->User->seletAllsaveBroker();
                
                $this->set('adminsavedata',$adminsavedata);
		
		//$this->set('usersearchdata',$this->params['form']['chkUser']); 		 	   
		
		if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 	
	}
        
        
        function sendsavesearch()
	{    
		$this->checkSession();
		$this->pageTitle='User Manager';
		$this->layout="after_adminlogin";  
		
                
                
                $headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: leads@straightforwardlistings.com <leads@straightforwardlistings.com>\r\n";
		$sub = "Following search parameter are set by the user";
                
                $message = "";
		
		$message .= "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /><style type=\"text/css\">@import url(\"http://navsoft.co.in/mlsupdate123/css/mail.css\");</style></head><body>";
$message .="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>
		  <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:center;\">Following Save Search Users are Set</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  
  <tr>
    <td style=\"text-align:center; color:#7486c0; font-weight:bold; font-size:16px;\">&nbsp;</td>
  </tr>";
  
                
                
                
                for($i=0;$i<sizeof($_SESSION['savesearchuser']);$i++)
                {
                        $condition = "usersearch_id=".$_SESSION['savesearchuser'][$i]."";
                        
                        $filelds = array('Neighborhood.neighborhood_name');
                        
                        $usersearch_neighborhood= $this->Searchneighborhood->allUserSearchNeighborhood($condition, $filelds, NULL, NULL, NULL);                        
                                                       
                        $condition = "Usersearch.id=".$_SESSION['savesearchuser'][$i]."";		
                        
                        $filelds = array('User.email','Usersearch.serach_name','City.city_name','Usersearch.add_date', 'Usersearch.minprice','Usersearch.maxprice','Category.category_name');
                        
                        $save_search = $this->Usersearch->allUserSearch($condition, $filelds, NULL, NULL, NULL);
                        
                        
                         $condition = "Searchfilter.usersearch_id=".$_SESSION['savesearchuser'][$i]."";	
                        
                        $userSearchFilter = $this->Searchfilter->allUserSearchFilter($condition, NULL, NULL, NULL, NULL);
	       
	            
               
              $filterdata = array();
		foreach($userSearchFilter as $value)
			{				
				
			$condition = "Filter.id = ".$value['Searchfilter']['filter_id']."";
			
			$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			$order_by = "Categoryfilter.id asc";     
	 
	 		array_push($filterdata,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			}
                        
                        //print_r($filterdata);
                        
                        $condition = "isdelete='0' and isblocked= 0 AND type='e'";
			
			$filelds = array('name','file','url');
			
			$order_by = "rand()"; 	
			
			$advertise = $this->Advertise->findAllAdvertiseDetail($condition,$filelds,$order_by,NULL,NULL); 
                        
                        
                        
                        $neighborhoods = '';
		foreach($usersearch_neighborhood as $value) {
			    $neighborhoods .= $value['Neighborhood']['neighborhood_name'].', ';
			      }
                        
                  $message .= "<tr>
    <td>
	<table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
                        
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Search Name :</td>
        <td style=\"font-size:12px;\">".$save_search[0]['Usersearch']['serach_name']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">City Name :</td>
        <td style=\"font-size:12px;\">".$save_search[0]['City']['city_name']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Neighborhhods :</td>
        <td style=\"font-size:12px;\">".substr($neighborhoods, 0, -1)."</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Category :</td>
        <td style=\"font-size:12px;\">".$save_search[0]['Category']['category_name']."</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Price Range :</td>
        <td style=\"font-size:12px;\">";
         if(empty($save_search[0]['Usersearch']['minprice']) && empty($save_search[0]['Usersearch']['maxprice']))
			      $message .= "Not Mentioned";
			      else
                              $message .= $save_search[0]['Usersearch']['minprice'] .'-'.$save_search[0]['Usersearch']['maxprice'];
			  
     $message .=  "</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Search Date :</td>
        <td style=\"font-size:12px;\">".date("jS F Y",strtotime($save_search[0]['Usersearch']['add_date']))."</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     ";
          
        if(sizeof($filterdata)>0) { 
         $message .="
          <tr>
            <td colspan=\"2\"  valign=\"middle\" align=\"center\" height=\"24\"><strong>Advance Filter</strong></td>

            
          </tr>
         <tr><td colspan=\"2\">
              <table width=\"100%\">
		  <tr><td>
          ";

	    $message .=  "<div style=\"width:672px;
	height:auto;
	margin:0;
	padding-top:10px;
	clear:both;
	float:left;\">";
		   
		   $category_filter_id = 0;
		   $category = 1;
		  for($fn=0;$fn<sizeof($filterdata);$fn++) { 
		   
         if($category_filter_id!=$filterdata[$fn][0]['Categoryfilter']['id']) { 

	if($category_filter_id!=0)
	$message .= "</div>";
	
	if($category==1)
	{
		if($category_filter_id!=0)
		$message .= "</div><div style=\"width:672px;
	height:auto;
	margin:0;
	padding-top:10px;
	clear:both;
	float:left;\">";
		
		$message .= "<div style=\"width:210px;
	height:auto;
	margin-bottom:40px;
	margin-left:5px;
	margin-right:12px;
	padding:0;
	float:left;\">";
	}
	elseif($category==2)
		$message .= "<div style=\"width:210px;
	height:auto;
	margin-bottom:40px;
	margin-right:12px;
	padding:0;
	float:left;\">";
	elseif($category==3)
	{
	$message .= "<div style=\"width:210px;
	height:auto;
	margin-bottom:40px;
	margin-right:12px;
	padding:0;
	float:left;\">";
	$category=0;
	}
	$message .= "<div style=\"width:210px;
	height:33px;
	padding:0;
	margin-top:25px;
	clear:both;
	border-bottom:solid 1px #cccccc;
	border-top:solid 1px #cccccc;\">";
	$message .= "<div class=\"innerleft_parametertxt\">".$filterdata[$fn][0]['Categoryfilter']['category_filter_name']."</div>";
	$message .= "</div>";
	$category++;
	}
			


 
 
 
 $message .="<div style=\"width:210px;
	height:20px;
	padding:0;
	margin-top:5px;
	clear:both;\">
  
<div class=\"inner_3_des_list\" style=\"font-size:12px;\">".$filterdata[$fn][0]['Filter']['filter_name']."</div>
</div> ";
			
		$category_filter_id = $filterdata[$fn][0]['Categoryfilter']['id'];
			
 //$message .= "</div></div>";
        
                  }
                  
                  
      $message .="</td></tr>      
    </table></td>
  </tr>";
  }
   $message .="</table></td></tr><tr><td style=\"width:600px;
	height:20px;
	padding:0;
	margin:0;
	border-top: 1px dashed;
	color:#993333\">&nbsp;</td></tr>";
                        
                }
                
                
                
$message .=  "
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td width=\"300\"><a href=\"http://".$advertise[0]['Advertise']['url']."\" target=\"_blank\"><img src=\"".URL."uploadimage/add/".$advertise[0]['Advertise']['file']." \" border=\"0\" /></a></td>
        <td width=\"10\">&nbsp;</td>
        <td width=\"300\"><a href=\"http://".$advertise[1]['Advertise']['url']."\" target=\"_blank\"><img src=\"".URL."uploadimage/add/".$advertise[1]['Advertise']['file']." \" border=\"0\" /></a></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align=\"center\" class=\"img_link\"><a href=\"http://".$advertise[0]['Advertise']['url']."\" target=\"_blank\">".$advertise[0]['Advertise']['url']." </a></td>
        <td>&nbsp;</td>
        <td align=\"center\" class=\"img_link\"><a href=\"http://".$advertise[1]['Advertise']['url']."\" target=\"_blank\">".$advertise[1]['Advertise']['url']."</a></td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body></html>";

               
               
               $subject = "Following search parameter are set by the user.";
		
                for($n=0; $n<sizeof($this->params['form']['chkUser']);$n++)
                {
                       //print_r($this->params['form']['chkUser'][$n]);
                        $sent = mail($this->params['form']['chkUser'][$n],$subject,$message,$headers);
                }
               // echo $message; exit;
                
		$this->Session->setFlash('Mail successfully send to the broker');
                   
		
		if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}
                
                
               // $this->redirect('users/managebroker');                
               $this->redirect('usersearchs/managesavesearch'); 
	}
        
        //...............Search By..................//
        
        function searchbybroker($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'User Management';
		 // print_r($this->params);
		  //exit;		  
		  if(isset($this->params['form']['srchkey'])) 
		  		{ 				    
		             $searchkey=trim($this->params['form']['srchkey']);
					 $this->set('searchkey',$searchkey);
			     }
			
		 
			else
		    {			
				$this->set('searchkey',$searchkey);
				$cri="isdelete='0' AND mlsID>0 order by User.id asc";		
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('admindata',$this->User->seletAlluser($cri,$limit,$page)); 	 
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
			$cri="isdelete='0' AND mlsID>0  AND ".$searchby." LIKE '%".$searchkey."%' order by User.id asc";	
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->User->seletAlluser($cri,$limit,$page)); 	 
		 }
		
		$this->render('managebroker');
		
  }
 //..........................................//
 
 
 function sendevent($event_id=NULL)
 	{
		
				if(empty($event_id))
				$this->redirect('/');
		
                $this->checkBroker();
                $this->layout = "before_userlogin";
                $this->pageTitle = 'Straightforwardlistings';
                
                $this->cityData('city','no');
                
                $condition = "event_id=".$event_id;
                
                //$filelds = array('title','description','id','date','stime','etime','created');
                
                $order_by = "id desc";
                
                $this->set('eventaddressdata',$this->Eventsendaddress->allEventAddress($condition, NULL, $order_by, NULL, NULL));		 
                
                $this->set('eventID',$event_id);	
                $this->set('indexContent1',$this->translateContent1());	
                $this->set('advertise',$this->siteAdd());
                $this->set('indexContent',$this->translateContent());
		  
  }
  
  function sendmailaddress()
 	{
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');                       
                    
                     
                         
                        
                        if(!empty($this->data['Eventsendaddress']['email']))
                        {
                              $event_id = $this->data['Eventsendaddress']['event_id'];
                              
                              $allAddress =  explode("\n",$this->data['Eventsendaddress']['email']);
                              
                               $this->Event->id = $this->data['Eventsendaddress']['event_id'];	
                                $this->dataEvent = $this->Event->read();
                      
                     
                              for($i=0;$i<sizeof($allAddress);$i++)
                              {                                
                                   if(!empty($allAddress[$i]))
                                   {                                        
                                       $this->Eventsendaddress->saveAddress($event_id,$allAddress[$i]);                      
                                        			        
                                   }
                              }
                                
                        }
                        $this->Session->setFlash('Address successfully Added ');
                        $this->redirect('users/sendevent/'.$event_id);
                       
				  
  }
  
  
  function sendinvite()
 	{
			$this->checkBroker();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Straightforwardlistings';
			
			$this->cityData('city','no');                       
                    
                     
                       
                        
                        if(!empty($this->params['form']['chkUser']))
                        {
                              $event_id = $this->data['Event']['id'];                           
                              
                              
                               $this->Event->id = $this->data['Event']['id'];	
                                $this->dataEvent = $this->Event->read();
                      
                                /************Create .vcs file ******************/
                                $content = '';
                                
                               // echo $this->dataEvent['Event']['date'];
                                
                                $Filename = "Event" . $this->dataEvent['Event']['id'] . ".vcs";
                               // $content .= header("Content-Type: text/x-vCalendar");
                              //  $content .= header("Content-Disposition: inline; filename=$Filename");
                                $DescDump = str_replace("\r", "=0D=0A=", $this->dataEvent['Event']['description']);                    
                                $vCalStart = date("Ymd\THi00", strtotime($this->dataEvent['Event']['date']));
                                $vCalEnd = date("Ymd\THi00", strtotime($this->dataEvent['Event']['enddate']));
                                
                                $content .="BEGIN:VCALENDAR";
                                $content .="VERSION:1.0";
                                $content .="PRODID:SSPCA Web Calendar";
                                $content .="TZ:-07";
                                $content .="BEGIN:VEVENT";
                                $content .="SUMMARY:".$this->dataEvent['Event']['title']."\n";
                                $content .="DESCRIPTION;ENCODING=QUOTED-PRINTABLE: ". $DescDump . "\n"; 
                                $content .="DTSTART:". $vCalStart . "\n"; 
                                $content .="DTEND: ".$vCalEnd . "\n";
                                $content .="END:VEVENT";
                                $content .="END:VCALENDAR";
                                
                                $fp = fopen($Filename,'w');
                                fwrite($fp,$content);
                                fclose($fp);
                                
                              
                             /**************************************************/
                     
                              for($i=0;$i<sizeof($this->params['form']['chkUser']);$i++)
                              {                                
                                   
                                  $email = $this->Eventsendaddress->findById($this->params['form']['chkUser']) ;
                                  
                                  $message='';
                                  /************************************************************/
                                  
                                  
                                  /***********************************************************/
                                  
                                  $message .="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>		 
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  
  <tr>
    <td style=\"text-align:center; color:#7486c0; font-weight:bold; font-size:16px;\">&nbsp;</td>
  </tr>
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td width=\"125\" valign=\"top\">Event Title : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['title']."</td>
      </tr>
      
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Event Start Date : </td>
        <td width=\"600\" style=\"font-size:12px;\">".date("jS F Y",strtotime($this->dataEvent['Event']['date']))." ".$this->dataEvent['Event']['stime']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Event End Date : </td>
        <td width=\"600\" style=\"font-size:12px;\">".date("jS F Y",strtotime($this->dataEvent['Event']['enddate']))." ".$this->dataEvent['Event']['etime']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Event Description : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['description']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     <tr>
        <td width=\"125\" valign=\"top\">Event Location : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['location']." ".$this->dataEvent['Event']['street']." ".$this->dataEvent['Event']['city']." ".$this->dataEvent['Event']['state']." ".$this->dataEvent['Event']['zip']. "</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Contact Name : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['name']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Contact Phone  : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['phone']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
       <tr>
        <td width=\"125\" valign=\"top\">Contact Email  : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$this->dataEvent['Event']['email']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>Click here if you want to <a href=\"".URL."users/eventresponse/".$this->params['form']['chkUser'][$i]."/a\" target=\"_blank\" class=\"left_menu_txt\">Accept</a>&nbsp; OR  <a href=\"".URL."users/eventresponse/".$this->params['form']['chkUser'][$i]."/r\" target=\"_blank\" class=\"left_menu_txt\">Reject</a></td>
      </tr>
    </table></td>
  </tr>
  
  
     </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>";


        //print_r($_SESSION['userEmail']); exit;
                 $headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		
		 $eol="\r\n";
                $mime_boundary=md5(time());
                //$headers  = "MIME-Version: 1.0\r\n";
                //$headers .= "Content-Type: multipart/related; boundary=\"".$mime_boundary."\"".$eol;
                $headers .= "From: ".$_SESSION['userEmail']."\r\n";
                                  
                          //$emailsubject ="fsdf";
                          //mail($email['Eventsendaddress']['email'],$subject,$body,$headers);
                          //mail($email['Eventsendaddress']['email'], 'basak.santanu@gmail.com', $emailsubject, $body, $Filename);
			
        if (is_file($Filename))
             {              
                              
               $handle=fopen($Filename, 'rb');
               $f_contents=fread($handle, filesize($Filename));
               $f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
               fclose($handle);
              
               # Attachment
               //$message .= "--".$mime_boundary.$eol;
               //$message .= "Content-Type: application/octet-stream; name=\"".$Filename."\"".$eol;
               //$message .= "Content-Transfer-Encoding: base64".$eol;
              // $message .= "Content-Disposition: attachment; filename=\"".$Filename."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
              // $message .= $f_contents.$eol.$eol;
               
             }	

       
            $subject = "New Event ".$this->dataEvent['Event']['title'];
                              $sent = mail($email['Eventsendaddress']['email'],$subject,$message,$headers);       
                                    
                                 $this->Session->setFlash('Invitation has been sent to the selected user ');
                        $this->redirect('users/sendevent/'.$event_id);
                               
                                }
                        }
                                
        }
                       
                       
	function deleteeventaddress($address_id=NULL,$event_id=NULl)
		{
			$this->checkBroker();			
			if($this->Eventsendaddress->deleteeventaddress($address_id))
				{
					$this->Session->setFlash('The Address has been successfully Deleted');
				}	
			 $this->redirect('users/sendevent/'.$event_id);
                }
                
                
        
        function send_mail($emailaddress, $fromaddress, $emailsubject, $body, $attachments)
        {
               $eol="\r\n";
  $mime_boundary=md5(time());
  $headers ='';
  # Common Headers
  $headers .= 'From: MyName<'.$fromaddress.'>'.$eol;
  $headers .= 'Reply-To: MyName<'.$fromaddress.'>'.$eol;
  $headers .= 'Return-Path: MyName<'.$fromaddress.'>'.$eol;    // these two to set reply address
  //$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">".$eol;
  $headers .= "X-Mailer: PHP v".phpversion().$eol;          // These two to help avoid spam-filters

  # Boundry for marking the split & Multitype Headers
  $headers .= 'MIME-Version: 1.0'.$eol;
  $headers .= "Content-Type: multipart/related; boundary=\"".$mime_boundary."\"".$eol;

  $msg = "";      
  
  if ($attachments != false)
  {

   
     //echo $i.'<br> B ';
	 if (is_file($attachments))
     {  
	 	
      	
       
       $handle=fopen($attachments, 'rb');
       $f_contents=fread($handle, filesize($attachments));
       $f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
       fclose($handle);
       
       # Attachment
       $msg .= "--".$mime_boundary.$eol;
       $msg .= "Content-Type: application/octet-stream; name=\"".$attachments."\"".$eol;
       $msg .= "Content-Transfer-Encoding: base64".$eol;
       $msg .= "Content-Disposition: attachment; filename=\"".$attachments."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
       $msg .= $f_contents.$eol.$eol;
       
     }
   
  }
  
 
  mail($emailaddress, $emailsubject, $msg, $headers);
  
              
        }
        
         function eventresponse($address_id=NULL,$responsestatus=NULL)
		{
			$this->layout = "before_userlogin";
                        $this->pageTitle = 'Straightforwardlistings';
	
		$this->cityData('city','no');	
			if($this->Eventsendaddress->eventresponse($address_id,$responsestatus))
				{
                                        $this->set('status',$responsestatus);
                                        $this->set('advertise',$this->siteAdd());
                                        $this->set('indexContent',$this->translateContent());	
                                       
				}	
			
                }
                
         function deletealladdress()
		{		
	
			//print_r($this->params); exit;
			 $mailing_id = $this->data['mailing_id'];
			 $arrayId = 'chkUser_'.$mailing_id;
			 foreach($this->data[$arrayId] as $id)
			     {
				   //$this->User->execute("DELETE FROM users where id=".$id);
				   $this->Address->del($id); 
				 }
				  $this->Session->setFlash('Address(s) successfully Deleted');
				 
	 	 
			 $this->redirect('/mynewsletter/'.$this->data['main_id']);			 

          }
          
           function deletenewsletter($listing_id)
		{		
	
			
                        $this->checkUser();                       
                        
                        if($this->Listing->deletenewsletter($listing_id,$_SESSION['userId']))   {                    
	 	        $this->Session->setFlash('This listing successfully remove from your Newsletter list');
                        } else
                         $this->Session->setFlash('This listing unsuccessfull to remove from your Newsletter list');
			 $this->redirect('/users/preparemail');			 

          }
        
   function saveAddress()
		{		
                        //print_r($this->params);
			$this->checkSession();
                        $this->User->execute("Delete from savebroker");
                         foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->User->execute("Insert into savebroker (email) values ('$id')");
			      }
				
                   $this->redirect('users/managebroker'); 
                        		 

          }
	
}
?>