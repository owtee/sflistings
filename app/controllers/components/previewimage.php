<?
            header("Content-type: image/png");
			// Create the image
			$im = imagecreatetruecolor($box_w, $box_h);
			// Create some colors
			$white = imagecolorallocate($im, 255, 255, 255);
			$grey = imagecolorallocate($im, 128, 128, 128);
			$black = imagecolorallocate($im, 0, 0, 0);
			imagefilledrectangle($im, 0, 0, $box_w-1, $box_h-1, $white);
			// Add the text
			imagettftext($im, $font_size, 0, 10, 20, $black, $font_face, $text);
			imagepng($im);
			imagedestroy($im);
?>
