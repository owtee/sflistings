<?php
class DownloadComponent extends Object
{
	function dodownloadcsv()
	{
	  header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename=samplecsv.csv" );
      header("Content-Transfer-Encoding: binary");
      header("Content-Length: ".filesize(('sample/samplecsv.csv')));
      readfile('sample/samplecsv.csv');	 	
    } 
	function dodownloadexcel()
	{
	  header("Content-Type: application/octet-stream");
      header("Content-Disposition: attachment; filename=sampleexcel.xls" );
      header("Content-Transfer-Encoding: binary");
      header("Content-Length: ".filesize(('sample/sampleexcel.xls')));
      readfile('sample/sampleexcel.xls');	 	
    } 
		
}
?>