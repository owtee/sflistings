<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class ContentsController extends AppController
{
  var $name = 'Contents'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Content','Adminmainmenu','Admin','User','City');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function managecontent()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'CMS Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page)); 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	 
  }
  function content($id=NULL)
  {
  		
		$this->layout = "before_userlogin";
    	$this->pageTitle = 'Mlsupdate';
		$description = $this->Content->getcontent($id); 
	 	$this->set('description',$description);	
		 
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id desc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));			
		$cityID = 34;
		
		$this->set('city_id',$cityID);	 
		 
		$this->set('urlkey','city');
		$this->set('content_id',$id);
		$this->set('indexContent',$this->translateContent());	
		$this->set('advertise',$this->siteAdd());	 
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activecontent($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active CMS';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactivecontent($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive CMS';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'CMS Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
		}
	  else
		{
		    $searchkey="";
		    $cri="isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="isdelete='0' AND cms_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'CMS Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND cms_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'CMS Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete content   ===============================


 function deletecontent($id)
{
	$this->checkSession();  
    $this->Content->del($id);
	$this->Session->setFlash('The CMS is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete content   ===============================

//=====================================   Start add content   ===============================
 	 

function addcontent()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New CMS';
	  if(!empty($this->data))
        {
			$description_update = str_replace("<p>", "", $this->data['Content']['description']);			
			$this->data['Content']['description'] = str_replace("</p>", "", $description_update);
			if($this->Content->findAllBycms_name($this->data['Content']['cms_name']))
				{
				 	$this->Session->setFlash('This cms is already exists !');
				 	$this->redirect('/contents/addcontent');
				 } 
		 	 else
		   		{
					$this->data['Content']['modifiedon']=date('Y-m-d');
					$this->data['Content']['createdon']=date('Y-m-d');
					$this->Content->save($this->data);
					$this->Session->setFlash('New CMS is successfully added');
					$this->redirect('/contents/managecontent');
          		 }
         }
  }

//=====================================   End add content   ===============================

//=====================================   Start Edit content   ===============================


function editcontent($id = null)
{
   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->Content->id=$id;
	   $this->data = $this->Content->read();
	 }
   else
     {  
	 	$description_update = str_replace("<p>", "", $this->data['Content']['description']);			
			$this->data['Content']['description'] = str_replace("</p>", "", $description_update); 
	  	if ($this->Content->save($this->data['Content']))
	       {
		        $idd=$this->data['Content']['id'];
		   		$updatefn=$this->Content->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
		 	    $this->redirect($controller.'/managecontent'.'?page='.$pagenum);
		   }
    }
}
 
//=====================================   End Edit content   ===============================

//=====================================   Start View content   ===============================

function viewcontent($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View CMS';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->Content->id = $id;
		$this->set('userdata',$this->Content->read(null,id));
  	}
   else
   {
	 $this->Content->id = $id;
     $this->set('userdata',$this->Content->read());
	}   
}

//=====================================   End View content   ===============================



//=====================================   Start block content   ===============================
function blockcontent($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->Content->blockcontentfn($id);
		//$this->Content->execute("update contents  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This CMS is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block content   ===============================

//=====================================   Start Unblock content   ===============================

function unblockcontent($id=NULL)
	{
		 $this->Content->unblockcontentfn($id);
		 //$this->Content->execute("update contents  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This CMS is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock content   ===============================

//=====================================   Start blockall content   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->Content->blockallfn($id);
			  //$this->Content->execute("update contents  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall content   ===============================

//=====================================   Start Unblockall content   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->Content->unblockallfn($id);
		   //$this->Content->execute("update contents  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall content   ===============================
	 
//=====================================   Start deleteall content   ===============================
	 
function deleteall()
	 {
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->Content->del($id);
		 	}
		 $this->Session->setFlash('CMS(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall content   ===============================

//=====================================   Start searchbyletter content   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter content   ===============================

//=====================================   Start editactivecontent content   ===============================

 function editactivecontent($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active CMS';
	  if(empty($this->data))
		{
			$this->Content->id=$this->params['pass'][0];
		    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['Content']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->Content->id=$this->data['Content']['id'];
				   $this->Content->save($this->data);
				   $idd=$this->data['Content']['id'];
				   $this->Content->editactivecontentfn($idd);
				   //$this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent content   ===============================



//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND cms_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND cms_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================



 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Content->find($cri));
	
   }
 //---------------end of page view-----------------  
 
 
 //---------------front page view-----------------
 
  function feedback()
   {
   		$this->layout = "before_userlogin";
    	$this->pageTitle = 'Mlsupdate';	
		 
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id desc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));			
		$cityID = 34;		
		$this->set('city_id',$cityID);	 
		 
		$this->set('urlkey','city');
		
		if(!empty($this->data))
        {	
							   
						     
			if(empty($this->data['Feedback']['name']))
			{
			 	$this->Session->setFlash('Please enter Name !!');
			 	
			}
			elseif(empty($this->data['Feedback']['email']))
			{
			 	$this->Session->setFlash('Please enter Email !');
			 	
			} 
			
			else
		    {
				$message .= "Name: ".$this->data['Feedback']['name']."<br>";
				$message .= "E-Mail: ".$this->data['Feedback']['email']."<br>";
				if (!empty($this->data['Feedback']['phone']))
				{
				$message .= "Phone: ".$this->data['Feedback']['phone']."<br>";
				}
				$message .= "<br>";
				$message .= "Comments:<br>";
				$message .= $this->data['Feedback']['comments'];
				
				$subject = "MLS Update 123 Feedback";
				$headers  = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= "From: ".$this->data['Feedback']['name']." <".$this->data['Feedback']['email'].">\r\n";
				
				if (mail('basak.santanu@gmail.com',$subject,$message,$headers))
				{
					$this->Session->setFlash('Feedback sent successfully');	
					$this->redirect('/feedback');	 			
				}
				else
				{
					$this->Session->setFlash('Feedback could not be sent.  Please try again.');				
				
				}
			
			} 
			
		 
        }
		
		$this->set('indexContent',$this->translateContent());	
		$this->set('advertise',$this->siteAdd());
		
	
   }
 //---------------end of page view-----------------  
 
 }
?>