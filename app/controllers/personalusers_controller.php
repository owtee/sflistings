<?php
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class PersonalusersController extends AppController
{
	var $name = 'Personalusers'; 
	var $helpers = array('Html','javascript','Ajax','pagination','common'); 
	var $uses=array('Personaluser');
	var $components = array('Pagination');
    var $layout='alluser';
	
	
  //..................Display Page ....................//
   function manageuser($id = NULL)
   {    
	   $this->checkSession();
	   $this->pageTitle='Personaluser Manager';
	   $this->layout="after_adminlogin";  
  
		$cri="isdelete='0' order by id asc";		
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('admindata',$this->Personaluser->seletAlluser($cri,$limit,$page)); 	 	   
   
	  if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
  }
  //..................................................//
  
  
  //...Add New Personaluser.......
  
  function adduser()
  {
	$this->layout = "after_adminlogin";
    $this->pageTitle = 'Add User';	
	
	
	 if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		
	$this->set('type','add');
	  if(!empty($this->data))
        {	
							   
			if(!isset($this->data['Personaluser']['id']))
			{
				$this->data['Personaluser']['id'] = NULL;			
			}
			     
			if($this->Personaluser->availableUsername($this->data['Personaluser']['username'],$this->data['Personaluser']['id']))
			{
			 	$this->Session->setFlash('This username is already exists !');
			 	$this->addPageRedirect($this->data['Personaluser']['pagenum'],$this->data['Personaluser']['id']);
			}
			elseif($this->Personaluser->availableEmail($this->data['Personaluser']['email'],$this->data['Personaluser']['id']))
			{
			 	$this->Session->setFlash('This email is already exists !');
			 	$this->addPageRedirect($this->data['Personaluser']['pagenum'],$this->data['Personaluser']['id']);
			} 
			else
		    {		
			
				if(!isset($this->data['Personaluser']['id']))
					{					
						$this->data['Personaluser']['lastlogindate']=time();
						$this->data['Personaluser']['adddate']=time();
						$this->data['Personaluser']['password']= md5($this->data['Personaluser']['password']);
					}
				 $this->Personaluser->save($this->data);
				 $this->redirect('/Personalusers/manageuser');
				 
				 if(!isset($this->data['Personaluser']['id']))			
				 $this->Session->setFlash('New user is successfully added');
				 else
				 $this->Session->setFlash('User is successfully updated');
			 
			} 
			
		 
        }
		
		
  }
 //....End Add New Personaluser end..........
 
 function addPageRedirect($pageno,$id = NULL)
 	{
		if(isset($id))
		$this->redirect('/personalusers/edituser/'.$id.'/manageuser?page='.$page);
	}
 
 function edituser($id = null)
	{
	  
	   $this->checkSession();
	   $this->layout='after_adminlogin'; 
	   $this->pageTitle = 'Edit User';
	   $this->Personaluser->id=$id;	
	   $this->data = $this->Personaluser->read();
	   $this->set('user_id',$id);       
	   $this->set('type','edit');
	   $this->render('adduser');
	}
 
 
  //....Delete Personaluser start...........
  function deleteuser($id)
	{
		
		$this->checkSession();  
		
		if($this->Personaluser->deleteuser($id))
		{
		       $this->Session->setFlash('The Personaluser is successfully Deleted');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }
   }
 //....Delete Personaluser end...........
 
 //...............View User...................//
 function viewuser($id=null)
 {
	$this->checkSession();
	$this->layout="after_adminlogin";
	$this->pageTitle='View Personaluser';   
	$this->set('type','view');
	$this->set('user_id',$id);    
	$this->Personaluser->id = $id;
	$this->set('userdata',$this->Personaluser->read());
	//$this->data = $this->Personaluser->read();
	 $this->render('adduser');
   } 
 
 //.................End View User.................//
 
 //....Block Personaluser start..........
 	function blockuser($id=NULL)
	{
			$this->layout = "after_adminlogin";
			$this->Personaluser->blockUser($id);	 
			$this->Session->setFlash('This Personaluser is successfully blocked');
			
	      	$controller=$this->params['controller'];
		    $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
 //....Block Personaluser end.........
 //....UnBlock Personaluser start.....
 	function unblockuser($id=NULL)
	{
	
		$this->layout = "after_adminlogin";
		$this->Personaluser->unblockUser($id);	 
		$this->Session->setFlash('This user is successfully Unblocked');
		$controller=$this->params['controller'];
		$action=$this->params['pass'][1];
		if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
		else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
		
   } 
 //.......Unblock Personaluser end......
 //........................................................................................BlockAll Admin start
 	function blockall()
	 {
	 	
	   
	 			foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Personaluser->blockUser($id);
				 }
				  $this->Session->setFlash('Record(s) successfully blocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 
			
				 if($this->params['form']['key']!='')
				 {
				 	
				   $this->redirect($controller.'/'.$action.'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	   }	
 //........................................................................................BlockAll Admin end
 //.........................................................................................UnBlockAll start
 	function unblockall()
	 {
	
	 			
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				    $this->Personaluser->unblockUser($id);
				 }
				  $this->Session->setFlash('Record(s) successfully unblocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //..........................................................................................UnblockAll end
 //............................................................................................Delete All start
 	function deleteall()
	 {
	  //pr($this);
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Personaluser->execute("DELETE FROM personalusers where id=".$id);
				 }
				  $this->Session->setFlash('Personaluser(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //...Delete All end......
 
 
 //...............Search By..................//
 function searchbyname($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Personaluser Management';
		 // print_r($this->params);
		  //exit;		  
		  if(isset($this->params['form']['srchkey'])) 
		  		{ 				    
		             $searchkey=$this->params['form']['srchkey'];
					 $this->set('searchkey',$searchkey);
			     }
			
		 
			else
		    {			
				$this->set('searchkey',$searchkey);
				$cri="isdelete='0' order by id asc";		
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('admindata',$this->Personaluser->seletAlluser($cri,$limit,$page)); 	 
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
			$cri="isdelete='0' AND ".$searchby." LIKE '%".$searchkey."%' order by id asc";	
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Personaluser->seletAlluser($cri,$limit,$page)); 	 
		 }
		
		$this->render('manageuser');
		
  }
 //..........................................//
 
 
 //...Active Personaluser start......
 	function activeuser($page=1)
 	{ 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active Personaluser';	
		  	  
		  $cri="isdelete='0' AND isblocked='0' order by id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->Personaluser->seletAlluser($cri,NULL, NULL, $limit, $page));   
		  $this->render('manageuser');
	}
	
 //...Inactive USer start....
 	function inactiveuser($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Personaluser';
		  
		   $cri="isdelete='0' AND isblocked='1' order by id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->Personaluser->seletAlluser($cri,NULL, NULL, $limit, $page));   
		  $this->render('manageuser');
  }
  
 //................................................................................................Search ByName Active start
 
 

  
  }
 

?>