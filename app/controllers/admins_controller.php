<?php
ob_start();
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class AdminsController extends AppController
{
	var $name = 'Admins'; 
	var $helpers = array('Html','javascript','pagination'); 
	var $uses=array('Admin');
	var $components = array('Pagination');
  function login() 
  {
	$this->layout="admin_login";
	$this->pageTitle = 'Admin Login';
    $this->set('admin_error', '');
	if (!empty($this->data))
     {
           
			$someone = $this->Admin->findByUsername($this->data['Admin']['username']);
			if($someone['Admin']['password'] == $this->data['Admin']['password'] and $someone['Admin']['isblocked'] =='0' and $someone['Admin']['is_deleted'] =='0')
      		{
				$this->Session->write('admin_id', $someone['Admin']['id']);
				$this->Session->write('username', $someone['Admin']['username']);
				$this->Session->write('firstname',$someone['Admin']['firstname']);
				$this->Session->write('lastname',$someone['Admin']['lastname']);
				$this->Session->write('roleid',$someone['Admin']['roleid']);
				$curdate=date('Y-m-d');
				$curtime=date('g:i:s a');
				//$this->data['Admin']['login_ip']=$_SERVER['REMOTE_ADDR'];
				echo $update_query="update admins set last_logindate='".$curdate."'".",last_logintime='".$curtime."' ,login_ip='".$_SERVER['REMOTE_ADDR']."' where id='".$someone['Admin']['id']."'";
				//exit;
				mysql_query($update_query);
				$this->redirect('/admins/cpanel');
			}
			else
			{
			  $this->set('admin_error','Invalid Login or Password or You Are Blocked.');
			}

    }
    else
	{
	 $this->set('error', 'Field cannot be left empty');
	}
	 
 }
 
 ///////////////////After Admin Login//////
 //....................................................................................Add Admin start
  function addadmin()
  {
	$this->layout = "after_adminlogin";
        $this->pageTitle = 'Add New Admin';
	//$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  if(!empty($this->data))
        {
			//pr($this->data);
			//exit();
			if($this->Admin->findAllByusername($this->data['Admin']['username']))
			{
			 $this->Session->setFlash('This username is already exists ! Try with different user name');
			 $this->redirect('/admins/addadmin');
			}
			elseif($this->Admin->findAllByemail($this->data['Admin']['email']))
			{
			 $this->Session->setFlash('This email address is already exists ! Try with different email address');
			 $this->redirect('/admins/addadmin');
			} 
		  else
		   {
			 
			 //$adate=$this->params['form']['adate'];
			 //$arr=explode('/',$adate);
		     //$activedate=$arr[2]."-".$arr[1]."-".$arr[0];
			 //$this->data['Member']['expiry_date']=$activedate;
			 /*$this->data['Admin']['roleid']=$this->params['form']['roleid'];
			 $sql_rolename="select * from rolemasters where id=".$this->data['Admin']['roleid'];
			 $res_rolename=mysql_query($sql_rolename);
			 $row_rolename=mysql_fetch_array($res_rolename); 
			 $this->data['Admin']['admin_role']=$row_rolename['role_name'];*/
			 $this->data['Admin']['last_logindate']=date("Y-m-d");
			 $this->data['Admin']['last_logintime']=date(" h:i:s a");
			 $this->Admin->save($this->data);
             $this->Session->setFlash('New admin is successfully added');
			 $this->redirect('/admins/manageadmin');
			}
        }
  }
 //....................................................................................Add Admin end
 //....................................................................................Delete Admin start
 

 
 	function deleteadmin($id)
{
	$this->checkSession();  
    $this->Admin->del($id);
	$this->Session->setFlash('The Admin is successfully Deleted');
           $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}   
}
 //.....................................................................................Delete Admin end
 //......................................................................................Block Admin start
 	function blockadmin($id=NULL)
	{
	$this->layout = "after_adminlogin";
	$this->Admin->execute("update admins  set isblocked='1' where id=".$id);	 
	 $this->Session->setFlash('This Admin is successfully blocked');
	      $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
 //......................................................................................Block Admin end
 //.......................................................................................UnBlock Admin start
 	function unblockadmin($id=NULL)
	{
	//$this->Plan->saveField('isblocked','0');
	 $this->Admin->execute("update admins  set isblocked='0' where id=".$id);
	$this->Session->setFlash('This admin is successfully Unblocked');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];
		   		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	//$this->redirect('/Plans/manageuser');	
   } 
 //.......................................................................................Unblock Admin end
 //........................................................................................BlockAll Admin start
 	function blockall()
	 {
	    // pr($this->params['form']);
	 			foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Admin->execute("update admins  set isblocked='1' where id=".$id);
				 }
				  $this->Session->setFlash('Record(s) successfully blocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				   $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	   }	
 //........................................................................................BlockAll Admin end
 //.........................................................................................UnBlockAll start
 	function unblockall()
	 {
	 //pr($this->params['form']);
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Admin->execute("update admins  set isblocked='0' where id=".$id);
				 }
				  $this->Session->setFlash('Record(s) successfully unblocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //..........................................................................................UnblockAll end
 //............................................................................................Delete All start
 	function deleteall()
	 {
	  //pr($this);
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Admin->del($id);
				 }
				  $this->Session->setFlash('Admin(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //.............................................................................................Delete All end
 //.............................................................................................Active Admin start
 	function activeadmin($page=1)
 	{ 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active Admin';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  $cri="is_deleted='0' AND isblocked='0'";
		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  $this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));   
	}
 //.............................................................................................Active Admin end
 //..............................................................................................Inactive Admin start
 	function inactiveadmin($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Admin';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  $cri="is_deleted='0' AND isblocked='1'";
		 
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  $this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
  }
 //...............................................................................................Inactive Admin end
 //................................................................................................Search ByName Active start
 	function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$this->set('panelmenus',$this->Adminmainmenu->findAll());
   		//$this->pageTitle='Edit Admin';
		$cri="is_deleted='0' AND isblocked='0' AND username LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));
	 }
 //.................................................................................................Search ByName Active end
 //..................................................................................................Search By letter Start
 	function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Admin';
		$this->set('panelmenus',$this->Adminmainmenu->findAll());
		$this->set('panelmenus',$this->Adminmainmenu->findAll());
		$cri="is_deleted='0' AND username LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));
   	} 
 //...................................................................................................Search By letter End
 //....................................................................................................Search By Name Start
 	 function searchbyname($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Admin Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=trim($this->params['form']['srchkey']);
		  else
		    {
		      $searchkey="";
			  $cri="is_deleted='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND username LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND email LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
		if(($searchby=="position") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND admin_role LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
  }
 //.....................................................................................................Search By Name End
 //......................................................................................................Search By Option Active start
 	function srchbyoptionactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Admin Management';
		  $this->set('panelmenus',$this->Admin->findAll());
		  
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=trim($this->params['form']['srchkey']);
		  else
		    {
		      $searchkey="";
			  $cri="is_deleted='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND username LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="is_deleted='0' AND username LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
		if(($searchby=="position") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND admin_role LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //........................................................................................................Search By Option Active end
 //......................................................................................................Search By Option Active start
 	function srchbyoptioninactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'Admin Management';
		$this->set('panelmenus',$this->Admin->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=trim($this->params['form']['srchkey']);
		  else
		    {
		      $searchkey="";
			  $cri="is_deleted='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND username LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="is_deleted='0' AND username LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
		if(($searchby=="position") && isset($searchkey))
		  {
			$cri="is_deleted='0' AND admin_role LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //........................................................................................................Search By Option Active end
 //..........................................................................................................Edit Active Admin start
 	 function editactiveadmin()
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit Active admin';
		$this->set('panelmenus',$this->Adminmainmenu->findAll());
			   if(empty($this->data))
				{
					
					//pr($this->params);
				 
					$this->Admin->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Admin->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
				//pr($this->params);
				// exit();
					//$gte=$this->Admin->findByusername($this->data['Admin']['username']);
					$gte=$this->Admin->findByusername($this->data['Admin']['username']);
					$gte1=$this->Admin->findByemail($this->data['Admin']['email']);
						if($gte && $gte['Admin']['id']!=$this->data['Admin']['id'])
						//if($gte && ($gte['Admin']['id']!=$this->data['Admin']['id']))
						{
							 $this->Session->setFlash('This Admin is already exists ! Please try with a different user name.');
							 $this->redirect($controller.'admins/editadmin/'.$id);
							 
						}
						if($gte1 && $gte1['Admin']['id']!=$this->data['Admin']['id'])
						//if($gte && ($gte['Admin']['id']!=$this->data['Admin']['id']))
						{
							 $this->Session->setFlash('This Admin is already exists ! Please try with a different email address.');
							 $this->redirect($controller.'admins/editadmin/'.$id);
							 
						}
						else
						{
						$id=$this->data['Admin']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Admin->id=$this->data['Admin']['id'];
							   $this->Admin->save($this->data);
							   }
							//pr($this->params);   
							//exit();
						$this->Session->setFlash('Selected record successfully updated!');
						 //$this->redirect('admins/activeadmin');
						 
						 
						 
						if(isset($this->params['form']['skey']) && ($this->params['form']['skey']!=''))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
					}	
				}
				
 }  
 //............................................................................................................Edit Active Admin end
 //.............................................................................................................Edit Inactive Admin start
 	function editinactiveadmin($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive admin';    
		$this->set('panelmenus',$this->Adminmainmenu->findAll());    
			   if(empty($this->data))
				{
					//pr($this->params);
					$this->Admin->id=$this->params['pass'][0];
				    $this->data = $this->Admin->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						//$gte=$this->Admin->findByusername($this->data['Admin']['username']);
						$gte=$this->Admin->findByusername($this->data['Admin']['username']);
						$gte1=$this->Admin->findByemail($this->data['Admin']['email']);
						if($gte && $gte['Admin']['id']!=$this->data['Admin']['id'])
						//if($gte && ($gte['Admin']['id']!=$this->data['Admin']['id']))
						{
							 $this->Session->setFlash('This Admin is already exists ! Please try with a different user name.');
							 $this->redirect($controller.'admins/editadmin/'.$id);
							 
						}
						elseif($gte1 && $gte1['Admin']['id']!=$this->data['Admin']['id'])
						{
							$this->Session->setFlash('This Admin is already exists ! Please try with a different email address.');
							$this->redirect($controller.'admins/editadmin/'.$id);
						}
						else
						{
						$id=$this->data['Admin']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Admin->id=$this->data['Admin']['id'];
							   $this->Admin->save($this->data);
			           			}
							   
						 $this->Session->setFlash('Selected record successfully updated!');
						 //$this->redirect('admins/inactiveadmin');
				        if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							  }
						}
						
					}
				}
	   }
 //..............................................................................................................Edit Inactive Admin end
    function cpanel()
	{
		$this->checkSession();
		$this->pageTitle = 'Admin cpanel';
		$this->layout="after_adminlogin";
		//$count=1;
		//$sqlqry= $this->Adminsubmenu->findAll(array('Adminsubmenu.is_deleted' => '= 1'));
	//	$this->set('dashboard_data',$sqlqry); 
		
		//$this->set('dashboard_data',$this->Adminsubmenu->findAll(null, array('id', 'adminmainmenu_id', 'adminsubmenu_name','adminsubmenu_order','pageurl','dashboardicon','Adminsubmenu.is_deleted' => '= 1'))
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		$condition="is_deleted='1'";
		//$this->set('dashboard_data',$this->Adminsubmenu->findAll($condition,NULL));
	 }		
//............................##......................................................................Manage Admin start
 function manageadmin($status=NULL)
 {
   $this->checkSession();
   $this->pageTitle='Admin cpanel';
   $this->layout="after_adminlogin";
  // $this->set('panelmenus',$this->Adminmainmenu->findAll());
   $cri="is_deleted='0'order by email asc"; 
   list($order,$limit,$page) = $this->Pagination->init($cri);
  $this->set('admindata',$this->Admin->findAll($cri,NULL, NULL, $limit, $page));  
  if(isset($_REQUEST['menu_id']))
		{
			//$_SESSION['status']='';
			//$_SESSION['skey']='';
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}
  
 }
  
//.............................##.....................................................................Manage Admin end 
 function viewadmin($id=null)
 {
   $this->checkSession();
   $this->layout="after_adminlogin";
   $this->pageTitle='View Admin';
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   if(!$id)
   {
   		echo $id=$_REQUEST['id'];
        $this->Admin->id = $id;
		$this->set('admindata',$this->Admin->read(null,id));
     ///it should leave blank
   }
   else
   {
   	 //$id=$_REQUEST['id'];
	 $id;
	 $this->Admin->id = $id;
     $this->set('admindata',$this->Admin->read());
   } 
   
 }
 
 /*function editadmin($id = null)
 {
     $this->checkSession();
     $this->layout='after_adminlogin';
    if(empty($this->data))
    {  
        $this->User->id=$id;
        $this->data = $this->User->read();    
   }
   else
   {
     
	 if($this->User->save($this->data['User']))
        {
		   $this->Session->setFlash('The record is successfully Edited');
           $this->redirect('users/manageadmin');
         }
 
  }
 }*/
 
function editadmin($id = null)
{     

    //pr($id);
	//exit;
     $this->checkSession();
     $this->layout='after_adminlogin';
	 $this->set('id',$id);
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	if (empty($this->data))
    {
        $this->Admin->id = $id;
        $this->data = $this->Admin->read();
		$this->set('admin', $this->Admin->read(null, $id));
    }
    else
    { 
	   	//$cri="username='".$this->data['Admin']['username']."' OR email='".$this->data['Admin']['email']."'";
		//$gte=$this->Admin->findAll($cri);
			$gte=$this->Admin->findByusername($this->data['Admin']['username']);
			$gte1=$this->Admin->findByemail($this->data['Admin']['email']);
	    	if($gte && $gte['Admin']['id']!=$this->data['Admin']['id'])
			//if($gte && ($gte['Admin']['id']!=$this->data['Admin']['id']))
			{
				 $this->Session->setFlash('This Admin is already exists ! Please try with a different user name.');
				 $this->redirect($controller.'admins/editadmin/'.$id);
				 
			}
			elseif($gte1 && $gte1['Admin']['id']!=$this->data['Admin']['id'])
			{
				$this->Session->setFlash('This Admin is already exists ! Please try with a different email address.');
				$this->redirect($controller.'admins/editadmin/'.$id);
			}
			else
			{
			 $this->data['Admin']['id']=$id;
			 $this->data['Admin']['roleid']=1;
			 //$sql_rolename="select * from rolemasters where id=".$this->data['Admin']['roleid'];
			 //$res_rolename=mysql_query($sql_rolename);
			 //$row_rolename=mysql_fetch_array($res_rolename);
			 $this->data['Admin']['admin_role']='admin';
			 
			if ($this->Admin->save($this->data['Admin']))
			{
			   $controller=$this->params['controller'];
			   $pagenum=$this->params['form']['pagenum'];		 
			   //echo  $action=$this->params['pass'][1];  
			   $this->Session->setFlash('The record is successfully Edited');
			   //$this->redirect('admins/manageadmin');
			   $this->redirect($controller.'/manageadmin'.'?page='.$pagenum);
			}
	  	}
    }
}

 
 
 
 
 
 function logout()
   {
	  /* unset($_SESSION['admin_id']);
	   session_destroy();
       //setcookie("validate",'',time()-3600);
       $this->redirect('/admin');*/
	}

}
?>