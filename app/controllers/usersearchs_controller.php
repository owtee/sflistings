<?php
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class UsersearchsController extends AppController
{
	var $name = 'Usersearch'; 
	var $helpers = array('Html','javascript','pagination','Ajax'); 
	var $uses=array('Usersearch','Searchfilter','Searchneighborhood','Filter');
	var $components = array('Pagination','Frontpagination');
    //var $layout='alluser';
	
	//.......Start page................
   
		
		
	function managesavesearch()
	{    
		$this->checkSession();
		$this->pageTitle='User Manager';
		$this->layout="after_adminlogin";  		
	
	 
		$condition = "";
		
		list($order,$limit,$page) = $this->Pagination->init($condition);		
		
		$filelds = array('User.email','Usersearch.serach_name','Usersearch.add_date', 'Usersearch.id');
                
                $order_by = "id desc";   
		
		$this->set('save_search',$this->Usersearch->allUserSearch($condition, $filelds, $order_by, $limit, $page));		   
		
		if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 	
	}
	
	
	
        
        
        function deletesavesearch($id=NULL)
	{    
		$this->checkSession();  
		
		if($this->Usersearch->del($id))
		{
		       $this->Session->setFlash('The User Save Search is successfully Deleted');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }	
	}
        
	function searchbyname($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'User Management';
		 		  
		  if(isset($this->params['form']['srchkey'])) 
		  	{ 				    
				$searchkey=trim($this->params['form']['srchkey']);
				$this->set('searchkey',$searchkey);
			}
			
		 
			else
			{			
				$this->set('searchkey',$searchkey);
				$cri="isdelete='0' order by User.id asc";		
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('admindata',$this->User->seletAlluser($cri,$limit,$page)); 	 
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {
			
			$condition = "Usersearch.serach_name LIKE '%".$searchkey."%'";
			
			list($order,$limit,$page) = $this->Pagination->init($condition);		
			
			$filelds = array('User.email','Usersearch.serach_name','Usersearch.add_date', 'Usersearch.id');
			
			$this->set('save_search',$this->Usersearch->allUserSearch($condition, $filelds, NULL, $limit, $page));
			
			
		 }
		
		$this->render('managesavesearch');
		
  }
        
        function viewusersearch($id = NULL)
	{    
		$this->checkSession();
                $this->pageTitle='View Save Search';
		$this->layout="after_adminlogin";
                
                $condition = "usersearch_id=$id";
                
                $filelds = array('Neighborhood.neighborhood_name');
		
		$usersearch_neighborhood= $this->Searchneighborhood->allUserSearchNeighborhood($condition, $filelds, NULL, NULL, NULL);
                
                $this->set('usersearch_neighborhood', $usersearch_neighborhood);
                
                
                               
                $condition = "Usersearch.id=$id";		
		
		$filelds = array('User.email','Usersearch.serach_name','City.city_name','Usersearch.add_date', 'Usersearch.minprice','Usersearch.maxprice','Category.category_name');
		
		$save_search = $this->Usersearch->allUserSearch($condition, $filelds, NULL, NULL, NULL);
               
               $this->set('save_search',$save_search);
	       
	       $condition = "Searchfilter.usersearch_id=$id";	
               
               $userSearchFilter = $this->Searchfilter->allUserSearchFilter($condition, NULL, NULL, NULL, NULL);
	       
	       
	       //print_r($userSearchFilter);
               
              $filter_data = array();
		foreach($userSearchFilter as $value)
			{				
				
			$condition = "Filter.id = ".$value['Searchfilter']['filter_id']."";
			
			$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			$order_by = "Categoryfilter.id asc";     
	 
	 		array_push($filter_data,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			}
               $this->set('filterdata',$filter_data);
               
              //print_r($filter_data);
               
                
               
		
             
	}
	
	
		
 
}
?>