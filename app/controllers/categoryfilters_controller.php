<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class CategoryfiltersController extends AppController
{
  var $name = 'Categoryfilters'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Categoryfilter','Adminmainmenu','Admin');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function managecategoryfilter()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Categoryfilter Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page)); 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	 
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activecategoryfilter($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active Categoryfilter';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactivecategoryfilter($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive Categoryfilter';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Categoryfilter Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
		}
	  else
		{
		    $searchkey="";
		    $cri="isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="isdelete='0' AND category_filter_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'Categoryfilter Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND category_filter_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND category_filter_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'Categoryfilter Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND category_filter_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND category_filter_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete categoryfilter   ===============================


 function deletecategoryfilter($id)
{
	$this->checkSession();  
    $this->Categoryfilter->del($id);
	$this->Session->setFlash('The Categoryfilter is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete categoryfilter   ===============================

//=====================================   Start add categoryfilter   ===============================
 	 

function addcategoryfilter()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New Categoryfilter';
	  if(!empty($this->data))
        {
			if(empty($this->data['Categoryfilter']['category_filter_name']))
				{
				 	$this->Session->setFlash('Category filter name is already exists !');
				 	//$this->redirect('/categoryfilters/addcategoryfilter');
				 } 
			else if($this->Categoryfilter->findAllBycategory_filter_name($this->data['Categoryfilter']['category_filter_name']))
				{
				 	$this->Session->setFlash('This categoryfilter name is already exists !');
				 	$this->redirect('/categoryfilters/addcategoryfilter');
				 } 
		 	 else
		   		{
					$this->data['Categoryfilter']['modifiedon']=date('Y-m-d');
					$this->data['Categoryfilter']['createdon']=date('Y-m-d');
					$this->Categoryfilter->save($this->data);
					$this->Session->setFlash('New Categoryfilter is successfully added');
					//echo '<pre>';
						//print_r($this->data);
					//echo '</pre>';
					$this->redirect('/categoryfilters/managecategoryfilter');
          		 }
         }
  }

//=====================================   End add categoryfilter   ===============================

//=====================================   Start Edit categoryfilter   ===============================


function editcategoryfilter($id = null)
{

   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->Categoryfilter->id=$id;
	   $this->data = $this->Categoryfilter->read();
	 }
   else
     { 

	  	if ($this->Categoryfilter->save($this->data['Categoryfilter']))
	       {
		        $idd=$this->data['Categoryfilter']['id'];
		   		$updatefn=$this->Categoryfilter->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->Categoryfilter->execute("UPDATE `categoryfilters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Categoryfilter']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
				$this->Session->setFlash('Categoryfilter name is successfully updated');
		 	    $this->redirect($controller.'/managecategoryfilter'.'?page='.$pagenum);
		   }
    }
}
 
//=====================================   End Edit categoryfilter   ===============================

//=====================================   Start View categoryfilter   ===============================

function viewcategoryfilter($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View Categoryfilter';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->Categoryfilter->id = $id;
		$this->set('userdata',$this->Categoryfilter->read(null,id));
  	}
   else
   {
	 $this->Categoryfilter->id = $id;
     $this->set('userdata',$this->Categoryfilter->read());
	}   
}

//=====================================   End View categoryfilter   ===============================

//=====================================   Start delete categoryfilter   ===============================


/*function deleteCategoryfilter1($id)
	{   
	   $this->checkSession();	
       $this->Content->execute("update Contents  set isdelete='1' where id=".$id);
	   $this->Session->setFlash('The CMS is successfully deleted');
	   $controller=$this->params['controller'];
	   $action=$this->params['pass'][1];   
	   if($this->params['pass'][1]!='')
	    {
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][1].'&page='.$this->params['url']['page']);
		}
		else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}
    }
*/
//=====================================   End delete categoryfilter   ===============================

//=====================================   Start block categoryfilter   ===============================
function blockcategoryfilter($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->Categoryfilter->blockcategoryfilterfn($id);
		//$this->Categoryfilter->execute("update Categoryfilters  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This Categoryfilter is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block categoryfilter   ===============================

//=====================================   Start Unblock categoryfilter   ===============================

function unblockcategoryfilter($id=NULL)
	{
		 $this->Categoryfilter->unblockcategoryfilterfn($id);
		 //$this->Categoryfilter->execute("update Categoryfilters  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This Categoryfilter is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock categoryfilter   ===============================

//=====================================   Start blockall categoryfilter   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->Categoryfilter->blockallfn($id);
			  //$this->Categoryfilter->execute("update Categoryfilters  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall categoryfilter   ===============================

//=====================================   Start Unblockall categoryfilter   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->Categoryfilter->unblockallfn($id);
		   //$this->Categoryfilter->execute("update Categoryfilters  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall categoryfilter   ===============================
	 
//=====================================   Start deleteall categoryfilter   ===============================
	 
function deleteall()
	 {
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->Categoryfilter->del($id);
		 	}
		 $this->Session->setFlash('Categoryfilter(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall categoryfilter   ===============================

//=====================================   Start searchbyletter categoryfilter   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter categoryfilter   ===============================

//=====================================   Start editactivecategoryfilter categoryfilter   ===============================

 function editactivecategoryfilter($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active Categoryfilter';
	  if(empty($this->data))
		{
			$this->Categoryfilter->id=$this->params['pass'][0];
		    $this->data = $this->Categoryfilter->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['Categoryfilter']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->Categoryfilter->id=$this->data['Categoryfilter']['id'];
				   $this->Categoryfilter->save($this->data);
				   $idd=$this->data['Categoryfilter']['id'];
				   $this->Categoryfilter->editactivecategoryfilterfn($idd);
				   //$this->Categoryfilter->execute("UPDATE `Categoryfilters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent categoryfilter   ===============================

//=====================================   Start editinactivecategoryfilter categoryfilter   ===============================

/*function editinactivecontent($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive CMS';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
	               
			   if(empty($this->data))
				{
					//pr($this->params);
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Content->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Content->id=$this->data['Content']['id'];
							   $this->Content->save($this->data);
							   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
							  
							   
			           }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
					     if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
				}
	   }*/

//=====================================   End editactivecontent content   ===============================

//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND category_filter_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND category_filter_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Categoryfilter->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================

//=====================================   Start searchbyname1 content   ===============================

/*
 	 function searchbyname1($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND category_filter_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		
  }*/
//=====================================   End searchbyname1 content   ===============================

 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Categoryfilter->find($cri));
   }
 //---------------end of page view-----------------  
 
 }
?>