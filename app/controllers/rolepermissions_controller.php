<?php
ob_start();
class RolepermissionsController extends AppController {
var $name='Rolepermissions';
var $helpers = array('Html', 'Form','javascript','Pagination' );
var $uses=array('Rolepermission','Permissionmaster','Rolemaster','Admin');
    var $components = array ('Pagination'); // Added 
	function index()
	{
	    $this->checkSession();	
	$this->redirect('/rolepermissions/manage');
	}

function manage(){
 $this->checkSession();	
//$this->layout="admin_body";
//$this->title="Set permission";
 //pr($this->data['Rolepermission']);
 $status="all";
 $skey=NULL;
 $page=1;
 $role_name=$this->data['Rolepermission']['role_name'];
 $role_status=$this->data['Rolepermission']['role_status'];
 $select_role_name="SELECT role_name FROM `rolemasters` WHERE role_name='$role_name'";
 $res_role_name=mysql_query( $select_role_name);
 $num_role_name=mysql_num_rows($res_role_name);                
 $created_date=date("Y-m-d");
	  if((!empty($role_name)) && ($num_role_name=="0"))
	  {
				 $sql_role="INSERT INTO `rolemasters` (`role_name`,`role_status`,`created_date`) VALUES('$role_name','$role_status','$created_date')";
				 mysql_query($sql_role);
				 $role_id=mysql_insert_id();
				 
				 foreach($this->data['Rolepermission'] as $key =>$value)
				 {
					  if($key!="role_name"){
					       $role_explode=explode("_",$key);
						   $menu_id=$role_explode[0];
						   $role_field=$role_explode[1];
						   $role_array[$menu_id][$role_field]=$value;
					   }
				 }
					foreach( $role_array as $role_array1 => $role_array2){
						$sql ="insert INTO `rolepermissions` SET ";
						foreach($role_array2 as $key => $value){
							$sql .= "`".$key."`='1',";
						}
						$sql .= "`rolemaster_id`='".$role_id."',";
						$sql .= "`admin_menu_id`='".$role_array1."'";
						$this->Rolepermission->execute($sql);
					}
					 $this->Session->setFlash('You have successfully created the role');
					$this->redirect('/rolemasters/viewrole/'.$status.'/'.$skey.'/'.$page);
					
		  }	
		  else
		  {
		     if(empty($role_name)){
			  $this->Session->setFlash('You have not select any role');
			 }
			 else{
			 $this->Session->setFlash('Record is already exists');
			 }
			 $this->redirect('/permissionmasters');
		  }
		 	
    }     
function show($id=NULL) {
/*echo $id;
exit();*/
 
 $this->checkSession();	
 $this->layout="after_adminlogin";
 $this->title="Set permission";
 $role_name="SELECT `role_name`,role_status FROM `rolemasters` WHERE id='".$id."' ";
 $res_name=mysql_query($role_name);
 $row_name=mysql_fetch_array($res_name);
 $nameofrole=$row_name['role_name'];
 $role_staus=$row_name['role_status'];
 $permission=$this->Permissionmaster->query("SELECT * FROM `permissionmasters`");
 $coun=count($permission);
	   for($i=0;$i<$coun;$i++)
	   { 
	  
	  // pr($permission[$i]);
		   foreach($permission[$i]['permissionmasters'] as $key=>$value)
		   { 
		   if($key!="id" && $key!="admin_menu_id")
		   {
		    $tab[$key]=$key;
			$checkbox[$i][$key]=$value;
		   }
		   if($key=="admin_menu_id")
		   {
		     $sql_menu="SELECT `menu_name` FROM `admin_menus` WHERE `menu_id`='".$value."'";
			 $res=mysql_query($sql_menu) or die(mysql_error());
			 $row=mysql_fetch_array($res);
			 $menu_name[$i][$key]=$row['menu_name'];
			 $menu_id[$i][$key]=$value;
	$check_select[$i][$key]=$this->Rolepermission->query("SELECT * FROM rolepermissions WHERE admin_menu_id='".$value."' AND `rolemaster_id`='".$id."'");
			
		   }
		       if(!empty($value))
				   {
				$menu[$i][$key]=$value;
					}
		   }
		}
		$this->set('tab',$tab);
		$this->set('selectcheck',$checkbox);
		$this->set('menu_name',$menu_name);
		$this->set('menu_id',$menu_id);
		$this->set('check_select',$check_select);
		$this->set('role_name',$nameofrole);
		$this->set('role_id',$id);
		$this->set('role_status',$role_staus);
		
}
function edit($id=NULL){
$role_name=$this->data['Rolepermission']['role_name'];
$role_status=$this->data['Rolepermission']['role_status'];
//echo $role_name;
 $select_role_name="SELECT role_name FROM `rolemasters` WHERE role_name='$role_name'";
 $res_role_name=mysql_query( $select_role_name);
 $num_role_name=mysql_num_rows($res_role_name);
 $modified_date=date("Y-m-d");
 $status="all";
 $skey=NULL;
 $page=1;
//echo $num_role_name;
	  if((!empty($role_name)) && ($num_role_name<=1))
	  {
        $sql_role="UPDATE `rolemasters` SET `role_name`='$role_name',`role_status`='$role_status',`modified_date`='$modified_date' WHERE id='".$id."'";
				 mysql_query($sql_role);
				 $this->Rolepermission->execute("DELETE FROM `rolepermissions` WHERE rolemaster_id='".$id."'");
				  foreach($this->data['Rolepermission'] as $key =>$value)
				 {
					  if($key!="role_name"){
					       $role_explode=explode("_",$key);
						   $menu_id=$role_explode[0];
						   $role_field=$role_explode[1];
						   $role_array[$menu_id][$role_field]=$value;
					   }
				 }
					foreach( $role_array as $role_array1 => $role_array2){
						$sql ="INSERT INTO `rolepermissions` SET ";
						foreach($role_array2 as $key => $value){
							$sql .= "`".$key."`='1',";
						}
						$sql .= "`rolemaster_id`='".$id."',";
						$sql .= "`admin_menu_id`='".$role_array1."'";
						
						$this->Rolepermission->execute($sql);
					}
			}	
			//echo $sql;	
			$this->Session->setFlash('The Role updated successfully');
			$this->redirect('/rolemasters/viewrole/'.$status.'/'.$skey.'/'.$page);
		
}
		 
}	
?>	