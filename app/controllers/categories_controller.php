<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class CategoriesController extends AppController
{
  var $name = 'Categories'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Category','Adminmainmenu','Admin','Filter','Categoryassignfilter');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function managecategory()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Category Management';	 
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page)); 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	 
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activecategory($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active Category';	 
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactivecategory($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive Category';	 
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Category Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
		}
	  else
		{
		    $searchkey="";
		    $cri="isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="isdelete='0' AND category_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'Category Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND category_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND category_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'Category Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND category_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND category_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete category   ===============================


 function deletecategory($id)
{
	$this->checkSession();  
    $this->Category->del($id);
	$this->Session->setFlash('The Category is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete category   ===============================

//=====================================   Start add category   ===============================
 	 

function addcategory()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New Category';
	 
	  if(!empty($this->data))
        {
			if(empty($this->data['Category']['category_name']))
				{
				 	$this->Session->setFlash('Category name is empty !');
				 	//$this->redirect('/categories/addcategory');
				 } 
			else if($this->Category->findAllBycategory_name($this->data['Category']['category_name']))
				{
				 	$this->Session->setFlash('This category name is already exists !');
				 	$this->redirect('/categories/addcategory');
				 } 
		 	 else
		   		{
				
					$this->data['Category']['modifiedon']=date('Y-m-d');
					$this->data['Category']['createdon']=date('Y-m-d');
					$this->Category->save($this->data);
					$this->Session->setFlash('New Category is successfully added');
					$this->redirect('/categories/managecategory');
          		 }
         }
  }

//=====================================   End add category   ===============================

//=====================================   Start Edit category   ===============================


function editcategory($id = null)
{
   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->Category->id=$id;
	   $this->data = $this->Category->read();
	 }
   else
     {   
	  	if ($this->Category->save($this->data['Category']))
	       {
		        $idd=$this->data['Category']['id'];
		   		$updatefn=$this->Category->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->Category->execute("UPDATE `categorys` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Category']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
				$this->Session->setFlash('Category name is successfully updated');
		 	    $this->redirect($controller.'/managecategory'.'?page='.$pagenum);
		   }
    }
}
 
//=====================================   End Edit category   ===============================

//=====================================   Start View category   ===============================

function viewcategory($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View Category';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->Category->id = $id;
		$this->set('userdata',$this->Category->read(null,id));
  	}
   else
   {
	 $this->Category->id = $id;
     $this->set('userdata',$this->Category->read());
	}   
}

//=====================================   End View category   ===============================



//=====================================   Start block category   ===============================
function blockcategory($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->Category->blockcategoryfn($id);
		//$this->Category->execute("update Categorys  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This Category is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block category   ===============================

//=====================================   Start Unblock category   ===============================

function unblockcategory($id=NULL)
	{
		 $this->Category->unblockcategoryfn($id);
		 //$this->Category->execute("update Categorys  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This Category is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock category   ===============================

//=====================================   Start blockall category   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->Category->blockallfn($id);
			  //$this->Category->execute("update Categorys  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall category   ===============================

//=====================================   Start Unblockall category   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->Category->unblockallfn($id);
		   //$this->Category->execute("update Categorys  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall category   ===============================
	 
//=====================================   Start deleteall category   ===============================
	 
function deleteall()
	 {
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->Category->del($id);
		 	}
		 $this->Session->setFlash('Category(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall category   ===============================

//=====================================   Start searchbyletter category   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter category   ===============================

//=====================================   Start editactivecategory category   ===============================

 function editactivecategory($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active Category';
	  if(empty($this->data))
		{
			$this->Category->id=$this->params['pass'][0];
		    $this->data = $this->Category->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['Category']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->Category->id=$this->data['Category']['id'];
				   $this->Category->save($this->data);
				   $idd=$this->data['Category']['id'];
				   $this->Category->editactivecategoryfn($idd);
				   //$this->Category->execute("UPDATE `Categorys` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent category   ===============================



//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND category_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND category_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Category->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================



 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Category->find($cri));
   }
 //---------------end of page view-----------------  
 
 //------------------- Assign Filter In Category ------------------------//
 
 function assignCategoryFilter($id = null)
	{
	   $this->checkSession();
	   $this->layout='after_adminlogin';
	   
	  $allFilterData = $this->Categoryassignfilter->findAllFilterByOption($id); 
	  $categor_assign_filter = array();
	  for($count=0; $count<sizeof($allFilterData); $count++)
	  		{
				array_push($categor_assign_filter,$allFilterData[$count]['Categoryassignfilter']['filter_id']);
			}
	
	   $this->set('categor_assign_filters',$categor_assign_filter);
	    
  	 $condition = "Filter.isdelete='0'";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc";     
	 
	 $this->set('filterdata',$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));
	 
	 $this->set('categorydata',$this->Category->findAllByOption($id)); 
	}
 
function addfiltercategory()
	{
	   $this->checkSession();
	   $this->layout='after_adminlogin';	   
	   
		$this->Categoryassignfilter->deleteData($this->data['Categoryassignfilter']['categorie_id']);
		
		
	 	foreach($this->params['form']['filter_id'] as $id)
			{
		  		$return_true = $this->Categoryassignfilter->saveData($id,$this->data['Categoryassignfilter']['categorie_id']);
		 	}
			
			if($return_true)
			$this->Session->setFlash('Filter assign successfully in this category!');
			
		$controller=$this->params['controller'];
		$action=$this->params['pass'][1];
		   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/managecategory'.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				 $this->redirect($controller.'/managecategory'.'?page='.$this->params['url']['page']);
			} 
		
	}
	
 //-----------------------------------------------------------------------//
 
 
 }
?>