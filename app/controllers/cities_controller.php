<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class CitiesController extends AppController
{
  var $name = 'Cities'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('City','Adminmainmenu','Admin');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function managecity()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'City Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	
	 $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page)); 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);	 
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activecity($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active City';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactivecity($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive City';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'City Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
			
		}
	  else
		{
		    $searchkey="";
		    $cri="isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="isdelete='0' AND city_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'City Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND city_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND city_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'City Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND city_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND city_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete city   ===============================


 function deletecity($id)
{
	$this->checkSession();  
    $this->City->del($id);
	$this->Session->setFlash('The City is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete city   ===============================

//=====================================   Start add city   ===============================
 	 

function addcity()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New City';
	  if(!empty($this->data))
        {
			if(empty($this->data['City']['city_name']))
				{
				 	$this->Session->setFlash('City name is empty !');
				 	$this->redirect('/cities/addcity');
				 } 
			else if($this->City->findAllBycity_name($this->data['City']['city_name']))
				{
				 	$this->Session->setFlash('This city name is already exists !');
				 	$this->redirect('/cities/addcity');
				 } 
		 	 else
		   		{
					$this->data['City']['modifiedon']=date('Y-m-d');
					$this->data['City']['createdon']=date('Y-m-d');
					$this->City->save($this->data);
					$this->Session->setFlash('New City is successfully added');
					$this->redirect('/cities/managecity');
          		 }
         }
  }

//=====================================   End add city   ===============================

//=====================================   Start Edit city   ===============================


function editcity($id = null)
{
   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->City->id=$id;
	   $this->data = $this->City->read();
	 }
   else
     {   
	  	if ($this->City->save($this->data['City']))
	       {
		        $idd=$this->data['City']['id'];
		   		$updatefn=$this->City->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->City->execute("UPDATE `citys` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['City']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
				$this->Session->setFlash('City name is successfully updated');
		 	    $this->redirect($controller.'/managecity'.'?page='.$pagenum);
		   }
    }
}
 
//=====================================   End Edit city   ===============================

//=====================================   Start View city   ===============================

function viewcity($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View City';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->City->id = $id;
		$this->set('userdata',$this->City->read(null,id));
  	}
   else
   {
	 $this->City->id = $id;
     $this->set('userdata',$this->City->read());
	}   
}

//=====================================   End View city   ===============================

//=====================================   Start delete city   ===============================


/*function deleteCity1($id)
	{   
	   $this->checkSession();	
       $this->Content->execute("update Contents  set isdelete='1' where id=".$id);
	   $this->Session->setFlash('The CMS is successfully deleted');
	   $controller=$this->params['controller'];
	   $action=$this->params['pass'][1];   
	   if($this->params['pass'][1]!='')
	    {
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][1].'&page='.$this->params['url']['page']);
		}
		else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}
    }
*/
//=====================================   End delete city   ===============================

//=====================================   Start block city   ===============================
function blockcity($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->City->blockcityfn($id);
		//$this->City->execute("update Citys  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This City is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block city   ===============================

//=====================================   Start Unblock city   ===============================

function unblockcity($id=NULL)
	{
		 $this->City->unblockcityfn($id);
		 //$this->City->execute("update Citys  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This City is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock city   ===============================

//=====================================   Start blockall city   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->City->blockallfn($id);
			  //$this->City->execute("update Citys  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall city   ===============================

//=====================================   Start Unblockall city   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->City->unblockallfn($id);
		   //$this->City->execute("update Citys  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall city   ===============================
	 
//=====================================   Start deleteall city   ===============================
	 
function deleteall()
	 {
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->City->del($id);
		 	}
		 $this->Session->setFlash('City(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall city   ===============================

//=====================================   Start searchbyletter city   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter city   ===============================

//=====================================   Start editactivecity city   ===============================

 function editactivecity($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active City';
	  if(empty($this->data))
		{
			$this->City->id=$this->params['pass'][0];
		    $this->data = $this->City->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['City']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->City->id=$this->data['City']['id'];
				   $this->City->save($this->data);
				   $idd=$this->data['City']['id'];
				   $this->City->editactivecityfn($idd);
				   //$this->City->execute("UPDATE `Citys` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent city   ===============================

//=====================================   Start editinactivecity city   ===============================

/*function editinactivecontent($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive CMS';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
	               
			   if(empty($this->data))
				{
					//pr($this->params);
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Content->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Content->id=$this->data['Content']['id'];
							   $this->Content->save($this->data);
							   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
							  
							   
			           }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
					     if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
				}
	   }*/

//=====================================   End editactivecontent content   ===============================

//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND city_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND city_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->City->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================

//=====================================   Start searchbyname1 content   ===============================

/*
 	 function searchbyname1($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND city_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		
  }*/
//=====================================   End searchbyname1 content   ===============================

 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->City->find($cri));
   }
 //---------------end of page view-----------------  
 
 }
?>