<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class UnsubscribedsController extends AppController
{
  var $name = 'Unsubscribeds'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Unsubscribed','Adminmainmenu','Admin');
  var $components = array('Pagination'); 
  var $layout='allAdvertise';
//=====================================   Start Manage Content  =====================================  
 
   
  //..................Display Page ....................//
  
  
  
  
  
  function unsubscribeaddress($type=NULL, $id = NULL)
   {    
			$this->checkSession();
			$this->pageTitle='Address Manager';
			$this->layout="after_adminlogin"; 			
			$cri="GROUP BY Unsubscribed.email";					
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Unsubscribed->findAlladdressDetail($cri,NULL,NULL,$limit,$page)); 	 	   
			
			if(isset($_REQUEST['menu_id']))
			{			
				$_SESSION['menu_id']=$_REQUEST['menu_id'];
			} 
			if(!isset($type))
			$type = 's'; 
			$this->set('type',$type); 	 	   
  }
  //..................................................//
  
  
  //...Add New Advertise.......
  
  function addadvertise($type=NULL)
  {
	$this->layout = "after_adminlogin";
    $this->pageTitle = 'Add Advertise';	
	
		
		
		if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		if(!isset($type))
			$type = 's'; 
			$this->set('type',$type); 	 
		
		$this->set('action','add');
		$file_path = getcwd()."/uploadimage/add";
			
	  if(!empty($this->data))
        {	
				
			if(!isset($this->data['Advertise']['id']))
			{
				$this->data['Advertise']['id'] = NULL;			
			}
			  
			if($this->data['Advertise']['name']=='')
			{
			 	$this->Session->setFlash('The name should not be blank !');
			 	$this->addPageRedirect($this->data['Advertise']['pagenum'],$this->data['Advertise']['id']);
			}
			elseif($this->data['Advertise']['url']=='')
			{
			 	$this->Session->setFlash('The url should not be blank !');
			 	$this->addPageRedirect($this->data['Advertise']['pagenum'],$this->data['Advertise']['id']);
			}
			     
			elseif($this->Advertise->availableAdvertisename($this->data['Advertise']['name'],$this->data['Advertise']['id'],$this->data['Advertise']['type']))
			{
			 	$this->Session->setFlash('This name is already exists !');
			 	$this->addPageRedirect($this->data['Advertise']['pagenum'],$this->data['Advertise']['id']);
			}
			elseif($this->Advertise->availableEmail($this->data['Advertise']['url'],$this->data['Advertise']['id'],$this->data['Advertise']['type']))
			{
			 	$this->Session->setFlash('This url is already exists !');
			 	$this->addPageRedirect($this->data['Advertise']['pagenum'],$this->data['Advertise']['id']);
			} 
			else
		    {			
				if(!isset($this->data['Advertise']['id']))
					{	
												
						$this->data['Advertise']['createdon']=date('Y-m-d');						
					}
				else
					{					
						$this->data['Advertise']['modifiedon']=date('Y-m-d');						
					}
					
					
					
					if(isset($this->data['Advertise']['PhotoT']) && $this->data['Advertise']['PhotoT']=='Remove_Photo')
					{						
						unlink($file_path."/".$this->data['Advertise']['imghidden']);	
						unlink($file_path."/thumb/".$this->data['Advertise']['imghidden']);	
						$this->data['Advertise']['file'] = true;							
					}
					else if(isset($this->data['Advertise']['PhotoT']) && $this->data['Advertise']['PhotoT']=='Change_Photo')
					{
						unlink($file_path."/thumb/".$this->data['Advertise']['imghidden']);
						$this->data['Advertise']['file'] = $this->uploadImage($this->data);
					
					}
					elseif(isset($this->data['Advertise']['PhotoT']) && $this->data['Advertise']['PhotoT']=='Keep_Photo')
					{
						$this->data['Advertise']['file'] = $this->data['Advertise']['imghidden'];	
					}
					else 
					{
							
						if(!empty($this->data['Advertise']['imagename']['name']))	
						{							
						$this->data['Advertise']['file'] = $this->uploadImage($this->data);							
						}
						else
						$this->data['Advertise']['file'] = true;					
					}
							
										
					
					if($this->data['Advertise']['file'])
					{	
						if(isset($this->data['Advertise']['url']))
						$this->data['Advertise']['url'] = str_replace("http://", "", $this->data['Advertise']['url']);	
						
						if($this->Advertise->save($this->data))				
						{
							if(!isset($this->data['Advertise']['id']))			
							$this->Session->setFlash('New Advertise is successfully added');
							else
							$this->Session->setFlash('Advertise is successfully updated');	
							
												
							$this->redirect('/Addresses/managesitead/'.$this->data['Advertise']['type']);
											
						
						}
					}
			}		 
        }
		
		
  }
 //....End Add New Advertise end..........
 
 function addPageRedirect($pageno,$id = NULL)
 	{
		if(isset($id))
		$this->redirect('/Addresses/editAdvertise/'.$id.'/manageAdvertise?page='.$page);
	}
 
 function editAdvertise($id = null)
	{
	  
	   $this->checkSession();
	   $this->layout='after_adminlogin'; 
	   $this->pageTitle = 'Edit Advertise';
	   $this->Advertise->id=$id;	
	   $this->data = $this->Advertise->read();
	   $this->set('advertise_id',$id); 
	   $this->set('advertisedata',$this->data);      
	   $this->set('action','edit');
	   $this->set('type',$this->data['Advertise']['type']); 	 
	   $this->render('addadvertise');
	   
	}
 
 
  //....Delete Advertise start...........
  function deleteAddress($id)
	{
		
		$this->checkSession();  
		
		if($this->Unsubscribed->deleteAdddress($id))
		{
		       $this->Session->setFlash('The Address is successfully Deleted');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/unsubscribeaddress/'.$type.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/unsubscribeaddress/'.$type.'?page='.$this->params['url']['page']);
				}   
		 }
   }
 //....Delete Advertise end...........
 
 //...............View Advertise...................//
 function viewAdvertise($id=null)
 {
	$this->checkSession();
	$this->layout="after_adminlogin";
	$this->pageTitle='View Advertise';   
	$this->set('action','view');
	$this->set('Advertise_id',$id);    
	$this->Advertise->id = $id;
	$this->set('advertisedata',$this->Advertise->read());
	 $this->set('type',$this->data['Advertise']['type']); 	
	 $this->render('addadvertise');
   } 
 
 //.................End View Advertise.................//
 
 //....Block Advertise start..........
 	function blockAdvertise($id=NULL,$type=NULL)
	{
			$this->layout = "after_adminlogin";
			$this->Advertise->blockAdvertise($id);	 
			$this->Session->setFlash('This Advertise is successfully blocked');
			
	      	$controller=$this->params['controller'];
		    $action=$this->params['pass'][1]; 
			  
		   if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/managesitead/'.$type.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
			else
			{
				$this->redirect($controller.'/managesitead/'.$type.'?page='.$this->params['url']['page']);
			}
	} 
 //....Block Advertise end.........
 //....UnBlock Advertise start.....
 	function unblockAdvertise($id=NULL,$type=NULL)
	{
	
		$this->layout = "after_adminlogin";
		$this->Advertise->unblockAdvertise($id);	 
		$this->Session->setFlash('This Advertise is successfully Unblocked');
		$controller=$this->params['controller'];
		$action=$this->params['pass'][1];
		if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/managesitead/'.$type.'/'.$this->params['pass'][2].'/'.$this->params['pass'][3].'?page='.$this->params['url']['page']);
			}
		else
			{
			$this->redirect($controller.'/managesitead/'.$type.'?page='.$this->params['url']['page']);
			}
		
   } 
 //.......Unblock Advertise end......
 //........................................................................................BlockAll Admin start
 	function blockall()
	 {
	 	
	  
	 			foreach($this->params['form']['chkAdvertise'] as $id)
			     {
				   $this->Advertise->blockAdvertise($id);
				 }
				 
				  $this->Session->setFlash('Record(s) successfully blocked');
				  
				 $controller=$this->params['controller'];
				 //$action=$this->params['form']['referer'];		 
			
				 if($this->params['form']['key']!='')
				 {				 	
				   $this->redirect($controller.'/managesitead/'.$this->params['form']['type'].'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/managesitead/'.$this->params['form']['type'].'?page='.$this->params['form']['pagenum']);
				 }
	   }	
 //........................................................................................BlockAll Admin end
 //.........................................................................................UnBlockAll start
 	function unblockall()
	 {
	 			
		foreach($this->params['form']['chkAdvertise'] as $id)
		 {
			$this->Advertise->unblockAdvertise($id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		
		  if($this->params['form']['key']!='')
				 {				 	
				   $this->redirect($controller.'/managesitead/'.$this->params['form']['type'].'/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/managesitead/'.$this->params['form']['type'].'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //..........................................................................................UnblockAll end
 //............................................................................................Delete All start
 	function deleteall()
	 {
	  //pr($this);
	 		    foreach($this->params['form']['chkAdvertise'] as $id)
			     {
				   $this->Advertise->execute("DELETE FROM unsubscribeds where id=".$id);
				 }
				  $this->Session->setFlash('Address(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				 if($this->params['form']['key']!='')
				 {				 	
				   $this->redirect($controller.'/unsubscribeaddress/'.$this->params['form']['key'].'/'.$this->params['form']['searchkey'].'?page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/unsubscribeaddress/'.$this->params['form']['type'].'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //...Delete All end......
 
 
 //...............Search By..................//
 function searchbyname($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Advertise Management';
		 	  
		  if(isset($this->params['form']['srchkey'])) 
		  		{ 				    
		             $searchkey=trim($this->params['form']['srchkey']);
					 $this->set('searchkey',$searchkey);
			     }
			
		 
			else
		    {			
				$this->set('searchkey',$searchkey);
                                
                                $cri=" Unsubscribed.email='".$this->params['form']['type']."' order by Unsubscribed.id desc";		
                                list($order,$limit,$page) = $this->Pagination->init($cri);
                                $this->set('admindata',$this->Unsubscribed->findAlladdressDetail($cri,NULL,NULL,$limit,$page)); 	
                                
				 
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
			
                        
                    $cri="Unsubscribed.email LIKE '%".$searchkey."%'  order by Unsubscribed.id desc";	
                    
                    list($order,$limit,$page) = $this->Pagination->init($cri);
                    $this->set('admindata',$this->Unsubscribed->findAlladdressDetail($cri,NULL,NULL,$limit,$page)); 
		 }
		$this->set('type',$this->params['form']['type']);
		$this->render('unsubscribeaddress');
		
  }
 //..........................................//
 
 
 //...Active Advertise start......
 	function activeaddvertise($type=NULL,$page=1)
 	{ 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active Advertise';	
		  	  
		  $cri="isdelete='0' AND isblocked='0' AND type = '$type' order by id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->Advertise->findAllAdvertiseDetail($cri,NULL,NULL,$limit,$page)); 
		  $this->set('type',$type);	 
		  $this->render('managesitead');
	}
	
 //...Inactive Advertise start....
 	function inactiveaddvertise($type=NULL,$page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Advertise';
		  
		   $cri="isdelete='0' AND isblocked='1' AND type = '$type' order by id asc";		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  
		  $this->set('admindata',$this->Advertise->findAllAdvertiseDetail($cri,NULL,NULL,$limit,$page)); 
		  $this->set('type',$type);	 	 
		  $this->render('managesitead');
  }
  
 //................................................................................................Search ByName Active start
 
 
 
 
 //...Inactive Advertise start....

  
  
  function uploadImage($data)
  		{
			
			$fileName = false;
			$file_path = getcwd()."/uploadimage/add";	
			
			if(!empty($data['Advertise']['imagename']['name']) && ($data['Advertise']['imagename']['type']=='image/jpeg' || $data['Advertise']['imagename']['type']=='image/png' || $data['Advertise']['imagename']['type']=='image/pjpeg'))
				{
					
					
					if($data['Advertise']['imagename']['size'] <2097152)
						{
							
							$fileName = mt_rand().'_'.$data['Advertise']['imagename']['name'];
							
							$filestatus = move_uploaded_file($data['Advertise']['imagename']['tmp_name'],$file_path."/fullsize/".$fileName);			
							
							$source_fileName = $file_path."/fullsize/".$fileName;						
							
							$thumb = $file_path."/thumb/".$fileName;
							
							$show = $file_path."/".$fileName;
							
							$this->make_thumb($source_fileName,$thumb,96,138);
							$this->make_thumb($source_fileName,$show,300,300);
							
						
						}else
						$this->Session->setFlash('Uplaod must be smaller than 2 MB !');
					}
					else
					$this->Session->setFlash('Upload must be a jpeg or png file image');
					
					
					return $fileName;
			
		}
	
	
  function make_thumb($img_name,$filename,$new_w,$new_h)
			{
				$ext='jpg';
				if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
					$src_img=imagecreatefromjpeg($img_name);
					
				if(!strcmp("png",$ext))
					$src_img=imagecreatefrompng($img_name);
			
		
				$old_x=imageSX($src_img);
				$old_y=imageSY($src_img);
			
				$ratio1=$old_x/$new_w;
				$ratio2=$old_y/$new_h;
				if($ratio1>$ratio2) {
					$thumb_w=$new_w;
					$thumb_h=$old_y/$ratio1;
				}
				else 
					{
						$thumb_h=$new_h;
						$thumb_w=$old_x/$ratio2;
					}
			
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			
			if(!strcmp("png",$ext))
				imagepng($dst_img,$filename);
			else
				imagejpeg($dst_img,$filename);
			
			imagedestroy($dst_img);
			imagedestroy($src_img);
		}
  
 
 }
?>