<?php
ob_start();
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class MobileController extends AppController
{
	var $name = 'Mobile'; 
	var $helpers = array('Html','javascript','pagination','Ajax'); 
	var $uses=array('Listing','Neighborhood','Filter','Category','City','Imagelisting','Filterlisting','User','Neighborhoodlisting','Listing_counter','Language','Adminmainmenu','Admin','User_ad','Metatag','Statistics');
	//'Search','Neighborhood','Filter','Category','Listing','Imagelisting','User','Filterlisting','Searchneighborhood','Usersearch','Searchfilter','Language','Statistics'
	var $components = array('Pagination','Frontpagination','Email');
  	
	//.......Start page................
   function index($category_id=NULL)
 	{
	
	    $this->layout = "mobile";
        $this->pageTitle = 'Mlsupdate123 Mobile';	

		$condition = "isdelete='0' and isblocked= 0";
		$filelds = array('category_name','id');
		$order_by = "id asc";     
		
		$categorydata = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);
		$this->set('categorydata',$categorydata);
		
        	
  	}
	function search($category_id=NULL)
 	{
	
	    $this->layout = "mobile";
		$this->pageTitle = 'Mlsupdate123 Mobile';
		$categoryName="";

		
		
		

		switch($category_id)
		{
		case "1":
		$categoryName="Rental Apartment Listings";
		break;
		case "3":
		$categoryName="Apartment Sale Listings";
		break;
		case "8":
		$categoryName="Buildings For Sale";
		break;
		case "9":
		$categoryName="Single Family Home";
		break;
		case "12":
		$categoryName="Commercial Space Retail";
		break;
		case "14":
		$categoryName="Private Party Spaces";
		break;
		}

		$cityOption="";
		$neighborOption="";
		if(isset($category_id))
		{
		     
			 $sql="SELECT cities.id AS CITY_ID,cities.city_name AS CITY_NAME FROM cities"; 
			 
			 $rs = mysql_query($sql) or die(mysql_error().$sql);
		
			 while($rec=mysql_fetch_assoc($rs))
			{  
			$cityOption.="<option value=\"{$rec['CITY_ID']}\">{$rec['CITY_NAME']}</option>";
			}
           		
		}
		
		$this->set('cityOption',$cityOption);
		$this->set('categoryName',$categoryName);
		$this->set('categoryID',$category_id);
		
		
        	
  	}
	
	function filter()
	{
	
	    $this->layout = "mobile";
        $this->pageTitle = 'Mlsupdate123 Mobile';
		
		$cityID = 1;
		if(isset($_SESSION['cityID']))
		$cityID = $_SESSION['cityID'];
		
		if(isset($_POST['cityID']))	
		{	
			$cityID = $_POST['cityID'];	
			$_SESSION['cityID'] = $_POST['cityID'];
		}
		
		$this->set('city_id',$cityID);
		
		$catID = 1;
		if(isset($_SESSION['categoryID']))
		$catID = $_SESSION['categoryID'];
		
		if(isset($_POST['categoryID']))	
		{	
			$catID = $_POST['categoryID'];		
			$_SESSION['categoryID'] = $_POST['categoryID'];
		}
		
		$this->set('urlkey','search');		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id=$cityID";
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "neighborhood_name asc";   
		  
		$neighborhooddata = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
	   
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $neighborhooddata = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborhooddata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($neighborhooddata as $key=>$value)
                        {
                                   $neighborhooddata[$key]['Neighborhood']['neighborhood_name'] = $this->changeLang2($neighborhooddata[$key]['Neighborhood']['neighborhood_name']); 
                        }
                    
                }
                
                
		$this->set('neighborhooddata',$neighborhooddata);	
		 	
			
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		} 
		$this->set('languagedata',$languagedata);	
			
			
		
	 	$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$citydata= $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($citydata as $key=>$value)
                        {
                                   $citydata[$key]['City']['city_name'] = $this->changeLang2($citydata[$key]['City']['city_name']); 
                        }
                    
                }
		
		$this->set('citydata',$citydata);
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc,Filter.id asc";     
	 
	 $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);

        if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($filterdata as $key=>$value)
                        {
                                  $filterdata[$key]['Filter']['filter_name'] = $this->changeLang2($filterdata[$key]['Filter']['filter_name']); 
                                  $filterdata[$key]['Categoryfilter']['category_filter_name'] = $this->changeLang2($filterdata[$key]['Categoryfilter']['category_filter_name']);  
                        }
                    
                }
                
        
	  $this->set('filterdata',$filterdata);	 
	
	 
	  $condition = "isdelete='0' AND id=$catID";
	 
	 $filelds = array('category_name');	
	 
	 $categorydata = $this->Category->findAllCategoryDetail($condition, $filelds, NULL, NULL, NULL);
         
         if($this->checkLang() != 'en')
		{
                        $categorydata = $this->changeLang('1',"Category","category_name",$categorydata);
		} 
	  $this->set('category_data',$categorydata);
	  $this->set('controller',$this->params['controller']);
	  $this->set('referer',$this->params['url']['url']);
	  $this->set('indexContent',$this->translateContent());				
	
	}
	
	function frontPaginationTotalPage($total_record,$show)
		{
			
			
			$pageCount = ceil($total_record / $show );
			
			return $pageCount;
			
		}
	
	function result($page=1)
	{
	 $this->layout = "mobile";
	 $this->pageTitle = 'Mlsupdate123 Mobile - Search Result';
	 
		if($_POST)
		{	
		    $searchText=$_POST['searchText'];
			$cityID = $_POST['cityID'];
			$categoryID=$_POST['categoryID'];
			$neighborrhoodID=$_POST['neighborrhoodID'];
			$minPrice=$_POST['amtMin'];
			$maxPrice=$_POST['amtMax'];
			
			
			
			//---- Start By Abhsihek for keeping search statistics
		$cityID = $_POST['cityID'];
		$categoryID=$_POST['categoryID'];
		$minPrice=$_POST['amtMin'];
		$maxPrice=$_POST['amtMax'];
		$neighborhoodArray=array();
		$languageArray=array();
		$typeArray=array();
		$filtersUsedArray=array();
		if(isset($this->data['Listing']['neighborhood']))
		{
		    foreach($this->data['Listing']['neighborhood'] as $neighborhood)
			{
			   //$this->Statistics->insertInfo($cityID,$categoryID,$neighborhood,$minPrice,$maxPrice);
			   array_push($neighborhoodArray,$neighborhood);
			}
			
		}
		
		if(isset($this->data['Listing']['language']))
		{
		
		    foreach($this->data['Listing']['language'] as $language)
			{
			   array_push($languageArray,$language);
			}
		
		}
		
		if(isset($this->data['Listing']['type']))
		{
		
		    foreach($this->data['Listing']['type'] as $type)
			{ 
			   array_push($typeArray,$type);
			}
		
		}
		//Now filling all filter's id
		if(isset($this->data['Filterlisting']['filterid']))
		{
		    foreach($this->data['Filterlisting']['filterid'] as $filter)
			{ 
			   array_push($filtersUsedArray,$filter);
			}
		}
		
		$this->Statistics->insertInfo($cityID,$categoryID,$neighborhoodArray,$languageArray,$typeArray,$minPrice,$maxPrice,$filtersUsedArray);
		//exit(0);
		
		//---- End By Abhsihek 
			
			
			
			
			
			
			$listingDetails=array();
			
			$categoryName="";
			switch($categoryID)
			{
			case "1":
			$categoryName="Rental Apartment Listings";
			break;
			case "3":
			$categoryName="Apartment Sale Listings";
			break;
			case "8":
			$categoryName="Buildings For Sale";
			break;
			case "9":
			$categoryName="Single Family Home";
			break;
			case "12":
			$categoryName="Commercial Space Retail";
			break;
			case "14":
			$categoryName="Private Party Spaces";
			break;
			default:
			$categoryName="Searched for '{$searchText}' ";
			break;
			}
			$this->set('searchTitle',$categoryName);
			
			//Text Search
			if(isset($searchText) && !empty($searchText) )
			{
			   //$condition1='';
			   
			   //$condition1="Listing.id={$searchText} OR  LIKE '%{$searchText}%' OR Listing.zip LIKE '%{$searchText}%' OR Listing.phone LIKE '%{$searchText}%' AND Listing.is_deleted='1'";
			   
			   /*$condition1=array(
			    "Listing.is_deleted" => "1", 
			   "OR" => array (
				   "Listing.id" => "{$searchText}",
				   "Listing.zip" => "%{$searchText}%",
				   "Listing.phone" => "%{$searchText}%",
				   "Listing.streetname" => "%{$searchText}%",
				   "Listing.email" => "%{$searchText}%"));
			   $order_by='Listing.price DESC';
			   $listingDetails1=$this->Listing->findAll($condition1,"DISTINCT Listing.id,Listing.price,Listing.neighborhood_id,Listing.neighborhood_id,Listing.streetnumber,Listing.streetname,Listing.availabledate", $order_by, NULL, NULL);
			   $this->set('listingDetail1',$listingDetails1);*/
			   
			   //----------- The logic for search from search box-----//
			   
			   
			   $city_detail = $this->City->findBycity_name(trim($searchText));
				 	if($city_detail)
					{
						$condition1 .=" OR Listing.city_id = ".$city_detail['City']['id']."";
					}
					
					$category_detail = $this->Category->findBycategory_name(trim($searchText));
				 	if($category_detail)
					{
						$condition1 .=" OR Listing.category_id = ".$category_detail['Category']['id']."";
					}
					
					$neighborhood_detail = $this->Neighborhood->findByneighborhood_name(trim($searchText));
				 	if($neighborhood_detail)
					{
						$condition1 .=" OR Neighborhoodlisting.neighborhood_id = ".$neighborhood_detail['Neighborhood']['id']."";
					}
                                        
                                        if(is_numeric(trim($searchText)))
                                        {
                                             $condition .=  "Listing.id='".trim($searchText)."' OR ";  
                                        }
					
				 $condition = "Listing.active=1 AND Listing.is_deleted=1 AND ( $condition streetnumber LIKE '%".trim($searchText)."%' OR streetname LIKE '%".trim($searchText)."%' OR state LIKE '%".trim($searchText)."%' OR phone LIKE '%".trim($searchText)."%' OR Listing.email LIKE '%".trim($searchText)."%' OR CONCAT( streetnumber,' ',streetname ) LIKE '%".trim($searchText)."%' $condition1 )" ;		 
				//}
				
			//echo $condition; 
			
			
			$order_by = "Listing.price ASC";     
			
			$filelds = array('Listing.id DISTINCT','User.email','User.id','Listing.price','Listing.created','Listing.availabledate','Listing.streetnumber','Listing.streetname','City.city_name','Listing.price_request','Listing.zip');
			
			$listing_detail = $this->Listing->allListing($condition, $filelds, $order_by, NULL, $page);
                        
                        $all_record = $this->Listing->allListing($condition, $filelds, $order_by, NULL, $page);
                        
                        $totalPage = $this->frontPaginationTotalPage(sizeof($all_record),NULL);
                        
                        $this->set('totalPage',$totalPage);
			
			//print_r($listing_detail); exit;
					
			$result_neighborhood =array();
			
			foreach($listing_detail as $neighborhood)
			{
			
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{	
								
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						if($neighborhoodDetail)
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					}
					
					
				array_push($result_neighborhood,$neighborhood_list);				
			}
			
			//print_r($listing_detail);exit;
			$this->set('listingDetail1',$listing_detail);
			$this->set('result_found',count($listing_detail));
					   
			   //-------- End of Logic of search --------//
			      
			}
			else
			{
			
				$conditionType='';
				if(isset($cityID) && !empty($cityID))
				{
				  $conditionType.= "Listing.city_id='{$cityID}' AND Listing.is_deleted='1' AND ";
				}
				
				
				/*if(isset($neighborrhoodID) && !empty($neighborrhoodID) )
				{
				  $conditionType.="Listing.neighborhood_id='{$neighborrhoodID}' OR Listing.neighborhood_id=null AND "; 
				} */
				
				//---------- for searching type -------/
				
                      if(isset($this->data['Listing']['type']) && !empty($this->data['Listing']['type']))
                                {
                                      $conditionType .=" (";  
                                        $view_type_array = $this->data['Listing']['type'];
                                        for($i=0;$i<sizeof($this->data['Listing']['type']);$i++)
                                                {                                                       
                                                        if($this->data['Listing']['type'][$i]=='video')
                                                        {
                                                               $conditionType .="  Listing.video_url!=''"; 
                                                        }
                                                        else if($this->data['Listing']['type'][$i]=='floorplans')
                                                        {
                                                             $conditionType .=" Listing.floorplan!=''";     
                                                        }
                                                        else if($this->data['Listing']['type'][$i]=='pictures')
                                                        {
                                                             $conditionType .=" Imagelisting.file!=''";     
                                                        }
                                                    
                                                    if(sizeof($this->data['Listing']['type'])==$i+1)
                                                       $conditionType .= ") AND ";
                                                    else
                                                        $conditionType .= " OR";     
                                                }
                                               
                                                
                                        
                                } 
								
								
								//------ For searching the Language
								
								$conditionlanguage ='';
								if(isset($this->data['Listing']['language']) && !empty($this->data['Listing']['language']))
								{
									$conditionLanguage ='(';
												 $count=0;
									foreach($this->data['Listing']['language'] as $value)
										{
															   if($count>0)
															   $conditionLanguage .=' OR ';
										
															 $conditionLanguage .=" User.gender like '%,".$value."' or  User.gender like '".$value.",%' or User.gender like '%,".$value.",%' or User.gender =".$value."";
															 $count++;
										}
													 $conditionLanguage .=") AND " ;  
									
								}
				
				$order_by='Listing.price DESC';
				$conditionType.=(isset($conditionLanguage) && !empty($conditionLanguage)) ? $conditionLanguage:'' ;
				
				$conditionType.="Listing.category_id={$categoryID} AND Listing.price >={$minPrice} AND Listing.price <={$maxPrice} GROUP BY Listing.id";
				
				
				$filtered_listingDetail=$this->Listing->findAll($conditionType,NULL, $order_by, NULL, NULL);
			
				
	    $result_array=array();
		$listingDetails=array();
		
		
		
	   /* if(isset($neighborrhoodID) && !empty($neighborrhoodID) )	
		{
			for($i=0; $i<sizeof($filtered_listingDetail); $i++)
				{
				$neighborhood_data = array();
				
				for($j=0; $j<sizeof($filtered_listingDetail[$i]['Neighborhoodlisting']); $j++)
						{
						    if(isset($filtered_listingDetail[$i]['Neighborhoodlisting'][$j]['neighborhood_id']))
							{
							array_push($neighborhood_data,$filtered_listingDetail[$i]['Neighborhoodlisting'][$j]['neighborhood_id']);
							}
						}
					
					
					if(sizeof($neighborhood_data)>0)
						{
							if($this->checkTwoNeighborhood($neighborrhoodID,$neighborhood_data))
							array_push($result_array,$filtered_listingDetail[$i]);
						}						
				
				}
				
				$listingDetails=$result_array;
		}
		else
		{
		$listingDetails=$filtered_listingDetail;
		}*/
		
		if(isset($this->data['Listing']['neighborhood']))
		{
			for($i=0; $i<sizeof($filtered_listingDetail); $i++)
				{
				$neighborhood_data = array();
				
				for($j=0; $j<sizeof($filtered_listingDetail[$i]['Neighborhoodlisting']); $j++)
						{
							array_push($neighborhood_data,$filtered_listingDetail[$i]['Neighborhoodlisting'][$j]['neighborhood_id']);
						}
					
					
					if(sizeof($neighborhood_data)>0)
						{
							if($this->checkTwoNeighborhood($this->data['Listing']['neighborhood'],$neighborhood_data))
							array_push($result_array,$filtered_listingDetail[$i]);
						}						
				
				}
				$listingDetails=$result_array;
		}
		else
		{
		 $listingDetails=$filtered_listingDetail;
		}
				
				
				
				
			$result_array_filter=array();
				//----------------Now Checking the other Filters ---------------//
			
            			
		    if(isset($this->data['Filterlisting']['filterid']) && !empty($this->data['Filterlisting']['filterid']))
			{
				$filter_array = array();
				for($i=0; $i<sizeof($listingDetails); $i++)
				{
					$temp_filter=array();
					for($j=0; $j<sizeof($listingDetails[$i]['Filterlisting']); $j++)
						{
							array_push($temp_filter,$listingDetails[$i]['Filterlisting'][$j]['filter_id']);
						}
					array_push($filter_array,$temp_filter);	
				}
				
            
					    if(sizeof($filter_array)>0)
						{
						    for($i=0;$i<sizeof($filter_array);$i++)
							{
							if($this->checkTwoArray($this->data['Filterlisting']['filterid'],$filter_array[$i]))
							array_push($result_array_filter,$listingDetails[$i]);
							}
						}
						
			   // print_r($filter_array);
				//print_r($this->data['Filterlisting']['filterid']);
				//exit(0);
				$this->set('listingDetail',$result_array_filter);	
                $this->set('result_found',count($result_array_filter));				
			}	
			else
			{
			     $this->set('listingDetail',$listingDetails);
				 $this->set('result_found',count($listingDetails));		
				
			}


		
		
		}
						
	}
	}
	 

	
	function viewlisting($id=null)
	{
	 $this->layout = "mobile";
	 $this->pageTitle = 'Mlsupdate123 Mobile - View Property';
	 
	  $condition="Listing.id={$id} AND is_deleted='1'";
	  $listingDetail=$this->Listing->findAll($condition,NULL, NULL, NULL, NULL);
	  $this->set('listingDetail',$listingDetail);
	  
	  
		$filter_data = array();
        $allfilter = '';
		
		        //Now Counting Neighborhood...
				$neighborhoodlist=array();
		         //Getting the each neighborhood name individually
				for($i=0;$i<sizeof($listingDetail[0]['Neighborhoodlisting']); $i++)
				{
                				
						$condition = "Neighborhood.id = ".$listingDetail[0]['Neighborhoodlisting'][$i]['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						
				array_push($neighborhoodlist,$neighborhoodDetail[0]['Neighborhood']['neighborhood_name']);				
				}
				$neighborhoodlist=array_filter($neighborhoodlist);
				$neighborhoodliststr=implode(",",$neighborhoodlist);
				$this->set('neighborhoodliststr',$neighborhoodliststr);
		
		foreach($listingDetail[0]['Filterlisting'] as $value)
			{	
			$allfilter .=$value['filter_id'].',';	
			}
                        $filterdata = array();
                        if($allfilter!='')
                        {
                                $allfilters = substr($allfilter,0, -1);
                                
                                $condition = "Filter.isdelete='0' AND Filter.id in($allfilters)";
                                
                                $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
                                
                                $order_by = "Categoryfilter.id asc";     
                                
                               $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
                                
                                if($this->checkLang() != 'en')
                                {
                                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
                                }
                                
                        }
      
	  $this->set('filterdata',$filterdata);
	  $this->set('indexContent',$this->translateContent());
	  $this->set('indexContent1',$this->translateContent1());
	  //print_r($this->Listing->findAll($condition,NULL, NULL, NULL, NULL));
	  //exit(0);
	  
	}
	function about()
 	{
	
	    $this->layout = "mobile";
	    $this->pageTitle = 'Mlsupdate123 Mobile - About US';
		
        	
  	}
	function contact()
 	{
	
	    $this->layout = "mobile";
		$this->pageTitle = 'Mlsupdate123 Mobile  - Contact Us';
		
		
  	}
	
	 function checkTwoArray($array1,$array2)
			{
				
				for($i=0;$i<sizeof($array1);$i++)
					{
						$check=false;
						
							for($j=0;$j<sizeof($array2);$j++)
								{								
									if($array2[$j]==$array1[$i])
										{											
											$check=true;	
											break;
										}
															
								}
								
								if($check==true)
								break;
						
					}
					
					return $check;
			}
			
			/*function checkTwoNeighborhood($val,$array2)
			{
				$check=false;
				for($i=0;$i<sizeof($array2);$i++)
					{
						if($val==$array2[$i])
						{							
						$check=true;	
						break;
						}
					}
					return $check;
			}*/
			
			function checkTwoNeighborhood($array1,$array2)
			{
				
				for($i=0;$i<sizeof($array1);$i++)
					{
						$check=false;
						
							for($j=0;$j<sizeof($array2);$j++)
								{								
									if($array2[$j]==$array1[$i])
										{											
											$check=true;	
											break;
										}
															
								}
								
								if($check==true)
								break;
						
					}
					
					return $check;
			}
			
			 function getTime($lat, $lng)
			{
				$url = "http://ws.geonames.org/timezone?lat={$lat}&lng={$lng}";
				$timedata = file_get_contents($url);
				$sxml = simplexml_load_string($timedata);
				return $sxml->timezone->time;
			}


 function searchresult($cat_id=null, $city_id=null, $min=null, $max=null)
	 {
                             // Configure::write('debug', 3);
		//url params : $cat_id=null, $city_id=null, $min=null, $max=null

		$this->layout = "mobile";
		$this->pageTitle = 'Mlsupdate123 Mobile - Search Result';

		//$urls = $this->params['url'];

		//$cat_id = $urls['cat_id'];
		//$city_id = $urls['city_id'];
		//$min = $urls['min'];
		//$max = $urls['max'];
		
		$cond = "category_id = $cat_id AND city_id = $city_id AND ( price BETWEEN $min AND $max) ";
		
		$listingDetails = $this->Listing->findAll($cond);
		
        $this->set('listingDetails',$listingDetails);
        $this->set('result_found',count($listingDetails));	
                               //print_r($listingDetails); 
		
	 }
	

			
	 
}
//Array ( [0] => Array ( [Listing] => Array ( [id] => 125 [user_id] => 23 [category_id] => 1 [streetnumber] => [streetname] => [unit] => [city_id] => 1 [state] => NY [zip] => [notexact] => 0 [neighborhood_id] => 1 [title] => [description] => True Two Bedroom with exposed Brick [bed] => [bath] => [sqft] => [sqm] => [area] => SqFt [price] => 2500 [video_url] => [broker_url] => [broker] => 0 [active] => 1 [sold] => 0 [name] => Chris Morley [phone] => 917-655-1920 [text] => 1 [blackberry] => 0 [email] => Chrisnycc@yahoo.com [phoneoffice] => [fax] => [im] => [skype] => [facebook] => [myspace] => [linkedin] => [otherone] => [othertwo] => [otherthree] => [floorplan] => [created] => 2008-08-17 21:58:20 [modified] => 2008-08-17 21:50:03 [availabledate] => 0000-00-00 [is_deleted] => 1 [pet_description] => [price_request] => N ) [City] => Array ( [id] => 1 [city_name] => Manhattan [modifiedon] => 2009-01-05 [createdon] => 2008-12-20 [isdelete] => 0 [isblocked] => 0 ) [Category] => Array ( [id] => 1 [category_name] => Rental Apartment Listings [modifiedon] => 2009-02-11 [createdon] => 0000-00-00 [isdelete] => 0 [isblocked] => 0 ) [User] => Array ( [id] => 23 [fname] => [lname] => [address] => [countrystate] => [city_name] => [location] => [username] => chrisnycc@yahoo.com [email] => chrisnycc@yahoo.com [telephone] => [zip] => [password] => 929064f2a141f812f1c2efb3ff8194ca [isblocked] => 0 [isdelete] => 0 [lastlogindate] => 2008-12-19 08:21:24 [lastloginip] => [mlsID] => 40mo0960475 [adddate] => 1215717056 [approve] => y [broker_url] => [phoneoffice] => [fax] => [im] => [skype] => [facebook] => [myspace] => [gender] => 1 [description] => [cellno] => ) [Listing_counter] => Array ( [id] => 108 [listing_id] => 125 [counter] => 3 [status] => 1 ) [Neighborhoodlisting] => Array ( [id] => 97 [neighborhood_id] => 1 [listing_id] => 125 [0] => Array ( [id] => 97 [neighborhood_id] => 1 [listing_id] => 125 ) ) [Imagelisting] => Array ( [id] => [file] => [listing_id] => ) [Filterlisting] => Array ( [0] => Array ( [id] => 545 [filter_id] => 184 [listing_id] => 125 ) [1] => Array ( [id] => 546 [filter_id] => 182 [listing_id] => 125 ) [2] => Array ( [id] => 547 [filter_id] => 63 [listing_id] => 125 ) [3] => Array ( [id] => 548 [filter_id] => 192 [listing_id] => 125 ) [4] => Array ( [id] => 549 [filter_id] => 23 [listing_id] => 125 ) [5] => Array ( [id] => 550 [filter_id] => 181 [listing_id] => 125 ) [6] => Array ( [id] => 551 [filter_id] => 204 [listing_id] => 125 ) ) ) ) 
?>