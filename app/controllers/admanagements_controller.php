<?
/// <summary>  
 ///****************************************************************************

///Company Name: Navigators Software Private Limited

///Class Name :ContentsController.

///Description  : Manages logic on addition,deletion,editing and  blocking. 

///Created By : Surit Nath.

///Created On : November 19 2008

///------------------------------------------------------------------------------------------------

/// Date         |       Modified By           |           Details

///------------------------------------------------------------------------------------------------

/// 10th December 2008| Sujay Bhattacharya    |  For adding ad from user end. Go to "Sujay's" code portion  

///------------------------------------------------------------------------------------------------ ///****************************************************************************    /// </summary> 
?>
<?php ob_start(); ?>
<?php
class AdmanagementsController extends AppController
{
  var $name = 'Admanagements'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Admanagement','Admanagementphoto','Admin','Sicategory','Siservice','Countrie','State','Sicity','Sianswer','Siquestion');
  var $components = array('Pagination'); 
  var $layout='alluser';
  var $levelId;/////used for recursion in the function categoryLevelCreation for keeping the category in the array
  var $indexCategory=0;/////used for recursion in the function categoryLevelCreation for keeping the category in the array
  var $categoryName;/////used for recursion in the function categoryLevelCreation for keeping the category in the array
  var $catId;
  var $catLevelId;
  var $levelStr='';/////used for recursion in the function categoryLevelCreation for keeping the category in the array
  
  var $catName;//=array();///use in the recursion function
	var $indexCat=0;
  
  function manageadmanagement()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Ad Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isdelete='0'"."order by country asc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 $this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page)); 
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);  
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}   
  }
  
  
  function manageflagge()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Flagge Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isflagged='0' AND isdelete='0'"."order by country asc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	 $this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page)); 
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);  
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}   
  }
  
  
  function adadmanagement()
  {
	$this->layout = "after_adminlogin";
    $this->pageTitle = 'Add New AD';
	//$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  if(!empty($this->data))
        {
			//pr($this->data);
			//exit();
			if($this->Admanagement->findAllBycms_name($this->data['Admanagement']['addtitle']))
			{
			 $this->Session->setFlash('This cms is already exists !');
			 $this->redirect('/admanagements/addadmanagement');
			} 
		  else
		   {
			 
			 //$adate=$this->params['form']['adate'];
			 //$arr=explode('/',$adate);
		     //$activedate=$arr[2]."-".$arr[1]."-".$arr[0];
				$this->data['Admanagement']['modifiedon']=date('Y-m-d');
				$this->data['Admanagement']['createdon']=date('Y-m-d');
				$this->Admanagement->save($this->data);
				$this->Session->setFlash('New AD is successfully added');
				$this->redirect('/admanagements/manageaddmanagement');
            
			}
        }
  }
 function deleteadmanagement($id)
{
	$this->checkSession();  
    $this->Admanagement->del($id);
	$this->Session->setFlash('The AD is successfully Deleted');
           $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}   
}

 function deleteflaggeadmanagement($id)
{
	$this->checkSession();  
    $this->Admanagement->del($id);
	$this->Session->setFlash('The AD is successfully Deleted');
           $controller=$this->params['controller'];
		   $action='manageadmanagement'; 
		  //  pr($controller);
		 //  pr($action);
		   
		 //  exit;  
		   //addmanagements/manageaddmanagement?menu_id=45
		  $this->redirect($controller.'/'.$action);
			
}



 function deleteadmanagementphoto($id)
{
    //echo "pp";
	$this->checkSession();  
   $this->Admanagementphoto->del($id);
	$this->Session->setFlash('The AD is successfully Deleted');
           $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];
		   $sub=$this->params['url']['cid'];
		  // pr($sub);  
		   if(isset($this->params['pass'][2])!='')
			{
			//pr($this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']));
			//$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			 $this->redirect($controller.'/'.$action.'/'.$sub.'?page='.$this->params['url']['page']);
			}   
}


//...................Edit Profile Details...//
  
  function viewimageadd($id)
  {
     $this->layout = "after_adminlogin";
	 //   $this->commonEvent();
       $m=0;
	 /* if($this->params['url']['redirection']!=1)  
	  {
	      $redirect_status=0;
		  
		  foreach($this->params['form']['chkEvent'] as $photopath)
		  {
			$event_id=$this->params['form']['event_id'];
			$club_user_id=$this->params['form']['club_user_id'];
			$clubphoto_id=$this->params['form']['photo_id'][$m];
			$clubalbum_id=$this->params['form']['album_id'];
			
			 $this->Clubeventphoto->execute("insert into clubeventphotos(`clubadmin_id`,`clubalbum_id`,`clubphoto_id`,`clubevent_id`,`clubevent_photopath`)values('$this->club_user_id','$clubalbum_id','$clubphoto_id','$event_id','$photopath')");
			 $redirect_status=1;
			 $m++;
		  }
		   if($redirect_status==1)
		   {
		     $this->Session->setFlash(" $m photo(s) added successfully");
		     $this->redirect("/clubphotos/clubeventviewphoto?redirection=1&id=$this->event_id");
		   }
	    }*/
	  ////////////////////////////Club photo show page////////////////////////////////////////
	 // echo $id;
	  
	   //pr($this->params['url']);
	   $this->set("cid",$id);
	   $eventCondition = array("admanagements_id" => $id);
	   $event_data=$this->Admanagementphoto->findAll($eventCondition);
	   $this->set("event_data",$event_data);
	  
	 ////////////////////////////////////////End////////////////////////////////////////////////////////
	 
	  
  }
  
  
  //.................End Edit profile Details......//
  
 
 //-----------------   start view adds ------------------
   function viewbyads($id = null)
  {
   $this->checkSession();
   $this->pageTitle='Personaluser Manager';
   $this->layout="after_adminlogin";
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   $this->set('user_id',$id);
   $cri=" personalusers_id='$id' and isdelete='0' and isblocked='0'"."order by id desc";
   //pr($cri);
   //exit;
   list($order,$limit,$page) = $this->Pagination->init($cri);
   $this->set('Ad_data',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page)); 
   if(isset($_REQUEST['menu_id']))
		{
			//$_SESSION['status']='';
			//$_SESSION['skey']='';
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
  }
  
  //----------------------  end eds ----------------------


function editcontent($id = null)
{
   
   $this->checkSession();
   $this->layout='after_adminlogin';
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   if(empty($this->data))
    {  
	   $this->Content->id=$id;
	   $this->data = $this->Content->read();
		//$this->set('category', $this->Category->read(null, $id));
		//$this->set('plandetails',$this->Plan->findAll());
	}
    else
    {   
	  	//$this->data['Content']['expiry_date'];
		if ($this->Content->save($this->data['Content']))
	       {
		  // $test=$this->Content->upread();
		 //  pr($test);
		  // pr('xcxcxcxc');
		   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
     	  // $sqldate_update="UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'";
		  // mysql_query($sqldate_update);
		   $controller=$this->params['controller'];
		   $pagenum=$this->params['form']['pagenum'];
		  // $this->Session->setFlash('dfdfdfdfd');		 
		   //echo  $action=$this->params['pass'][1];  
		   //$this->Session->setFlash('The record is successfully edited');
		   $this->redirect($controller.'/managecontent'.'?page='.$pagenum);
		   }
    }
  }
 function viewadmanagement($id=null)
 {
  $this->layout="after_adminlogin";
   $this->pageTitle='View AD';
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   if(!$id)
   {
   		echo $id=$_REQUEST['id'];
        $this->Admanagement->id = $id;
		$this->set('userdata',$this->Admanagement->read(null,id));
   }
   else
   {
	 //$id;
	 $this->Admanagement->id = $id;
     $this->set('userdata',$this->Admanagement->read());
	 }   
}
 function deleteadmanagement1($id)
{   
	$this->checkSession();	
    //$this->Plan->del($id);
	//$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $this->Admanagement->execute("update admanagements  set isdelete='1' where id=".$id);
	 $this->Session->setFlash('The AD is successfully deleted');
           $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if($this->params['pass'][1]!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][1].'&page='.$this->params['url']['page']);
			}
			else
			{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}

    //$this->flash('The user with ID: '.$id.' has been deleted.', '/Plans/manageuser');
	//$this->redirect("/Plans/manageuser");
}
function blockadmanagement($id=NULL)
	{
	$this->layout = "after_adminlogin";
	$this->Admanagement->execute("update admanagements  set isblocked='1' where id=".$id);	 
	$this->Session->setFlash('This AD is successfully blocked');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			//$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'?page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
function unblockadmanagement($id=NULL)
	{
	//$this->Plan->saveField('isblocked','0');
	 $this->Admanagement->execute("update admanagements  set isblocked='0' where id=".$id);
	 $this->Session->setFlash('This AD is successfully Unblocked');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];
		   		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	//$this->redirect('/Plans/manageuser');	
   } 
   
   
  function flaggadmanagement($id=NULL)
	{
	$this->layout = "after_adminlogin";
	$this->Admanagement->execute("update admanagements  set isflagged='1' where id=".$id);	 
	$this->Session->setFlash('This AD is successfully flagged');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			//$this->redirect($controller.'/'.$action.'/'.$this->params['pass'][2].'?page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
	
	
function unflaggadmanagement($id=NULL)
	{
	//$this->Plan->saveField('isblocked','0');
	 $this->Admanagement->execute("update admanagements  set isflagged='0' where id=".$id);
	 $this->Session->setFlash('This AD is successfully Unflagged');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];
		   		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	//$this->redirect('/Plans/manageuser');	
   } 	 
function activeadmanagement($page=1)
 { 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active AD';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  $cri="isdelete='0' AND isblocked='0'";
		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  $this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));   
}	 	 
//==============================================================
//Used to display/view the list of inactive users of Plan
//============================================================== 	 
function inactiveadmanagement($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive AD';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	 
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
  }
 function blockall()
	 {
	             //pr($this->params['form']);
	 			foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Admanagement->execute("update admanagements  set isblocked='1' where id=".$id);
				 }
				// pr("sdsds");
				   pr($this->params);
				  $controller=$this->params['controller'];
				  $action=$this->params['form']['referer'];
				  pr($action);
				exit;
				 $this->Session->setFlash('Record(s) successfully blocked');				 
				 //$this->redirect('addmanagements/manageaddmanagement?menu_id=45');
				  if($this->params['form']['key']!='')
				  {
				  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				  }
				  else
				  {
				  $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				  }
				 
	   }	 
//========================================================
//Used to unblock multiple records of Plan
//======================================================== 
function unblockall()
	 {
	  //pr($this->params['form']);
	             $this->Session->setFlash('hhh');
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Admanagement->execute("update admanagements  set isblocked='0' where id=".$id);
				 }
				  $this->Session->setFlash('Record(s) successfully unblocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 pr($this->params['form']['referer']);
				 pr($this->params['form']);
				 exit;
				 if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
//========================================================
//Used to delete all records  of Plan
//======================================================== 	
	 
function deleteall()
	 {
	 
	 		    foreach($this->params['form']['chkUser'] as $id)
			     {
				   $this->Addcontent->del($id);
				   //$this->Plan->execute("update plans  set isdelete='1' where id=".$id);
				 }
				  $this->Session->setFlash('AD(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
   	} 
 function searchbyname($page=1)
 {
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'AD Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			//print "1234";
			//$searchkey=$this->params['form']['srchkey']."%"; 
			$cri="isdelete='0' AND addtitle like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="pid") && isset($this->params['form']['srchkey']))
		  {
			//$searchkey=$this->params['form']['srchkey']."%"; 
			//$searchkey=$this->params['form']['srchkey'];
			$cri="isdelete='0' AND id like '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		     }
		/*if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			//$searchkey=$this->params['form']['srchkey']."%"; 
			//$searchkey=$this->params['form']['srchkey'];
			$cri="isdelete='0' AND id like '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Addmanagement->findAll($cri,NULL, NULL, $limit, $page));  
		     }	*/ 
		if(($searchby=="position") && isset($this->params['form']['srchkey']))
		  {
			//$searchkey=$this->params['form']['srchkey']."%"; 
			//$searchkey=$this->params['form']['srchkey'];
			$cri="isdelete='0' AND admin_role like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		}
		if(isset($this->params['form']['countrysrchkey']))
		   {
		      $countrysrchkey=$this->params['form']['countrysrchkey'];
		   }
		else
			{
		     $countrysrchkey="";
			}
			// pr($countrysrchkey);
		if(($searchby=="ptitle") && isset($countrysrchkey))
		  {
		   	$cri="isdelete='0' AND country LIKE '".$countrysrchkey."%' ";
			//pr("asas");
			//pr($cri);
			// pr($this->params);
		    list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		  $admininfo=$this->Admin->admin_permission(); 
	      $this->set('result_check',$admininfo);  
	     	
  }

  
 function editactivecontent($page=1)
   {
	     $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit Active CMS';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
			   if(empty($this->data))
				{
					
					//pr($this->params);
					
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Content->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   //$adate=$this->params['form']['adate'];
							   //$arr=explode('/',$adate);
							   //$activedate=$arr[2]."-".$arr[1]."-".$arr[0];
							   //$this->data['Member']['expiry_date'];//=$activedate;
							   
							   $this->Content->id=$this->data['Content']['id'];
							   $this->Content->save($this->data);
							   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
							   //$sqldate_update="UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'";
				               //mysql_query($sqldate_update);
							   }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
						//$this->redirect('admins/manageadmin');
					 // if(isset($this->params['pass'][1]) && ($this->params['pass'][1]!=NULL))
					   if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							  }
						}
				}
 }  
 
function editinactiveadmanagement($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive AD';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
	               
			   if(empty($this->data))
				{
					//pr($this->params);
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Admanagement->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Admanagement->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Admanagement->id=$this->data['Admanagement']['id'];
							   $this->Admanagement->save($this->data);
							   $this->Admanagement->execute("UPDATE `admanagements` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Admanagement']['id']."'");
							  // $sqldate_update="UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'";
					          //mysql_query($sqldate_update);
							   
			           }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
					     if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
				}
	   }

// -------   searchbyinactive addmanagement ------------------	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
		if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		 else
		    {
		      $searchkey="";
			} 
			
		if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
			 $this->set('searchby',$searchby);
		   }
		else
		   {
		     $searchby="";	
			}  
		if(($searchby=="pname") && isset($searchkey))
		  {	
				//$this->pageTitle='Edit Admin';
				$cri="isdelete='0' AND isblocked='1' AND addtitle LIKE '".$searchkey."%'";
				//pr($cri);
				//$this->set('skey',$this->params['url']['skey']);
				$cond=0;
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
		  
		  }
		if(($searchby=="pid") && isset($searchkey))
		  {	
				//$this->pageTitle='Edit Admin';
				$cri="isdelete='0' AND isblocked='1' AND id LIKE '".$searchkey."%'";
				//pr($cri);
				//$this->set('skey',$this->params['url']['skey']);
				$cond=0;
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
		  
		  } 
		if(isset($this->params['form']['countrysrchkey']))
		   {
		     
		    $countrysrchkey=$this->params['form']['countrysrchkey'];
		   }
		else
		   { 
		     $countrysrchkey="";
			} 
			// pr($countrysrchkey);
		if(($searchby=="ptitle") && isset($countrysrchkey))
		  {
		   
			$cri="isdelete='0'  AND isblocked='1' AND country LIKE '".$countrysrchkey."%' ";
			//pr("asas");
			//pr($cri);
			// pr($this->params);
		   	list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		}
	 }	
//----------------------  end ---------------------------------------------------

//................. starts by search by name active ....................................................

function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
		if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		 else
		    {
		      $searchkey="";
			} 
			
		if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
			 $this->set('searchby',$searchby);
		   }
		else
		   {
		     $searchby="";	
			}  
		if(($searchby=="pname") && isset($searchkey))
		  {	
				//$this->pageTitle='Edit Admin';
				$cri="isdelete='0' AND isblocked='0' AND addtitle LIKE '".$searchkey."%'";
				//pr($cri);
				//$this->set('skey',$this->params['url']['skey']);
				$cond=0;
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
		  
		  }
		if(($searchby=="pid") && isset($searchkey))
		  {	
				//$this->pageTitle='Edit Admin';
				$cri="isdelete='0' AND isblocked='0' AND id LIKE '".$searchkey."%'";
				//pr($cri);
				//$this->set('skey',$this->params['url']['skey']);
				$cond=0;
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
		  
		  } 
		if(isset($this->params['form']['countrysrchkey']))
		   {
		     
		    $countrysrchkey=$this->params['form']['countrysrchkey'];
		   }
		else
		   { 
		     $countrysrchkey="";
			} 
			// pr($countrysrchkey);
		if(($searchby=="ptitle") && isset($countrysrchkey))
		  {
		   
			$cri="isdelete='0'  AND isblocked='0'  AND country LIKE '".$countrysrchkey."%' ";
			//pr("asas");
			//pr($cri);
			// pr($this->params);
		   	list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		}
	 }	

////................. end  by search by name active ....................................................


//....................................................................................................Search By Name Start
 	 function searchbyname1($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'AD Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		  else
		   	{
		     $searchby="";
			}
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND addtitle like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		 if(($searchby=="pid") && isset($searchkey))
		  {
			$cri="isdelete='0' AND id like '".$searchkey."%'";
			
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		 if(isset($this->params['form']['countrysrchkey']))
		   {
		     
		    $countrysrchkey=$this->params['form']['countrysrchkey'];
		   }
		 else
		 	{
		     $countrysrchkey="";
			 }
			
		if(($searchby=="ptitle") && isset($countrysrchkey))
		  {
		   
			$cri="isdelete='0' AND country LIKE '".$countrysrchkey."%' ";
			//pr($cri);
			// pr($this->params);
		    
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Admanagement->findAll($cri,NULL, NULL, $limit, $page));  
		}
		
  }
 //.....................................................................................................Search By Name End
//......................................................................................................Search By Option Active start
 	function srchbyoptionactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND cms_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //........................................................................................................Search By Option Active end
   //......................................................................................................Search By Option Active start
 	function srchbyoptioninactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="isdelete='0' AND cms_name LIKE '".$searchkey."%'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //...................................Search By Option Active end
 
 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'thankster.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Admanagement->find($cri));
   }
 //---------------end of page view-----------------  
 
 ////////////////////////End of Surit's Code////////////////////////////////////////////////
 
 
 ////////////////////Start of Sujay's Code/////////////////////////////////////////////////
 
 function selectcategory()
 {
 
    $this->layout="after_userlogin";
	$this->pageTitle = 'Ad Management';
	//////Have to determine userdid/////
	
	///////End/////////////////////////
	
	
	/////////////To show the parent category///////////////////
	$cri=" is_delete='0' and parent_id='0' order by id asc";
    $sicategory=$this->Sicategory->findAll($cri);
	$this->set('sicategory',$sicategory);
	////////////End///////////////////////////////
	
	if(isset($this->params['form'])) {
	     $this->Session->write('sessioncategoryId', $this->params['form']['categoryIdValue']);
		 $this->Session->write('sessionserviceId', $this->params['form']['serviceId']);
		 $this->Session->write('sessionlevelId', $this->params['form']['categorylevel']);
		 $this->Session->write('selectedCategory', $this->params['form']['selectedCategory']);
		 $this->redirect("/admanagements/adinformation?action=adinformation");
	
	} else {
	   if(isset($this->params['url']['action'])) {
	    if($this->params['url']['action']=='back') {
			$sessioncategoryId=$this->Session->read('sessioncategoryId');
			$sessionserviceId=$this->Session->read('sessionserviceId');
			$sessionlevelId=$this->Session->read('sessionlevelId');
			$selectedCategory=$this->Session->read('selectedCategory');
			$this->categoryLevelCreation($sessioncategoryId,$sessionserviceId,$sessionlevelId);///This function to form an array of category according to level. It will used when we back from adinformation page to show selected category and service/////////
			
			$this->serviceSelection($sessioncategoryId,$sessionserviceId);///This function needed to show service for the given category id/////////
			
			$recurse=$this->recursion($sessioncategoryId);
			$countCat=count($this->catName);
			$toparrow='';
			 if($countCat>0) {
				$cntCat=$countCat-1;
				while($cntCat>=0)
				{
				   if($cntCat==0) {
				   
					  $toparrow.="".$this->catName[$cntCat]."&nbsp;&raquo;";
				   
				   } else {
				   
					  $toparrow.=$this->catName[$cntCat]."&nbsp;&raquo;";
				   }
				   
				  $cntCat--;
				   
				}
			}
			$this->set('toparrow',$toparrow);
			$this->set('categoryName',$this->categoryName);
			$this->set('selectedCategory',$selectedCategory);
			$this->set('catId',$this->catId);
			$this->set('catLevelId',$this->catLevelId);
			$this->set('cntLevel',count($this->catLevelId));
			$this->set('action','back');
			
		} 
	   }
	}
	
 
 }
 
 function categoryLevelCreation($sessioncategoryId=0,$sessionserviceId=0,$sessionlevelId=0)
 {
        $cri_service=" `is_delete`='0' and `id`='$sessioncategoryId'";
        $sicategory_service=$this->Sicategory->find($cri_service);
		
		$count_service=count($sicategory_service);
		$this->levelId=$sessionlevelId;
		$this->levelStr.=$sessionlevelId.',';////This string will be input in hidden field adinformation 1st page ie selectcategory/////
		
		if($count_service>0)
		{
			if($sicategory_service['Sicategory']['parent_id']!=0)
			{
			 $cri_parent="`is_delete`='0' and `parent_id`='".$sicategory_service['Sicategory']['parent_id']."'";
			 $sicategory1_service=$this->Sicategory->findAll($cri_parent);
			 $index=0;
			 foreach($sicategory1_service as $sicat) {
			 
				 $this->categoryName[$this->levelId][$index]=$sicat['Sicategory']['category_name'];
				
				 $this->catId[$this->levelId][$index]=$sicat['Sicategory']['id'];
				 $this->catLevelId[$this->levelId][$index]=$sicat['Sicategory']['silevelnumber'];
				 $index++;
				
			  }
			
			   $this->levelId=$this->levelId-1;
			   
			  $this->set('levelStr',$this->levelStr);
			  if($this->levelId!=1) {///This condition is applied as 1st level is alwayas in display in ad 1st page///
			    $this->categoryLevelCreation($sicategory_service['Sicategory']['parent_id'],$sessionserviceId,$this->levelId);
			  } else {
			     return 0;
			  }
			}
			else
			{
			
			  return 0;
			}
		}
 }
 function serviceSelection($sessioncategoryId=0,$sessionserviceId=0)
 {
     
     /////////////To select service///////////////////
	$cri=" is_delete='0' and `is_active`='1' and sicategory_id='$sessioncategoryId' order by id asc";
    $siservice=$this->Siservice->findAll($cri);
	$this->set('siservice',$siservice);
	//pr($siservice);
	$str='';
	$k=0;
	foreach($siservice as $serv) {
	  
	  if($serv['Siservice']['id']==$sessionserviceId) {
	      $selected="checked=checked";
	   } else {
	       $selected="";
	   }
	   $str.="<input name='serviceId' type='radio' id='serviceId[$k]' value='".$serv['Siservice']['id']."' ".$selected." /><span class='singer-name'>".$serv['Siservice']['service_name']."</span>&nbsp;&nbsp;&nbsp;&nbsp;";
	    $k++;
	}
	$this->set('serviceStr',$str);
	////////////End///////////////////////////////
 }
 function recursion($categoryId)
	 {
	    $cri=" `is_delete`='0' and `id`='$categoryId'";
        $sicategory=$this->Sicategory->findAll($cri);
		$count=count($sicategory);
		//pr($sicategory);
		if($count>0)
		{
			if($sicategory[0]['Sicategory']['parent_id']!=0)
			{
				$cri_parent="`is_delete`='0' and `id`='".$sicategory[0]['Sicategory']['id']."'";
				$sicategory1=$this->Sicategory->findAll($cri_parent);
				$this->catName[$this->indexCat]=$sicategory1[0]['Sicategory']['category_name'];
				//$this->catId[$this->indexCat]=$sicategory1[0]['Sicategory']['id'];
				$this->indexCat++;
				$this->recursion($sicategory1[0]['Sicategory']['parent_id']);
			}
			else
			{
			
			   $this->catName[$this->indexCat]=$sicategory[0]['Sicategory']['category_name'];
			   //$this->catId[$this->indexCat]=$sicategory[0]['Sicategory']['id'];
			   return $this->catName;
			}
		}
		
		
	 }
	 
 function preview()
 {
    $this->layout="after_userlogin";
	$this->pageTitle = 'Preview Ad';
     
 }
 
 function adinformation()
 {
      $this->layout="after_userlogin";
	  $this->pageTitle = 'Preview Ad';
	 
	       $recurse=$this->recursion($_SESSION['sessioncategoryId']);
		   $countCat=count($this->catName);
			$toparrow='';
			 if($countCat>0) {
				$cntCat=$countCat-1;
				while($cntCat>=0)
				{
				   if($cntCat==0) {
				   
					  $toparrow.="".$this->catName[$cntCat];
				   
				   } else {
				   
					  $toparrow.=$this->catName[$cntCat]."&nbsp;&raquo;";
				   }
				   
				  $cntCat--;
				   
				}
			}
		    $this->set('toparrow',$toparrow);
			$sessioncategoryId=$this->Session->read('sessioncategoryId');
			$sessionserviceId=$this->Session->read('sessionserviceId');
			$sessionlevelId=$this->Session->read('sessionlevelId');
			$selectedCategory=$this->Session->read('selectedCategory');
			
			  /////////////To select service///////////////////
			$cri=" is_delete='0' and `is_active`='1' and id='$sessionserviceId' order by id asc";
			$siservice=$this->Siservice->find($cri);
			$this->set('siservice',$siservice);
			////////////Code for showing question and answers in the ad information page//////////////
			//$question_array=array();
			$arrayIndex=0;
			foreach($siservice['Siquestion'] as $siquestion) {
			    $question_array['question'][$arrayIndex]=$siquestion['question'];
				if($siquestion['question_type']=='checkbox' || $siquestion['question_type']=='dropdown' || $siquestion['question_type']=='radio')                {
				  
				  $criAns=" id='".$siquestion['id']."' order by id asc";
				  $sianswer=$this->Siquestion->find($criAns);
				  if($siquestion['question_type']=='dropdown') {
				     $string="<select name=".$arrayIndex."Ans0 id=".$arrayIndex."Ans0><option value=0>--Select Answer--</option>";
				  }
				  for($k=0;$k<count($sianswer['Sianswer']);$k++) {
				      if($siquestion['question_type']=='dropdown') {
					       $string.="<option value=".$sianswer['Sianswer'][$k]['answer'].">".$sianswer['Sianswer'][$k]['answer']."</option>";
					   } else {
					        $string.="<input type=".$siquestion['question_type']." name=".$arrayIndex."Ans".$k." id=".$arrayIndex."Ans".$k." value=".$sianswer['Sianswer'][$k]['answer'].">&nbsp;".$sianswer['Sianswer'][$k]['answer']."&nbsp;&nbsp;";
					   }
				     $question_array['ans'][$arrayIndex][$k]=$sianswer['Sianswer'][$k]['answer'];
				  }
				  
				  if($siquestion['question_type']=='dropdown') {
				     $string.="</select>";
				  }
				  
				} else {
				     $question_array['ans'][$arrayIndex]='';
					 $string="<input type='text' name='".$arrayIndex."Ans0' id='".$arrayIndex."Ans0'>";
				}
			    //pr($answer_array);
				$question_array['state'][$arrayIndex]=$siquestion['question_type'];
				
				$question_array['AnswerString'][$arrayIndex]=$string;
				$string='';
			    $arrayIndex++;
			}
			if(!empty($question_array)) {
			   $this->set('questionAnswer',$question_array);
			   $countQuestionAns=count($question_array['question']);
		       $this->set('countQuestionAns',$countQuestionAns);
		    } else {
			    $this->set('questionAnswer','');
			   //$countQuestionAns=count($question_array['question']);
		       $this->set('countQuestionAns','0');
			}
			 
			 echo ROOT;
			 echo "<br>";
			 echo BASE_NAME;
			 $server_path=BASE_NAME;
			
			  $absolute_path=ROOT."/app/webroot/img/photo/";
			  $this->Session->write('server_path', $server_path);
			 $this->Session->write('absolute_path', $absolute_path);
			//////////////End of the question and answer page//////////////////////////////////////
			
			/////////////End of selection  of service/////////////////////
			
			//////////////For Country Dropdown//////////////////////
			$countrie=$this->Countrie->findAll();
			$this->set('countrie',$countrie);
			//pr($countrie);
			//////////////End of the country Dropdown///////////
			
			//////////////For state Dropdown//////////////////////
			//$state=$this->State->findAll();
			//$this->set('state',$state);
			//pr($countrie);
			//////////////End of the state Dropdown///////////
			
			//////////////For city Dropdown//////////////////////
			//$city=$this->Sicity->findAll();
			//$this->set('city',$city);
			//pr($countrie);
			//////////////End of the city Dropdown///////////
 
 }
 
 
 function subCategory($parentid=0)
 {
   $parentid=$_REQUEST['parentid'];
	/////////////To show the parent category///////////////////
	$cri=" is_delete='0' and parent_id='".$parentid."' order by id asc";
    $sicategory=$this->Sicategory->findAll($cri);
	$this->set('sicategory',$sicategory);
	////////////End///////////////////////////////
	$this->render('updateselectcategories','Ajax');
 }
 
 function uploader()
 {
    $this->layout="uploader_userlogin";
	$this->pageTitle = 'Preview Ad';
 }
 /////////////////End of Sujay's code portion////////////////////////////////////////////
 
 }
?>