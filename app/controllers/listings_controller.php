<?php
ob_start();
//////
/////**** This section is Totaly used for managing different tasks of administrator ******////////////
class ListingsController extends AppController
{
	var $name = 'Listings'; 
	var $helpers = array('Html','javascript','pagination','Ajax'); 
	var $uses=array('Listing','Neighborhood','Filter','Category','City','Imagelisting','Filterlisting','User','Neighborhoodlisting','Listing_counter','Adminmainmenu','Admin','User_ad','Metatag','Statistics','Advertise');
	var $components = array('Pagination','Email','Currency_conversion','UserAgent');
    //var $layout='alluser';
	
	//.......Start page................
   function index($category_id=NULL)
 	{
		//$this->chk_controller();
		//print_r($this->params);exit;
		
        	
		$this->checkUser();
		
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';	
        	
		
		$this->cityData('city','no');

		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('category_name','id');
		
		$order_by = "id asc"; 
		
		$category_data = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);    
		//print_r($category_data);
		/*if($this->checkLang() != 'en')
		{
		 $category_data = $this->changeLang('1',"Category","category_name",$category_data);
		}*/
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $category_data = $this->changeLang('1',"Category","category_name",$category_data);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($category_data as $key=>$value)
                        {
                                   $category_data[$key]['Category']['category_name'] = $this->changeLang2($category_data[$key]['Category']['category_name']); 
                        }
                    
                }
                
		//print_r($category_data);exit;
		$this->set('categorydata',$category_data);	 
		
		if($category_id==NULL)
		 {
		 	$catID = $category_data[0]['Category']['id'];
		 }
		 else
		 {
			 $catID = $category_id;			 
		 }
			
		$this->set('selectCategory',$catID);	 
		
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Filter.require','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc,Filter.id asc";     
	 $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
	 //print_r($filterdata);exit;
	/*if($this->checkLang() != 'en')
	{
	 $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
	 $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
	}*/
        
        if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($filterdata as $key=>$value)
                        {
                                  $filterdata[$key]['Filter']['filter_name'] = $this->changeLang2($filterdata[$key]['Filter']['filter_name']); 
                                  $filterdata[$key]['Categoryfilter']['category_filter_name'] = $this->changeLang2($filterdata[$key]['Categoryfilter']['category_filter_name']);  
                        }
                    
                }
        
        
	 $this->set('filterdata',$filterdata);	
	 
	 
	 $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc"; 
		    
		$citydata = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{

                        $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($citydata as $key=>$value)
                        {
                                   $citydata[$key]['City']['city_name'] = $this->changeLang2($citydata[$key]['City']['city_name']); 
                        }
                    
                }
		
		$this->set('citydata',$citydata);	
		
		$condition = "isdelete='0' and isblocked= 0 and city_id = ".$citydata[0]['City']['id'];
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "neighborhood_name asc";     
		
		$neighborhooddata = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		/*if(!empty($neighborHood) && $this->checkLang() != 'en')
		{
		 $neighborHood = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborHood);
		}*/
                
                 if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $neighborhooddata = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborhooddata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($neighborhooddata as $key=>$value)
                        {
                                   $neighborhooddata[$key]['Neighborhood']['neighborhood_name'] = $this->changeLang2($neighborhooddata[$key]['Neighborhood']['neighborhood_name']); 
                        }
                    
                }
                
		$this->set('neighborHood',$neighborhooddata);
		
		$condition = "isdelete='0' and isblocked='0' and User.id = ".$_SESSION['userId'];			
		
		$this->set('user_data',$this->User->seletAlluser($condition,NULL,NULL));
			
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		if(!empty($this->data))
		 {
		 //print_r($this->data);exit;
		 	
		 	
		    $this->data['Listing']['price'] = str_replace(",", "", $this->data['Listing']['price']);
			$this->data['Listing']['price'] = str_replace("$", "", $this->data['Listing']['price']);
			
			 $video_url = str_replace("http://", "", $this->data['Listing']['video_url']);			
			 $this->data['Listing']['video_url'] = str_replace("watch?v=", "v/",  $video_url);
			 
				if(isset($this->data['Listing']['broker_url']))
				$this->data['Listing']['broker_url'] = str_replace("http://", "", $this->data['Listing']['broker_url']);
				if(isset($this->data['Listing']['facebook']))
				$this->data['Listing']['facebook'] = str_replace("http://", "", $this->data['Listing']['facebook']);
				if(isset($this->data['Listing']['myspace']))
				$this->data['Listing']['myspace'] = str_replace("http://", "", $this->data['Listing']['myspace']);
				if(isset($this->data['Listing']['linkedin']))
				$this->data['Listing']['linkedin'] = str_replace("http://", "", $this->data['Listing']['linkedin']);
				if(isset($this->data['Listing']['otherone']))
				$this->data['Listing']['otherone'] = str_replace("http://", "", $this->data['Listing']['otherone']);
				if(isset($this->data['Listing']['othertwo']))
				$this->data['Listing']['othertwo'] = str_replace("http://", "", $this->data['Listing']['othertwo']);
				if(isset($this->data['Listing']['otherthree']))
				$this->data['Listing']['otherthree'] = str_replace("http://", "", $this->data['Listing']['otherthree']);
				
				if(isset($this->data['Listing']['smonth']) && isset($this->data['Listing']['sday']) && isset($this->data['Listing']['syear']))
		$this->data['Listing']['availabledate'] = $this->data['Listing']['syear'].'-'.$this->data['Listing']['smonth'].'-'.$this->data['Listing']['sday'];
			$this->data['Listing']['active'] = 1;
			
			if($this->Listing->save($this->data))
			{				
				 $this->Session->setFlash('Listing has been created and is now active.');
				$listing_id = mysql_insert_id();				
			}
			
			$this->data['Filterlisting']['listing_id'] = $listing_id;
			
			
			if(isset($this->data['Neighborhoodlisting']['neighborhood_id'])) {
			foreach($this->data['Neighborhoodlisting']['neighborhood_id'] as $value)
				{
					$this->Neighborhoodlisting->saveNeighborhood($value,$listing_id);
				}
			}
			
			if(isset($this->data['Filterlisting']['filterid'])) {
			foreach($this->data['Filterlisting']['filterid'] as $value)
				{
					$this->Filterlisting->saveFilter($value,$listing_id);
				}
			}
			
			
				
			foreach($this->data['Imagelisting'] as $key=> $value)
				{
				
					
					if(!empty($value['name']) && $value['type']=='image/jpeg')
					{
						//if($value['size'] <2097152)
						//{
						$fileName = mt_rand().'_'.$value['name'];
						
						$filestatus = move_uploaded_file($value['tmp_name'],$file_path."/".$fileName);			
						
						$source_fileName = $file_path."/".$fileName;						
						
						$thumb = $file_path."/thumb/".$fileName;
						$this->make_thumb($source_fileName,$thumb,300,200);
						$this->Imagelisting->insertImage($fileName,$listing_id);
						//}
					}				
					
				}	
			
		}				
 		$this->set('advertise',$this->siteAdd());
		$this->set('type','add');
  	}
	function manageadscount()
	{
		 $this->checkSession();
		 $this->layout = "after_adminlogin";
		 $this->pageTitle = 'Ads Count Management';
		// $this->uses=array('City','Adminmainmenu','Admin','Listing');
		 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		 $condition = "";
		
		$filelds = array('Listing.id','Listing.title','Listing_counter.counter');
		
		$order_by = "Listing_counter.counter desc"; 
		   
		 list($order,$limit,$page) = $this->Pagination->init($condition);
		 $userdata = $this->Listing->allListing($condition,NULL, $order_by, $limit, $page);
		 
		
		 $this->set('userdata',$userdata); 
		 if(isset($_REQUEST['menu_id']))
			{
				$_SESSION['menu_id']=$_REQUEST['menu_id'];
			}  
		 $admininfo=$this->Admin->admin_permission(); 
		 $this->set('result_check',$admininfo);	
	}
	function editmetatag($id=NULL)
	{
	  //print_r($this->data);exit;
	   $this->checkSession();
	   $this->layout = "after_adminlogin";
	   $this->pageTitle = 'Metatag Management';	
	   //$this->cityData('city');
	   if(!empty($this->data))
	   {
			if($this->Metatag->save($this->data))
			{	
			
				if(isset($this->data['Metatag']['id']))
				{
				 $listing_id = $this->data['Metatag']['id'];
				 $this->Session->setFlash('Metatag has been updated.');
				 }
				 else	
				 {
					 $this->Session->setFlash('Metatag has been created and is now active.');
				 
				 }
				
			}
		$this->redirect('/listings/managemetatags');	
	   }
	   else
	   {
	     $this->data = $this->Metatag->read(null, $id);
	   }	
	}
	function managemetatags()
	{
		 $this->checkSession();
		 $this->layout = "after_adminlogin";
		 $this->pageTitle = 'Metatag Management';
		// $this->uses=array('City','Adminmainmenu','Admin','Listing');
		 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		 $condition = "";
		
		$filelds = array('Metatag.id','Metatag.title','Metatag.keyword','Metatag.description');
		
		$order_by = "Metatag.id ASC"; 
		   
		 list($order,$limit,$page) = $this->Pagination->init($condition);
		 $userdata = $this->Metatag->findAll($condition,NULL, $order_by, $limit, $page);
		 
		
		 $this->set('userdata',$userdata); 
		 if(isset($_REQUEST['menu_id']))
			{
				$_SESSION['menu_id']=$_REQUEST['menu_id'];
			}  
		 $admininfo=$this->Admin->admin_permission(); 
		 $this->set('result_check',$admininfo);	
	}
function searchbyname($page=1,$searchby=NULL, $searchkey=NULL)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'User Management';
		 // print_r($this->params);
		  //exit;		  
		  if(isset($this->params['form']['srchkey'])) 
		  		{ 				    
		             $searchkey=trim($this->params['form']['srchkey']);
					 $this->set('searchkey',$searchkey);
			     }
			
		 
			else
		    {			
				$this->set('searchkey',$searchkey);
				
					$condition = "";
					
					$filelds = array('Listing.id','Listing.title','Listing_counter.counter');
					
					$order_by = "Listing_counter.counter desc"; 
					
					list($order,$limit,$page) = $this->Pagination->init($condition);
					$userdata = $this->Listing->allListing($condition,NULL, $order_by, $limit, $page);
			}
			  
				
		 if(isset($this->params['form']['searchby']))
		   {
				$searchby=$this->params['form']['searchby'];
				$this->set('searchby',$searchby);
		   }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
				$searchkey = substr($searchkey,2);
				$condition ='Listing.id = '.$searchkey;
				
				$filelds = array('Listing.id','Listing.title','Listing_counter.counter');
				
				$order_by = "Listing_counter.counter desc"; 
				
				list($order,$limit,$page) = $this->Pagination->init($condition);
				$userdata = $this->Listing->allListing($condition,NULL, $order_by, $limit, $page);
		 }
		 $this->set('userdata',$userdata); 
		$this->render('manageadscount');
		
  }	
	
	function addlisting($type=NULL)
 	{
		
		$this->checkUser();
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Straightforwardlistings';	
		
		
              //print_r($this->data); exit;
                
		$typeArr = array('0'=>'image/jpeg',
				 '1'=>'image/jpg',
				 '2'=>'image/gif',
				 '3'=>'image/png',
				 '4'=>'image/pjpeg',
				 '5'=>'image/pjpg',
				 '6'=>'image/pgif',
				 '7'=>'image/ppng');
		 $file_path = getcwd()."/uploadimage";	
		
		if(!empty($this->data))
		 {
		 
			if($this->data['Listing']['price1']=='N')
			{
				$this->data['Listing']['price'] = str_replace(",", "", $this->data['Listing']['price']);
				$this->data['Listing']['price'] = str_replace("$", "", $this->data['Listing']['price']);
				$this->data['Listing']['price_request'] = 'N';
			}
			else
			{
			$this->data['Listing']['price'] = '';
			$this->data['Listing']['price_request'] = 'Y';
			}
			 $video_url = str_replace("http://", "", $this->data['Listing']['video_url']);			
			 $this->data['Listing']['video_url'] = str_replace("watch?v=", "v/",  $video_url);
			 
				if(isset($this->data['Listing']['broker_url']))
				$this->data['Listing']['broker_url'] = str_replace("http://", "", $this->data['Listing']['broker_url']);
				if(isset($this->data['Listing']['facebook']))
				$this->data['Listing']['facebook'] = str_replace("http://", "", $this->data['Listing']['facebook']);
				if(isset($this->data['Listing']['myspace']))
				$this->data['Listing']['myspace'] = str_replace("http://", "", $this->data['Listing']['myspace']);
				if(isset($this->data['Listing']['linkedin']))
				$this->data['Listing']['linkedin'] = str_replace("http://", "", $this->data['Listing']['linkedin']);
				if(isset($this->data['Listing']['otherone']))
				$this->data['Listing']['otherone'] = str_replace("http://", "", $this->data['Listing']['otherone']);
				if(isset($this->data['Listing']['othertwo']))
				$this->data['Listing']['othertwo'] = str_replace("http://", "", $this->data['Listing']['othertwo']);
				if(isset($this->data['Listing']['otherthree']))
				$this->data['Listing']['otherthree'] = str_replace("http://", "", $this->data['Listing']['otherthree']);
				
				if(isset($this->data['Listing']['smonth']) && isset($this->data['Listing']['sday']) && isset($this->data['Listing']['syear']))
		        $this->data['Listing']['availabledate'] = $this->data['Listing']['syear'].'-'.$this->data['Listing']['smonth'].'-'.$this->data['Listing']['sday'];
			    $this->data['Listing']['active'] = 1;
			
			
                        
			
			///////////////////Upload Floor Plan /////////////////////
                        
                       
			
			$this->data['Listing']['floorplan'] ='';
			if(isset($this->data['Listings']['PhotoT']))
				{
				
				if($this->data['Listings']['PhotoT']=='Change_Photo') {
				unlink(getcwd()."/uploadimage/thumb/".$this->data['Listings']['hiddenname']);

				unlink(getcwd()."/uploadimage/".$this->data['Listings']['hiddenname']);
				
				if(!empty($this->data['Listings']['floorplan']['name']) && in_array(strtolower($this->data['Listings']['floorplan']['type']),$typeArr))
					{
						//print_r($value);exit;	
						//if(($this->data['LisListingsting']['floorplan']['size']) <2097152)
						//{
							$fileName = mt_rand().'_'.$this->data['Listings']['floorplan']['name'];
							
							$filestatus = move_uploaded_file($this->data['Listings']['floorplan']['tmp_name'],$file_path."/".$fileName);			
							//exit;
							$source_fileName = $file_path."/".$fileName;						
							
							$thumb = $file_path."/thumb/".$fileName;
							$this->make_thumb($source_fileName,$thumb,150,100);
							$this->data['Listing']['floorplan'] = $fileName;
							
						//}
					}
			
                                }
				
				
				
				elseif($this->data['Listings']['PhotoT']=='Remove_Photo') {
				
				unlink(getcwd()."/uploadimage/thumb/".$this->data['Listings']['hiddenname']);
				unlink(getcwd()."/uploadimage/".$this->data['Listings']['hiddenname']);
				$this->data['Listing']['floorplan'] ='';
				}
                                elseif($this->data['Listings']['PhotoT']=='Keep_Photo')
                                {                                        
                                        
                                  $this->data['Listing']['floorplan'] =$this->data['Listings']['hiddenname'];
                                  
                                 
                                }
				
				} else {
                                        
                                       
				//echo $file_path1 = getcwd()."/uploadimage/floorplan/";
				//floorplan
				//$file_path//E:\xampp\htdocs\straightforwardlistings\app\webroot/uploadimage
				//print_r($this->data);exit;
				if(!empty($this->data['Listings']['floorplan']['name']) && in_array(strtolower($this->data['Listings']['floorplan']['type']),$typeArr))
					{
						
						//if(($this->data['Listings']['floorplan']['size']) <2097152)
						//{
							$fileName = mt_rand().'_'.$this->data['Listings']['floorplan']['name'];
							
							$filestatus = move_uploaded_file($this->data['Listings']['floorplan']['tmp_name'],$file_path."/".$fileName);			
							//exit;
							$source_fileName = $file_path."/".$fileName;						
							
							$thumb = $file_path."/thumb/".$fileName;
							$this->make_thumb($source_fileName,$thumb,150,100);
							$this->data['Listing']['floorplan'] = $fileName;
                                                        
                                                         //print_r($this->data);exit;
							
						//}
					}
			
			}
			
			//////////////////////////////////////////////////////////
			
		//print_r($this->data);exit;
			
			if($this->Listing->save($this->data))
			{	
			
				if(isset($this->data['Listing']['id']))
				{
				 $listing_id = $this->data['Listing']['id'];
				 $this->Session->setFlash('Listing has been updated.');
				 }
				 else	
				 {
				 	$listing_id = mysql_insert_id();	
				   $counter = 1;	
				 	$this->ad_data = $this->User_ad->findByUser_id($_SESSION['userId']);
					$this->ad_data['User_ad']['user_id'] = $_SESSION['userId'];
					if(!empty($this->ad_data))
					{
					  $counter = $counter + $this->ad_data['User_ad']['count'];	
					}  
					$this->ad_data['User_ad']['count'] = $counter;
					$this->User_ad->save($this->ad_data);
				 $this->Session->setFlash('Listing has been created and is now active.');
				 
				 }
				
			}

			$this->data['Filterlisting']['listing_id'] = $listing_id;
			if(isset($this->data['Listing']['type']) && $this->data['Listing']['type']=='edit')
			{
				
				$this->Neighborhoodlisting->deleteNeighborhood($listing_id);
			}
			//exit;
			if(isset($this->data['Neighborhoodlisting']['neighborhood_id'])) {
			foreach($this->data['Neighborhoodlisting']['neighborhood_id'] as $value)
				{
					//echo $value;
					$this->Neighborhoodlisting->saveNeighborhood($value,$listing_id);
				}
				//exit;
			}
			if(isset($this->data['Listing']['type']) && $this->data['Listing']['type']=='edit')
			{
				$listing_id = $this->data['Listing']['id'];
				$this->Filterlisting->deleteFilter($listing_id);
			}
			if(isset($this->data['Filterlisting']['filterid'])) {
			//echo 'true';exit;
			foreach($this->data['Filterlisting']['filterid'] as $value)
				{
					$this->Filterlisting->saveFilter($value,$listing_id);
				}
			}
			
			if(isset($this->data['PhotoT']))
				{
                                        if($this->data['Listing']['type']=='copy')
					$this->data['Listing']['id']=$listing_id;
                                        
					for($i=1;$i<=sizeof($this->data['Imagelisting']);$i++)
						{
							
							if(isset($this->data['PhotoT'][$i]) && $this->data['PhotoT'][$i]=='Change_Photo')
								{
									unlink(getcwd()."/uploadimage/thumb/".$this->data['Imagelistingkeep'][$i]);
									unlink(getcwd()."/uploadimage/".$this->data['Imagelistingkeep'][$i]);
									
									$this->Imagelisting->deleteImage($this->data['Imagelistingkeep'][$i],$this->data['Listing']['id']);
									
										if(!empty($this->data['Imagelisting'][$i]['name']) && in_array(strtolower($this->data['Imagelisting'][$i]['type']),$typeArr))
										{
											//if($this->data['Imagelisting'][$i]['size'] <2097152)
											//{
												$fileName = mt_rand().'_'.$this->data['Imagelisting'][$i]['name'];
												
												$filestatus = move_uploaded_file($this->data['Imagelisting'][$i]['tmp_name'],$file_path."/".$fileName);			
												
												$source_fileName = $file_path."/".$fileName;						
												
												$thumb = $file_path."/thumb/".$fileName;
												$this->make_thumb($source_fileName,$thumb,150,100);
												$this->Imagelisting->insertImage($fileName,$this->data['Listing']['id']);
											//}
										}
								
								}
							elseif(isset($this->data['PhotoT'][$i]) && $this->data['PhotoT'][$i]=='Remove_Photo')
								{
									unlink(getcwd()."/uploadimage/thumb/".$this->data['Imagelistingkeep'][$i]);
									unlink(getcwd()."/uploadimage/".$this->data['Imagelistingkeep'][$i]);
									$this->Imagelisting->deleteImage($this->data['Imagelistingkeep'][$i],$this->data['Listing']['id']);
								}
                                                                
                                                        elseif(isset($this->data['PhotoT'][$i]) && $this->data['PhotoT'][$i]=='Keep_Photo')
								{
									if($this->data['Listing']['type']=='copy')
									$this->Imagelisting->insertImage($this->data['Imagelistingkeep'][$i],$this->data['Listing']['id']);
								}
							elseif(!isset($this->data['PhotoT'][$i]))
								{
									if(!empty($this->data['Imagelisting'][$i]['name']) && in_array(strtolower($this->data['Imagelisting'][$i]['type']),$typeArr))
										{
											//if($this->data['Imagelisting'][$i]['size'] <2097152)
											//{
												$fileName = mt_rand().'_'.$this->data['Imagelisting'][$i]['name'];
												
												$filestatus = move_uploaded_file($this->data['Imagelisting'][$i]['tmp_name'],$file_path."/".$fileName);			
												
												$source_fileName = $file_path."/".$fileName;						
												
												$thumb = $file_path."/thumb/".$fileName;
												$this->make_thumb($source_fileName,$thumb,150,100);
												$this->Imagelisting->insertImage($fileName,$this->data['Listing']['id']);
											//}
										}
									
								}
								
							
						}			
					
				}	else {		
				//print_r($this->data['Imagelisting']);exit;
			foreach($this->data['Imagelisting'] as $key=> $value)
				{		
                                       
                                       //print_r($value);
					if(!empty($value['name']) && in_array(strtolower($value['type']),$typeArr))
					{                                                
						
						//if($value['size'] <2097152)
						//{
							$fileName = mt_rand().'_'.$value['name'];
							
							$filestatus = move_uploaded_file($value['tmp_name'],$file_path."/".$fileName);			
							//exit;
							$source_fileName = $file_path."/".$fileName;						
							
							$thumb = $file_path."/thumb/".$fileName;
							$this->make_thumb($source_fileName,$thumb,150,100);
							$this->Imagelisting->insertImage($fileName,$listing_id);
						//}
					}
								
					
				}	
				
				}
				
				$this->searchUser($this->data, $listing_id);
			
		}		
		
		//$this->redirectPage();
		$this->set('advertise',$this->siteAdd());
                if(isset($type) && $type=='ad')
                $this->redirect('/listings/managelisting');
                else
		$this->redirect('/mylisting');
		
	}
	
	
	
	function redirectPage()
	{
		
		if(isset($_SESSION['image_add_success']) && $_SESSION['image_add_success'] == '1' && isset($_SESSION['data_add_success']) && $_SESSION['data_add_success'] ==1)
		{
			$this->redirect('/mylisting');
			unset($_SESSION['image_add_success']);
			unset($_SESSION['data_add_success']);
			unset($_SESSION['listingID']);		
		}
	}
	
	function mylisting($page=1,$id=null)
 	{
	   
		
		$this->checkUser();
		$this->layout = "before_userlogin";
                $this->pageTitle = 'Straightforwardlistings';	
				
		unset($_SESSION['listingDetail']);
		
		$this->cityData('city','no');
		
		if(is_null($id))
		{
		   $condition = "user_id ='".$_SESSION['userId']."' AND is_deleted = 1";
           $id=$_SESSION['userId'];		   
	    }
		else
		{
		    $condition = "user_id ='".$id."' AND is_deleted = 1";
		}
		//$condition = "user_id = '".$_SESSION['userId']."' AND is_deleted = 1";
		 $order_by = "Listing.id desc";   
		  
			
			$this->set('totalPage',$this->frontPagination($condition,LIMIT));
			$this->set('page',$page);
			$this->set('id',$id);
			$this->set('link','mylisting');
		
		$filelds = array('Listing.id DISTINCT','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, LIMIT, $page);	
		//print_r($listingDetail);exit;
                
                $all_record = $this->Listing->allListing($condition, $filelds, $order_by, NULL, $page);
                        
                $totalPage = $this->frontPaginationTotalPage(sizeof($all_record),LIMIT);
                        
                $this->set('totalPage',$totalPage);
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                                $allNeighborHoodListing = array();
				$neighborhood_list ='';
                                
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{						
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        array_push($allNeighborHoodListing,$neighborhoodDetail[0]['Neighborhood']['neighborhood_name']);
                                                }
                                                
                                                
						
					}
                                       
                                         if(!empty($allNeighborHoodListing))
                                                {
                                                      sort($allNeighborHoodListing);
                                                      // print_r($allNeighborHoodListing);
                                                      foreach($allNeighborHoodListing as $value)  
                                                        $neighborhood_list .=$value.', ';
                                                }
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	

		$this->set('neighborhoodDetail',$neighborhood_data);	
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		$this->set('advertise',$this->siteAdd());			
		
		 
	}
        
        
       
        
	function make_thumb($img_name,$filename,$new_w,$new_h)
			{
				$ext='jpg';
				if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
					$src_img=imagecreatefromjpeg($img_name);
					
				if(!strcmp("png",$ext))
					$src_img=imagecreatefrompng($img_name);
			
		
				$old_x=imageSX($src_img);
				$old_y=imageSY($src_img);
			
				$ratio1=$old_x/$new_w;
				$ratio2=$old_y/$new_h;
				if($ratio1>$ratio2) {
					$thumb_w=$new_w;
					$thumb_h=$old_y/$ratio1;
				}
				else 
					{
						$thumb_h=$new_h;
						$thumb_w=$old_x/$ratio2;
					}
			
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			
			if(!strcmp("png",$ext))
				imagepng($dst_img,$filename);
			else
				imagejpeg($dst_img,$filename);
			
			imagedestroy($dst_img);
			imagedestroy($src_img);
		}

		function make_image($filename)
			{
				//echo $filename;
				$mHeight = 330;
				list($width, $height, $type, $attr) = @getimagesize($filename);
				if($height >= $mHeight)
				{
					$target_height = $mHeight;
					$target_width = round($width * ($mHeight/$height));
				}
				else
				{
				     $target_width = $width;
					 $target_height = $height;
				}
				//exit;
				return array("width"=>$target_width,"height"=>$target_height);		 	
		}
                
                
                function make_floorplan($filename)
			{
				//echo $filename;
				$mWidth = 300;
				list($width, $height, $type, $attr) = @getimagesize($filename);
				if($width >= $mWidth)
				{
					$target_width = $mWidth;
					$target_height = round($mWidth * ($height/$width));
				}
				else
				{
				     $target_width = $width;
				     $target_height = $height;
				}
				//exit;
				return array("width"=>$target_width,"height"=>$target_height);		 	
		}
                
                function frontPaginationTotalPage($total_record,$show)
		{
			
			
			$pageCount = ceil($total_record / $show );
			
			return $pageCount;
			
		}
	function listingdetail_popup($listing_id=NULL,$h_count = NULL)
	 	{
		 if(empty($listing_id))
		$this->redirect('/');
		
		if(!empty($h_count))
		{
			$h_count = $h_count + 1;
			$this->set('h_count',$h_count);
		}
		else
		{
			$this->set('h_count','1');	
		}
		
		$this->layout = "before_userlogin";
    	        $this->pageTitle = 'Straightforwardlistings';
		
		$this->cityData('city','no');
	        $this->listing_data = $this->Listing_counter->findByListing_id($listing_id);	
		
              
                
		$counter = 1;
		if(!empty($this->listing_data['Listing_counter']['counter']))
		{
			$counter = $counter + $this->listing_data['Listing_counter']['counter'];
		}
		$this->listing_data['Listing_counter']['listing_id'] = $listing_id;
		$this->listing_data['Listing_counter']['counter'] = $counter;
		$this->Listing_counter->save($this->listing_data);
		
		$condition = "Listing.id = ".$listing_id." group by Listing.id";		
		$conditionN = $condition;
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
		
		 //echo date("d-m-Y H:i:s", mktime())." - 2st begin\n\n";flush();
		 //echo '<br>';
		
            if(empty($listingDetail))
		$this->redirect('/');   
		
		if($listingDetail)
		{
			
			$condition = "User.id = ".$listingDetail[0]['Listing']['user_id'];		
			
			$userInfo = $this->User->seletAlluser($condition, NULL, NULL, NULL, NULL);	
			
			$this->set('userInfo',$userInfo);	
		}
		
	
		
		$filter_data = array();
                $allfilter = '';
		foreach($listingDetail[0]['Filterlisting'] as $value)
			{
				
			$allfilter .=$value['filter_id'].',';	
			//$condition = "Filter.id = ".$value['filter_id']."";
			
			//$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			//$order_by = "Categoryfilter.id,Filter.id asc";     
	 
	 		//array_push($filter_data,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			}
                        $filterdata = array();
                        if($allfilter!='')
                        {
                                $allfilters = substr($allfilter,0, -1);
                                
                                $condition = "Filter.isdelete='0' AND Filter.id in($allfilters)";
                                
                                $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
                                
                                $order_by = "Categoryfilter.id asc";     
                                
                               $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
                                
                                if($this->checkLang() != 'en')
                                {
                                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
                                }
                                
                        }
                        $this->set('filterdata',$filterdata);	
                //print_r($filter_data);
			
		$neighborhood_data = array();
		//print_r($listingDetail); exit;
                $neighborhood_list ='';
		for($i=0;$i<sizeof($listingDetail[0]['Neighborhoodlisting']); $i++)
			{
                               //echo $listingDetail[0]['Neighborhoodlisting'][0]['neighborhood_id']; exit;
				
				//foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					//{						
						$condition = "Neighborhood.id = ".$listingDetail[0]['Neighborhoodlisting'][$i]['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					//}
				array_push($neighborhood_data,$neighborhoodDetail[0]['Neighborhood']['neighborhood_name']);				
			}
		//print_r($neighborhood_data);	
		//echo $neighborhood_list; exit;
		
		$this->set('neighborhoodDetail',$neighborhood_data);
                $this->set('neighborhood_list',$neighborhood_list);
                
                  // echo date("d-m-Y H:i:s", mktime())." - 3rd begin\n\n";flush();
		// echo '<br>';      
	    
		if(isset($_SESSION['userId']) || isset($_SESSION['listingDetail']))	
		{
			
			
			if(isset($_SESSION['userId'])) 
			{
				$con = "user_id = '".$_SESSION['userId']."' AND is_deleted = 1";			
				$this->set('prevnext', $this->Listing->findNeighbours($con, 'Listing.id', $listing_id)); 
				
				$existNewsletterresult = $this->Listing->existNewsletter($listing_id,$_SESSION['userId']);		
			$this->set('existNewsletter',$existNewsletterresult);	
				
			} 
			else if(isset($_SESSION['listingDetail']))
			{
				$prevnext = array();
				
				foreach($_SESSION['listingDetail'] as $key=>$value)
					{
						if($value['Listing']['id']==$listing_id)
							{
								if($key!=0) {
								$prev_key=$key -1;
								$prevnext['next']['Listing']['id'] = $_SESSION['listingDetail'][$prev_key]['Listing']['id'];
								} else
								$prevnext['next'] = array();
																
								if($key != sizeof($_SESSION['listingDetail'])-1)
								{
								$next_key = $key +1;
								$prevnext['prev']['Listing']['id'] = $_SESSION['listingDetail'][$next_key]['Listing']['id'];
								}else
								$prevnext['prev'] = array();
								
								break;
							}
					}			
					
					
				$this->set('prevnext', $prevnext); 
			}
		}
                
                
		
		//$this->set('filterdata',$filter_data);
                
                 //echo date("d-m-Y H:i:s", mktime())." - 4th begin\n\n";flush();
		 //echo '<br>';
                $floorplan_image = ''; 
		
                if(!empty($listingDetail[0]['Listing']['floorplan']) && file_exists(getcwd()."/uploadimage/".$listingDetail[0]['Listing']['floorplan'])) {
		  $path = URL."uploadimage/".$listingDetail[0]['Listing']['floorplan'];
		  $arr = $this->make_floorplan(getcwd()."/uploadimage/".$listingDetail[0]['Listing']['floorplan']);
		
		  $floorplan_image = "<img src=\"".$path."\" width=\"".$arr['width']."\" height=\"".$arr['height']."\"  border=\"0\">";
		  //echo '<br>';
		
                }
			
		 //echo date("d-m-Y H:i:s", mktime())." - 5th begin\n\n";flush();
		 //echo '<br>';		
           
		$imgArr = array();
              
		for($i=0;$i<sizeof($listingDetail[0]['Imagelisting']);$i++)
		{
                    
		  $path = URL."uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file'];
                  
                
                  
                 if(file_exists(getcwd()."/uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file']))
                  {
                   
		  $arr = $this->make_image(getcwd()."/uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file']);
		   
		
                
               
		 
		  $width = empty($arr['width'])?"660":$arr['width'];
		  $height = empty($arr['height'])?"330":$arr['height'];
		  $imgArr[$listingDetail[0]['Imagelisting'][$i]['id']] = "<img src=\"".$path."\" width=\"".$width."\" height=\"".$height."\"  border=\"0\">";
		  //echo '<br>';
                  } 
		}
                //print_r($imgArr); exit;
             
              
            
                //echo date("d-m-Y H:i:s", mktime())." - 6th begin\n\n";flush();
		// echo '<br>';
		
                $this->set('floorplan_image',$floorplan_image);	
		$this->set('listingDetail',$listingDetail);	
	 	$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		$this->set('advertise',$this->siteAdd());
		$this->set('imgArr',$imgArr);		
		
	}

	function listingdetail($listing_id=NULL,$h_count = NULL)
 	{
	  $list = mysql_query("SELECT * FROM `listings` WHERE id = '".$listing_id."' and is_deleted='1'");
	  $list_fet = mysql_fetch_array($list);
	  
	  
	 if(empty($listing_id))
		{$this->redirect('/');}
		
	 if(empty($list_fet))
		{$this->redirect('/');}
		
		
		
		
		
     /*Detecting for moblile View*/
	  
	    $ostype=new UserAgent();
		if($ostype->getOS() == "iPhone" || $ostype->getOS() == "iPod" || $ostype->getOS() == "symbian" || $ostype->getOS() == false)
		{
		$extra = 'mobile/viewlisting/'.$listing_id;
		$this->redirect('/'.$extra);
		}
		
			
		if(!empty($h_count))
		{
			$h_count = $h_count + 1;
			$this->set('h_count',$h_count);
		}
		else
		{
			$this->set('h_count','1');	
		}
		
		$this->layout = "before_userlogin";
    	        $this->pageTitle = 'Straightforwardlistings';
		
		$this->cityData('city','no');
	        $this->listing_data = $this->Listing_counter->findByListing_id($listing_id);	
		
              
                
		$counter = 1;
		if(!empty($this->listing_data['Listing_counter']['counter']))
		{
			$counter = $counter + $this->listing_data['Listing_counter']['counter'];
		}
		$this->listing_data['Listing_counter']['listing_id'] = $listing_id;
		$this->listing_data['Listing_counter']['counter'] = $counter;
		$this->Listing_counter->save($this->listing_data);
		
		$condition = "Listing.id = ".$listing_id." group by Listing.id";		
		$conditionN = $condition;
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
		
		 //echo date("d-m-Y H:i:s", mktime())." - 2st begin\n\n";flush();
		 //echo '<br>';
		 if(empty($listingDetail))
		$this->redirect('/');
              
		
		if($listingDetail)
		{
			
			$condition = "User.id = ".$listingDetail[0]['Listing']['user_id'];		
			
			$userInfo = $this->User->seletAlluser($condition, NULL, NULL, NULL, NULL);	
			
			$this->set('userInfo',$userInfo);	
			
		}
		
	
		
		$filter_data = array();
                $allfilter = '';
		foreach($listingDetail[0]['Filterlisting'] as $value)
			{
				
			$allfilter .=$value['filter_id'].',';	
			//$condition = "Filter.id = ".$value['filter_id']."";
			
			//$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			//$order_by = "Categoryfilter.id,Filter.id asc";     
	 
	 		//array_push($filter_data,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			}
                        $filterdata = array();
                        if($allfilter!='')
                        {
                                $allfilters = substr($allfilter,0, -1);
                                
                                $condition = "Filter.isdelete='0' AND Filter.id in($allfilters)";
                                
                                $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
                                
                                $order_by = "Categoryfilter.id asc";     
                                
                               $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
                                
                                if($this->checkLang() != 'en')
                                {
                                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
                                }
                                
                        }
                        $this->set('filterdata',$filterdata);	
                //print_r($filter_data);
			
		$neighborhood_data = array();
		//print_r($listingDetail); exit;
                $neighborhood_list ='';
		for($i=0;$i<sizeof($listingDetail[0]['Neighborhoodlisting']); $i++)
			{
                               //echo $listingDetail[0]['Neighborhoodlisting'][0]['neighborhood_id']; exit;
				
				//foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					//{						
						$condition = "Neighborhood.id = ".$listingDetail[0]['Neighborhoodlisting'][$i]['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					//}
				array_push($neighborhood_data,$neighborhoodDetail[0]['Neighborhood']['neighborhood_name']);				
			}
		//print_r($neighborhood_data);	
		//echo $neighborhood_list; exit;
		
		$this->set('neighborhoodDetail',$neighborhood_data);
                $this->set('neighborhood_list',$neighborhood_list);
                
                  // echo date("d-m-Y H:i:s", mktime())." - 3rd begin\n\n";flush();
		// echo '<br>';      
	    
		if(isset($_SESSION['userId']) || isset($_SESSION['listingDetail']))	
		{
			
			
			if(isset($_SESSION['userId']) && !isset($_SESSION['listingDetail'])) 
			{
				$con = "user_id = '".$_SESSION['userId']."' AND is_deleted = 1";			
				$this->set('prevnext', $this->Listing->findNeighbours($con, 'Listing.id', $listing_id)); 
				
				$existNewsletterresult = $this->Listing->existNewsletter($listing_id,$_SESSION['userId']);		
			    $this->set('existNewsletter',$existNewsletterresult);	
				
			}	
			else if(isset($_SESSION['listingDetail']))
			{
				 $prevnext = array();
				 //print_r($_SESSION['listingDetail']);
                 //exit(0);	
				foreach($_SESSION['listingDetail'] as $key=>$value)
					{
						if($value['Listing']['id']==$listing_id)
							{
								if($key!=0) {
								$prev_key=$key -1;
								$prevnext['next']['Listing']['id'] = $_SESSION['listingDetail'][$prev_key]['Listing']['id'];
								} else
								$prevnext['next'] = array();
																
								if($key != sizeof($_SESSION['listingDetail'])-1)
								{
								$next_key = $key +1;
								$prevnext['prev']['Listing']['id'] = $_SESSION['listingDetail'][$next_key]['Listing']['id'];
								}else
								$prevnext['prev'] = array();
								
								break;
							}
					}			
					
					
				$this->set('prevnext', $prevnext); 
			}
		}
                
                
		
		//$this->set('filterdata',$filter_data);
                
                 //echo date("d-m-Y H:i:s", mktime())." - 4th begin\n\n";flush();
		 //echo '<br>';
                $floorplan_image = ''; 
		
                if(!empty($listingDetail[0]['Listing']['floorplan']) && file_exists(getcwd()."/uploadimage/".$listingDetail[0]['Listing']['floorplan'])) {
		  $path = URL."uploadimage/".$listingDetail[0]['Listing']['floorplan'];
		  $arr = $this->make_floorplan(getcwd()."/uploadimage/".$listingDetail[0]['Listing']['floorplan']);
		
		  $floorplan_image = "<img src=\"".$path."\" width=\"".$arr['width']."\" height=\"".$arr['height']."\"  border=\"0\">";
		  //echo '<br>';
		
                }
			
		 //echo date("d-m-Y H:i:s", mktime())." - 5th begin\n\n";flush();
		 //echo '<br>';		
           
		$imgArr = array();
              
		for($i=0;$i<sizeof($listingDetail[0]['Imagelisting']);$i++)
		{
                    
		  $path = URL."uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file'];
                  
                
                  
                 if(file_exists(getcwd()."/uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file']))
                  {
                   
		  $arr = $this->make_image(getcwd()."/uploadimage/".$listingDetail[0]['Imagelisting'][$i]['file']);
		   
		
                
               
		 
		  $width = empty($arr['width'])?"660":$arr['width'];
		  $height = empty($arr['height'])?"330":$arr['height'];
		  $imgArr[$listingDetail[0]['Imagelisting'][$i]['id']] = "<img src=\"".$path."\" width=\"".$width."\" height=\"".$height."\"  border=\"0\">";
		  //echo '<br>';
                  } 
		}
                //print_r($imgArr); exit;
             
              
            
                //echo date("d-m-Y H:i:s", mktime())." - 6th begin\n\n";flush();
		// echo '<br>';
		
	  
		
        $this->set('floorplan_image',$floorplan_image);	
		$this->set('listingDetail',$listingDetail);	
	 	$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		$this->set('advertise',$this->siteAdd());
		$this->set('imgArr',$imgArr);
		$this->set('listing_id',$listing_id);
			
		
	}
	
	
	function missinfo()
 	{		
		if($this->data)
		{
		    $headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
			$headers .= "From: ".$this->data['Miisinfo']['from']."\r\n";
			$sub = $this->data['Miisinfo']['subject'];
			$mes = $this->data['Miisinfo']['content'];
			
			$sent = mail($this->data['Miisinfo']['mailto'],$sub,$mes,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
			}
			}
			$this->redirect('/listingdetail/'.$this->data['Miisinfo']['listingID']);
			
		 
	}
	
	function updatelisting()
	{
		//echo $this->data['Listing']['id'];
		//print_r($this->data);
		 $file_path = getcwd()."/uploadimage";	
		
						
					for($i=1;$i<=sizeof($this->data['Imagelisting']);$i++)
						{
							
							if(isset($this->data['PhotoT'][$i]) && $this->data['PhotoT'][$i]=='Change_Photo')
								{
									unlink(getcwd()."/uploadimage/thumb/".$this->data['Imagelistingkeep'][$i]);
									unlink(getcwd()."/uploadimage/".$this->data['Imagelistingkeep'][$i]);
									
									$this->Imagelisting->deleteImage($this->data['Imagelistingkeep'][$i],$this->data['Listing']['id']);
									
										if(!empty($this->data['Imagelisting'][$i]['name']) && in_array(strtolower($this->data['Imagelisting'][$i]['type']),$typeArr))
										{
											//if($this->data['Imagelisting'][$i]['size'] <2097152)
											//{
												$fileName = mt_rand().'_'.$this->data['Imagelisting'][$i]['name'];
												
												$filestatus = move_uploaded_file($this->data['Imagelisting'][$i]['tmp_name'],$file_path."/".$fileName);			
												
												$source_fileName = $file_path."/".$fileName;						
												
												$thumb = $file_path."/thumb/".$fileName;
												$this->make_thumb($source_fileName,$thumb,150,100);
												$this->Imagelisting->insertImage($fileName,$this->data['Listing']['id']);
											//}
										}
								
								}
							elseif(isset($this->data['PhotoT'][$i]) && $this->data['PhotoT'][$i]=='Remove_Photo')
								{
									unlink(getcwd()."/uploadimage/thumb/".$this->data['Imagelistingkeep'][$i]);
									unlink(getcwd()."/uploadimage/".$this->data['Imagelistingkeep'][$i]);
									$this->Imagelisting->deleteImage($this->data['Imagelistingkeep'][$i],$this->data['Listing']['id']);
								}
							elseif(!isset($this->data['PhotoT'][$i]))
								{
									
									if(!empty($this->data['Imagelisting'][$i]['name']) && in_array(strtolower($this->data['Imagelisting'][$i]['type']),$typeArr))
										{
											
											//if($this->data['Imagelisting'][$i]['size'] <2097152)
											//{
												
												$fileName = mt_rand().'_'.$this->data['Imagelisting'][$i]['name'];
												
												
												$filestatus = move_uploaded_file($this->data['Imagelisting'][$i]['tmp_name'],$file_path."/".$fileName);			
												
												$source_fileName = $file_path."/".$fileName;						
												
												
												$thumb = $file_path."/thumb/".$fileName;
												$this->make_thumb($source_fileName,$thumb,150,100);
												$result = $this->Imagelisting->insertImage($fileName,$this->data['Listing']['id']);
												//echo "fsdf".$result; exit;
											//}
											
										}
									
								}
								
							
						}			
					
				
		$this->redirect('/editimage/'.$this->data['Listing']['id']);
	}
	function editlisting($id = NULL,$category_id=NULL)
	{ 
	  if(empty($id))
		$this->redirect('/');
		
	  $this->checkUser();
	  $this->layout = "before_userlogin";
          $this->pageTitle = 'Straightforwardlistings';	
	  $this->cityData('city','no');
	   
	   $this->Listing->id=$id;
       $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
	   $this->data = $this->Listing->read();
	 //print_r($this->data);exit;
	   $listnigdata = $this->data;
	   if(empty($listnigdata))
		$this->redirect('/');
		
	   $this->set('listnigdata',$this->data); 
	   
	   $this->set('listing_id',$id); 
	   
	   //print_r($this->data); 
	  
	   $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('category_name','id');
		
		$order_by = "id asc"; 
		
		$category_data = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);    
		//print_r($category_data);exit;
		if($this->checkLang() != 'en')
		{
		 $category_data = $this->changeLang('1',"Category","category_name",$category_data);
		}
		//$category_id = 
		//print_r($category_data);exit;
		//$this->set('categorydata',$category_data);
		$this->set('categorydata',$category_data);	 
		
		if($category_id==NULL)
		 {
		 	 //$catID = $category_data[0]['Category']['id'];
			 $catID = $listnigdata['Listing']['category_id'];
		 }
		 else
		 {
			 $catID = $category_id;			 
		 }	
		$this->set('selectCategory',$catID);
                
                
              
                
                
                
                
                
		
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Filter.require','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	  $order_by = "Categoryfilter.id asc,Filter.id asc";     
	 
		$filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
		if($this->checkLang() != 'en')
		{
		 $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
		 $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
		}
		 $this->set('filterdata',$filterdata);	
		//$this->set('filterdata',$filterdata);	
		//print_r($filterdata);exit;
		
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc"; 
		    
		$cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
		
		$this->set('citydata',$cityData);	
		
		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id = ".$this->data['City']['id'];
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "id asc";     
		
			
	   	$neighborHood = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		if(!empty($neighborHood) && $this->checkLang() != 'en')
		{
		 $neighborHood = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborHood);
		} 
		$this->set('neighborHood',$neighborHood);
	   	   
	  $this->set('type','edit');
	  
	   $this->set('indexContent',$this->translateContent());
	   $this->set('indexContent1',$this->translateContent1());
	   $this->set('advertise',$this->siteAdd());
	    $this->render('index');
		
			
	}
	
	
	function editimage($id = NULL,$category_id=NULL)
	{ 
	   $this->checkUser();
	   $this->layout = "before_userlogin";
	   $this->pageTitle = 'Straightforwardlistings';	
	   $this->cityData('city');
	   $imageName = $this->Listing->read(null, $id);
	   $this->set('imageName',$imageName['Imagelisting']);	
	   $this->set('listingId',$id);	
	   $this->set('indexContent',$this->translateContent());
	}
	
	function copylisting($id = NULL,$category_id=NULL)
	{ 
		
	  if(empty($id))
		$this->redirect('/');
		
	 $this->checkUser();
	  $this->layout = "before_userlogin";
          $this->pageTitle = 'Straightforwardlistings';	
	  $this->cityData('city','no');
	   
	   $this->Listing->id=$id;
           $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
	   $this->data = $this->Listing->read();
	   //print_r($this->data);exit;
	   $listnigdata = $this->data;
	   if(empty($listnigdata))
		$this->redirect('/');
	   
	   $this->set('listnigdata',$this->data); 
	   
	   $this->set('listing_id',$id); 
	   
	   //print_r($this->data); 
	  
	   $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('category_name','id');
		
		$order_by = "id asc"; 
		
		$category_data = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);    
		//print_r($category_data);exit;
		if($this->checkLang() != 'en')
		{
		 $category_data = $this->changeLang('1',"Category","category_name",$category_data);
		}
		//$category_id = 
		//print_r($category_data);exit;
		//$this->set('categorydata',$category_data);
		$this->set('categorydata',$category_data);	 
		
		if($category_id==NULL)
		 {
		 	 //$catID = $category_data[0]['Category']['id'];
			 $catID = $listnigdata['Listing']['category_id'];
		 }
		 else
		 {
			 $catID = $category_id;			 
		 }	
		$this->set('selectCategory',$catID);
                
                
              
                
                
                
                
                
		
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Filter.require','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	  $order_by = "Categoryfilter.id asc,Filter.id asc";     
	 
		$filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
		if($this->checkLang() != 'en')
		{
		 $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
		 $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
		}
		 $this->set('filterdata',$filterdata);	
		//$this->set('filterdata',$filterdata);	
		//print_r($filterdata);exit;
		
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc"; 
		    
		$cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
		
		$this->set('citydata',$cityData);	
		
		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id = ".$this->data['City']['id'];
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "id asc";     
		
			
	   	$neighborHood = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		if(!empty($neighborHood) && $this->checkLang() != 'en')
		{
		 $neighborHood = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborHood);
		} 
		$this->set('neighborHood',$neighborHood);
	   	   
	  $this->set('type','copy');
	  
	   $this->set('indexContent',$this->translateContent());
	   $this->set('indexContent1',$this->translateContent1());
	   $this->set('advertise',$this->siteAdd());
	    $this->render('index');
		
			
	

	 
	}
	function deletelisting($listing_id = NULL)
	{ 
	   $this->checkUser(); 	   
	   //if($this->Listing->deletelistingdetail1($listing_id))
	    mysql_query("UPDATE listings SET is_deleted = '0' WHERE id = '".$listing_id."'");
	    $this->Session->setFlash('The listing is successfully Deleted');
		$this->redirect($this->referer());

	}
	function deletelisting_all()
	{ 
	  echo $this->params['form']['chkUser'] ;
	 	  
	   $this->checkUser(); 	  
	   foreach($this->params['form']['chkUser'] as $id)
	   { 
	      mysql_query("UPDATE listings SET is_deleted = '0' WHERE id = '".$id."'");
	   //if($this->Listing->deletelistingdetail1($id))
	   }
	   $this->Session->setFlash('The listing is successfully Deleted');
	   $this->redirect($this->referer());

	}
	
	
	/*function deletelisting($listing_id = NULL)
	{ 
	   $this->checkUser(); 	   
	   if($this->Listing->deletelistingdetail1($listing_id))
	   $this->Session->setFlash('The listing is successfully Deleted');
		$this->redirect('/mylisting');
	}*/
	
	function listinglaw($id = NULL)
 	{
		$this->checkUser();
		$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';		
		
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
		$headers .= "From: fairhousing@straightforwardlistings.com <fairhousing@straightforwardlistings.com>\r\n";
		$sub = "Listing #".$id." Reported as Violating Fair Housing Laws";
		$mes = "Listing #".$id." has been reported as violating fair housing laws\r\n";
		$mes .= URL."listingdetail/".$id;
		$sent = mail('fairhousing@straightforwardlistings.com',$sub,$mes,$headers);

		if($sent){
				$this->Session->setFlash('straightforwardlistings.com has been notified. Thank you.');
		}
		
		
		 
	}
	
	function ajax_available($id = NULL)
 	{
		$this->checkUser();
                $this->layout = "ajax";			
		Configure::write('debug', '0');
		//$this->layout = "before_userlogin";
                //$this->pageTitle = 'Straightforwardlistings';	
		
		$condition = "Listing.id = ".$this->data['Listing']['id'];	
			
		$order_by = "Listing.id desc";   
				
		//$filelds = array('Listing.price','Listing.id','Listing.title');
		$this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting')));
		$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
                
               
                
                $subject = "Your Listing has been reported as No Longer Available";
		//$message = "<p>Your listing ID #".$this->data['Listing']['id']." has reported your listing as sold.<br>";
		//$message .= "If this is correct, please login below.<br>";
		//$message .= "<br>";
		//$message .= "<a href=\"http://straightforwardlistings.com/mylisting\">MLS Update 123 - My Listings</a>";
		//$message .= "</p>";
		//$message .= "<p style=\"color: red\">If we do not receive a response within 24 hours, your listing will be deactivated.</p>";
                
                
                $message ="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
         
	   <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>		
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
       
        <td colspan=\"2\" style=\"font-size:12px;\">Your listing ID #".$this->data['Listing']['id']." has been reported as No Longer Available.<br></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td><a href=\"".URL."listingdetail/".$this->data['Listing']['id']."\" target=\"_blank\" class=\"left_menu_txt\">Click here to see the property detail</a></td>
      </tr>
	  <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan=\"2\" valign=\"top\">If this is correct, please login below.<br>
	<a href=\"http://straightforwardlistings.com/mylisting\">StraightForwardListings - My Listings</a><br>
        <p style=\"color: red\">If we do not receive a response within 24 hours, your listing will be deactivated.</p></td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
     
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
     </table></td>
  </tr>
  
</table>";
                
                
                
                $headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: listingnolongeravailable@straightforwardlistings.com <listingnolongeravailable@straightforwardlistings.com>\r\n";
        $sent = mail($listingDetail[0]['User']['email'],$subject,$message,$headers);
                
                $sub = "Listing #".$this->data['Listing']['id']." Reported as Sold";
		$mes = "Listing #".$this->data['Listing']['id']." has been reported as sold\r\n";
		$mes .= "http://straightforwardlistings.com/listingdetail/".$this->data['Listing']['id'];

		 mail('listingnolongeravailable@straightforwardlistings.com',$sub,$mes,$headers);
		 mail('admin@straightforwardlistings.com',$sub,$mes,$headers);
                
                if($sent)
		echo 'yes';	
		else
		echo 'no';
                
	}
	function change_city()
		{
			//$this->checkUser();
			$this->layout = "ajax";
			$this->pageTitle = 'Straightforwardlistings';	
			//$this->render('change_city', 'ajax');
			//echo 'hello';
			Configure::write('debug', '0');
			$filelds = array('neighborhood_name','id');
			$condition = 'city_id='.$this->data['Listing']['city_id'];
			$order_by = "neighborhood_name asc";     
			
			$this->set('neighborHood',$this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL));
			//print_r($this->data['Listing']['city_id']);
			//$this->set('city_id',$this->data['Listing']['city_id']);
		}
		//{$this->data['Listing']['streetname']}
	function autocomplete()
	{
		Configure::write('debug', '0'); 
	    $this->set('posts',$this->Listing->findAll("streetname LIKE '".$this->data['Listing']['streetname']."%' GROUP BY streetname"));   
		$this->layout = "ajax";
		// print_r($this->data); 
	} 
	function ajax_addlaw($id = NULL)
 	{
                
		$this->checkUser();
                 $this->layout = "ajax";
                 Configure::write('debug', '0');
		//$this->layout = "before_userlogin";
               // $this->pageTitle = 'Straightforwardlistings';	
		$listing_id = $this->data['Listing']['id'];
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
		$headers .= "From: fairhousing@straightforwardlistings.com <fairhousing@straightforwardlistings.com>\r\n";
		$sub = "Listing L-".$listing_id." Reported as Violating Fair Housing Laws";
		
		$mes = "
		<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>
		  <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:center;\">Listing L-".$listing_id." Reported as Violating Fair Housing Laws</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  
  
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td valign=\"top\">Listing L-".$listing_id." has been reported as violating fair housing laws</td>
        </tr>
      <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
      <tr>
        <td valign=\"top\"><a href=\"".URL."listingdetail/".$listing_id."\" target=\"_blank\" class=\"left_menu_txt\">Click here to see the property detail</a></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>";
		
		//$mes = "Listing L-".$listing_id." has been reported as violating fair housing laws\r\n";
		//$mes .= "http://www.navsoft.co.in/straightforwardlistings/listingdetail/".$listing_id;
		
		

		$sent = mail('fairhousing@straightforwardlistings.com',$sub,$mes,$headers);
                
                
                
		if($sent)
		echo 'yes';	
		else
		echo 'no';		
		
	 
	}
	
	
	function addnewsletter($id = NULL)
 	{
		$this->checkUser();
		$this->layout = "before_userlogin";
    	$this->pageTitle = 'Straightforwardlistings';		
		if($this->Listing->addnewsletter($id,$_SESSION['userId']))
		$this->Session->setFlash('Straightforwardlistings.com has been notified. Thank you.');
		 
	}
	
	function ajax_missinfo()
 	{
		$this->checkUser();
		if($this->data)
		{
			$headers  = "MIME-Version: 1.0\r\n";
		    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: ".$this->data['Miisinfo']['from']."\r\n";
			$sub = $this->data['Miisinfo']['subject'];
			$content = $this->data['Miisinfo']['content'];
			

			$mes = "
		<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>
		  <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:center;\">".$this->data['Miisinfo']['subject']."</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  
  
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td valign=\"top\">".$content."</td>
        </tr>
      <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
      <tr>
        <td valign=\"top\">Thanks <br>
		".$this->data['Miisinfo']['from']."
							</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>";
			
			$sent = mail($this->data['Miisinfo']['mailto'],$sub,$mes,$headers);
			 mail('admin@straightforwardlistings.com',$sub,$mes,$headers);
			if($sent){
					$this->Session->setFlash('Message has been sent');
					$message = 'yes';			
			}
			else
			$message = 'no';	
		}
	}
	
	function ajax_addnews()
 	{
               
		$this->checkUser();
                $this->layout = "ajax";
                 Configure::write('debug', '0');
		if($this->data)
		{
			$id = $this->data['Newsletter']['listingID'];
			if($this->Listing->addnewsletter($id,$_SESSION['userId']))
			echo 'yes';		
			else
			echo 'no';		
		}
	}
	
	function frontPagination($criteria,$show)
		{
			
			$count = $this->Listing->findCount($criteria,0);
			$pageCount = ceil($count / $show );
			
			return $pageCount;
			
		}
		
		
		
	function ajax_addactive()
 	{
		$this->checkUser();
		if($this->data)
		{						
			
			
			if($this->data['Listing']['active']==1)
			$this->data['Listing']['active'] = 0;
			else
			$this->data['Listing']['active'] = 1;
			
			if($this->Listing->save($this->data))
			{
				$message = "yes";		
			} 
			else
			$message = "no";	
			
			$this->set('message',$message);	
			$this->set('active',$this->data['Listing']['active']);			
			$this->set('listing_id',$this->data['Listing']['id']);					
		}
	}
	
	function ajax_addsold()
 	{
		//$listingid,$sold
		/*$this->checkUser();
		if(!empty($listingid))
		{
			if($sold==1)
			$sold = 0;
			else
			$sold = 1;
			
			if($this->Listing->updateAvailable($sold,$listingid))
			{
				$message = "yes";		
			} 
			else
                                $message = "no";	
			
			$this->set('message',$message);	
			$this->set('sold',$sold);			
			$this->set('listing_id',$listingid);
                        $this->set('indexContent',$this->translateContent());
                        $this->set('indexContent1',$this->translateContent1());
		}*/
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		$this->checkUser();
		$listingid = $this->data['Miisinfo']['property_id'];
		$sold = $this->data['Miisinfo']['property'];
		//$sold1 = $sold;
		if(!empty($listingid))
		{
			/*if($sold==1)
			$sold = 0;
			else
			$sold = 1;*/
			
			if($this->Listing->updateAvailable($sold,$listingid))
			{
				$message = "yes";		
			} 
			else
                                $message = "no";	
			
			$this->set('message',$message);	
			$this->set('sold',$sold);			
			$this->set('listing_id',$listingid);
                        $this->set('indexContent',$this->translateContent());
                        $this->set('indexContent1',$this->translateContent1());
		}
	}
	
	function ajax_neighborhood()
 	{					
			
			
			if($this->data['Listing']['sold']==1)
			$this->data['Listing']['sold'] = 0;
			else
			$this->data['Listing']['sold'] = 1;
			
			if($this->Listing->save($this->data))
			{
				$message = "yes";		
			} 
			else
			$message = "no";	
			
			$this->set('message',$message);	
			$this->set('sold',$this->data['Listing']['sold']);			
			$this->set('listing_id',$this->data['Listing']['id']);					
		
	}
	
	function ajax_sharefriend()
 	{
		
		if($this->data)
		{
			
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: ".$this->data['Share']['email']."\r\n";
			$urname = $this->data['Share']['urname'];
			$email = $this->data['Share']['email'];
			$sendemail = $this->data['Share']['sendemail'];
			$comments_user = $this->data['Share']['comments'];
			$listing_id = $this->data['Share']['listingID'];
                        
                        
			
			$condition = "isdelete='0' and isblocked= 0 AND type='a'";
			
			$filelds = array('name','file','url');
			
			$order_by = "rand()"; 	
			
			$advertise = $this->Advertise->findAllAdvertiseDetail($condition,$filelds,$order_by,NULL,NULL); 
			
			$condition = "Listing.id = ".$listing_id;		
			$this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
			$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
                      //echo  $listingDetail[0]['Listing']['description']; exit;
                      
			
			$comments ="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
          <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>		 
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  <tr>
    <td align=\"left\" valign=\"top\">&nbsp;</td>
  </tr>
  <tr>
    <td><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"4\" cellspacing=\"4\">";
	$comments .="<tr>";
	$counter =0;
	foreach($listingDetail[0]['Imagelisting'] as $value) {  
	
  	if($counter%4==0)
		$comments .="</tr><tr>";
		$comments .="<td width=\"25%\"  valign=\"top\"><img src=\"".URL."uploadimage/thumb/".$value['file']."\" width=\"150\" height=\"100\"  /></td>";
  
	 $counter++; 
	}  
 $comments .=  " </tr></table></td>
  </tr>
  <tr>
    <td style=\"text-align:center; color:#7486c0; font-weight:bold; font-size:16px;\">&nbsp;</td>
  </tr>
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td width=\"125\" valign=\"top\">Description : </td>
        <td width=\"600\" style=\"font-size:12px;\">".$listingDetail[0]['Listing']['description']."</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">Price :</td>
        <td>";
        if($listingDetail[0]['Listing']['price_request']=='Y')
        $comments .="Price on request";
        else
        $comments .=$listingDetail[0]['Listing']['price'];
        
    $comments .="</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td><a href=\"".URL."listingdetail/".$listing_id."\" target=\"_blank\" class=\"left_menu_txt\">Click here to see the property detail</a></td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td valign=\"top\">&nbsp;</td>
        <td>".$comments_user."</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">";
     
						$imgCount=0;
						foreach($advertise as $image)
						{
						
						   $linkpic = $image['Advertise']['file'];
						   $imagePath = URL."uploadimage/add/".$linkpic;
						   $url = $image['Advertise']['url'];
						   $contents = "<a href=\"http://".$url."\"><img src=\"".$imagePath."\" alt=\"\" border=\"0\" ></a>";
										
						
						   if($imgCount%2 == 0)
						   {
						   echo "<tr>";
						   }
						   $comments .='<td width="50%" align="center" valign="top" class="newslr_border_b">'.$contents.'</td>';   
						  if($imgCount%2 == 1)
						   {
						   $comments .= "</tr>";
						   }
						$imgCount++;
						}
						
						 if($imgCount%2 == 1)
						   {
						   $comments .= "</tr>";
						   }
	  
	  
     $comments .="</table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>";
			
			
			$subject = $urname." would like to share a listing with you";
			
			$sent = mail($sendemail,$subject,$comments,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
					$message = 'yes';			
			}
			else
			$message = 'no';	
		}
	}
	
	
	function checkTwoArray($array1,$array2)
		{
			$check=false;	
			for($i=0;$i<sizeof($array1);$i++)
				{
					$check=false;
					
						for($j=0;$j<sizeof($array2);$j++)
							{								
								if($array2[$j]==$array1[$i])
									{											
										$check=true;	
										break;
									}
														
							}
							
							if($check==false)
							break;
					
				}
				
				return $check;
		}
			
	function checkTwoNeighborhood($array1,$array2)
		{
				
			for($i=0;$i<sizeof($array1);$i++)
				{
					$check=false;
					
						for($j=0;$j<sizeof($array2);$j++)
							{								
								if($array2[$j]==$array1[$i])
									{											
										$check=true;	
										break;
									}
														
							}
							
							if($check==true)
							break;
					
				}
				
				return $check;
		}
	
	
	
	public function searchUser($data,$listing_id) {
		
			
	
	$filter_array=array();
	$neighborhood_array=array();

	if(isset($this->data['Filterlisting']['filterid']))
		{			
			foreach($this->data['Filterlisting']['filterid'] as $value)
				{
					array_push($filter_array, $value);
				}
		}
	
	if(isset($this->data['Neighborhoodlisting']['neighborhood_id']))
		{			
			foreach($this->data['Neighborhoodlisting']['neighborhood_id'] as $value)
				{
					array_push($neighborhood_array, $value);
				}
		}
		
		
		
	if(isset($this->data['Listing']['price']) && !empty($this->data['Listing']['price']))
	$sql_select_search_id= "SELECT distinct us.id,u.email FROM `usersearches` us, `users` u WHERE  u.id=us.user_id AND `email_fire`='i' AND if (us.minprice<=0,'1',if (us.minprice<=".$this->data['Listing']['price'].",'1','0')) AND if (us.maxprice<=0,'1',if (us.maxprice>=".$this->data['Listing']['price'].",'1','0')) AND us.active='y' ";
        else
        $sql_select_search_id= "SELECT distinct us.id,u.email FROM `usersearches` us, `users` u WHERE  u.id=us.user_id AND `email_fire`='i'  AND us.active='y' ";
	
	$rs_select_search_id = mysql_query($sql_select_search_id);	
	
	$to='';
	
	//echo $sql_select_search_id;
	while($select_search_id=mysql_fetch_object($rs_select_search_id))
	{
		$all_neighborhood_of_searchname =array();
				
		$sql_select_neighborhood = "SELECT `neighborhood_id` FROM  `searchneighborhoods` WHERE `usersearch_id`=".$select_search_id->id."";
		
		$rs_select_neighborhood=mysql_query($sql_select_neighborhood);
		
		if(mysql_num_rows($rs_select_neighborhood) > 0)
			{
				while($select_neighborhood = mysql_fetch_object($rs_select_neighborhood))
					{
						array_push($all_neighborhood_of_searchname,$select_neighborhood->neighborhood_id);
					}
					
					$result=$this->checkTwoNeighborhood($neighborhood_array,$all_neighborhood_of_searchname);
					if($result==true)
					{
						
						$all_filter_of_searchname = array();
						$sql_select_filter = "SELECT `filter_id` FROM  `searchfilters` WHERE `usersearch_id`=".$select_search_id->id."";
						$rs_select_filter = mysql_query($sql_select_filter);
						if(mysql_num_rows($rs_select_filter) > 0)
							{
								while($select_filter=mysql_fetch_object($rs_select_filter))
									{
										array_push($all_filter_of_searchname,$select_filter->filter_id);
									}
									
									$result=$this->checkTwoArray($filter_array,$all_filter_of_searchname);
									if($result==true)
									$to.=$select_search_id->email.'^'.$select_search_id->id.',';					
							} else
							{			
								$to.=$select_search_id->email.'^'.$select_search_id->id.',';
							}
			
						
					}
										
			} else
			{			
				$to.=$select_search_id->email.'^'.$select_search_id->id.',';
			}
		
		
		
		
		
	}

		
		//////////////////////////////////////////////////////////////////////////
		
	if(!empty($to))
		{
        
			$headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: Straightforwardlistings<propertyalert@straightforwardlistings.com>' . "\r\n";
			
			
			
			
			$condition = "isdelete='0' and isblocked=0 AND type='p'";
			
			$filelds = array('id','name','file','url');
			
			$order_by = "rand()"; 	
			
			$advertise = $this->Advertise->findAllAdvertiseDetail($condition,$filelds,$order_by,NULL,NULL); 
			
			
			
			
			$condition = "Listing.id = ".$listing_id;
			
            $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
			$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
			
            $this->set('listingDetail',$listingDetail);
			$this->set('listing_id',$listing_id);
			$this->set('advertise', $advertise);
				
			$allAddress = explode(',',$to);

			foreach($allAddress as $value) {
			$toemail = explode('^',$value);
			$this->set('sid', $toemail[1]);
			$this->Email->template = 'email/confirm';
			$this->Email->to = $toemail[0];
                       /*$this->Email->bcc = 'amitra22@yahoo.com';
                        $this->Email->cc = 'chrisnycc@yahoo.com';*/
                        $this->Email->subject = 'Grab the new Property by Straightforwardlistings.com.';
                        $this->Email->from = 'propertyalert@straightforwardlistings.com';
                        $this->Email->fromName = 'Straightforwardlistings';
			            $result = $this->Email->send('email2');
			
			}

   
		
		//$subject = "Grab the new Property by straightforwardlistings.com.";
			
			//$sent = mail($to,$subject,$comments,$headers);
			
	
	
	
	//$sqlstr = "DELETE FROM $table WHERE id='$id'";
		}
	}
        
                //..................Display Page ....................//
    function managelisting($id = NULL)
     {    
                   $this->checkSession();
                   $this->pageTitle='Property Manager';
                   $this->layout="after_adminlogin";  
                
                $condition = "is_deleted=1";		
		
		$order_by = "Listing.id desc";   
		  
		$params1 = array();
                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
			
		
		$filelds = array('Listing.id','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
               
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));

		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
		             
                
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                               
				$neighborhood_list ='';
				/*foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{
                                               
						$condition = "Neighborhood.id ={$neighborhood_detail['id']}";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
					}*/
					
					$sql="SELECT neighborhoods.neighborhood_name FROM neighborhoods,neighborhoodlistings where neighborhoodlistings.neighborhood_id=neighborhoods.id AND neighborhoodlistings.listing_id={$neighborhood['Listing']['id']}"; 
					 
					 $rs = mysql_query($sql) or die(mysql_error().$sql);
				
					 while($rec=mysql_fetch_assoc($rs))
					{
						$neighborhood_list .=$rec['neighborhood_name'].',';	
					}
				   $neighborhood_list=substr($neighborhood_list,0,-1);
                 				
				
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);
        //echo count($neighborhood_data);
       // echo count($listingDetail);		
		//print_r($neighborhood_data);
		//exit(0);
		$this->set('neighborhoodDetail',$neighborhood_data);
		
                
                 if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		
      }
                
        function deleteproperty($id)
	{
		
		$this->checkSession();  
		
		if($this->Listing->deletelistingdetail1($id))
		{
		       $this->Session->setFlash('The Property is successfully Deleted');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }
   }
   
        function diactiveproperty($id)
	{
		
		$this->checkSession();  
		
		if($this->Listing->diactiveproperty($id))
		{
		       $this->Session->setFlash('The Property is successfully Deactive');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }
   }
   
   
    function activeproperty($id)
	{
		
		$this->checkSession();  
		
		if($this->Listing->activeproperty($id))
		{
		       $this->Session->setFlash('The Property is successfully Active');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }
   }
          
          
          function showactiveproperty($page=1)
 	{ 
                $this->checkSession();
                $this->layout = "after_adminlogin";
                $this->pageTitle = 'Active Property';	
		  	  		 
                  
                $condition = "is_deleted=1 AND active='1'";		
		
		$order_by = "Listing.id desc";   
		  
		$params1 = array();
                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
			
		
		$filelds = array('Listing.id','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
               
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));

		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
		             
                
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                               
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{
                                               
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);
                  
		  $this->render('managelisting');
	}
	
 //...Inactive USer start....
 	function showinactiveproperty($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Property';
		 $condition = "is_deleted=1 AND active='0'";		
		
		$order_by = "Listing.id desc";   
		  
		$params1 = array();
                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
			
		
		$filelds = array('Listing.id','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
               
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));

		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
		             
                
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                               
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{
                                               
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);
                  
		  $this->render('managelisting');
  }
  
  
  function showdeleteproperty($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Property';
		 $condition = "is_deleted='0'";		
		
		$order_by = "Listing.id desc";   
		  
		$params1 = array();
                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
			
		
		$filelds = array('Listing.id','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
               
                
                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));

		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
		             
                
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                               
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{
                                               
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);
                $this->set('typefunc','deleteProperty');
		  $this->render('managelisting');
  }
  
  
  function restoreproperty($id)
 	{
        
		
		$this->checkSession();  
		
		if($this->Listing->restoreproperty($id))
		{
		       $this->Session->setFlash('The Property is successfully Restore');
			   $controller=$this->params['controller'];
			   $action=$this->params['pass'][1];   
			   if(isset($this->params['pass'][2])!='')
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
				}
				else
				{
					 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
				}   
		 }

  }
  
  function addproperty($category_id=NULL)
  {
	$this->layout = "after_adminlogin";
        $this->pageTitle = 'Add Property';	
	
	
	 if(isset($_REQUEST['menu_id']))
		{			
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		
        $this->set('type','add');
        
        $condition = "isdelete='0' and isblocked= 0";
        
        $filelds = array('category_name','id');
        
        $order_by = "id asc"; 
        
        $category_data = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);    
        
        $this->set('categorydata',$category_data);	 
        
       
       if($category_id==NULL)
		 {
		 	$catID = $category_data[0]['Category']['id'];
		 }
		 else
		 {
			 $catID = $category_id;			 
		 }
			
		$this->set('selectCategory',$catID);	 
       
        
        $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
        
        $filelds = array('Filter.id','Filter.filter_name','Filter.require','Categoryfilter.id','Categoryfilter.category_filter_name');
        
        $order_by = "Categoryfilter.id asc,Filter.id asc";     
        $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
        
       
        $this->set('filterdata',$filterdata);	
	 
	 
        $condition = "isdelete='0' and isblocked= 0";
        
        $filelds = array('city_name','id');
        
        $order_by = "id asc"; 
        
        $cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
        
        $this->set('citydata',$cityData);	
		
        $condition = "isdelete='0' and isblocked= 0 and city_id = ".$cityData[0]['City']['id'];
        
        $filelds = array('neighborhood_name','id');
        
        $order_by = "neighborhood_name asc";     
        
        $this->set('neighborHood',$this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL));
		
		
		
			
		
		if(!empty($this->data))
		 {
		 
		 	
		 	
		    $this->data['Listing']['price'] = str_replace(",", "", $this->data['Listing']['price']);
			$this->data['Listing']['price'] = str_replace("$", "", $this->data['Listing']['price']);
			
			 $video_url = str_replace("http://", "", $this->data['Listing']['video_url']);			
			 $this->data['Listing']['video_url'] = str_replace("watch?v=", "v/",  $video_url);
			 
				if(isset($this->data['Listing']['broker_url']))
				$this->data['Listing']['broker_url'] = str_replace("http://", "", $this->data['Listing']['broker_url']);
				if(isset($this->data['Listing']['facebook']))
				$this->data['Listing']['facebook'] = str_replace("http://", "", $this->data['Listing']['facebook']);
				if(isset($this->data['Listing']['myspace']))
				$this->data['Listing']['myspace'] = str_replace("http://", "", $this->data['Listing']['myspace']);
				if(isset($this->data['Listing']['linkedin']))
				$this->data['Listing']['linkedin'] = str_replace("http://", "", $this->data['Listing']['linkedin']);
				if(isset($this->data['Listing']['otherone']))
				$this->data['Listing']['otherone'] = str_replace("http://", "", $this->data['Listing']['otherone']);
				if(isset($this->data['Listing']['othertwo']))
				$this->data['Listing']['othertwo'] = str_replace("http://", "", $this->data['Listing']['othertwo']);
				if(isset($this->data['Listing']['otherthree']))
				$this->data['Listing']['otherthree'] = str_replace("http://", "", $this->data['Listing']['otherthree']);
				
				if(isset($this->data['Listing']['smonth']) && isset($this->data['Listing']['sday']) && isset($this->data['Listing']['syear']))
		$this->data['Listing']['availabledate'] = $this->data['Listing']['syear'].'-'.$this->data['Listing']['smonth'].'-'.$this->data['Listing']['sday'];
			$this->data['Listing']['active'] = 1;
			
			if($this->Listing->save($this->data))
			{				
				 $this->Session->setFlash('Listing has been created and is now active.');
				$listing_id = mysql_insert_id();				
			}
			
			$this->data['Filterlisting']['listing_id'] = $listing_id;
			
			
			if(isset($this->data['Neighborhoodlisting']['neighborhood_id'])) {
			foreach($this->data['Neighborhoodlisting']['neighborhood_id'] as $value)
				{
					$this->Neighborhoodlisting->saveNeighborhood($value,$listing_id);
				}
			}
			
			if(isset($this->data['Filterlisting']['filterid'])) {
			foreach($this->data['Filterlisting']['filterid'] as $value)
				{
					$this->Filterlisting->saveFilter($value,$listing_id);
				}
			}
			
			
				
			foreach($this->data['Imagelisting'] as $key=> $value)
				{
				
					
					if(!empty($value['name']) && $value['type']=='image/jpeg')
					{
						//if($value['size'] <2097152)
						//{
						$fileName = mt_rand().'_'.$value['name'];
						
						$filestatus = move_uploaded_file($value['tmp_name'],$file_path."/".$fileName);			
						
						$source_fileName = $file_path."/".$fileName;						
						
						$thumb = $file_path."/thumb/".$fileName;
						$this->make_thumb($source_fileName,$thumb,300,200);
						$this->Imagelisting->insertImage($fileName,$listing_id);
						//}
					}				
					
				}	
			
		}				
 		
		
		
		
		
  }
  
  function editproperty($id = NULL,$category_id=NULL)
	{ 
	 
        $this->checkSession();
        $this->pageTitle='Edit Property';
        $this->layout="after_adminlogin";  
	   
	   $this->Listing->id=$id;	
	   $this->data = $this->Listing->read();
	   //print_r($this->data);exit;
	   $listnigdata = $this->data;
	   $this->set('listnigdata',$this->data); 
	   
	   $this->set('listing_id',$id); 
	   
	   //print_r($this->data); 
	  
	   $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('category_name','id');
		
		$order_by = "id asc"; 
		
		$category_data = $this->Category->findAllCategoryDetail($condition, $filelds, $order_by, NULL, NULL);    
		
                
		
		
		$this->set('categorydata',$category_data);	 
		
		if($category_id=='managelisting')
		 {
		 	 //$catID = $category_data[0]['Category']['id'];
			 $catID = $listnigdata['Listing']['category_id'];
		 }
		 else

		 {
			 $catID = $category_id;			 
		 }	
		$this->set('selectCategory',$catID);	 
		
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Filter.require','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc";     
	 
		$filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
		
		 $this->set('filterdata',$filterdata);	
		
		
		$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc"; 
		    
		$cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
		
		$this->set('citydata',$cityData);	
		
		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id = ".$this->data['City']['id'];
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "neighborhood_name asc";     
		
			
	   	$neighborHood = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		if(!empty($neighborHood) && $this->checkLang() != 'en')
		{
		 $neighborHood = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborHood);
		} 
		$this->set('neighborHood',$neighborHood);
	   	   
	    $this->set('type','edit');
	  
	   
	    $this->render('addproperty');
		
			
	}
        
        
        function viewproperty($listing_id=NULL)
 	{
		
                $this->checkSession();
                $this->pageTitle='View Property';
                $this->layout="after_adminlogin";  
		 $this->set('type','view');
		$this->cityData('city');
	        $this->listing_data = $this->Listing_counter->findByListing_id($listing_id);	
		
		
		$condition = "Listing.id = ".$listing_id." group by Listing.id";		
		$conditionN = $condition;
                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		$listingDetail = $this->Listing->allListing($condition, NULL, NULL, NULL, NULL);
		
		 //echo date("d-m-Y H:i:s", mktime())." - 2st begin\n\n";flush();
		 //echo '<br>';
		
		
		if($listingDetail)
		{
			
			$condition = "User.id = ".$listingDetail[0]['Listing']['user_id'];		
			
			$userInfo = $this->User->seletAlluser($condition, NULL, NULL, NULL, NULL);	
			
			$this->set('userInfo',$userInfo);	
		}
		
	
		
		/*$filter_data = array();
		foreach($listingDetail[0]['Filterlisting'] as $value)
			{
				
				
			$condition = "Filter.id = ".$value['filter_id']."";
			
			$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			$order_by = "Categoryfilter.id asc";     
	 
	 		array_push($filter_data,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			} */
                
                $filter_data = array();
                $allfilter = '';
		foreach($listingDetail[0]['Filterlisting'] as $value)
			{
				
			$allfilter .=$value['filter_id'].',';	
			//$condition = "Filter.id = ".$value['filter_id']."";
			
			//$filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
			
			//$order_by = "Categoryfilter.id,Filter.id asc";     
	 
	 		//array_push($filter_data,$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));	
			
			}
                        if($allfilter!='')
                        $allfilters = substr($allfilter,0, -1);
                        
                        $condition = "Filter.isdelete='0' AND Filter.id in($allfilters)";
                        
                        $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
                        
                        $order_by = "Categoryfilter.id asc";     
                        
                        $this->set('filterdata',$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL));
			
		$neighborhood_data = array();
		
		for($i=0;$i<sizeof($listingDetail[0]['Neighborhoodlisting'])-3; $i++)
			{
                               //echo $listingDetail[0]['Neighborhoodlisting'][0]['neighborhood_id']; exit;
				$neighborhood_list ='';
				//foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					//{						
						$condition = "Neighborhood.id = ".$listingDetail[0]['Neighborhoodlisting'][$i]['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					//}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		
		$this->set('neighborhoodDetail',$neighborhood_data);
                
                
	    
			
		
		//$this->set('filterdata',$filter_data);
                
                 
                
		$this->set('listingDetail',$listingDetail);	
	 	
		
	}
        
        
        function searchbyproperty($searchby=NULL, $searchkey=NULL,$page=1)
 		{
		
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Property Manager';
		 // print_r($this->params);
		  //exit;		  
                        if(isset($this->params['form']['srchkey'])) 
                              { 				    
                                      $searchkey=trim($this->params['form']['srchkey']);
                                      $this->set('searchkey',$searchkey);
                              }	 
                              else
                              {			
                                      $this->set('searchkey',$searchkey);
                                       $condition = "is_deleted=1 AND active='0'";		
                      
                                $order_by = "Listing.id desc";   
                                  
                                $params1 = array();
                                 $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
                                list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
                                        
                                
                                $filelds = array('Listing.id','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
                                
                               
                                
                                $this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting'),'$hasMany' => array('Filterlisting')));
                
                                $listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
                                             
                                
                                
                                $neighborhood_data = array();
                                
                                foreach($listingDetail as $neighborhood)
                                        {
                                               
                                                $neighborhood_list ='';
                                                foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
                                                        {
                                                               
                                                                $condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
                                                                $filelds = array('Neighborhood.neighborhood_name');			
                                                                $order_by = "Neighborhood.id asc";	 
                                                                $neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                                if(!empty($neighborhoodDetail))
                                                                {
                                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                                }
                                                        }
                                                array_push($neighborhood_data,$neighborhood_list);				
                                        }
			
		
		
			}
			  
				
                        if(isset($this->params['form']['searchby']))
                          {
                                $searchby=$this->params['form']['searchby'];
                                $this->set('searchby',$searchby);
                          }
		   else
		  		$this->set('searchby',$searchby);
			 	
		 
		  if(isset($searchby) && isset($searchkey))
		  {			
			                        
                        	if($searchby=='id')
                          $condition = "is_deleted=1 AND Listing.id LIKE '%".$searchkey."%' ";
                         else if($searchby=='address')
                         $condition = "is_deleted=1 AND (Listing.streetnumber LIKE '%".$searchkey."%' OR  Listing.streetname LIKE '%".$searchkey."%' OR CONCAT( streetnumber,' ',streetname ) LIKE '%".trim($searchkey)."%')";
                         
								else if($searchby=='neighborhood')
								{                        
										$neighborhood_detail = $this->Neighborhood->findByneighborhood_name(trim($searchkey));
										if($neighborhood_detail)
										{
												$condition =" Neighborhoodlisting.neighborhood_id = ".$neighborhood_detail['Neighborhood']['id']."";
										}
								}
								else if($searchby=='email')
								{
								  $user_email = $this->User->findByemail(trim($searchkey));
								  if($user_email)
								 {
								 //$condition = "is_deleted=1 AND (Listing.user_id LIKE '%".$user_email['User']['id']."%')";
								 $condition = "is_deleted=1 AND (Listing.user_id='".$user_email['User']['id']."')";
								 //print_r($user_email);
								 }
								 else
								 {
								 $condition = "is_deleted=1 AND (Listing.user_id LIKE 'NONE')";

								 }
								 
								}
                         
		$order_by = "Listing.id desc";   
		  
		$params1 = array();
                 $this->Listing->unbindModel(array('hasOne' => array('Imagelisting')));
		list($order,$limit,$page) = $this->Pagination->init($condition,$params1);	
			
		
		$filelds = array('Listing.id DISTINCT','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter');
		
               
                
                $this->Listing->unbindModel(array('hasOne' => array('Imagelisting')));

		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, $limit, $page);	
		             
                
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
                               
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{
                                               
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
                                                if(!empty($neighborhoodDetail))
                                                {
                                                        $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
                                                }
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);
		 }
		
		$this->render('managelisting');
		
        }
        
        function deleteall()
	 {
	  
                foreach($this->params['form']['chkUser'] as $id)
                 {                
				   $this->Listing->deletelistingdetail1($id);
				 }
				  $this->Session->setFlash('Listings(s) successfully Deleted');
                                  
				$controller=$this->params['controller'];
				//$action=$this->params['form']['referer'];
                                $action='managelisting';
                               
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	
	 
	  function addanotheraccount()
	 {
	  $user =  $this->User->findByemail($this->params['form']['email']);
	  if($user)
	  {
					foreach($this->params['form']['chkUser'] as $id)
					 {
							//echo $id ;
							
				   //$this->Listing->deletelistingdetail1($id);
				   
				   	mysql_query("UPDATE listings SET user_id={$user['User']['id']} WHERE id={$id}");
				 	}
				  $this->Session->setFlash("Listings(s) successfully Moved.");
	   }
	  else
	  {
	   $this->Session->setFlash("Listings(s) cannot be moved,Email not registered with us...");
	  }
                                  
				$controller=$this->params['controller'];
				//$action=$this->params['form']['referer'];
                                $action='managelisting';
                               
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 
		
	 }
        
        //..................................................//
		
	  
}
?>