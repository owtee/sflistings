<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class NeighborhoodsController extends AppController
{
  var $name = 'Neighborhoods'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Neighborhood','Adminmainmenu','Admin','City');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function manageneighborhood()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Neighborhood Management';
	 //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	 $cri="isdelete='0'"."order by id desc";
     list($order,$limit,$page) = $this->Pagination->init($cri);
	
	 $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page)); 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		}  
	 $admininfo=$this->Admin->admin_permission(); 
	 $this->set('result_check',$admininfo);
	 $arrCity = $this->Neighborhood->fetchcityfn();
	 $this->set('arrCity',$arrCity);
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activeneighborhood($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active Neighborhood';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactiveneighborhood($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive Neighborhood';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Neighborhood Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
		}
	  else
		{
		    $searchkey="";
		    $cri="isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="isdelete='0' AND neighborhood_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
	  $arrCity = $this->Neighborhood->fetchcityfn();
	 $this->set('arrCity',$arrCity);
	  //$this->redirect('/Neighborhoods/manageneighborhood');
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'Neighborhood Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND neighborhood_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND neighborhood_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'Neighborhood Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND neighborhood_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND neighborhood_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete neighborhood   ===============================


 function deleteneighborhood($id)
{
	$this->checkSession();  
    $this->Neighborhood->del($id);
	$this->Session->setFlash('The Neighborhood is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete neighborhood   ===============================

//=====================================   Start add neighborhood   ===============================
 	 

function addneighborhood()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New Neighborhood';
	  if(!empty($this->data))
        {
			if(empty($this->data['Neighborhood']['neighborhood_name']))
				{
				 	$this->Session->setFlash('Neighborhood name is empty !');
				 	
				 } 
				 
			else if($this->Neighborhood->findAllByneighborhood_name($this->data['Neighborhood']['neighborhood_name']))
				{
				 	$this->Session->setFlash('This neighborhood name is already exists !');
				 	$this->redirect('/neighborhoods/addneighborhood');
				 } 
		 	 else
		   		{
					$this->data['Neighborhood']['modifiedon']=date('Y-m-d');
					$this->data['Neighborhood']['createdon']=date('Y-m-d');
					$this->Neighborhood->save($this->data);
					$this->Session->setFlash('New Neighborhood is successfully added');
					//echo '<pre>';
						//print_r($this->data);
					//echo '</pre>';
					$this->redirect('/neighborhoods/manageneighborhood');
          		 }
         }
		 
		 $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id desc"; 
		    
		$cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
		$this->set('city_data',$cityData);
  }

//=====================================   End add neighborhood   ===============================

//=====================================   Start Edit neighborhood   ===============================


function editneighborhood($id = null)
{

   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->Neighborhood->id=$id;
	   $this->data = $this->Neighborhood->read();
	 }
   else
     { 

	  	if ($this->Neighborhood->save($this->data['Neighborhood']))
	       {
		        $idd=$this->data['Neighborhood']['id'];
		   		$updatefn=$this->Neighborhood->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->Neighborhood->execute("UPDATE `neighborhoods` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Neighborhood']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
				$this->Session->setFlash('Neighborhood name is successfully updated');
		 	    $this->redirect($controller.'/manageneighborhood'.'?page='.$pagenum);
		   }
    }
		 $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id desc"; 
		    
		$cityData = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
		$this->set('city_data',$cityData);
}
 
//=====================================   End Edit neighborhood   ===============================

//=====================================   Start View neighborhood   ===============================

function viewneighborhood($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View Neighborhood';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->Neighborhood->id = $id;
		$this->set('userdata',$this->Neighborhood->read(null,id));
  	}
   else
   {
	 $this->Neighborhood->id = $id;
     $this->set('userdata',$this->Neighborhood->read());
	}   
}

//=====================================   End View neighborhood   ===============================

//=====================================   Start delete neighborhood   ===============================


/*function deleteNeighborhood1($id)
	{   
	   $this->checkSession();	
       $this->Content->execute("update Contents  set isdelete='1' where id=".$id);
	   $this->Session->setFlash('The CMS is successfully deleted');
	   $controller=$this->params['controller'];
	   $action=$this->params['pass'][1];   
	   if($this->params['pass'][1]!='')
	    {
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][1].'&page='.$this->params['url']['page']);
		}
		else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}
    }
*/
//=====================================   End delete neighborhood   ===============================

//=====================================   Start block neighborhood   ===============================
function blockneighborhood($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->Neighborhood->blockneighborhoodfn($id);
		//$this->Neighborhood->execute("update Neighborhoods  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This Neighborhood is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block neighborhood   ===============================

//=====================================   Start Unblock neighborhood   ===============================

function unblockneighborhood($id=NULL)
	{
		 $this->Neighborhood->unblockneighborhoodfn($id);
		 //$this->Neighborhood->execute("update Neighborhoods  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This Neighborhood is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock neighborhood   ===============================

//=====================================   Start blockall neighborhood   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->Neighborhood->blockallfn($id);
			  //$this->Neighborhood->execute("update Neighborhoods  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall neighborhood   ===============================

//=====================================   Start Unblockall neighborhood   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->Neighborhood->unblockallfn($id);
		   //$this->Neighborhood->execute("update Neighborhoods  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall neighborhood   ===============================
	 
//=====================================   Start deleteall neighborhood   ===============================
	 
function deleteall()
	 {
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->Neighborhood->del($id);
		 	}
		 $this->Session->setFlash('Neighborhood(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall neighborhood   ===============================

//=====================================   Start searchbyletter neighborhood   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter neighborhood   ===============================

//=====================================   Start editactiveneighborhood neighborhood   ===============================

 function editactiveneighborhood($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active Neighborhood';
	  if(empty($this->data))
		{
			$this->Neighborhood->id=$this->params['pass'][0];
		    $this->data = $this->Neighborhood->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['Neighborhood']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->Neighborhood->id=$this->data['Neighborhood']['id'];
				   $this->Neighborhood->save($this->data);
				   $idd=$this->data['Neighborhood']['id'];
				   $this->Neighborhood->editactiveneighborhoodfn($idd);
				   //$this->Neighborhood->execute("UPDATE `Neighborhoods` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent neighborhood   ===============================

//=====================================   Start editinactiveneighborhood neighborhood   ===============================

/*function editinactivecontent($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive CMS';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
	               
			   if(empty($this->data))
				{
					//pr($this->params);
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Content->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Content->id=$this->data['Content']['id'];
							   $this->Content->save($this->data);
							   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
							  
							   
			           }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
					     if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
				}
	   }*/

//=====================================   End editactivecontent content   ===============================

//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND neighborhood_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND neighborhood_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Neighborhood->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================

//=====================================   Start searchbyname1 content   ===============================

/*
 	 function searchbyname1($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND neighborhood_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		
  }*/
//=====================================   End searchbyname1 content   ===============================

 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Neighborhood->find($cri));
   }
 //---------------end of page view-----------------  
 
 }
?>