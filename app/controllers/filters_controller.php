<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 06/12/2008
* Date of Modification: 
* Reason of the Controller: To Manage The Logic of content Model.
* Use Of This Class We Can addition,Deletion,Editing,Searching  of The 
* content.
*/
ob_start(); 

class FiltersController extends AppController
{
  var $name = 'Filters'; 
  var $helpers = array('Html', 'Form','javascript','pagination');
  var $uses=array('Filter','Adminmainmenu','Admin');
  var $components = array('Pagination'); 
  var $layout='alluser';
//=====================================   Start Manage Content  =====================================  
  function managefilter()
  {
  	 $this->checkSession();
     $this->layout = "after_adminlogin";
     $this->pageTitle = 'Filter Management';
	
	 $condition = "Filter.isdelete='0'";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Filter.modifiedon','Filter.require','Categoryfilter.category_filter_name','Filter.isblocked');
	 
	 $order_by = "Filter.id desc";
	 
     list($order,$limit,$page) = $this->Pagination->init($condition);
	 
	 $this->set('userdata',$this->Filter->findAllFilterDetail($condition, $filelds, $order_by, $limit, $page)); 
	 
	 if(isset($_REQUEST['menu_id']))
		{
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
		 
	 $admininfo=$this->Admin->admin_permission(); 
	 
	 $this->set('result_check',$admininfo);	 
  }
//=====================================   End Manage Content  =====================================  

//=====================================   Start Active Content  =====================================  

function activefilter($page=1)
  { 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Active Filter';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='0'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	 
 }
//=====================================   End Active Content  =====================================  

//=====================================   Start Inactive Content  =================================  

function inactivefilter($page=1)
 {
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Inactive Filter';
	  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  $cri="isdelete='0' AND isblocked='1'";
	  list($order,$limit,$page) = $this->Pagination->init($cri);
	  $this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);	
  }

//=====================================   End Inactive Content  =====================================  

//=====================================   Start Searchbyname Content  ===============================

 function searchbyname($page=1)
 {
		 
	  $this->checkSession();
	  $this->layout = "after_adminlogin";
	  $this->pageTitle = 'Filter Management';
	  if(isset($this->params['form']['srchkey'])) 
	  	{ 
			$searchkey=trim($this->params['form']['srchkey']);
		}
	  else
		{
		    $searchkey="";
		    $cri="Filter.isdelete='0'";
		    list($order,$limit,$page) = $this->Pagination->init($cri);
		    $this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
		 }
	  if(isset($this->params['form']['searchby']))
		 {
		    $searchby=$this->params['form']['searchby'];
		 }
	  else
	  	 {
		     $searchby="";
		 }	 
		
	  if(($searchby=="pname") && isset($searchkey))
	  	{
			$cri="Filter.isdelete='0' AND filter_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
	    }
	  if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			 $cri="Filter.isdelete='0' AND title like '".$searchkey."%'";
			 list($order,$limit,$page) = $this->Pagination->init($cri);
			 $this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
	  $admininfo=$this->Admin->admin_permission(); 
	  $this->set('result_check',$admininfo);
  }

//=====================================   End Searchbyname Content  =====================================  

//=====================================   Start srchbyoptionactive   =====================================  
function srchbyoptionactive()
	 {
	    $this->checkSession();
		$this->layout = "after_adminlogin";
		$this->pageTitle = 'Filter Management';
		if(isset($this->params['form']['srchkey']))
			{  
		   		 $searchkey=trim($this->params['form']['srchkey']);
			}
		else
		    {
		     	 $searchkey="";
				 $cri="isdelete='0'";
			     list($order,$limit,$page) = $this->Pagination->init($cri);
			     $this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
			}
		if(isset($this->params['form']['searchby']))
		   {
		   		 $searchby=$this->params['form']['searchby'];
		   }
		else
		   {
		       $searchby="";
		   }
		if(($searchby=="pname") && isset($searchkey))
		  {
				$cri="isdelete='0' AND filter_name like '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
				$cri="isdelete='0' AND filter_name LIKE '".$searchkey."%'";
				list($order,$limit,$page) = $this->Pagination->init($cri);
				$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
		  }
		$admininfo=$this->Admin->admin_permission(); 
	    $this->set('result_check',$admininfo); 
	  }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   End srchbyoptionactive   ===============================
function srchbyoptioninactive()
 {
	$this->checkSession();
	$this->layout = "after_adminlogin";
	$this->pageTitle = 'Filter Management';
	if(isset($this->params['form']['srchkey'])) 
		{ 
		    $searchkey=trim($this->params['form']['srchkey']);
		}	
	else
		{
		 	$searchkey="";
			$cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
		 }
	if(isset($this->params['form']['searchby']))
		{
		    $searchby=$this->params['form']['searchby'];
		 }
	else
		{
			$searchby="";
		}
	if(($searchby=="pname") && isset($searchkey))
		{
			$cri="isdelete='0' AND filter_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
		 }
	if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		{
			$cri="isdelete='0' AND filter_name LIKE '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));  
		}
	$admininfo=$this->Admin->admin_permission(); 
	$this->set('result_check',$admininfo);
 }

//=====================================   End srchbyoptionactive   ===============================

//=====================================   Start delete filter   ===============================


 function deletefilter($id)
{
	$this->checkSession();  
    $this->Filter->del($id);
	$this->Session->setFlash('The Filter is successfully Deleted');
    $controller=$this->params['controller'];
    $action=$this->params['pass'][1];   
    if(isset($this->params['pass'][2])!='')
		{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
		}
	else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}   
}

//=====================================   End delete filter   ===============================

//=====================================   Start add filter   ===============================
 	 

function addfilter()
  {
	  $this->layout = "after_adminlogin";
      $this->pageTitle = 'Add New Filter';
	  if(!empty($this->data))
        {
		
			if(empty($this->data['Filter']['filter_name']))
				{
				 	$this->Session->setFlash('Filter name is empty !');
				 	
				 } 
			else if($this->Filter->findAllByfilter_name($this->data['Filter']['filter_name']))
				{
				 	$this->Session->setFlash('This filter name is already exists !');
				 	$this->redirect('/filters/addfilter');
				 } 
		 	 else
		   		{
					if($this->data['Filter']['require']==1)
					$this->data['Filter']['require'] = 'y';
					else
					$this->data['Filter']['require'] = 'n';
					
					$this->data['Filter']['modifiedon']=date('Y-m-d');
					$this->data['Filter']['createdon']=date('Y-m-d');
					$this->Filter->save($this->data);
					$this->Session->setFlash('New Filter is successfully added');
					//echo '<pre>';
						//print_r($this->data);
					//echo '</pre>';
					$this->redirect('/filters/managefilter');
          		 }
         }
  }

//=====================================   End add filter   ===============================

//=====================================   Start Edit filter   ===============================


function editfilter($id = null)
{

   $this->checkSession();
   $this->layout='after_adminlogin';
   if(empty($this->data))
     {  
	   $this->Filter->id=$id;
	   $this->data = $this->Filter->read();
	 }
   else
     { 
	 		
			if($this->data['Filter']['require']==1)
			$this->data['Filter']['require'] = 'y';
			else
			$this->data['Filter']['require'] = 'n';
					
	  	if ($this->Filter->save($this->data['Filter']))
	       {
		   
		        $idd=$this->data['Filter']['id'];
		   		$updatefn=$this->Filter->updatefn($idd); 
	            $this->set('updatefn',$updatefn);	
				//$this->Filter->execute("UPDATE `filters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Filter']['id']."'");
     	  	    $controller=$this->params['controller'];
		        $pagenum=$this->params['form']['pagenum'];
				$this->Session->setFlash('Filter name is successfully updated');
		 	    $this->redirect($controller.'/managefilter'.'?page='.$pagenum);
		   }
    }
}
 
//=====================================   End Edit filter   ===============================

//=====================================   Start View filter   ===============================

function viewfilter($id=null)
 {
   $this->layout="after_adminlogin";
   $this->pageTitle='View Filter';
   if(!$id)
   	{
   		echo $id=$_REQUEST['id'];
        $this->Filter->id = $id;
		$this->set('userdata',$this->Filter->read(null,id));
  	}
   else
   {
	 $this->Filter->id = $id;
     $this->set('userdata',$this->Filter->read());
	}   
}

//=====================================   End View filter   ===============================

//=====================================   Start delete filter   ===============================


/*function deleteFilter1($id)
	{   
	   $this->checkSession();	
       $this->Content->execute("update Contents  set isdelete='1' where id=".$id);
	   $this->Session->setFlash('The CMS is successfully deleted');
	   $controller=$this->params['controller'];
	   $action=$this->params['pass'][1];   
	   if($this->params['pass'][1]!='')
	    {
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][1].'&page='.$this->params['url']['page']);
		}
		else
		{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
		}
    }
*/
//=====================================   End delete filter   ===============================

//=====================================   Start block filter   ===============================
function blockfilter($id=NULL)
	{
	 	$this->layout = "after_adminlogin";
		$this->Filter->blockfilterfn($id);
		//$this->Filter->execute("update Filters  set isblocked='1' where id=".$id);	 
		$this->Session->setFlash('This Filter is successfully blocked');
	    $controller=$this->params['controller'];
		$action=$this->params['pass'][1];   
		if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
//=====================================   End block filter   ===============================

//=====================================   Start Unblock filter   ===============================

function unblockfilter($id=NULL)
	{
		 $this->Filter->unblockfilterfn($id);
		 //$this->Filter->execute("update Filters  set isblocked='0' where id=".$id);
		 $this->Session->setFlash('This Filter is successfully Unblocked');
	     $controller=$this->params['controller'];
		 $action=$this->params['pass'][1];
		 if(isset($this->params['pass'][2])!='')
			{
				$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
		else
			{
				$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
    } 
	
//=====================================   End Unblock filter   ===============================

//=====================================   Start blockall filter   ===============================

function blockall()
	{
	   foreach($this->params['form']['chkUser'] as $id)
		 {
			  $this->Filter->blockallfn($id);
			  //$this->Filter->execute("update Filters  set isblocked='1' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully blocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 	{
		   		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 	}
		 else
			{
		  		 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 	}
	}

//=====================================   End blockall filter   ===============================

//=====================================   Start Unblockall filter   ===============================
		 

function unblockall()
	 {
	
		foreach($this->params['form']['chkUser'] as $id)
		 {
		   $this->Filter->unblockallfn($id);
		   //$this->Filter->execute("update Filters  set isblocked='0' where id=".$id);
		 }
		 $this->Session->setFlash('Record(s) successfully unblocked');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
		 {
		 	$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
		 }
		 else
		 {
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
		 }
	 }
	 
//=====================================   End Unblockall filter   ===============================
	 
//=====================================   Start deleteall filter   ===============================
	 
function deleteall()
	 {
	
	 	
		foreach($this->params['form']['chkUser'] as $id)
			{
		  		 $this->Filter->del($id);
		 	}
		 $this->Session->setFlash('Filter(s) successfully Deleted');
		 $controller=$this->params['controller'];
		 $action=$this->params['form']['referer'];
		 if($this->params['form']['key']!='')
			 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
			 }
		 else
			 {
			 	$this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
			 }
	 }

//=====================================   End deleteall filter   ===============================

//=====================================   Start searchbyletter filter   ===============================
	 	 
function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Member';
		$cri="isdelete='0' AND member_name LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
   	} 

//=====================================   End searchbyletter filter   ===============================

//=====================================   Start editactivefilter filter   ===============================

 function editactivefilter($page=1)
   {
	  $this->checkSession();
   	  $this->layout = "after_adminlogin";
   	  $this->pageTitle='Edit Active Filter';
	  if(empty($this->data))
		{
			$this->Filter->id=$this->params['pass'][0];
		    $this->data = $this->Filter->read(NULL,$this->params['pass'][0]);   
		}
	 else
	   {
			$id=$this->data['Filter']['id'];
			$action=$this->params['form']['action'];
			$referer=$this->params['form']['referer'];
			$controller=$this->params['controller'];
			$pagenum=$this->params['form']['pagenum'];
			if($this->data)
			  {
				   $this->Filter->id=$this->data['Filter']['id'];
				   $this->Filter->save($this->data);
				   $idd=$this->data['Filter']['id'];
				   $this->Filter->editactivefilterfn($idd);
				   //$this->Filter->execute("UPDATE `Filters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
			  }
			 $this->Session->setFlash('Selected record successfully updated!');
			 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
				{
					$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
				}
			 else
				{
					 if(!isset($pagenum))
					 	{
					  		$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
						 }
					 else
						 {
					   		$this->redirect($controller.'/'.$referer.'?page='.$pagenum);
					  	 }
				 }
		}
 }  
 
//=====================================   End editactivecontent filter   ===============================

//=====================================   Start editinactivefilter filter   ===============================

/*function editinactivecontent($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive CMS';
		//$this->set('panelmenus',$this->Adminmainmenu->findAll());
		//$this->set('vroles',$this->Countrie->generateList(NULL,NULL,NULL,'{n}.Countrie.cname','{n}.Countrie.cname'));
	               
			   if(empty($this->data))
				{
					//pr($this->params);
					//  $this->Plan->id=$id;
                    //  $this->data = $this->Plan->read();    
					$this->Content->id=$this->params['pass'][0];
					//$this->set('userdata',$this->Plan->read(NULL,$this->params['pass'][0]));
				    $this->data = $this->Content->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Content']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Content->id=$this->data['Content']['id'];
							   $this->Content->save($this->data);
							   $this->Content->execute("UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$this->data['Content']['id']."'");
							  
							   
			           }
							   
						 $this->Session->setFlash('Selected record successfully updated!');
					     if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							 }
						}
				}
	   }*/

//=====================================   End editactivecontent content   ===============================

//=====================================   Start searchbynameactive content   ===============================

	   
function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
		$cri="isdelete='0' AND isblocked='0' AND filter_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
	 }

//=====================================   End searchbynameactive content   ===============================

//=====================================   Start searchbynameinactive content   ===============================
	 
function searchbynameinactive($skey=NULL,$page=1)
	 {
		$this->checksession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='1' AND filter_name LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('userdata',$this->Filter->findAll($cri,NULL, NULL, $limit, $page));
	 }	
//=====================================   End searchbynameinactive content   ===============================

//=====================================   Start searchbyname1 content   ===============================

/*
 	 function searchbyname1($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'CMS Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND filter_name like '".$searchkey."%'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('userdata',$this->Content->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		
  }*/
//=====================================   End searchbyname1 content   ===============================

 //---------------front page view-----------------
 
  function view($pagename)
   {
    $this->layout = "site_template";
    $this->pageTitle = 'ServiceI.com - '.$pagename;
    $cri=("cmsurlname='".$pagename."'");
	$this->set('pagedata',$this->Filter->find($cri));
   }
 //---------------end of page view-----------------  
 
 }
?>