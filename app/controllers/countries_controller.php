<?php
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class CommercialusersController extends AppController
{
	var $name = 'Commercialusers'; 
	var $helpers = array('Html','javascript','pagination'); 
	var $uses=array('Commercialuser');
	var $components = array('Pagination');
    var $layout='alluser';
	
	//.......Start page................
   function index()
 	{
    $this->layout = "site_template";
    $this->pageTitle = 'thankster.com';
  	}
	
	//.......SignUp start................
  function signup()
  {
	$this->layout = "site_template";
    $this->pageTitle = 'chinatour.com - Create Account';
	//$this->set('panelmenus',$this->Adminmainmenu->findAll());
	$this->set('user_id',$this->Session->read('user_id'));
			
	  if(!empty($this->data))
        {
			if($this->Commercialusers->findAllByusername($this->data['Commercialusers']['loginid']))
			{
			 $this->Session->setFlash('This user is already exists !');
			 $this->redirect('/commercialusers/signup/');
			} 
		  else
		   {
		   
		   		if($this->params['form']['securitycode'] == $this->params['form']['captcha'])
					{
    							 $this->data['Commercialusers']['country']=$this->params['form']['country'];
								 $this->data['Commercialusers']['dob']=$this->params['form']['year']."-".$this->params['form']['month']."-".$this->params['form']['day'];
								 $this->data['Commercialusers']['createdon']=time();
								 $this->Commercialusers->save($this->data);
								 
								 $lastId=mysql_insert_id();
								 $this->Session->write("lastId",$lastId);
								 //$foldername = $this->data['Commercialusers']['loginid']; 
								// $destination = realpath('../../app/webroot/member/') . '/';				
								// $this->Newdir->createfolder($foldername,$destination); // Creation of folder
								 $this->redirect('/commercialusers/thankyou/');
								 $this->Session->setFlash('New user is successfully added!');
								 
								 
					}
						else
							{
									$this->Session->setFlash('Security code mismatch. Please enter the security code again.');
									$this->redirect('commercialusers/signup/');
							}
			}
        }
		
		
  }
  
 //......SignUp end.............
 
 //.....Login start.............
	
  function login() 
  {
	$this->layout = "site_template";
	$this->pageTitle='thankster.com - Login';
		
		
			if(!empty($this->data))
		 	{
					 $str=$this->data['Commercialusers']['loginid'];
					 $str1=$this->data['Commercialusers']['password'];
					 
					 $conditions=array ('loginid' => $str,'and'=>array('isblocked'=>'0','isdelete'=>'0','password'=>$str1));
					 if($this->Commercialusers->find($conditions))
					 {
						$user_details=$this->Commercialusers->find($conditions);
						$this->Commercialusers->execute("update commercialusers  set lastloginip='".$_SERVER['REMOTE_ADDR']."', lastlogindate='".date("Y-m-d")."', lastlogintime='".date("H:i:s")."' where id=".$user_details['Commercialusers']['id']);

						$this->Session->write('user_id',$user_details['Commercialusers']['id']);
						$this->Session->write('user_name',$user_details['Commercialusers']['loginid']);
						
				//*-------------Delete previous Step 1 -5 info if exist,to fource to create new stepping-----*//
					
				 $cri="ses_id='".$this->Session->read('user_id')."' order by id";
				
				 $cardinfo=$this->Card->find($cri);
				 $this->Card->del($cardinfo['Card']['id']);
				   
				 $addressbookinfo=$this->Addressbook->findAll($cri);
				 foreach($addressbookinfo as $val)
				   {
				   $this->Addressbook->delete($val['Addressbook']['id']);
				   }
				   
				   $messageinfo=$this->Message->findAll($cri);
				   foreach($messageinfo as $val)
				   {
				   $this->Message->del($val['Message']['id']);
				   }
				   
				   $commsginfo=$this->Commonmessage->find($cri);
				   $this->Commonmessage->del($commsginfo['Commonmessage']['id']);
						
						
				//-----------------------------End of deleting info--------------------------------------
						$this->redirect('/');				
					  }
					  else
					  {
					  		 $this->Session->setFlash('Invalid Login, Try again !');
					  }
			 }
			else
			 {
					   $this->validateErrors($this->Commercialusers);
					   
			 }
}
 
 
  //.....Login end and logout start.................. 
  function logout()
  {
   session_unset($_SESSION['user_id']);
   session_unset($_SESSION['user_name']);
   $this->redirect('/');
  } 
  
   //...logout end...............
   
   
   //........myaccount start...............
 function myaccount()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area';
	$logged_user_id=$this->Session->read('user_id');  
	$cri="id='$logged_user_id'";
	$this->set('logged_user',$this->Commercialusers->find($cri)); 
  }
  //...myaccount end...........................
  
  //........myaccount contactinfo start...............
  function myaccount_contactinfo()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - Contact Info';
	$logged_user_id=$this->Session->read('user_id');  
	if(!empty($this->data))
        {
		  $this->data['Commercialusers']['id']=$logged_user_id;
		  $this->data['Commercialusers']['country']=$this->params['form']['country'];
		  $this->data['Commercialusers']['modifiedon']=time();
		  $this->Commercialusers->save($this->data);
		  $this->Session->setFlash('Your profile is successfully updated!');
		}
	$cri="id='$logged_user_id'";
	$this->set('logged_user',$this->Commercialusers->find($cri)); 
  }
  //........myaccount contactinfo end...............
   
  //........myaccount changepassword start...............
  function myaccount_changepassword()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - Change Password';
	  
	
	if(!empty($this->data))
        {
		
		  $logged_user_id=$this->Session->read('user_id');
		  $cri="id='$logged_user_id'";
	      $logged_user=$this->Commercialusers->find($cri);
		  $logged_user_password=$logged_user['Commercialusers']['password'];
		  
		 if($this->data['Commercialusers']['oldpassword']==$logged_user_password)
		   {
		     $this->Commercialusers->execute("update commercialusers  set password='".$this->data['Commercialusers']['newpassword']."' where id=".$logged_user['Commercialusers']['id']);
		     $this->Session->setFlash('The Password is successfully updated!');
			 $this->redirect('/commercialusers/myaccount_changepassword/');
		   }
		  else
		   {
		     $this->Session->setFlash('Old Password mismatch. Please enter again!');
			 $this->redirect('/commercialusers/myaccount_changepassword/');
		   } 
		} 
  }
  //........myaccount changepassword end............... 
   
  //........myaccount changeservice start...............
  function myaccount_changeservice()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - Change Service';
	$logged_user_id=$this->Session->read('user_id');  
	$cri="id='$logged_user_id'";
	$this->set('logged_user',$this->Commercialusers->find($cri)); 
  }
  //........myaccount changeservice end............... 
   
  //........myaccount cancelservice start...............
  function myaccount_cancelservice()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - Cancel Service';
	$logged_user_id=$this->Session->read('user_id');  
	$cri="id='$logged_user_id'";
	$this->set('logged_user',$this->Commercialusers->find($cri)); 
  }
  //........myaccount cancelservice end............... 
  //........myaccount mysavedworks start...............
  function mysavedworks()
  {
    $this->checkuserSession(); 
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Welcome to Member Area - My Saved Works';
	$logged_user_id=$this->Session->read('user_id');  
	$cri="ses_id='$logged_user_id'";
	$this->set('myworks',$this->Save_work->find($cri)); 
  }
  //........myaccount mysavedworks end...............  
   
   
  //...after signup.......
 function thankyou()
  {
	$this->layout = "site_template";
    $this->pageTitle = 'thankster.com - thankyou';
	$this->set('user_id',$this->Session->read('user_id'));
  }
  //.......end.............
  function cards()
  {
     $this->checkuserSession(); 
    $this->layout = "site_template";
    $this->pageTitle = 'thankster.com - Select the cards';
  }
   function managecommercialuser()
  {
   $this->checkSession();
   $this->pageTitle='Commercialusers Manager';
   $this->layout="after_adminlogin";
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   $cri="isdelete='0'"."order by id desc";
   list($order,$limit,$page) = $this->Pagination->init($cri);
   $this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page)); 
   if(isset($_REQUEST['menu_id']))
		{
			//$_SESSION['isblocked']='';
			//$_SESSION['skey']='';
			$_SESSION['menu_id']=$_REQUEST['menu_id'];
		} 
  }
  //...Add New Commercialusers.......
  function adduser()
  {
	$this->layout = "after_adminlogin";
    $this->pageTitle = 'Add New Commercialusers';
	//$this->set('panelmenus',$this->Adminmainmenu->findAll());
	  if(!empty($this->data))
        {
			if($this->Commercialuser->findAllByloginId($this->data['Commercialuser']['loginid']))
			{
			 $this->Session->setFlash('This user is already exists !');
			 $this->redirect('/commercialusers/managecommercialuser');
			} 
		  else
		   {
			 $this->data['Commercialuser']['logindate']=time();
			 $this->Commercialuser->save($this->data);
			 $this->redirect('/commercialusers/managecommercialuser');
             $this->Session->setFlash('New user is successfully added');
			}
        }
  }
 //....End Add New Commercialusers end..........
  //....Delete Commercialusers start...........
 	function deleteuser($id)
{
	$this->checkSession();
	//echo   $id;
    //$this->Commercialuser->del($id);
	$this->Commercialuser->execute("delete from commercialusers where id=".$id);
	$this->Session->setFlash('The Commercialusers is successfully Deleted');
           $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			 $this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}   
}
 //....Delete Commercialusers end...........
 //....Block Commercialusers start..........
 	function blockuser($id=NULL)
	{
	$this->layout = "after_adminlogin";
	$this->Commercialuser->execute("update commercialusers  set isblocked='1' where id=".$id);	 
	 $this->Session->setFlash('This Commercialusers is successfully blocked');
	      $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];   
		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	} 
 //....Block Commercialusers end.........
 //....UnBlock Commercialusers start.....
 	function unblockuser($id=NULL)
	{
	//$this->Plan->saveField('isblocked','0');
	 $this->Commercialuser->execute("update commercialusers  set isblocked='0' where id=".$id);
	$this->Session->setFlash('This user is successfully Unblocked');
	       $controller=$this->params['controller'];
		   $action=$this->params['pass'][1];
		   		   if(isset($this->params['pass'][2])!='')
			{
			$this->redirect($controller.'/'.$action.'?skey='.$this->params['pass'][2].'&page='.$this->params['url']['page']);
			}
			else
			{
			$this->redirect($controller.'/'.$action.'?page='.$this->params['url']['page']);
			}
	//$this->redirect('/Plans/managecommercialuser');	
   } 
 //.......Unblock Commercialusers end......
 //........................................................................................BlockAll Admin start
 	function blockall()
	 {
	    // pr($this->params['form']);
	 			foreach($this->params['form']['chkCommercialusers'] as $id)
			     {
				   $this->Commercialuser->execute("update commercialusers  set isblocked='1' where id=".$id);
				 }
				  $this->Session->setFlash('Record(s) successfully blocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				   $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				   $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	   }	
 //........................................................................................BlockAll Admin end
 //.........................................................................................UnBlockAll start
 	function unblockall()
	 {
	 //pr($this->params['form']);
	 		    foreach($this->params['form']['chkCommercialusers'] as $id)
			     {
				   $this->Commercialuser->execute("update commercialusers  set isblocked='0' where id=".$id);
				 }
				  $this->Session->setFlash('Record(s) successfully unblocked');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				 if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //..........................................................................................UnblockAll end
 //............................................................................................Delete All start
 	function deleteall()
	 {
	  //pr($this);
	 		    foreach($this->params['form']['chkCommercialusers'] as $id)
			     {
				   $this->Commercialuser->del($id);
				 }
				  $this->Session->setFlash('Commercialusers(s) successfully Deleted');
				 $controller=$this->params['controller'];
				 $action=$this->params['form']['referer'];
				
				if($this->params['form']['key']!='')
				 {
				 $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['key'].'&page='.$this->params['form']['pagenum']);
				 }
				 else
				 {
				 $this->redirect($controller.'/'.$action.'?page='.$this->params['form']['pagenum']);
				 }
	 }	 
 //...Delete All end......
 //...Active Commercialusers start......
 	function activeuser($page=1)
 	{ 
		  $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Active Commercialusers';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  $cri="isdelete='0' AND isblocked='0'";
		  
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  $this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));
		  //$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));   
	}
 //...Active Commercialusers end........
 //...Inactive USer start....
 	function inactiveuser($page=1)
 	{
          $this->checkSession();
          $this->layout = "after_adminlogin";
          $this->pageTitle = 'Inactive Commercialusers';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  $cri="isdelete='0' AND isblocked='1'";
		 
		  list($order,$limit,$page) = $this->Pagination->init($cri);
		  $this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));  
  }
 //....Inactive Commercialusers end............
 //................................................................................................Search ByName Active start
 	function searchbynameactive($skey=NULL,$page=1)
	 {
		$this->checkSession();
   		$this->layout = "after_adminlogin";
   		//$this->pageTitle='Edit Admin';
		$cri="isdelete='0' AND isblocked='N' AND loginid LIKE '".$this->params['url']['skey']."%'";
		$this->set('skey',$this->params['url']['skey']);
		$cond=0;
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));
	 }
 //.................................................................................................Search ByName Active end
 //..................................................................................................Search By letter Start
 	function searchbyletter()
	{
        $this->checkSession();
	    $this->layout = "after_adminlogin";
        $this->pageTitle = 'Manage Commercialusers';
		$cri="isdelete='0' AND loginid LIKE '".$this->params['url']['skey']."%'";
	  	$this->set('skey',$this->params['url']['skey']);
		
		list($order,$limit,$page) = $this->Pagination->init($cri);
		$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));
   	} 
 //...................................................................................................Search By letter End
 //....................................................................................................Search By Name Start
 	 function searchbyname($page=1)
 		{
		 //pr($this->params);
		  //exit();
		  $this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Commercialusers Management';
		  //$this->set('panelmenus',$this->Adminmainmenu->findAll());
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));
			//$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND loginid = '".$searchkey."'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($searchkey))
		  {
			$cri="isdelete='0' AND company_email = '".$searchkey."'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialuser->findAll($cri,NULL, NULL, $limit, $page));  
		}
  }
 //.....................................................................................................Search By Name End
 
 //......................................................................................................Search By Option Active start
 	function srchbyoptionactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Commercialusers Management';
		  $this->set('panelmenus',$this->Adminmainmenu->findAll());
		  
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND loginid = '".$searchkey."'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="isdelete='0' AND email = '".$searchkey."'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //........................................................................................................Search By Option Active end
 //......................................................................................................Search By Option Active start
 	function srchbyoptioninactive()
	  {
	    //pr($this->params);
		//exit();
		$this->checkSession();
		  $this->layout = "after_adminlogin";
		  $this->pageTitle = 'Commercialusers Management';
		  $this->set('panelmenus',$this->Adminmainmenu->findAll());
		  
		  if(isset($this->params['form']['srchkey']))  
		    $searchkey=$this->params['form']['srchkey'];
		  else
		    {
		      $searchkey="";
			  $cri="isdelete='0'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));
			  }
		  if(isset($this->params['form']['searchby']))
		   {
		    $searchby=$this->params['form']['searchby'];
		   }
		   else
		     $searchby="";
			//pr($this->params['form']);  
		  if(($searchby=="pname") && isset($searchkey))
		  {
			$cri="isdelete='0' AND loginid = '".$searchkey."'";
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));  
		 }
		if(($searchby=="ptitle") && isset($this->params['form']['srchkey']))
		  {
			$cri="isdelete='0' AND email = '".$searchkey."'";
				
			list($order,$limit,$page) = $this->Pagination->init($cri);
			$this->set('admindata',$this->Commercialusers->findAll($cri,NULL, NULL, $limit, $page));  
		}
	  }
 //........................................................................................................Search By Option Active end
 //..........................................................................................................Edit Active Admin start
 	 function editactiveuser($page=1)
   {
	     $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit Active user';
		$this->set('panelmenus',$this->Adminmainmenu->findAll());
			   if(empty($this->data))
				{
					
					//pr($this->params);
					$this->Commercialusers->id=$this->params['pass'][0];
				    $this->data = $this->Commercialusers->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						$id=$this->data['Commercialusers']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Commercialusers->id=$this->data['Commercialusers']['id'];
							   $this->Commercialusers->save($this->data);
							   }
							  
						//pr($this->params);
						//exit();
						 $this->Session->setFlash('Selected record successfully updated!');
						if(isset($this->params['form']['skey']) && ($this->params['form']['skey']!=''))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							  }
						}
				}
 }  
 //............................................................................................................Edit Active Admin end
 //.............................................................................................................Edit Inactive Admin start
 	function editinactiveuser($page=1)
   {
	    $this->checkSession();
   		$this->layout = "after_adminlogin";
   		$this->pageTitle='Edit inavtive user';    
		$this->set('panelmenus',$this->Adminmainmenu->findAll());    
			   if(empty($this->data))
				{
					//pr($this->params);
					$this->Commercialusers->id=$this->params['pass'][0];
				    $this->data = $this->Commercialusers->read(NULL,$this->params['pass'][0]);   
				 }
				else
				{
						//pr($this);
						
						$id=$this->data['Commercialusers']['id'];
						$action=$this->params['form']['action'];
						$referer=$this->params['form']['referer'];
					 	$controller=$this->params['controller'];
						$pagenum=$this->params['form']['pagenum'];
							 if($this->data)
							   {
							   $this->Commercialusers->id=$this->data['Commercialusers']['id'];
							   $this->Commercialusers->save($this->data);
			           			}
							   
						 $this->Session->setFlash('Selected record successfully updated!');
						 //$this->redirect('commercialusers/inactiveuser');
					 if(isset($this->params['form']['skey']) && ($this->params['form']['skey']))
					    {
						$this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey'].'&page='.$pagenum);
						}
						else
						{
						     if(!isset($pagenum))
							 {
							  $this->redirect($controller.'/'.$action.'?skey='.$this->params['form']['skey']);
							 }
							 else
							 {
						       $this->redirect($controller.'/'.$referer.'?page='.$pagenum);
							  }
						}
				}
	   }
	   
	   
  //..................................................................................................Manage Admin end 
 function viewuser($id=null)
 {
   $this->checkSession();
   $this->layout="after_adminlogin";
   $this->pageTitle='View Commercialusers';
   //$this->set('panelmenus',$this->Adminmainmenu->findAll());
   if(!$id)
   {
   		echo $_REQUEST['id'];
		exit();
        $this->Commercialuser->id = $id;
		$this->set('admindata',$this->Commercialuser->read(null,id));
     ///it should leave blank
   }
   else
   {
   	 //echo $_REQUEST['id'];
	 //$id=$_REQUEST['id'];
	 $id;
	 $this->Commercialuser->id = $id;
     $this->set('admindata',$this->Commercialuser->read());
   } 
   
 }
 
 
 
function edituser($id = null)
{
   $this->checkSession();
   $this->layout='after_adminlogin';
   
   if(empty($this->data))
    {  
        $this->Commercialuser->id=$id;
		$this->data = $this->Commercialuser->read();
	}

			else
				{
				 $gte=$this->Commercialuser->findByusername($this->data['Commercialusers']['loginid']);
	   
					if($gte && $gte['Commercialusers']['id']!=$this->data['Commercialusers']['id'])
					{
						 $this->Session->setFlash('This user is already exists ! Please try with a diffrent user name.');
					}
					else
					{
				    $this->data['Commercialusers']['modifiedon']=time();
		 			$this->Commercialuser->save($this->data['Commercialusers']);
					$this->Session->setFlash('The record is successfully edited');
			        }
			 		$controller=$this->params['controller'];
				    $pagenum=$this->params['form']['pagenum'];		
					$this->redirect($controller.'/managecommercialuser'.'?page='.$pagenum);
 	  }
    //}
  }
 
}
?>