<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from categoryfilters table
* This class represent the categoryfilters table. It has some function that will provide details 
* of categoryfilter listing depending upon the conditions.
*/
class Categoryfilter extends AppModel
{
  // This name point to the categoryfilters table and can be accessed by controller the categoryfilter table by this name.
    var $name = 'Categoryfilter';
	//var $hasOne = array('Filter');
	public function updatefn($id)
	  {
	    $updatefn="UPDATE `categoryfilters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockcategoryfilterfn($id)
	  {
	    echo $blockcategoryfilterfn="update categoryfilters  set isblocked='1' where id='".$id."'";
		$blockcategoryfilterfn=mysql_query($blockcategoryfilterfn);
		$blockcategoryfilterfn=mysql_fetch_array($blockcategoryfilterfn);
		return ($blockcategoryfilterfn);
	  }
	public function unblockcategoryfilterfn($id)
	  {
	    echo $unblockcategoryfilterfn="update categoryfilters  set isblocked='0' where id='".$id."'";
		$unblockcategoryfilterfn=mysql_query($unblockcategoryfilterfn);
		$unblockcategoryfilterfn=mysql_fetch_array($unblockcategoryfilterfn);
		return ($unblockcategoryfilterfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update categoryfilters  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update categoryfilters  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactivecategoryfilterfn($id)     
		{
			$editactivecategoryfilterfn="UPDATE `categoryfilters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactivecategoryfilterfn=mysql_query($editactivecategoryfilterfn);
		}	
  
}

?>