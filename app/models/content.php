<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from contents table
* This class represent the contents table. It has some function that will provide details 
* of content listing depending upon the conditions.
*/
class Content extends AppModel
{
  // This name point to the contents table and can be accessed by controller the content table by this name.
    var $name = 'Content';
	public function updatefn($id)
	  {
	    $updatefn="UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockcontentfn($id)
	  {
	    $blockcontentfn="update contents  set isblocked='1' where id='".$id."'";
		$blockcontentfn=mysql_query($blockcontentfn);
		$blockcontentfn=mysql_fetch_array($blockcontentfn);
		return ($blockcontentfn);
	  }
	public function unblockcontentfn($id)
	  {
	    $unblockcontentfn="update contents  set isblocked='0' where id='".$id."'";
		$unblockcontentfn=mysql_query($unblockcontentfn);
		$unblockcontentfn=mysql_fetch_array($unblockcontentfn);
		return ($unblockcontentfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update contents  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update contents  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactivecontentfn($id)     
		{
			$editactivecontentfn="UPDATE `contents` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactivecontentfn=mysql_query($editactivecontentfn);
		}	
    public function getcontent($id='')
	{
		$condition = '';
		$condition = $id!=''?" AND id = ".$id."":"";
		$sql = 'SELECT * FROM `contents` WHERE 1'.$condition;
		$rs = mysql_query($sql) or die(mysql_error().$sql);
		$description = array();
		$rec = mysql_fetch_assoc($rs);
		$description = $rec['description'];
		return $description;
	}
}

?>