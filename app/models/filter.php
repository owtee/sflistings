<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from filters table
* This class represent the filters table. It has some function that will provide details 
* of filter listing depending upon the conditions.
*/
class Filter extends AppModel
{
  // This name point to the filters table and can be accessed by controller the filter table by this name.
    var $name = 'Filter';
	var $belongsTo = array('Categoryfilter' =>
                        array('className'    => 'Categoryfilter',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'categoryfilters_id'
                        )
                  );
				  
		

	public function updatefn($id)
	  {
	    $updatefn="UPDATE `filters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockfilterfn($id)
	  {
	    echo $blockfilterfn="update filters  set isblocked='1' where id='".$id."'";
		$blockfilterfn=mysql_query($blockfilterfn);
		$blockfilterfn=mysql_fetch_array($blockfilterfn);
		return ($blockfilterfn);
	  }
	public function unblockfilterfn($id)
	  {
	    echo $unblockfilterfn="update filters  set isblocked='0' where id='".$id."'";
		$unblockfilterfn=mysql_query($unblockfilterfn);
		$unblockfilterfn=mysql_fetch_array($unblockfilterfn);
		return ($unblockfilterfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update filters  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update filters  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactivefilterfn($id)     
		{
			$editactivefilterfn="UPDATE `filters` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactivefilterfn=mysql_query($editactivefilterfn);
		}
			
  public function findAllFilterDetail($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
	}

?>