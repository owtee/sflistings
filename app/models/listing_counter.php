<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Listing_counter extends AppModel
{
    var $name = 'Listing_counter';
	var $belongsTo  = array('Listing' =>
                         array('className'     => 'Listing',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => '',
                               'exclusive'     => false,
                               'finderQuery'   => '',
                         )
                  );
	public function allListing($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			 
			 return $all_result;
		}
		
	 public function deletelistingdetail($listing_id)     
		{	
			
			if($this->del($listing_id))			 
			 return true;
			 else
			 return false;
		}
		
	public function available($listing_id)     
		{	
			
			$sql = "UPDATE listings SET sold = 1 WHERE id=  $listing_id";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
		
	public function addnewsletter($listing_id,$user_id)     
		{	
			
			$sql = "INSERT INTO newsletters (user_id,listing_id) VALUES ($user_id,$listing_id)";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
		
	public function existNewsletter($listing_id,$user_id)     
		{	
			
			$sqlexistNewsletter = "SELECT * FROM newsletters WHERE user_id = $user_id AND listing_id = $listing_id";
			$rsExistNewsletter = mysql_query($sqlexistNewsletter);
			if(mysql_num_rows($rsExistNewsletter) > 0)
			return true;
			else
			return false;
		}
}
?>