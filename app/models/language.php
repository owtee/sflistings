<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Language extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Language';
	
	public function findAllLanguageDetail($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
  
}

?>