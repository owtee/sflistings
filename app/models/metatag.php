<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Metatag extends AppModel
{
    var $name = 'Metatag';
	public function allListing($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			 
			 return $all_result;
		}
		
	 public function deletelistingdetail($listing_id)     
		{	
			
			if($this->del($listing_id))			 
			 return true;
			 else
			 return false;
		}
		
	public function available($listing_id)     
		{	
			
			$sql = "UPDATE listings SET sold = 1 WHERE id=  $listing_id";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
		
	public function addnewsletter($listing_id,$user_id)     
		{	
			
			$sql = "INSERT INTO newsletters (user_id,listing_id) VALUES ($user_id,$listing_id)";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
		
	public function findAllMeta()     
		{	
			
			$sql = "SELECT * FROM `metatags`";
			$rs = mysql_query($sql);
			if(mysql_num_rows($rs) > 0)
			{
			    $rec = mysql_fetch_assoc($rs);
			    return $rec;
			}
			else
			return false;
		}
}
?>