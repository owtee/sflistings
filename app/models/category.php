<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from categories table
* This class represent the categories table. It has some function that will provide details 
* of category listing depending upon the conditions.
*/
class Category extends AppModel
{
  // This name point to the categories table and can be accessed by controller the category table by this name.
    var $name = 'Category';
	public function updatefn($id)
	  {
	    $updatefn="UPDATE `categories` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockcategoryfn($id)
	  {
	    echo $blockcategoryfn="update categories  set isblocked='1' where id='".$id."'";
		$blockcategoryfn=mysql_query($blockcategoryfn);
		$blockcategoryfn=mysql_fetch_array($blockcategoryfn);
		return ($blockcategoryfn);
	  }
	public function unblockcategoryfn($id)
	  {
	    echo $unblockcategoryfn="update categories  set isblocked='0' where id='".$id."'";
		$unblockcategoryfn=mysql_query($unblockcategoryfn);
		$unblockcategoryfn=mysql_fetch_array($unblockcategoryfn);
		return ($unblockcategoryfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update categories  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update categories  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactivecategoryfn($id)     
		{
			$editactivecategoryfn="UPDATE `categories` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactivecategoryfn=mysql_query($editactivecategoryfn);
		}	
		
	public function findAllByOption($id)     
		{
			$received_data=$this->findAllByid($id);
			return($received_data);
		}	
		
	public function findAllCategoryDetail($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
  
}

?>