<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Eventsendaddress extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Eventsendaddress';
	
        
	public function saveAddress($event_id,$address)
		{
                   $sqlExistData = "SELECT * FROM eventsendaddresses WHERE event_id=$event_id AND email='$address'";
                   $rsExistData = mysql_query($sqlExistData);
                   if(mysql_num_rows($rsExistData)==0)
                   {
                      $sql_insert_address = "Insert into eventsendaddresses(event_id,email) values ($event_id,'$address')";
		      $rs_insert_address = mysql_query($sql_insert_address);
                      if($rs_insert_address)
                      return true;
                      else
                      return false;
                   }
		}	
		
		public function allEventAddress($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
		
		public function deleteeventaddress($address_id)     
		{				
		    if($this->del($address_id))
		    return true;
		    else
		    return false;
		}
                
                public function eventresponse($address_id,$status)     
		{				
		    $sql_Update_status = "Update eventsendaddresses set response='$status' WHERE id=$address_id";
                    if(mysql_query($sql_Update_status))
		    return true;
		    else
		    return false;
		}
  
}

?>