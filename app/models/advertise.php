<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of Advertise listing depending upon the conditions.
*/
class Advertise extends AppModel
{
  // This name point to the cities table and can be accessed by controller the Advertise table by this name.
    var $name = 'Advertise';
	public function updatefn($id)
	  {
	    $updatefn="UPDATE `cities` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockAdvertise($id)
	  {
	    $blockAdvertisefn="update advertises  set isblocked='1' where id='".$id."'";
		if(mysql_query($blockAdvertisefn))		
		return true;
		else
		return false;
	  }
	public function unblockAdvertise($id)
	  {
	     $blockAdvertisefn="update advertises  set isblocked='0' where id='".$id."'";
		if(mysql_query($blockAdvertisefn))		
		return true;
		else
		return false;
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update cities  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update cities  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactiveAdvertisefn($id)     
		{
			$editactiveAdvertisefn="UPDATE `cities` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactiveAdvertisefn=mysql_query($editactiveAdvertisefn);
		}	
		
		public function findAllAdvertiseDetail($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
  
  	public function availableAdvertisename($name,$id=NULL,$type=NULL)
	  {	 
	  
	  	$cri = "name ='$name' AND type = '$type'";	
		if(!empty($id))
		$cri .="AND id <> $id";	
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function availableEmail($url,$id=NULL,$type=NULL)
	  {	 
	  	$cri = "url ='$url' AND type = '$type'";
		if(!empty($id))
		$cri .="AND id <> $id";			
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function deleteAdvertise($id)
	  {	 
	  	if($this->del($id))
		return true;
		else
		return false;
	  }
	  	public function findAlladvertisesDetail($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
}

?>