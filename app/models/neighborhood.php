<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from neighborhoods table
* This class represent the neighborhoods table. It has some function that will provide details 
* of neighborhood listing depending upon the conditions.
*/
class Neighborhood extends AppModel
{
  // This name point to the neighborhoods table and can be accessed by controller the neighborhood table by this name.
    var $name = 'Neighborhood';
	public function updatefn($id)
	  {
	    $updatefn="UPDATE `neighborhoods` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockneighborhoodfn($id)
	  {
	    echo $blockneighborhoodfn="update neighborhoods  set isblocked='1' where id='".$id."'";
		$blockneighborhoodfn=mysql_query($blockneighborhoodfn);
		$blockneighborhoodfn=mysql_fetch_array($blockneighborhoodfn);
		return ($blockneighborhoodfn);
	  }
	public function unblockneighborhoodfn($id)
	  {
	    echo $unblockneighborhoodfn="update neighborhoods  set isblocked='0' where id='".$id."'";
		$unblockneighborhoodfn=mysql_query($unblockneighborhoodfn);
		$unblockneighborhoodfn=mysql_fetch_array($unblockneighborhoodfn);
		return ($unblockneighborhoodfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update neighborhoods  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update neighborhoods  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactiveneighborhoodfn($id)     
		{
			$editactiveneighborhoodfn="UPDATE `neighborhoods` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactiveneighborhoodfn=mysql_query($editactiveneighborhoodfn);
		}	
    public function fetchcityfn()
	{
		$sql = "SELECT * FROM cities ORDER BY id ASC";
		$rs = mysql_query($sql) or die(mysql_error().$sql); 
		$arrCity = array();
		while($rec = mysql_fetch_assoc($rs))
		{
			$arrCity[$rec['id']] = $rec['city_name'];
		}
		return $arrCity; 
	}
	
	public function allNeighborhood($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
	

}

?>