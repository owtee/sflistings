<?php
/**
 * CupCake CMS <http://www.cupcakecms.com/>
 * Copyright (c)	2007, Jason A. Banico
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2007, Jason A. Banico
 * @link			http://www.sourceforge.net/projects/cupcakecms
 * @package			cupcakecms
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class Admin extends AppModel
{
  var $name = 'Admin';
	var $validate = array(
		'username' => VALID_NOT_EMPTY,
		'password' => VALID_NOT_EMPTY
	);
	public function admin_permission()
	{
	
	
	   $sql_check="select * from admins a,rolepermissions  rd where a.id=".$_SESSION['admin_id']." and a.roleid=rd.rolemaster_id and rd.admin_menu_id=".$_SESSION['menu_id'];
		$res_check=mysql_query($sql_check);
		$result_check=mysql_fetch_array($res_check);
		return ($result_check);
	
	}
 }
?>