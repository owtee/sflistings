<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Address extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Address';
	var $belongsTo = array(                 
				  'Mailinglist' =>
                        array('className'    => 'Mailinglist',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'mailinglist_id'
                        )
                  
				
                  );
        
	public function saveAddress($mailinglist_id,$address)
		{
                   $sqlExistData = "SELECT * FROM addresses WHERE mailinglist_id=$mailinglist_id AND email='$address'";
                   $rsExistData = mysql_query($sqlExistData);
                   if(mysql_num_rows($rsExistData)==0)
                   {
                      $sql_insert_address = "Insert into addresses(mailinglist_id,email) values ($mailinglist_id,'$address')"; 
                      if(mysql_query($sql_insert_address))
                      return true;
                      else
                      return false;
                   }
		}	
		
		public function findAlladdressDetail($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page,2);
			
			 return ($all_result);
		}
                
                public function deleteAdddress($id)     
		{
                      
		      $sql_Update_address = "Update addresses set status='0' where id=$id"; 
                      if(mysql_query($sql_Update_address))
                      return true;
                      else
                      return false;
		}
  
}

?>