<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Listing extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Listing';
	var $hasMany = array('Imagelisting' =>
                         array('className'     => 'Imagelisting',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         ),
			  'Filterlisting' =>
                         array('className'     => 'Filterlisting',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         ),
			  'Neighborhoodlisting' =>
                         array('className'     => 'Neighborhoodlisting',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         )
						
                  );
				  
	var $belongsTo = array(                 
				  'City' =>
                        array('className'    => 'City',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'city_id'
                        ),
                  
				  'Category' =>
                        array('className'    => 'Category',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'category_id'
                        ) ,
						
						'User' =>
                        array('className'    => 'User',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'user_id'
                        )
                  );
	var $hasOne = array(                 
				  'Listing_counter' =>
                        array('className'    => 'Listing_counter',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'listing_id'
                        ),
                        'Neighborhoodlisting' =>
                         array('className'     => 'Neighborhoodlisting',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         ),
                         'Imagelisting' =>
                         array('className'     => 'Imagelisting',                                                         
                               'foreignKey'    => 'listing_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         )
                  );
				  
	
	
	public function allListing($condition, $filelds, $order_by, $limit, $page)     
		{
                  
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			 
			 return $all_result;
		}
		
	 public function deletelistingdetail($listing_id)     
		{			
			if($this->del($listing_id))			 
			 return true;
			 else
			 return false;
		}
	 public function deletelistingdetail1($listing_id)     
		{	
			
			$sql = "UPDATE listings SET is_deleted = '0' WHERE id = '".$listing_id."'";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
                
         public function diactiveproperty($listing_id)     
		{	
			
			$sql = "UPDATE listings SET active = '0' WHERE id = '".$listing_id."'";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
          
          public function activeproperty($listing_id)     
		{	
			
			$sql = "UPDATE listings SET active = '1' WHERE id = '".$listing_id."'";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
                
	public function available($listing_id)     
		{	
			
			$sql = "UPDATE listings SET sold = 1 WHERE id=  $listing_id";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
		
	public function addnewsletter($listing_id,$user_id)     
		{	
			
                        $sqlexistNewsletter = "SELECT * FROM newsletters WHERE user_id = $user_id AND listing_id = $listing_id";
			$rsExistNewsletter = mysql_query($sqlexistNewsletter);
			if(mysql_num_rows($rsExistNewsletter) == 0)
                        {
                          $sql = "INSERT INTO newsletters (user_id,listing_id) VALUES ($user_id,$listing_id)";
                          if(mysql_query($sql))
                          return true;
                          else
                          return false;
                        } else
                         return false;
		}
		
	public function existNewsletter($listing_id,$user_id)     
		{	
			
			$sqlexistNewsletter = "SELECT * FROM newsletters WHERE user_id = $user_id AND listing_id = $listing_id";
			$rsExistNewsletter = mysql_query($sqlexistNewsletter);
			if(mysql_num_rows($rsExistNewsletter) > 0)
			return 'y';
			else
			return 'n';
		}
        public function updateAvailable($sold,$listing_id)     
		{
			
			$sqlupdateSold = "Update listings set sold='".$sold."' WHERE  id = $listing_id";
			$rsupdateSold = mysql_query($sqlupdateSold) or die(mysql_error().$sqlupdateSold);
			if($rsupdateSold)
			return true;
			else
			return false;
		}
		
	public function restoreproperty($listing_id)     
		{	
			
			$sql = "UPDATE listings SET is_deleted = '1' WHERE id = '".$listing_id."'";
			if(mysql_query($sql))
			return true;
			else
			return false;
		}
        public function deletenewsletter($listing_id,$user_id)     
		{	
			
			$delete_sql = "DELETE FROM newsletters WHERE listing_id=$listing_id AND user_id=$user_id";
			if(mysql_query($delete_sql))
			return true;
			else
			return false;
		}
}
?>