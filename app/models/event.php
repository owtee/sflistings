<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Event extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Event';
	
		public function allEvent($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			 
			 return $all_result;
		}
	 	  
	public function deleteevent($id)
	  {	 
	  	if($this->del($id))
		return true;
		else
		return false;
	  }
	   public function availableEvent($event_name,$id=NULL)
	  {	 
	  	$cri = "Event.title ='$event_name'";
		if(!empty($id))
		$cri .= " and Event.id <> $id" ;		
		   
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
}
?>