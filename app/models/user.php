<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class User extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'User';
		var $hasOne = array(                 
				  'User_ad' =>
                        array('className'    => 'User_ad',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'user_id'
                        )
                  );
		
	public function fetchcategoryfn($id)
		{
			$sql = "SELECT * from `categories`";
			$rs = mysql_query($sql) or die(mysql_error().$sql);
			$arrList = array();
			while($rec = mysql_fetch_assoc($rs))
			{
				$arrList[] = $rec['cms_name'];
			}
			return $arrList;
		} 
	public function fetchcityfn($id)
		{
			$sql = "SELECT * FROM `cities` ORDER BY id DESC";
			$rs = mysql_query($sql) or die(mysql_error().$sql);
			$arrCity = array();
			while($rec = mysql_fetch_assoc($rs))
			{
				$arrCity[$rec['id']] = $rec['city_name'];
			}
			return $arrCity;
		}	
		
	public function seletAlluser($cri,$limit,$page)
	  {	    
		 $all_result = $this->findAll($cri,NULL, NULL, $limit, $page);
		 return ($all_result);
	  }
	  
	  public function deleteuser($id)
	  {	 
	  	if($this->del($id))
		return true;
		else
		return false;
	  }
	  
	  public function availableBrokerID($broker_id,$id=NULL)
	  {	 
	  	$cri = "mlsID ='$broker_id' and User.isblocked='0' and User.isdelete='0'";
		if(!empty($id))
		$cri .= " and User.id <> $id" ;		
		
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function availablecontact($telephone_no,$id=NULL)
	  {	 
	  	$cri = "cellno ='$telephone_no' and User.isblocked='0' and User.isdelete='0'";
		if(!empty($id))
		$cri .= " and User.id <> $id" ;		
		
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function availableEmail($email,$id=NULL)
	  {	 
	  	$cri = "email ='$email' and User.isblocked='0' and User.isdelete='0'";
		if(!empty($id))
		$cri .= " and User.id <> $id" ;		
		   
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function blockUser($id)
	  {
	 	$block_user="update users set isblocked='1' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  }
	  
	  public function unblockUser($id)
	  {
	    $block_user="update users set isblocked='0' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  } 	
	  
	  public function userLogin($condition, $filelds)
	  {
	    $all_result = $this->findAll($condition, $filelds, NULL, NULL, NULL);	
		
		 return $all_result;		
	  } 
	  
	  public function userUpdate($user_id)
	  {
	  $update_user = "update users  set lastloginip='".$_SERVER['REMOTE_ADDR']."', lastlogindate=now() where id=$user_id";
	  if(mysql_query($update_user))
	  return true;
	  else
	  return false;
	  
	   
	  } 	
	  
	  
	   public function approveUser($id)
	  {
	 	$block_user="update users set approve='y' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  }
	  
	  public function disapproveUser($id)
	  {
	    $block_user="update users set approve='n' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  } 
	  
	  public function findContent()
	  {
	    $contentSql ="SELECT description FROM contents WHERE id =6"; 
		$rsContentSql = mysql_query($contentSql);
		if($rsContentSql)
		{
			 $content = mysql_fetch_array($rsContentSql); 
			return $content['description'];
		}
		else
		return false;
			
		
	  } 
	  
	  
	  public function userforgotPass($newpassword, $id)
	  {
	    $updateSql ="Update users SET password = md5('$newpassword') WHERE id = $id"; 
		$rsUpdateSql = mysql_query($updateSql);
		if($rsUpdateSql)
		return true;
		else
		return false;
			
		
	  } 
	  
	  
	  public function userChkValid($oldpassword, $id)
	  {
	  	
	    $updateSql ="Update users SET password = md5('$newpassword') WHERE id = $id"; 
		$rsUpdateSql = mysql_query($updateSql);
		if($rsUpdateSql)
		return true;
		else
		return false;
			
		
	  } 
	  
	  public function userchangePass($oldpassword, $id)
	  {
	  	
	    $updateSql ="Update users SET password = md5('$oldpassword') WHERE id = $id"; 
		$rsUpdateSql = mysql_query($updateSql);
		if($rsUpdateSql)
		return true;
		else
		return false;
			
		
	  }
	  
	public function seletAllsaveBroker()
	  {
	  	
                    $sql = "SELECT * from `savebroker`";
			$rs = mysql_query($sql) or die(mysql_error().$sql);
			$arrList = array();
			while($rec = mysql_fetch_assoc($rs))
			{
				$arrList[] = $rec['email'];
			}
			return $arrList;
			
		
	  }
}
?>