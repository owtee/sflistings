<?php
/**
 * CupCake CMS <http://www.cupcakecms.com/>
 * Copyright (c)	2007, Jason A. Banico
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2007, Jason A. Banico
 * @link			http://www.sourceforge.net/projects/cupcakecms
 * @package			cupcakecms
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
class Personaluser extends AppModel
{
  var $name = 'Personaluser';
	var $validate = array(
		'id' => VALID_NOT_EMPTY,
		'username' => VALID_NOT_EMPTY,
		'password' => VALID_NOT_EMPTY,
		//'role' => VALID_NOT_EMPTY,
		'email' => VALID_NOT_EMPTY,
		'lastlogindate' => VALID_NOT_EMPTY
		
	);
	
	public function seletAlluser($cri,$limit,$page)
	  {	    
		 $all_result = $this->findAll($cri,NULL, NULL, $limit, $page);
		 return ($all_result);
	  }
	  
	  public function deleteuser($id)
	  {	 
	  	if($this->del($id))
		return true;
		else
		return false;
	  }
	  
	  public function availableUsername($user_name,$id=NULL)
	  {	 
	  	$cri = "username ='$user_name'";
		if(!empty($id))
		$cri .= " and id <> $id" ;		
		
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function availableEmail($email,$id=NULL)
	  {	 
	  	$cri = "email ='$email'";
		if(!empty($id))
		$cri .= " and id <> $id" ;		
		   
		 if($this->findAll($cri))
		 return true;
		 else
		 return false;
	  }
	  
	  public function blockUser($id)
	  {
	 	$block_user="update personalusers set isblocked='1' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  }
	  
	  public function unblockUser($id)
	  {
	    $block_user="update personalusers set isblocked='0' where id=$id"; 
		if(mysql_query($block_user))
		 return true;
		 else
		 return false;		
		
	  }
	  
	 
	
 }
?>