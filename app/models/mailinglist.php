<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Mailinglist extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Mailinglist';
	var $belongsTo = array('User' =>
                         array('className'     => 'User',                                                         
                               'foreignKey'    => 'user_id',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => ''
                         )
                  );
	/*public function updatefn($id)
	  {
	    $updatefn="UPDATE `cities` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` ='".$id."'";
		$updatefn=mysql_query($updatefn);
		$updatefn=mysql_fetch_array($updatefn);
		return ($updatefn);
	  }
	public function blockcityfn($id)
	  {
	    echo $blockcityfn="update cities  set isblocked='1' where id='".$id."'";
		$blockcityfn=mysql_query($blockcityfn);
		$blockcityfn=mysql_fetch_array($blockcityfn);
		return ($blockcityfn);
	  }
	public function unblockcityfn($id)
	  {
	    echo $unblockcityfn="update cities  set isblocked='0' where id='".$id."'";
		$unblockcityfn=mysql_query($unblockcityfn);
		$unblockcityfn=mysql_fetch_array($unblockcityfn);
		return ($unblockcityfn);
	  }
	public function blockallfn($id)
	  {
	    $blockallfn="update cities  set isblocked='1' where id='".$id."'";
		$blockallfn=mysql_query($blockallfn);
		
	  }
	public function unblockallfn($id)     
		{
			$unblockallfn="update cities  set isblocked='0' where id='".$id."'";
			$unblockallfn=mysql_query($unblockallfn);
		}
	public function editactivecityfn($id)     
		{
			$editactivecityfn="UPDATE `cities` SET `modifiedon` = '".date('Y-m-d')."' WHERE `id` = '".$id."'";
			$editactivecityfn=mysql_query($editactivecityfn);
		}	
		*/
		public function findAllmailingDetail($condition, $filelds, $order_by, $limit, $page)     
		{				
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			
			 return ($all_result);
		}
  
}

?>