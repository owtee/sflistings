<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Usersearch extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Usersearch';
    var $belongsTo = array(
		    'User' =>
                        array('className'    => 'User',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'user_id'
                        ),
		      'Category' =>
                        array('className'    => 'Category',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'category_id'
                        ),
			'City' =>
                        array('className'    => 'City',
                              'conditions'   => '',
                              'order'        => '',
                              'dependent'    =>  true,
                              'foreignKey'   => 'city_id'
                        )
                  );
	
	public function allUserSearch($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page);
			 
			 return $all_result;
		}
		
		
	public function deleteUserSearch($id)
	  {	 
	  	if($this->del($id))
		return true;
		else
		return false;
	  }	  
}
?>