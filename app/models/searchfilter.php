<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Searchfilter extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Searchfilter';
    
     function saveFilter($value,$savesearch_id)
		{			
			$insert_neighborhood_table = "INSERT INTO searchfilters (usersearch_id,filter_id) VALUES ($savesearch_id,$value)";
			if(mysql_query($insert_neighborhood_table))
			return true;
			else
			return false;			
		}
		
	public function allUserSearchFilter($condition, $filelds, $order_by, $limit, $page)     
		{	
			
			 $all_result = $this->findAll($condition, $filelds, $order_by, $limit, $page,2);
			 
			 return $all_result;
		}
		
	function deleteSearchFilter($savesearch_id)
		{				
			$delete_save_filter = "DELETE FROM searchfilters WHERE usersearch_id = $savesearch_id";
			if(mysql_query($delete_save_filter))
			return true;
			else
			return false;
			
		}
		
	
	
	
	
		  
}
?>