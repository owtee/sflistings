<?php
/**
* Navigators Software Private Limited
* Name: Surit Nath.
* Date: 09/12/2008
* Date of Modification: 
* Reason of the Model: To get All details of Content from cities table
* This class represent the cities table. It has some function that will provide details 
* of city listing depending upon the conditions.
*/
class Ads extends AppModel
{
  // This name point to the cities table and can be accessed by controller the city table by this name.
    var $name = 'Listings';
	public function fetchcategoryfn($id)
		{
			$sql = "SELECT * from `categories`";
			$rs = mysql_query($sql) or die(mysql_error().$sql);
			$arrList = array();
			while($rec = mysql_fetch_assoc($rs))
			{
				$arrList[] = $rec['cms_name'];
			}
			return $arrList;
		} 
	public function fetchcityfn($id)
		{
			$sql = "SELECT * from `cities`";
			$rs = mysql_query($sql) or die(mysql_error().$sql);
			$arrCity = array();
			while($rec = mysql_fetch_assoc($rs))
			{
				$arrCity[] = $rec['city_name'];
			}
			return $arrCity;
		}
	public function neighborhood($id='')
	{
		$condition = '';
		$condition = $id!=''?" AND city_id = ".$id."":" AND city_id = 34";
		$sql = 'SELECT * FROM `neighborhoods` WHERE 1'.$condition;
		$rs = mysql_query($sql) or die(mysql_error().$sql);
		$arrNeighborhood = array();
		while($rec = mysql_fetch_assoc($rs))
		{
			$arrNeighborhood[] = $rec['neighborhood_name'];
		}
		return $arrNeighborhood;
	}	 	  
}
?>