<?php
/**
* Navigators Software Private Limited
* Name: Abhishek Shaw
* Date: 10/21/2009
* Date of Modification: 
* Reason of the Model: To get All details Statistics 
* 
*/
class Statistics extends AppModel
{
    var $name = 'Statistics';
	public function insertInfo($cityID=0,$categoryID=0,$neighborhoodArray,$languageArray,$typeArray,$minPrice=0,$maxPrice=0,$filtersUsedArray)
	{
	    $minPrice=(isset($minPrice) && !empty($minPrice)) ? $minPrice:0;
		$maxPrice=(isset($maxPrice) && !empty($maxPrice)) ? $maxPrice:0;
		$neighborhood=implode(",",$neighborhoodArray);
		$language=implode(",",$languageArray);
		$type=implode(",",$typeArray);
		$filters=implode(",",$filtersUsedArray);
			      //getting the newyork time
	        $adjusted_time=$this->getTime(42.1497,-74.9384);

	    $qry="insert into statistics(CITY,CATEGORY,NEIGHBORHOOD_LIST,MIN_PRICE,MAX_PRICE,BROKER_LANGUAGE,SELECT_TYPE,FILTER_USED,DATE) values ({$cityID},{$categoryID},\"{$neighborhood}\",{$minPrice},{$maxPrice},\"{$language}\",\"{$type}\",\"{$filters}\",\"{$adjusted_time}\")"; 
		//echo $qry;
		//exit(0);
		if(mysql_query($qry))
		 return true;
		 else
		 return false;		

	}
	
	function insertPropertyInfo($listing_id=0)
	{
	        //getting the newyork time
	        $adjusted_time=$this->getTime(42.1497,-74.9384);
			$qry="insert into `property_statistics` (PROPERTY_ID,DATE) values ({$listing_id},\"{$adjusted_time}\")";
	        if(mysql_query($qry))
			return true;
			else
			return false;
	}
	
		    function getTime($lat, $lng)
			{
				$url = "http://ws.geonames.org/timezone?lat={$lat}&lng={$lng}";
				$timedata = file_get_contents($url);
				$sxml = simplexml_load_string($timedata);
				$adjusted_time=$sxml->timezone->time.":".date('s');
				return $adjusted_time;
			}


}
?>