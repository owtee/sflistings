function validate()
{
	if (document.getElementById('AdminUsername').value.search(/\S/)==-1) 
   {
	alert('User Name should not be blank.');
    document.getElementById('AdminUsername').focus();
	return false;
	}
	if (document.getElementById('AdminPassword').value.search(/\S/)==-1) 
   {
	alert('Password should not be blank.');
    document.getElementById('AdminPassword').focus();
	return false;
	}
	if(document.getElementById('AdminConfirmpwd').value!=document.getElementById('AdminPassword').value)
  {
	   alert('Password does not match with confirm password');
	   document.getElementById('AdminConfirmpwd').value='';
	   document.getElementById('AdminPassword').value='';
	   document.getElementById('AdminPassword').focus();
	   return false;
  }
  if (document.getElementById('AdminFirstname').value.search(/\S/)==-1) 
   {
	alert('First Name should not be blank.');
    document.getElementById('AdminFirstname').focus();
	return false;
	}
	if (document.getElementById('AdminLastname').value.search(/\S/)==-1) 
   {
	alert('Last Name should not be blank.');
    document.getElementById('AdminLastname').focus();
	return false;
	}
	if (document.getElementById('AdminEmail').value.search(/\S/)==-1) 
	{
	alert('Please enter your valid Email id.');
    document.getElementById('AdminEmail').focus();
	return false;
	}
	
	var regEmail = /^([-a-zA-Z0-9._]+@[-a-zA-Z0-9.]+(\.[-a-zA-Z0-9]+)+)$/;
	if(!regEmail.test(document.getElementById('AdminEmail').value))
	{
		alert("Invalid email address.");
		document.getElementById('AdminEmail').value=''
		document.getElementById('AdminEmail').focus();
		return false;
	}
	

	
 
 else
  return true
}