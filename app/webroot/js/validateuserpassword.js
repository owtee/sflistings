// JavaScript Document
function validate()
{
	

  if(document.getElementById('UserOldpassword').value.search(/\S/)==-1) 
  {
	alert('Old Password should not be blank.');
    document.getElementById('UserOldpassword').focus();
	return false;
  }
  
  if(document.getElementById('UserNewpassword').value.search(/\S/)==-1) 
  {
	alert('New Password should not be blank.');
    document.getElementById('UserNewpassword').focus();
	return false;
  }
  
  if(document.getElementById('UserRenewpassword').value.search(/\S/)==-1) 
  {
	alert('Confirm New Password should not be blank.');
    document.getElementById('UserRenewpassword').focus();
	return false;
  }
  
  if(document.getElementById('UserRenewpassword').value!=document.getElementById('UserNewpassword').value)
  {
	   alert('New Password does not match with confirm New Password.');
	   document.getElementById('UserRenewpassword').value='';
	   document.getElementById('UserNewpassword').value='';
	   document.getElementById('UserNewpassword').focus();
	   return false;
  }
 

}