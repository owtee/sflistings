// JavaScript Document

function validate()
{
	if (document.getElementById('TourTourname').value.search(/\S/)==-1) 
   	{
	alert('Tour Name should not be blank.');
    document.getElementById('TourTourname').focus();
	return false;
	}
	/*if (document.getElementById('tourtype').value.search(/\S/)==-1) 
   	{
	alert('Select a Tour Type.');
    document.getElementById('tourtype').focus();
	return false;
	}*/
	if (document.getElementById('TourDescription').value.search(/\S/)==-1) 
   	{
	alert('Description should not be blank.');
    document.getElementById('TourDescription').focus();
	return false;
	}
	var wordLimit = 100;
	
	 var text1 = document.getElementById('TourDescription').value;
	 var text2 = text1.replace(/\s+/g, ' ');
	 var text3 = text2.split(' ');
	 var numberOfWords = text3.length;
	
	 if(numberOfWords > wordLimit)
	 {
	  alert('You can Not Give More Than 100 Character In Description Field!');
	  document.getElementById('TourDescription').focus();
	  return false;
	 }
	
	if (document.getElementById('TourDuration').value.search(/\S/)==-1) 
   	{
	alert('Duration should not be blank.');
    document.getElementById('TourDuration').focus();
	return false;
	}
	if(isNaN(document.getElementById('TourDuration').value))
	{
		//number.value = "";
		alert("Please enter only numeric value for Duration");
		document.getElementById('TourDuration').focus();
		return false;	
	}
	if (document.getElementById('TourDuration').value==0) 
   	{
	alert('Please enter more than Zero');
    document.getElementById('TourDuration').focus();
	return false;
	}
	/*if (!IsNumeric(document.getElementById('TourDuration').value)) 
   	{
	alert('Please Give The Duration In Numaric Value.');
    document.getElementById('TourDuration').focus();
	return false;
	}*/
	if (document.getElementById('TourDepartingcity').value.search(/\S/)==-1) 
   	{
	alert('Departing city should not be blank.');
    document.getElementById('TourDepartingcity').focus();
	return false;
	}
	if (document.getElementById('TourEndcity').value.search(/\S/)==-1) 
   	{
	alert('End city should not be blank.');
    document.getElementById('TourEndcity').focus();
	return false;
	}
	if (document.getElementById('refree_search').value.search(/\S/)==-1) 
   	{
	alert('Attraction should not be blank.');
    document.getElementById('refree_search').focus();
	return false;
	}
	if (document.getElementById('TourDepartureschedule').value.search(/\S/)==-1) 
   	{
	alert('Departure schedule should not be blank.');
    document.getElementById('TourDepartureschedule').focus();
	return false;
	}
	if (document.getElementById('TourPrice').value.search(/\S/)==-1) 
   	{
	alert('Price should not be blank.');
    document.getElementById('TourPrice').focus();
	return false;
	}
	if(isNaN(document.getElementById('TourPrice').value))
	{
		//number.value = "";
		alert("Please enter only numeric value for Tour Price");
		document.getElementById('TourPrice').focus();
		return false;	
	}
	if (document.getElementById('TourPrice').value==0) 
   	{
	alert('Please enter more than Zero');
    document.getElementById('TourPrice').focus();
	return false;
	}
	
	
}

