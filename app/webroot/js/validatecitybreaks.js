// JavaScript Document

function validate()
{
	if (document.getElementById('CitybreakCitybreakname').value.search(/\S/)==-1) 
   	{
	alert('Citybreak Name should not be blank.');
    document.getElementById('CitybreakCitybreakname').focus();
	return false;
	}
	
	if (document.getElementById('CitybreakDescription').value.search(/\S/)==-1) 
   	{
	alert('Description should not be blank.');
    document.getElementById('CitybreakDescription').focus();
	return false;
	}
	var wordLimit = 100;
	
	 var text1 = document.getElementById('CitybreakDescription').value;
	 var text2 = text1.replace(/\s+/g, ' ');
	 var text3 = text2.split(' ');
	 var numberOfWords = text3.length;
	
	 if(numberOfWords > wordLimit)
	 {
	  alert('You can Not Give More Than 100 Character In Description Field!');
	  document.getElementById('CitybreakDescription').focus();
	  return false;
	 }
	if (document.getElementById('CitybreakDeparture').value.search(/\S/)==-1) 
   	{
	alert('Departure should not be blank.');
    document.getElementById('CitybreakDeparture').focus();
	return false;
	}
	
	if (document.getElementById('CitybreakPrice').value.search(/\S/)==-1) 
   	{
	alert('Price should not be blank.');
    document.getElementById('CitybreakPrice').focus();
	return false;
	}
	
	if(isNaN(document.getElementById('CitybreakPrice').value))
	{
		//number.value = "";
		alert("Please enter only numeric value for Citybreak Price");
		document.getElementById('CitybreakPrice').focus();
		return false;	
	}
	if (document.getElementById('CitybreakPrice').value==0) 
   	{
	alert('Please enter more than Zero');
    document.getElementById('CitybreakPrice').focus();
	return false;
	}
	
	if (document.getElementById('CitybreakDuration').value.search(/\S/)==-1) 
   	{
	alert('Duration should not be blank.');
    document.getElementById('CitybreakDuration').focus();
	return false;
	}
	if(isNaN(document.getElementById('CitybreakDuration').value))
	{
		//number.value = "";
		alert("Please enter only numeric value for Duration");
		document.getElementById('CitybreakDuration').focus();
		return false;	
	}
	if (document.getElementById('CitybreakDuration').value==0) 
   	{
	alert('Please enter more than Zero');
    document.getElementById('CitybreakDuration').focus();
	return false;
	}
	if (!IsNumeric(document.getElementById('CitybreakDuration').value)) 
   	{
	alert('Please Give The Duration In Numaric Value.');
    document.getElementById('CitybreakDuration').focus();
	return false;
	}
	
	

	if (document.getElementById('priceperperson').value!="") 
   	{
		if(isNaN(document.getElementById('priceperperson').value))
		{
			//number.value = "";
			alert("Please enter only numeric value for Price per person");
			document.getElementById('priceperperson').focus();
			return false;	
		}
	}
}
/*function changeDivslastname()
{
document.getElementById('lastname').style.display='block' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsfirstname()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='block';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsemail()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='block';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsconfirmemail()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='block';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsaddress()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='block';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsstate()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='block';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivszip()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='block';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivscity()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='block';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsusername()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='block';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivspassword()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='block';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsconfirmpassword()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='block';
document.getElementById('code').style.display='none';
}
function changeDivscode()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('city').style.display='none';
document.getElementById('state').style.display='none';
document.getElementById('zip').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='block';
}
function changeDivssubmit()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('confirmemail').style.display='none';
document.getElementById('address').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}*/
