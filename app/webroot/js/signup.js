// JavaScript Document

function validate()
{
	//alert(document.getElementById('PersonaluserUsertypeId').value);
	
	if (document.getElementById('PersonaluserFname').value.search(/\S/)==-1) 
   {
	alert('First Name should not be blank.');
    document.getElementById('PersonaluserFname').focus();
	return false;
	}
	if (document.getElementById('PersonaluserLname').value.search(/\S/)==-1) 
   {
	alert('Last Name should not be blank.');
    document.getElementById('PersonaluserLname').focus();
	return false;
	}
	if (document.getElementById('PersonaluserGender').value.search(/\S/)==-1) 
   {
	alert('Select Your Gender.');
    document.getElementById('PersonaluserGender').focus();
	return false;
	}
	if (document.getElementById('date').value.search(/\S/)==-1) 
   {
	alert('date should not be blank.');
    document.getElementById('date').focus();
	return false;
	}
	if (document.getElementById('month').value.search(/\S/)==-1) 
   {
	alert('month should not be blank.');
    document.getElementById('month').focus();
	return false;
	}
	if (document.getElementById('year').value.search(/\S/)==-1) 
   {
	alert('year should not be blank.');
    document.getElementById('year').focus();
	return false;
	}
	
	
	if (document.getElementById('PersonaluserNationality').value.search(/\S/)==-1) 
   {
	alert('Nationality should not be blank.');
    document.getElementById('PersonaluserNationality').focus();
	return false;
	}
	if (document.getElementById('PersonaluserLanguage').value.search(/\S/)==-1) 
   {
	alert('Language should not be blank.');
    document.getElementById('PersonaluserLanguage').focus();
	return false;
	}
	if (document.getElementById('PersonaluserAddress').value.search(/\S/)==-1) 
   {
	alert('Address should not be blank.');
    document.getElementById('PersonaluserAddress').focus();
	return false;
	}
	if (document.getElementById('PersonaluserLocation').value.search(/\S/)==-1) 
   {
	alert('Town/City should not be blank.');
    document.getElementById('PersonaluserLocation').focus();
	return false;
	}
	if (document.getElementById('PersonaluserCountrystate').value.search(/\S/)==-1) 
   {
	alert('County/State/Province should not be blank.');
    document.getElementById('PersonaluserCountrystate').focus();
	return false;
	}
	if (document.getElementById('country').value.search(/\S/)==-1) 
   {
	alert('Country should not be blank.');
    document.getElementById('country').focus();
	return false;
	}
	if (document.getElementById('PersonaluserUsername').value.search(/\S/)==-1) 
   {
	alert('Username should not be blank.');
    document.getElementById('PersonaluserUsername').focus();
	return false;
	}
	if(document.getElementById('PersonaluserPassword').value.search(/\S/)==-1) 
    {
	alert('Password should not be blank.');
	document.getElementById('PersonaluserPassword').focus();
	return false;
    }
	if(document.getElementById('PersonaluserConfirmpassword').value.search(/\S/)==-1) 
   {
	alert('Personaluser Confirmpassword should not be blank.');
    document.getElementById('PersonaluserConfirmpassword').focus();
	return false;
   }
    if(document.getElementById('PersonaluserConfirmpassword').value!=document.getElementById('PersonaluserPassword').value)
    {
	   alert('Password does not match with confirm password.');
	   document.getElementById('PersonaluserConfirmpassword').value='';
	   document.getElementById('PersonaluserPassword').value='';
	   document.getElementById('PersonaluserPassword').focus();
	   return false;
     }
	if (document.getElementById('PersonaluserEmail').value.search(/\S/)==-1) 
	{
	alert('Please enter your valid Email id.');
    document.getElementById('PersonaluserEmail').focus();
	return false;
	}
	
	var regEmail = /^([-a-zA-Z0-9._]+@[-a-zA-Z0-9.]+(\.[-a-zA-Z0-9]+)+)$/;
	if(!regEmail.test(document.getElementById('PersonaluserEmail').value))
	{
		alert("Invalid email address.");
		document.getElementById('PersonaluserEmail').value=''
		document.getElementById('PersonaluserEmail').focus();
		return false;
	}
  	
  	radiochoice=false;
    
    var rbOriginal=document.adduser.foreigner;
    for(i=0;i<rbOriginal.length;i++)
    {
      if (rbOriginal[i].checked)
	  {
      radiochoice=true;  
      break;
	  }
    }
	
	radiotravelhelper=false;
    
    var rbtravelhelper=document.adduser.travelhelper;
    for(j=0;j<rbtravelhelper.length;j++)
    {
      if (rbtravelhelper[j].checked)
	  {
      radiotravelhelper=true;  
      break;
	  }
    }
	//var writerchk=document.getElementById("writer").checked;
	
	//alert(document.getElementById("writer").value);
    //if ((radiochoice==false) && (radiotravelhelper==false) && (document.adduser.writer.checked==false))
	if ((radiochoice==false) && (radiotravelhelper==false))
   {
	alert('Please choose a User type');
	return false;
	}
	else
	{
		return true;
	}
	
	if (document.getElementById('PersonaluserTemplate').value.search(/\S/)==-1) 
	{
	alert('Please Choose Your picture.');
    document.getElementById('PersonaluserTemplate').focus();
	return false;
	}
	
	if(document.getElementById('PersonaluserTemplate').value != "")
			{
				var fileinput = document.getElementById("PersonaluserTemplate");
				if(!fileinput ) return "";
				var filename = fileinput.value;
				if( filename.length == 0 ) return "";
				var dot = filename.lastIndexOf(".");
				if( dot == -1 ) return "";
				var extension = filename.substr(dot,filename.length);
				
				if( extension=='.jpeg' || extension=='.JPEG' || extension=='.gif'  || extension=='.GIF' || extension=='.jpg'  || extension=='.JPG' || extension=='.png'  || extension=='.PAG' || extension=='.bmp' || extension=='.BMP' )
				{
				}
				else
				{
				alert("Sorry,please uplode '.jpeg' ,'.gif','.jpg','.png' or '.bmp' type file ");
				return false;
				}
			}
   
}

