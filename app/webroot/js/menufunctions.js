// JavaScript Document
function DisplayHideModuleItems(div_id,up_down_image_div)
{
	//alert(div_id);
	//alert(up_down_image_div);
	var display_status = document.getElementById(div_id).style.display;
	if(display_status == "")
	{
		document.getElementById(div_id).style.display = "none";
		document.getElementById(up_down_image_div).innerHTML = '<img src="../img/arrowdown.gif" width="9" height="9" title="Expand"/>';
	}	
	else
	{
		document.getElementById(div_id).style.display = "";
		document.getElementById(up_down_image_div).innerHTML = '<img src="../img/arrowup.gif" width="9" height="9" title="Collapse"/>';
	}		
}

function CollapseAll()
{
	var div_elm = document.getElementsByTagName('div');
	for(var i = 0; i < div_elm.length; i++)
	{
		if(((div_elm[i].id.indexOf('main_menu') != -1) && (div_elm[i].id.indexOf('up_down_image_main_menu'))))// || ((div_elm[i].id.indexOf('sub_menu_items') != -1))))
			div_elm[i].style.display = "none";
		if(div_elm[i].id.indexOf('up_down_image_main_menu') != -1)
			div_elm[i].innerHTML = '<img src="../img/arrowdown.gif" width="9" height="9" title="Expand"/>';
	}	
}

function ExpandAll()
{
	var div_elm = document.getElementsByTagName('div');
	for(var i = 0; i < div_elm.length; i++)
	{
		if((div_elm[i].id.indexOf('mainnav') != -1) || (div_elm[i].id.indexOf('main_menu') != -1) ||
		     (div_elm[i].id.indexOf('sub_menu') != -1) || (div_elm[i].id.indexOf('sub_menu_items') != -1))
		{
			div_elm[i].style.display = "";
		}
	}
}

function ShowOrHideAdminMenu(show_or_hide)
{
	if(show_or_hide == "hide")
	{
		document.getElementById('left-column').style.display = "none";
		document.getElementById('hidden-column').style.display = "";
	}
	else if(show_or_hide == "show")
	{
		document.getElementById('left-column').style.display = "";
		document.getElementById('hidden-column').style.display = "none";
	}
}

/*showHideTooltip = function (event_type,str) 
{
	//var obj = event.srcElement;
	with(document.getElementById("tooltip")) 
	{
		innerHTML = str;
		with(style) 
		{
			if(event_type == "onMouseOut") 
			{
				display = "none";
			} 
			else 
			{
				display = "inline";
				left = event.x+5;
				top = event.y+15;
			}
		}
	}
}*/


