
// JavaScript Document for manageplan page
function pagination(id)
{
	document.manageuser.pages.value=id;
	document.manageuser.submit();
}

//Delete/block/unblock the selected user

function checkAll(isChecked,formName)
{	
	for(i=0;i<document.getElementById(formName).elements.length;i++)
	 {
		     if(document.getElementById(formName).elements[i].type=="checkbox" && document.getElementById(formName).elements[i].disabled==false)
			 document.getElementById(formName).elements[i].checked=isChecked;
	 }
}

function selectCheckbox(formName, string, field, str)
{
	var j = 0;
	if(document.getElementById(field).checked==true)
	{
	
	   for(i=0;i<document.getElementById(formName).elements.length;i++)
		{
				if(document.getElementById(formName).elements[i].type=="checkbox" && document.getElementById(formName).elements[i].checked==true) 
				{
					j++;
				}
		}
		if(j==1)
		  j=0;
	}
	else
	{
	  
		for(i=0;i<document.getElementById(formName).elements.length;i++)
		{
				if(document.getElementById(formName).elements[i].type=="checkbox" && document.getElementById(formName).elements[i].checked==true) 
				 {
					j++;
				 }
		}
	}
	if(j==0)
	{
		alert("Please select at least one "+string+".");
		return false;
	}
	else
	 
		return true;
}

function modifyall(str,formName,controller)
{
  
	   if(str==1)
		{	
				if(controller=='listings')
				success = confirm('Are you sure you want to delete this listing (s)?');
				else if(controller=='addresses')
				success = confirm('Are you sure you want to delete this email address(s)?');
				else if(controller=='unsubscribeds')
				success = confirm('Are you sure you want to delete this email address(s)?');
				else
				success = confirm('Are you sure you want to delete the selected user(s)?');
				
				if(success)
				{	
				
					document.getElementById(formName).action="/"+controller+"/deleteall";	 
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
					
		}
		if(str==2)
		{			
			
				success = confirm('Are you sure you want to Block the selected user(s)?');
				if(success)
				{				
					
					document.getElementById(formName).action="/"+controller+"/blockall";	
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
			
			
		}
		if(str==3)
		{		
			
				success = confirm('Are you sure you want to Unblock the selected user(s)?');
				if(success)
				{					
					var adshow='unblockall';
					
					document.getElementById(formName).action="/"+controller+"/unblockall";
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
			
		 }
		 if(str==4)
		{			
			
				success = confirm('Are you sure you want to Move the selected property(s)?');
				if(success)
				{				
					
					document.getElementById(formName).action="../"+controller+"/addanotheraccount/";	
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
			
			
		}
		
		return false;
}



function sendSearch(formName)
{

	  if(selectCheckbox(formName,'savesearch','UserId',null))
			{
		
			
				success = confirm('Are you sure you want to send save search to the broker of selected user(s)?');
				if(success)
				{	
				
					document.getElementById(formName).action="/users/managebroker";	 
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
					
		
			}
		return false;
}


function send(formName)
{

	if(selectCheckbox(formName,'broker','UserEmail',null))
			{
				success = confirm('Are you sure you want to send mail to the selected broker(s)?');
				if(success)
				{	
				
					document.getElementById(formName).action="/users/sendsavesearch";	 
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
			}		
		
		
		return false;
}


//--CHECK/UNCHECK MASTER checkbox
function checkmainbox()
{
	var selectflag=0;
	var totalcount=getchkboxcount(document.manageplan.chkUser);
	for(i=0;i<document.manageplan.elements.length;i++)
	{
		if(document.manageplan.elements[i].name=='chkAll' && document.manageplan.chkAll.checked==true)
			  document.manageplan.chkAll.checked=false;
	}
	for(i=0;i<document.manageplan.elements.length;i++)
	{
		if(document.manageplan.elements[i].type=="checkbox" && document.manageplan.elements[i].checked==true && document.manageplan.elements[i].name!='chkAll')
		{
			selectflag++;
		}
	}

	//if(document.manageplan.chkAll.checked==false && selectflag==totalcount)
	if(document.getElementById('PlanId').checked==false && selectflag==totalcount)
	
	{
		 document.getElementById('PlanId').checked=true;
	}
	
}

function getchkboxcount()
{
	var selectflag=0;
	for(i=0;i<document.manageplan.elements.length;i++)
	{
			if(document.manageplan.elements[i].type=="checkbox" && document.manageplan.elements[i].name!='chkAll')
		{
					  selectflag++;
		}
	}
	return selectflag;
}	


function validationField(section)
{
	if(section=='filter')
		{
			if(document.getElementById('FilterFilterName').value.search(/\S/)==-1)
			{
				alert("Please enter Filter Name.");
				document.getElementById('FilterFilterName').focus();
				return false;
			}
		}
		
		if(section=='city')
		{
			if(document.getElementById('CityCityName').value.search(/\S/)==-1)
			{
				alert("Please enter City Name.");
				document.getElementById('CityCityName').focus();
				return false;
			}
		}
		if(section=='neighborhood')
		{
			if(document.getElementById('NeighborhoodNeighborhoodName').value.search(/\S/)==-1)
			{
				alert("Please enter Neighborhood Name.");
				document.getElementById('NeighborhoodNeighborhoodName').focus();
				return false;
			}
		}
		if(section=='category')
		{
			if(document.getElementById('CategoryCategoryName').value.search(/\S/)==-1)
			{
				alert("Please enter Category Name.");
				document.getElementById('CategoryCategoryName').focus();
				return false;
			}
		}
		
		if(section=='categoryfilter')
		{
			if(document.getElementById('CategoryfilterCategoryFilterName').value.search(/\S/)==-1)
			{
				alert("Please enter Category filter Name.");
				document.getElementById('CategoryfilterCategoryFilterName').focus();
				return false;
			}
		}
}

function saveAddress(formName)
{
	
				success = confirm('Are you sure you want to add the selected broker(s) in your list?');
				
				if(success)
				{	
				
					document.getElementById(formName).action="/users/saveAddress";	 
					
					document.getElementById(formName).submit();
				}
				else
				{
					return false;
				}
}

