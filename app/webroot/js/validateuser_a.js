// JavaScript Document
// JavaScript Document
function validate()
{
	if (document.getElementById('UserUsername').value.search(/\S/)==-1) 
   {
	alert('User Name should not be blank.');
    document.getElementById('UserUsername').focus();
	return false;
	}
	if (document.getElementById('UserPassword').value.search(/\S/)==-1) 
   {
	alert('Password should not be blank.');
    document.getElementById('UserPassword').focus();
	return false;
	}
	if(document.getElementById('UserConfirmpwd').value!=document.getElementById('UserPassword').value)
  {
	   alert('Password does not match with confirm password');
	   document.getElementById('UserConfirmpwd').value='';
	   document.getElementById('UserPassword').value='';
	   document.getElementById('UserPassword').focus();
	   return false;
  }
  if (document.getElementById('UserFname').value.search(/\S/)==-1) 
   {
	alert('First Name should not be blank.');
    document.getElementById('UserFname').focus();
	return false;
	}
	if (document.getElementById('UserLname').value.search(/\S/)==-1) 
   {
	alert('Last Name should not be blank.');
    document.getElementById('UserLname').focus();
	return false;
	}
	if (document.getElementById('UserEmail').value.search(/\S/)==-1) 
	{
	alert('Please enter your valid Email id.');
    document.getElementById('UserEmail').focus();
	return false;
	}
	
	var regEmail = /^([-a-zA-Z0-9._]+@[-a-zA-Z0-9.]+(\.[-a-zA-Z0-9]+)+)$/;
	if(!regEmail.test(document.getElementById('UserEmail').value))
	{
		alert("Invalid email address.");
		document.getElementById('UserEmail').value=''
		document.getElementById('UserEmail').focus();
		return false;
	}

	
 
 else
  return true
}
function changeDivslastname()
{
document.getElementById('lastname').style.display='block' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsfirstname()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='block';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsemail()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='block';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsgender()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsusername()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='block';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivspassword()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='block';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='none';
}
function changeDivsconfirmpassword()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='block';
document.getElementById('code').style.display='none';
}
function changeDivscode()
{
document.getElementById('lastname').style.display='none' ;
document.getElementById('firstname').style.display='none';
document.getElementById('email').style.display='none';
document.getElementById('username').style.display='none';
document.getElementById('password').style.display='none';
document.getElementById('confirmpassword').style.display='none';
document.getElementById('code').style.display='block';
}