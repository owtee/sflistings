
// JavaScript Document for manageplan page
function pagination(id)
{
	document.manageuser.pages.value=id;
	document.manageuser.submit();
}

//Delete/block/unblock the selected user

function checkAll(isChecked)
{
	for(i=0;i<document.getElementById('manageuser').elements.length;i++)
	 {
		     if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].disabled==false)
			 document.getElementById('manageuser').elements[i].checked=isChecked;
	 }
}

function selectCheckbox(str)
{
	var j = 0;
	if(document.getElementById('PersonaluserId').checked==true)
	{
	
	   for(i=0;i<document.getElementById('manageuser').elements.length;i++)
		{
				if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].checked==true) 
				{
					j++;
				}
		}
		if(j==1)
		  j=0;
	}
	else
	{
	  
		for(i=0;i<document.getElementById('manageuser').elements.length;i++)
		{
				if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].checked==true) 
				 {
					j++;
				 }
		}
	}
	if(j==0)
	{
		alert("Please select at least one user.");
		return false;
	}
	else
	 
		return true;
}

function modifyall(str)
{
	
	   if(str==1)
		{
			
			if(selectCheckbox(str))
			{
				success = confirm('Are you sure you want to delete the selected user(s)?');
				if(success)
				{
					/*document.manageadmin.mode.value="d";
					document.manageadmin.submit();*/
					var adshow='deleteall';
					var key = new Array();
					for(i=0,j=0;i<document.getElementById('manageuser').elements.length;i++)
					{
						if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].checked==true && document.getElementById('manageuser').elements[i].name!='chkAll')
						{
									  key[j]=document.getElementById('manageuser').elements[i].value;
									  j++;
									  
						}
					}
					//document.getElementById('optype').value=adshow;
					//document.manageadmin.submit();
					//sendRequest("../backpages/populate_table_winks.php","status="+adshow+"&key="+key,"POST");
					document.getElementById('operationtype').value=adshow;
					//document.getElementById('optype').value=adshow;
					if(document.getElementById('searchkey').value!='' && document.getElementById('key').value!='')
					document.getElementById('referer').value='searchbyname';
					else
					document.getElementById('referer').value='manageuser';
					
				
						document.getElementById('manageuser').action='deleteall';
					
					 
					
					document.getElementById('manageuser').submit();
				}
				else
				{
					return false;
				}
			}
			
		}
		if(str==2)
		{
			
			if(selectCheckbox(str))
			{
				success = confirm('Are you sure you want to Block the selected user(s)?');
				if(success)
				{
					/*document.manageadmin.mode.value="b";
					document.manageadmin.submit();*/
					var adshow='blockall';
					var key = new Array();
					for(i=0,j=0;i<document.getElementById('manageuser').elements.length;i++)
					{
						if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].checked==true && document.getElementById('manageuser').elements[i].name!='chkAll')
						{
									  key[j]=document.getElementById('manageuser').elements[i].value;
									  j++;
									  
						}
					}
					document.getElementById('operationtype').value=adshow;
					
					//document.getElementById('optype').value=adshow;
					if(document.getElementById('searchkey').value!='' && document.getElementById('key').value!='')
					document.getElementById('referer').value='searchbyname';
					else
					document.getElementById('referer').value='manageuser';					
					
					
					document.getElementById('manageuser').action='blockall';				
					
					
					document.getElementById('manageuser').submit();
				}
				else
				{
					return false;
				}
			}
			
		}
		if(str==3)
		{
			
			if(selectCheckbox(str))
			{
				success = confirm('Are you sure you want to Unblock the selected user(s)?');
				if(success)
				{
					/*document.manageadmin.mode.value="ub";
					document.manageadmin.submit();*/
					var adshow='unblockall';
					var key = new Array();
					for(i=0,j=0;i<document.getElementById('manageuser').elements.length;i++)
					{
						if(document.getElementById('manageuser').elements[i].type=="checkbox" && document.getElementById('manageuser').elements[i].checked==true && document.getElementById('manageuser').elements[i].name!='chkAll')
						{
									  key[j]=document.getElementById('manageuser').elements[i].value;
									  j++;
									  
						}
					}
					document.getElementById('operationtype').value=adshow;
					//document.getElementById('optype').value=adshow;
					if(document.getElementById('searchkey').value!='' && document.getElementById('key').value!='')
					document.getElementById('referer').value='searchbyname';
					else
					document.getElementById('referer').value='manageuser';
					document.getElementById('manageuser').action='unblockall';
					document.getElementById('manageuser').submit();
				}
				else
				{
					return false;
				}
			}
		 }
		return false;
}

//--CHECK/UNCHECK MASTER checkbox
function checkmainbox()
{
	var selectflag=0;
	var totalcount=getchkboxcount(document.manageplan.chkUser);
	for(i=0;i<document.manageplan.elements.length;i++)
	{
		if(document.manageplan.elements[i].name=='chkAll' && document.manageplan.chkAll.checked==true)
			  document.manageplan.chkAll.checked=false;
	}
	for(i=0;i<document.manageplan.elements.length;i++)
	{
		if(document.manageplan.elements[i].type=="checkbox" && document.manageplan.elements[i].checked==true && document.manageplan.elements[i].name!='chkAll')
		{
			selectflag++;
		}
	}

	//if(document.manageplan.chkAll.checked==false && selectflag==totalcount)
	if(document.getElementById('PlanId').checked==false && selectflag==totalcount)
	
	{
		 document.getElementById('PlanId').checked=true;
	}
	
}

function getchkboxcount()
{
	var selectflag=0;
	for(i=0;i<document.manageplan.elements.length;i++)
	{
			if(document.manageplan.elements[i].type=="checkbox" && document.manageplan.elements[i].name!='chkAll')
		{
					  selectflag++;
		}
	}
	return selectflag;
}	
