
//TINY MCE CONFIG
//------------------------------------------------------------------------------------------------------------------------	
	tinyMCE.init({
	    mode : "textareas",
		theme : "advanced",

		plugins : "emotions,imagemanager,filemanager,style,layer,table,save,advhr,advimage,advlink,emotions,insertdatetime,preview,media,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,pagebreak,ibrowser",
		//file_browser_callback : "TinyMCE_simplebrowser_browse", // This is required
        
		//plugin_simplebrowser_browselinkurl : 'jscripts/tiny_mce/plugins/simplebrowser/browser.html?Connector=connectors/php/connector.php',
       //plugin_simplebrowser_browseimageurl : 'jscripts/tiny_mce/plugins/simplebrowser/browser.html?Type=Image&Connector=connectors/php/connector.php',
     
	   //plugin_simplebrowser_browseflashurl : 'jscripts/tiny_mce/plugins/simplebrowser/browser.html?Type=Flash&Connector=connectors/php/connector.php'
		
		theme_advanced_buttons1 :"separator,cut,copy,paste,separator,fontselect,fontsizeselect,separator,bold,italic,underline,strikethrough,separator,forecolor,backcolor,cleanup,insertdate,separator",
		theme_advanced_buttons2 :"separator,undo,redo,separator,sub,sup,bullist,numlist,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,outdent,indent,separator,charmap,separator,emotions,image,media,separator,insertfile,advlink",
		//,emotions,separator
		theme_advanced_buttons3 :"tablecontrols,advhr,separator,print,separator,ltr,rtl,separator,ibrowser",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
        content_css : "example_advanced.css"
        
	});
//------------------------------------------------------------------------------------------------------------------------	
	//JS VALIDATION FOR THIS PAGE
