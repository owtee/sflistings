<?

/// <summary>            ///****************************************************************************

///Company Name: Navigators Software Private Limited

///File Name :tree_structure.thtml page used for tree structure 

///Description  : Manages Tree Structure

///Created By : Sujay Bhattacharya

///Created On : November 20 2008

///------------------------------------------------------------------------------------------------

/// Date         |       Modified By           |           Details

///------------------------------------------------------------------------------------------------

///      |                        |   

///------------------------------------------------------------------------------------------------ ///****************************************************************************    /// </summary> 
?>
<?php
      echo $javascript->link('utils/utils');
	  echo $javascript->link('src/tree');
	  ?>
	  <?php echo $javascript->link('categorytreestructure'); ?>
<?php  echo $html->css('categorytreestructure');
       echo $html->css('zpcal');
      
	   echo $html->css('tree_lines');
	    echo $html->css('tree');
	   
 ?>

<div class="pageMain">
<form action="#" name="frmQuickAssign" method="post" flexy:ignore id="frmQuickAssign">
<input type="hidden" id="typeid" name="typeid" value="">
<input type="hidden" id="predivId" name="predivId" value="">
<input type="hidden" name="copyID" id="copyID" value="" />
<input type="hidden" id="control" name="control" value="1">
<input type="hidden" value="ie5menu" id="divmenuid" name="divmenuid" />

<script language="javascript">
// JavaScript Document
var ie5menu=document.getElementById('ie5menu');

var menuskin = "skin1"; // skin0, or skin1
var display_url = 0; // Show URLs in status bar?
function showmenuie5(e) {
var rightedge = document.body.clientWidth-e.clientX;
var bottomedge = document.body.clientHeight-e.clientY;
if (rightedge < ie5menu.offsetWidth)
ie5menu.style.left = document.body.scrollLeft + e.clientX - ie5menu.offsetWidth;
else
ie5menu.style.left = document.body.scrollLeft + e.clientX;
if (bottomedge < ie5menu.offsetHeight)
ie5menu.style.top = document.body.scrollTop + e.clientY - ie5menu.offsetHeight;
else
ie5menu.style.top = document.body.scrollTop + e.clientY;
ie5menu.style.visibility = "visible";
return false;
}



var menuskin = "skin1"; // skin0, or skin1
//var display_url = 0; // Show URLs in status bar?
function showmenuie4() {
var rightedge = document.body.clientWidth-event.clientX;
var bottomedge = document.body.clientHeight-event.clientY;
if (rightedge < ie5menu.offsetWidth)
ie5menu.style.left = document.body.scrollLeft + event.clientX - ie5menu.offsetWidth;
else
ie5menu.style.left = document.body.scrollLeft + event.clientX;
if (bottomedge < ie5menu.offsetHeight)
ie5menu.style.top = document.body.scrollTop + event.clientY - ie5menu.offsetHeight;
else
ie5menu.style.top = document.body.scrollTop + event.clientY;
ie5menu.style.visibility = "visible";
return false;
}


function click(e) {
 
  if (navigator.appName == 'Netscape'
           && e.which == 3) 
		   {
				
				var presentId=document.getElementById('typeid').value;
				var presentType = presentId.split('^');
				
				if(presentType[0]=="location") 
				{
					
					ie5menu.className = menuskin;
					document.oncontextmenu = showmenuie5;
					ie5menu.style.display = '';
				}
				else
				{
					
					ie5menu.style.visibility = "hidden";
					
					
				}
      		} 
   else {
     if (navigator.appName == 'Microsoft Internet Explorer'
          && event.button==2)
		  {
         		var presentId=document.getElementById('typeid').value;
				var presentType = presentId.split('^');
				
				
				
				if(presentType[0]=="location") 
				{
					ie5menu.className = menuskin;
					document.oncontextmenu = showmenuie4;
					//frame2menu.style.visibility = "hidden";		
					ie5menu.style.display = '';
				}
				else // if(presentType[0]=="survey") 
				{
					
					//frame2menu.className = menuskin;
					//document.oncontextmenu = showmenuie42;
					ie5menu.style.visibility = "hidden";
					//frame2menu.style.display = "";	
					
				}
				
			} 
         }
	
		
   return true;
 }
 
 

		
		
		var tree; // Create global variable for tree

		function toggleLines() {
		var l = document.getElementById("lines");
			l.disabled = !l.disabled;
		}

		function toggle_expandOnLabel() {
			var e = document.getElementById('expandOnLabel')
		
			if (tree.config.expandOnLabel)
				e.innerHTML='Turn On expandOnLabel (currently off)'
			else
				e.innerHTML='Turn Off expandOnLabel (currently on)'

			tree.config.expandOnLabel = (!tree.config.expandOnLabel)
		}

		
		// Create the Zapatec Tree
		function zptree_init() 
		{
			
			tree = new Zapatec.Tree('tree', { saveState: true, saveId: "saveState" });
			tree.onItemSelect = function(item_id) {
				var c = document.getElementById("console");
				if (c != null)
					c.innerHTML = "Item with ID " + item_id + " is currently selected";
			};
		}
		
		
		function mouseOver(divname)
		{
			if(document.getElementById(divname).className!='menuitemsdisable')
			{
			document.getElementById(divname).className='text_left_panel_01';
			}
		}
		
		function mouseOut(divname)
		{
			if(document.getElementById(divname).className!='menuitemsdisable')
			{
			document.getElementById(divname).className='text_left_panel_02';
			}
		}
		
		function deselectIcon(iconID)
		{
			
			document.getElementById(iconID).style.display="none";
			
		}
		
		function selectIcon(iconID)
		{
			document.getElementById(iconID).style.display="block";
			
		}
		
		
		function staticbar()
{
	barheight=document.getElementById("topbar").offsetHeight
	var ns = (navigator.appName.indexOf("Netscape") != -1) || window.opera;
	var d = document;
	function ml(id)
	{
		var el=d.getElementById(id);
		if (!persistclose || persistclose && get_cookie("remainclosed")=="")
		el.style.visibility="visible"
		if(d.layers)el.style=el;
		el.sP=function(x,y){this.style.left=x+"px";this.style.top=y+"px";};
		el.x = 250;
		if (verticalpos=="fromtop")
		el.y = startY;
		else{
		el.y = ns ? pageYOffset + innerHeight : iecompattest().scrollTop + iecompattest().clientHeight;
		el.y -= startY;
		}
		return el;
	}
	window.stayTopLeft=function()
	{
		if (verticalpos=="fromtop")
		{
			var pY = ns ? pageYOffset : iecompattest().scrollTop;
			ftlObj.y += (pY + startY - ftlObj.y)/8;
		}
		else
		{
			var pY = ns ? pageYOffset + innerHeight - barheight: iecompattest().scrollTop + iecompattest().clientHeight - barheight;
			ftlObj.y += (pY - startY - ftlObj.y)/8;
		}
		ftlObj.sP(ftlObj.x, ftlObj.y);
		setTimeout("stayTopLeft()", 10);
	}
	ftlObj = ml("topbar");
	stayTopLeft();
}

function hideRightClickMenue()
{
		//ie5menu.style.display="none";
		
}
</script>
<div id="ie5menu" class="skin1" >
<div id="menu1" class="menuitems" onMouseOver="javascript:mouseOver('menu1')" onMouseOut="javascript:mouseOut('menu1')"  onClick="javascript:createNewLocation()">Add</div>
<div id="menu2" class="menuitems" onMouseOver="javascript:mouseOver('menu2')" onMouseOut="javascript:mouseOut('menu2')" onClick="javascript:renameLocation()">Edit</div>
<div id="menu3" class="menuitems" onMouseOver="javascript:mouseOver('menu3')" onMouseOut="javascript:mouseOut('menu3')"  onClick="javascript:deleteExistingLocation()">Delete</div>
</div>
<script language="javascript" type="text/javascript">
 <!-- DivID and ID if the selected items start here  -->	  
		function clientValue(id,divid)
		{
			
			
			ie5menu.style.visibility = "hidden";
			//document.frmQuickAssign.typeid.value=id;
			document.getElementById('typeid').value=id;
			prevDivName=document.getElementById('predivId').value;
			
			if(prevDivName!='')
			{
				document.getElementById(prevDivName).style.background="";
			}
			
			document.getElementById(divid).style.background="#E9EAEB";
			document.getElementById('predivId').value=divid;
			document.getElementById('control').value=0;
			
			
		}
		
		<!-- DivID and ID if the selected items end here  -->	
		

</script>


<script language="javascript">
document.onmousedown=click
</script>	
<div style="width:100%; height:790px; overflow-y: auto; scrollbar-arrow-color: 
#CDCDCD; scrollbar-base-color: #DCDCDC;scrollbar-DarkShadow-Color: #CDCDCD;"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" onClick="javascript:hideRightClickMenue();">
                <tr>
                  <td valign="top"><div class="scroll-tree" id="treediv"  >
			<ul id='tree'>
			<?php
			   $catname=array();
			   $m=0;
			   
			   foreach($sicategory as $sicat) {
			     //if(!(in_array($sicat['Sicategory']['id'],$catname)) {
			       if($sicat['Sicategory']['parent_id']==0) { 
				      $sql_select_sicategories="select * from `sicategories` where
					                           `parent_id`='".$sicat['Sicategory']['id']."' 
											   and `is_delete`='0' order by `id` asc";
					   $query_sicategories=mysql_query($sql_select_sicategories);
					   $num_of_rows_sicategories=mysql_num_rows($query_sicategories);
					  $catname[$m]=$sicat['Sicategory']['id'];
					   $m++;
				   
			  
			?>
				<li>
					<div id="location<?=$m;?>"  onClick="javascript:clientValue('location^<?=$m;?>','location<?=$m;?>');" >
					<img border='0' src='<?php echo $html->webroot."img/icon_project_01.gif";?>' alt='Client' />
					<strong> <?=$sicat['Sicategory']['category_name'];?></strong></div>
					<?php
					if($num_of_rows_sicategories>0)
					{
					?>
					<ul>
					<?php
					    while($row_sicategories=mysql_fetch_array($query_sicategories))
						{
						     $sql_select_sicategories1="select * from `sicategories` where
					                           `parent_id`='".$row_sicategories['id']."' 
											   and `is_delete`='0' order by `id` asc";
							  $query_sicategories1=mysql_query($sql_select_sicategories1);
							  $num_of_rows_sicategories1=mysql_num_rows($query_sicategories1);
						     $catname[$m]=$row_sicategories['id'];
					          $m++;
					?>
					    
						<li><div id="location<?=$m;?>"  onClick="javascript:clientValue('location^<?=$m;?>','location<?=$m;?>');" >
						<img border='0' src='<?php echo $html->webroot."img/icon_project_01.gif";?>' alt='Client' />
						<strong> <?=$row_sicategories['category_name'];?></strong></div>
						
						  <?php
						     if($num_of_rows_sicategories1>0)
							 {
						  ?>
						      <ul>
						  <?php
								while($row_sicategories1=mysql_fetch_array($query_sicategories1))
								{
									 $sql_select_sicategories2="select * from `sicategories` where
													   `parent_id`='".$row_sicategories1['id']."' 
													   and `is_delete`='0' order by `id` asc";
									  $query_sicategories2=mysql_query($sql_select_sicategories2);
									   $num_of_rows_sicategories2=mysql_num_rows($query_sicategories2);
									 $catname[$m]=$row_sicategories1['id'];
									  $m++;
					       ?>
						
						<li><div id="location<?=$m;?>"  onClick="javascript:clientValue('location^<?=$m;?>','location<?=$m;?>');" >
						<img border='0' src='<?php echo $html->webroot."img/icon_project_01.gif";?>' alt='Client' />
						<strong> <?=$row_sicategories1['category_name'];?></strong></div>
						 <?php
						     if($num_of_rows_sicategories2>0)
							 {
						  ?>
						      <ul>
						  <?php
								while($row_sicategories2=mysql_fetch_array($query_sicategories2))
								{
									 $sql_select_sicategories3="select * from `sicategories` where
													   `parent_id`='".$row_sicategories2['id']."' 
													   and `is_delete`='0' order by `id` asc";
									  $query_sicategories3=mysql_query($sql_select_sicategories3);
									  $num_of_rows_sicategories3=mysql_num_rows($query_sicategories3);
									 $catname[$m]=$row_sicategories2['id'];
									  $m++;
					       ?>
							   <li><div id="location<?=$m;?>"  onClick="javascript:clientValue('location^<?=$m;?>','location<?=$m;?>');" >
							<img border='0' src='<?php echo $html->webroot."img/icon_project_01.gif";?>' alt='Client' />
							<strong> <?=$row_sicategories2['category_name'];?></strong></div>
							<?php
						     if($num_of_rows_sicategories3>0)
							 {
						  ?>
						      <ul>
						  <?php
								while($row_sicategories3=mysql_fetch_array($query_sicategories3))
								{
									 
									 $catname[$m]=$row_sicategories3['id'];
									  $m++;
					       ?>
						
						<li><div id="location<?=$m;?>"  onClick="javascript:clientValue('location^<?=$m;?>','location<?=$m;?>');" >
						<img border='0' src='<?php echo $html->webroot."img/icon_project_01.gif";?>' alt='Client' />
						<strong> <?=$row_sicategories3['category_name'];?></strong></div>
							
							<?php 
							    }
							?>
						</ul>
						 <?php
						       }
						  ?>
							</li>
							<?php 
							    }
							?>
						</ul>
						 <?php
						       }
						  ?>
						  </li>
							<?php 
							    }
							?>
						</ul>
						 <?php
						   }
						  ?>
						
						</li>
						
					<?php 
					    }
					?>
					</ul>
					<?php
					}
					?>
					
				</li>
				<?php
				  }
				 }
				
				 ?>
				<!--<li><div id="location0"  onClick="javascript:clientValue('location^0','location0');" >
					<img border='0' src='' alt='Client' />
					<strong> <? ;?></strong></div></li>
				<li>
					<ul>
						<li><div id="location0"  onClick="javascript:clientValue('location^0','location0');" >
					<img border='0' src='' alt='Client' />
					<strong> World Wide</strong></div></li>
						<li><div id="location0"  onClick="javascript:clientValue('location^0','location0');" >
					<img border='0' src='' alt='Client' />
					<strong> World Wide</strong></div></li>
						<li><div id="location0"  onClick="javascript:clientValue('location^0','location0');" >
					<img border='0' src='' alt='Client' />
					<strong> World Wide</strong></div></li>
					</ul>
				</li>
				<li><div id="location0"  onClick="javascript:clientValue('location^0','location0');" >
					<img border='0' src='' alt='Client' />
					<strong> World Wide</strong></div></li>-->
				
			</ul>		
							
		</div>
		</td>
                </tr></table></div>
