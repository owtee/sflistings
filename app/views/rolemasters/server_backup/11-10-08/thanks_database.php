-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) collate latin1_general_ci NOT NULL default '',
  `password` varchar(30) collate latin1_general_ci NOT NULL default '',
  `fname` varchar(50) collate latin1_general_ci NOT NULL default '',
  `lname` varchar(50) collate latin1_general_ci NOT NULL default '',
  `email` varchar(150) collate latin1_general_ci NOT NULL default '',
  `altemail` varchar(150) collate latin1_general_ci NOT NULL default '',
  `address` varchar(150) collate latin1_general_ci NOT NULL default '',
  `city` varchar(150) collate latin1_general_ci NOT NULL default '',
  `state` varchar(150) collate latin1_general_ci NOT NULL default '',
  `province` varchar(150) collate latin1_general_ci NOT NULL default '',
  `zip` varchar(150) collate latin1_general_ci NOT NULL default '',
  `country` varchar(50) collate latin1_general_ci default '0',
  `phone` varchar(150) collate latin1_general_ci NOT NULL default '',
  `skype` varchar(150) collate latin1_general_ci NOT NULL default '',
  `comments` text collate latin1_general_ci NOT NULL,
  `lastlogindate` date NOT NULL default '0000-00-00',
  `lastlogintime` varchar(12) collate latin1_general_ci NOT NULL default '',
  `lastloginip` varchar(20) collate latin1_general_ci NOT NULL default '',
  `isblocked` char(1) collate latin1_general_ci NOT NULL default '0',
  `isdelete` char(1) collate latin1_general_ci NOT NULL default '0',
  `createdon` text collate latin1_general_ci,
  `modifiedon` text collate latin1_general_ci,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `users`
-- 

INSERT INTO `users` VALUES (4, 'kallol', 'kal', 'kallol', 'samanta', 'kallolsam@gmail.com', 'kk', 'asd lane', 'howrah', 'wb', 'asd', '657567', '103', '909089097890', 'fgf@ss', 'it;s working', '2008-08-08', '07:40:51', '192.168.0.116', '0', '0', '1215843069', '1216366173');
INSERT INTO `users` VALUES (5, 'test', 'test', 'test1', 'test2', 'test@test.com', '', '', '', '', '', '', '', '', '', '', '2008-07-17', '13:06:58', '192.168.0.116', '0', '0', '1216039558', '1216279867');
        