<?php
class CommonHelper extends Helper {
	function countdaysleft($diff)
	{
		
		if($diff>0)
		{
			if($diff>60)
			{
				$sD = 86400;
				$sH = 3600;
				$sM = 60;
				$d = ((int)($diff / $sD));
				$agoDay = ($d>1) ? $d." days" : $d." day";
				$diff = ($diff - ($d * $sD));
				$h = ((int)($diff / $sH));
				$agoHour = ($h>1) ? $h." hrs" : $h." hr";
				$diff = ($diff - ($h * $sH));
				$m = ((int)($diff / $sM));
				$agoMin = ($m>1) ? $m." mins" : $m." min";
				$diff = ($diff - ($m * $sM));
				//return $agoDay." ".$agoHour." ".$agoMin;
				return $agoDay." ".$agoHour;
			}
			else
				return "Less than a minute.";
		}
		else
		{
			return "Project Expired.";
		}
	}
	  function selectTag($title,$class,$firstElm,$optionArr=array(),$selected=null,$linkPage,$divname)
		{
		
		  //pr("alert('first')");
		  
			$selectTag='<select name="'.$title.'" id="'.$title.'" class="'.$class.'" onchange="return false;">';
			if($firstElm!=NULL)
			
				$selectTag.='<option value="">'.$firstElm.'</option>';
				
			foreach($optionArr as $key=>$val)
				{
					if($key==$selected)
						$selectTag.='<option value="'.$key.'" selected="selected">'.$val.'</option>';
					else
						$selectTag.='<option value="'.$key.'">'.$val.'</option>';
				}
			$selectTag.='</select>';
			$selectTag.='<script type="text/javascript">';
			$selectTag.="alert(".$title.")";
			//pr("alert(".$title.")");
			$selectTag.="Event.observe('".$title."', 'change', function(event)";
				$selectTag.="{ var comboval=document.getElementById('".$title."').value; ";
				//$selectTag.="alert(comboval);";
				
				$selectTag.="new Ajax.Updater(document.createElement('div'),'$linkPage&orderBy='+comboval, {asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '";
				foreach($divname as $key=>$val)
					{
						$selectTag.=$val;
						$selectTag.=' ';
					}
				$selectTag.="']}); return false;";
				$selectTag.='}, false);';
				
			$selectTag.='</script>';
		return $selectTag;
		}
	
	function checkBox($uniqueId,$linkUrl,$updateDiv,$chk,$unchk)
		{
			$checkBox='<input name="checkbox" type="checkbox" id="'.$uniqueId.'"/>';
			$checkBox.='<script type="text/javascript">';
			$checkBox.="Event.observe('".$uniqueId."', 'click', ";
			$checkBox.='function(event) ';
				$checkBox.='{ '; 
					$checkBox.="if(document.getElementById('".$uniqueId."').checked==true) ";
					$checkBox.='new Ajax.Updater';
						$checkBox.='( ';
						$checkBox.="document.createElement('div'), ";
						$checkBox.="'".$linkUrl."/".$chk."', ";
							$checkBox.='{ ';
								$checkBox.="asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '".$updateDiv."'] ";
							$checkBox.='} ';
						$checkBox.='); ';
						
					$checkBox.=" if(document.getElementById('".$uniqueId."').checked==false) ";
					$checkBox.='new Ajax.Updater ';
						$checkBox.='( ';
						$checkBox.="document.createElement('div'), ";
						$checkBox.="'".$linkUrl."/".$unchk."', ";
							$checkBox.='{ ';
								$checkBox.="asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '".$updateDiv."'] ";
							$checkBox.='} ';
						$checkBox.='); ';
					
				$checkBox.='}, false); ';
			$checkBox.='</script>';
			return $checkBox;
		}
		
	function input($uniqueId,$linkUrl,$updateDiv,$remain,$ar)
		{
			foreach($ar as $key=>$val)
				{
					$attribute=$key.'="'.$val.'"&nbsp;';
				}
	
			$textBox='<input name="'.$uniqueId.'" type="input" id="'.$uniqueId.'" '.$attribute.' onblur="return false;"/>';
			$textBox.='<script type="text/javascript">';
			$textBox.="Event.observe('".$uniqueId."', 'blur', ";
			$textBox.='function(event) ';
				$textBox.='{ var comboval=document.getElementById("'.$uniqueId.'").value; if(document.getElementById("'.$uniqueId.'").value=="") return false; 
				if(parseInt(document.getElementById("'.$uniqueId.'").value)<='.$remain.') '; 
					//$checkBox.="if(document.getElementById('".$uniqueId."').checked==true) ";
					$textBox.='new Ajax.Updater';
						$textBox.='( ';
						$textBox.="document.createElement('div'), ";
						$textBox.="'".$linkUrl."/'+comboval, ";
							$textBox.='{ ';
								$textBox.="asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '".implode(" ",$updateDiv)."'] ";
							$textBox.='} ';
						$textBox.='); else alert("Quantity in stock : "+'.$remain.'+"");';
				$textBox.='}, false); ';
			$textBox.='</script>';
			return $textBox;
		}
		
	function inputTag($uniqueId,$linkUrl,$updateDiv,$ar)
		{
			$spltArr=explode("/",$uniqueId);
			$attribute="";
			foreach($ar as $key=>$val)
				{
					$attribute.=$key.'="'.$val.'" ';
				}

			$uniqueId=ucfirst($spltArr['0']).ucfirst($spltArr['1']);
			$textBox='<input name="data['.$spltArr['0'].']['.$spltArr['1'].']" type="input" id="'.$uniqueId.'" '.$attribute.' onblur="return false;"/>';
			$textBox.='<script type="text/javascript">';
			$textBox.="Event.observe('".$uniqueId."', 'blur', ";
			$textBox.='function(event) ';
				$textBox.='{ var comboval=document.getElementById("'.$uniqueId.'").value; if(document.getElementById("'.$uniqueId.'").value=="") return false;'; 
					//$checkBox.="if(document.getElementById('".$uniqueId."').checked==true) ";
					$textBox.='new Ajax.Updater';
						$textBox.='( ';
						$textBox.="document.createElement('div'), ";
						$textBox.="'".$linkUrl."/'+comboval, ";
							$textBox.='{ ';
								$textBox.="asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '".implode(" ",$updateDiv)."'] ";
							$textBox.='} ';
						$textBox.=');';
				$textBox.='}, false); ';
			$textBox.='</script>';
			return $textBox;
		}


		
		########### ADDDED BY SANJIB FOR TICKET PRICE SECTION #######
		

		function selectTicTag($title,$firstElm,$optionArr=array(),$selected= null,$linkPage,$comboid,$divname=array('ajaxPagination','ajaxSorting'))
		{
		//pr($selected);
			$selectTag='<select name="'.$title.$comboid.'" id="'.$title.$comboid.'" onchange="return false;">';
			if($firstElm!=NULL)
				$selectTag.='<option value="0">'.$firstElm.'</option>';
				
			foreach($optionArr as $key=>$val)
				{
					if($key==$selected)
						$selectTag.='<option value="'.$key.'" selected="selected">'.$val.'</option>';
					else
						$selectTag.='<option value="'.$key.'">'.$val.'</option>';
				}
			$selectTag.='</select>';
			$selectTag.='<script type="text/javascript">';
			$selectTag.="Event.observe('".$title.$comboid."', 'change', function(event)";
				$selectTag.="{ var comboval=document.getElementById('".$title.$comboid."').value; ";
				$selectTag.="new Ajax.Updater(document.createElement('div'),'$linkPage/'+comboval, {asynchronous:true, evalScripts:true, requestHeaders:['X-Update', '";
				foreach($divname as $key=>$val)
					{
						$selectTag.=$val;
						$selectTag.=' ';
					}
				$selectTag.="']}); return false;";
				$selectTag.='}, false);';
			$selectTag.='</script>';
		return $selectTag;
		}
		
function countTimeSpent($date)
{
	
	$timestamp1=strtotime("now");
	$timestamp2=strtotime($date); 
	$today = date("D j M. Y",$timestamp2);
	$difference = $timestamp1 - $timestamp2;
	$day_diff=floor($difference / 84600);
	if($day_diff >=1 && $day_diff<7)
	{
		if($day_diff==1)
		{
			$value_date="updated about a day ago";
		}
		else
		{
			$value_date="updated about ".$day_diff." days ago";
		}
		
	}
	if($day_diff >=7 && $day_diff<30)
	{
		$week_valu2=floor($day_diff/7);
		if($week_valu2==1)
		{
			$value_date="updated about a week ago";
		}
		else
		{
			$value_date="updated about ".$week_valu2." weeks ago";
		}
	}
	if($day_diff >=30 && $day_diff<365)
	{
		$month_valu2=floor($day_diff/30);
		if($month_valu2==1)
		{
			$value_date="updated about a month ago";
		}
		else
		{
			$value_date="updated about ".$month_valu2." months ago";
		}
	}
	if($day_diff >365)
	{
		$year_valu2=floor($day_diff/365);
		if($year_valu2==1)
		{
			$value_date="updated about a year ago";
		}
		else
		{
			$value_date="updated about ".$year_valu2." years ago";
		}
	}
	if($day_diff<1)
	{
		  $difference -= 84600 * floor($difference / 84600);
		  $value_date=floor($difference / 3600);
		  if($value_date<1)
		  {
			 $difference -= 3600 * floor($difference / 3600);
			 $value_date=floor($difference / 60);
			 if($value_date==0 || $value_date==1)
			 {
				$value_date="updated about ".$value_date." Minute ago";
			 }
			 else
			 {
				$value_date="updated about ".$value_date." Minutes ago";
			 }
		  }
		  else
		  {
			if($value_date!=1)
			{
				$value_date="updated about ".$value_date." hours ago";
			}
			else
			{
				$value_date="updated about ".$value_date." hour ago";
			}
		  }
	}
	
	return $value_date;
}

	
} 
?>