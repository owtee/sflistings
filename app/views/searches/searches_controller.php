<?php
///////////**** This section is Totaly used for managing different tasks of administrator ******////////////
class SearchesController extends AppController
{
	var $name = 'Searches'; 
	var $helpers = array('Html','javascript','pagination','Ajax'); 
	var $uses=array('Search','Neighborhood','Filter','Category','Listing','Imagelisting','User','Filterlisting','Searchneighborhood','Usersearch','Searchfilter','Language','Statistics','Advertise');
	var $components = array('Pagination','Frontpagination','Currency_conversion','PHPMailer');
    //var $layout='alluser';
	
	//.......Start page................
   function index($city_id=NULL, $category_id=NULL)
 	{
		$this->layout = "before_userlogin";
        $this->pageTitle = 'Mlsupdate';	
		
		$this->set('stat_city_id',$city_id);
		$this->set('stat_category_id',$category_id); 
		
		$this->checkUserSession();	
		 
		$cityID = 1;
		if(isset($_SESSION['cityID']))
		$cityID = $_SESSION['cityID'];
		
		if(isset($city_id))	
		{	
			$cityID = $city_id;		
			$_SESSION['cityID'] = $city_id;
		}
		
		$this->set('city_id',$cityID);
		
		$catID = 1;
		if(isset($_SESSION['categoryID']))
		$catID = $_SESSION['categoryID'];
		
		if(isset($category_id))	
		{	
			$catID = $category_id;		
			$_SESSION['categoryID'] = $catID;
		}
		
		$this->set('urlkey','search');		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id=$cityID";
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "neighborhood_name asc";   
		  
		$neighborhooddata = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		
		/*if(!empty($neighborhooddata) && $this->checkLang() != 'en')
		{
		 $neighborhooddata = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborhooddata);
		}*/
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $neighborhooddata = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborhooddata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($neighborhooddata as $key=>$value)
                        {
                                   $neighborhooddata[$key]['Neighborhood']['neighborhood_name'] = $this->changeLang2($neighborhooddata[$key]['Neighborhood']['neighborhood_name']); 
                        }
                    
                }
                
                
		$this->set('neighborhooddata',$neighborhooddata);	
		 	
			
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		} 
		$this->set('languagedata',$languagedata);	
			
			
		
	 	$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$citydata= $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($citydata as $key=>$value)
                        {
                                   $citydata[$key]['City']['city_name'] = $this->changeLang2($citydata[$key]['City']['city_name']); 
                        }
                    
                }
		
		$this->set('citydata',$citydata);
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc,Filter.id asc";     
	 
	 $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);

	 //print_r($filterdata);exit;
	/*if($this->checkLang() != 'en')
	{
	 $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
	 $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
	}*/
        
        
        if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $filterdata = $this->changeLang('1',"Filter","filter_name",$filterdata);
                        $filterdata = $this->changeLang('1',"Categoryfilter","category_filter_name",$filterdata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($filterdata as $key=>$value)
                        {
                                  $filterdata[$key]['Filter']['filter_name'] = $this->changeLang2($filterdata[$key]['Filter']['filter_name']); 
                                  $filterdata[$key]['Categoryfilter']['category_filter_name'] = $this->changeLang2($filterdata[$key]['Categoryfilter']['category_filter_name']);  
                        }
                    
                }
                
        
	 $this->set('filterdata',$filterdata);	 
	
	 
	  $condition = "isdelete='0' AND id=$catID";
	 
	 $filelds = array('category_name');	
	 
	 $categorydata = $this->Category->findAllCategoryDetail($condition, $filelds, NULL, NULL, NULL);
         
         if($this->checkLang() != 'en')
		{
                        $categorydata = $this->changeLang('1',"Category","category_name",$categorydata);
		} 
		$this->set('category_data',$categorydata);
	 
	 $this->set('emailFire','');
         
        
	    
	  $this->set('controller',$this->params['controller']);
	  $this->set('referer',$this->params['url']['url']);
	  $this->set('indexContent',$this->translateContent());				
	  $this->set('advertise',$this->siteAdd());
 	
  	}
	
	
function editSearch($id=NULL)
 	{
		if(empty($id))
		$this->redirect('/');
		
		$this->layout = "before_userlogin";
                $this->pageTitle = 'Mlsupdate';	 
		$this->checkUserSession();	
		
		$cityID = 1;
		if(isset($_SESSION['cityID']))
		$cityID = $_SESSION['cityID'];
		
		if(isset($city_id))	
		{	
			$cityID = $city_id;		
			$_SESSION['cityID'] = $city_id;
		}
		
		$this->set('city_id',$cityID);
		
		$catID = 31;
		if(isset($_SESSION['categoryID']))
		$catID = $_SESSION['categoryID'];
		
		if(isset($category_id))	
		{	
			$catID = $category_id;		
			 $_SESSION['categoryID'] = $catID;
		}
		
		$this->set('urlkey','search');		
		
		$condition = "isdelete='0' and isblocked= 0 and city_id=$cityID";
		
		$filelds = array('neighborhood_name','id');
		
		$order_by = "id asc";     
		$neighborhooddata = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);
		if(!empty($neighborhooddata) && $this->checkLang() != 'en')
		{
		 $neighborhooddata = $this->changeLang('1',"Neighborhood","neighborhood_name",$neighborhooddata);
		} 
		$this->set('neighborhooddata',$neighborhooddata);	 	
		
	 	$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));
		
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		} 
		$this->set('languagedata',$languagedata);
		
		
		
	 $condition = "Filter.isdelete='0' AND Filter.id in (SELECT filter_id FROM `categoryassignfilters` WHERE categorie_id = $catID)";
	 
	 $filelds = array('Filter.id','Filter.filter_name','Categoryfilter.id','Categoryfilter.category_filter_name');
	 
	 $order_by = "Categoryfilter.id asc,Filter.id asc"; 
	 
	 $filterdata = $this->Filter->findAllFilterDetail($condition, $filelds, $order_by, NULL, NULL);
		
	 $this->set('filterdata',$filterdata);	 
	
	 
	  $condition = "isdelete='0' AND id=$catID";
	 
	 $filelds = array('category_name');	
	 
	 $this->set('category_data',$this->Category->findAllCategoryDetail($condition, $filelds, NULL, NULL, NULL));
	 
	 $this->set('emailFire','');
	 
		$condition = "Usersearch.id=$id AND user_id=".$_SESSION['userId'].""; 	
		
		$this->set('save_search',$this->Usersearch->allUserSearch($condition, NULL, NULL, NULL, NULL));
		
		$condition = "usersearch_id=$id"; 	
		
		$usersearch_neighborhood= $this->Searchneighborhood->allUserSearchNeighborhood($condition, NULL, NULL, NULL, NULL);
		
		$user_search_neighborhood = array();
		
		foreach($usersearch_neighborhood as $value)
		{
			array_push($user_search_neighborhood,$value['Searchneighborhood']['neighborhood_id']);
		}
		
		$this->set('usersearch_neighborhood',$user_search_neighborhood);
		
		
		$condition = "usersearch_id='".$id."'"; 		
		
		$usersearch_filter = $this->Searchfilter->allUserSearchFilter($condition, NULL, NULL, NULL, NULL);
		
		$user_search_filter = array();
		
		foreach($usersearch_filter as $value)
		{
			array_push($user_search_filter,$value['Searchfilter']['filter_id']);
		}
		
		$this->set('usersearch_filter',$user_search_filter);
		
		$this->set('usersearch_id',$id);
	    
	  $this->set('controller',$this->params['controller']);
	  $this->set('referer',$this->params['url']['url']);
	  $this->set('indexContent',$this->translateContent());				
	  $this->set('advertise',$this->siteAdd());
	  
	  $this->render('index');	
 		
  	}
	function city($id=NULL)
	{
		$this->layout = "before_userlogin";
                $this->pageTitle = 'Mlsupdate';
	} 
	
	function ajax_missinfo()
 	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		//print_r($this->data);
		//$this->checkUser();
		if($this->data)
		{
			 $headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";		
			$headers .= "From: ".$this->data['Miisinfo']['from']."\r\n";
			$sub = $this->data['Miisinfo']['subject'];
			//$mes = $this->data['Miisinfo']['content'];
                        
                         $mes ="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
         
	   <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>		
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  
  <tr>
    <td style=\"text-align:center; color:#7486c0; font-weight:bold; font-size:16px;\">&nbsp;</td>
  </tr>
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td width=\"25\" valign=\"top\"></td>
        <td width=\"700\" style=\"font-size:12px;\">".$this->data['Miisinfo']['content']."</td>
      </tr>
      <tr>
        <td colspan=\"2\">&nbsp;</td>
        
      </tr>
      <tr>
        <td colspan=\"2\">Thanks & Regards<br>
                  ".$this->data['Miisinfo']['from']."
        
        </td>
        
      </tr>
      <tr>
        <td colspan=\"2\">&nbsp;</td>
        
      </tr>
    </table></td>
  </tr>
  
</table>";
			
			$sent = mail($this->data['Miisinfo']['mailto'],$sub,$mes,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
					echo $message = 1;			
			}
			else
			echo $message = 0;	
		}
	}
	function ajax_suggestion()
 	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		//print_r($this->data);
		//$this->checkUser();
		if($this->data)
		{
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: ".$this->data['Miisinfo']['from']."\r\n";
			$headers .= "Bcc: admin@mlsupdate123.com\r\n";
			$sub = $this->data['Miisinfo']['subject'];
			
			$mes = $this->data['Miisinfo']['content'];
			
			$mes .="<br/>---------------<br/><strong>Regards From,<br /></strong><img src='http://www.mlsupdate123.com/img/logo.jpg' alt='MLSUpdate123' />";
			
			$sent = mail($this->data['Miisinfo']['mailto'],$sub,$mes,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
					echo $message = 1;			
			}
			else
			echo $message = 0;	
		}
	}
	
	function ajax_email()
 	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');

		$condition = "isdelete='0' and isblocked= 0 AND type='a'";
			
			$filelds = array('name','file','url');
			
			$order_by = "rand()"; 	
			
			$advertise = $this->Advertise->findAllAdvertiseDetail($condition,$filelds,$order_by,NULL,NULL); 
		if($this->data)
		{
		    
			$fromaddress="inquiry@mlsupdate123.com";
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$headers .= "From: {$fromaddress}\r\n";
			$headers .= "Reply-To: {$fromaddress}\r\n";
            $headers .= "Return-Path: {$fromaddress}\r\n";
			$headers .= "Bcc: admin@mlsupdate123.com\r\n";
			//$headers .= "Bcc: abhishek@navsoft.in\r\n";
			
			$sub = $this->data['Emailinfo']['subject'];
			$mes = "Sender's Email : ".$this->data['Emailinfo']['from']."<br /> Sender's Phone No : ".$this->data['Emailinfo']['phone']."<br />".$this->data['Emailinfo']['content'];
			$listinglink="http://www.mlsupdate123.com/listingdetail/".$this->data['Emailinfo']['listinglink'];
			$mes .="<br/><a href='{$listinglink}'> Click here to view this listing </a><br />---------------<br />
			<strong>Regards From,<br /></strong><img src='http://www.mlsupdate123.com/img/logo.jpg' alt='MLSUpdate123' />";
			
			$comments="<table><tr>
    <td><table width=\"650\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">";
     
						$imgCount=0;
						foreach($advertise as $image)
						{
						
						   $linkpic = $image['Advertise']['file'];
						   $imagePath = URL."uploadimage/add/".$linkpic;
						   $url = $image['Advertise']['url'];
						   $contents = "<a href=\"http://".$url."\"><img src=\"".$imagePath."\" alt=\"\" border=\"0\" ></a>";
										
						
						   if($imgCount%2 == 0)
						   {
						   echo "<tr>";
						   }
						   $comments .='<td width="50%" align="center" valign="top" class="newslr_border_b">'.$contents.'</td>';   
						  if($imgCount%2 == 1)
						   {
						   $comments .= "</tr>";
						   }
						$imgCount++;
						}
						
						 if($imgCount%2 == 1)
						   {
						   $comments .= "</tr>";
						   }
	  
	  
     $comments .="</table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>";
			$mes .=$comments;
			$sent = mail($this->data['Emailinfo']['mailto'],$sub,$mes,$headers);
			
			
			if($sent){
					$this->Session->setFlash('Message has been sent');
					echo $message = 1;			
			}
			else
			echo $message = 0;	
		}
	}
	
	
	function ajax_availability()
	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		$this->checkUser();
		$listingid = $this->data['Miisinfo']['property_id'];
		$sold = $this->data['Miisinfo']['sold'];
		if(!empty($listingid))
		{
			if($sold==1)
			$sold = 0;
			else
			$sold = 1;
			
			if($this->Listing->updateAvailable($sold,$listingid))
			{
				$message = "yes";		
			} 
			else
                                $message = "no";	
			
			$this->set('message',$message);	
			$this->set('sold',$sold);			
			$this->set('listing_id',$listingid);
                        $this->set('indexContent',$this->translateContent());
                        $this->set('indexContent1',$this->translateContent1());
		}
	}
	function ajax_suggestion1()
 	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		//print_r($this->data);
		//$this->checkUser();
		if($this->data)
		{
			$headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
			$headers .= "From: ".$this->data['Miisinfo']['from1']."\r\n";
			
			$sub = $this->data['Miisinfo']['subject1'];
			$mes = $this->data['Miisinfo']['content1'];
			
			$sent = mail($this->data['Miisinfo']['mailto1'],$sub,$mes,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
					echo $message = 1;			
			}
			else
			echo $message = 0;	
		}
	}
	function ajax_suggestion2()
 	{
		$this->layout = 'ajax';
		Configure::write('debug', '0');
		//print_r($this->data);
		//$this->checkUser();
		if($this->data)
		{
                        $headers  = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";			
			
			$headers .= "From: ".$this->data['Miisinfo']['from2']."\r\n";
			$headers .= "Bcc: admin@mlsupdate123.com\r\n";
			$sub = $this->data['Miisinfo']['subject2'];
                        
                        $comments ="<table width=\"748\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 1px #993333; background-color:#ffffff;\">
  <tr>
    <td><table width=\"734\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td style=\"background-color:#A44141; height:80px;\">
		<table width=\"720\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
         
	   <tr>
		  
            <td  valign=\"bottom\" style=\"font-family: Book Antiqua; font-size:26px; color:#ffffff; text-align:left;\"><img src=\"".URL."img/logo.jpg\" /></td>
          </tr>		
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      
    </table>    </td>
  </tr>
  
  <tr>
    <td style=\"text-align:center; color:#7486c0; font-weight:bold; font-size:16px;\">&nbsp;</td>
  </tr>
  <tr>
    <td><table width=\"725\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
      <tr>
        <td width=\"25\" valign=\"top\"></td>
        <td width=\"700\" style=\"font-size:12px;\">".$this->data['Miisinfo']['content2']."</td>
      </tr>
      <tr>
        <td colspan=\"2\">&nbsp;</td>
        
      </tr>
      <tr>
        <td colspan=\"2\">Thanks & Regards<br>
                  ".$this->data['Miisinfo']['from2']."
        
        </td>
        
      </tr>
      <tr>
        <td colspan=\"2\">&nbsp;</td>
        
      </tr>
    </table></td>
  </tr>
  
</table>";
                        
			//$mes = $this->data['Miisinfo']['content2'];
			
			$sent = mail($this->data['Miisinfo']['mailto2'],$sub,$comments,$headers);
	
			if($sent){
					$this->Session->setFlash('Message has been sent');
					echo $message = 1;			
			}
			else
			echo $message = 0;	
		}
	}
	function result($page=1)
 	{
              
              
		$this->checkUserValidSession();	
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Mlsupdate'; 
						
		$this->set('urlkey','search');		
		
	 	$condition = "isdelete='0' AND isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));	 			
		
		//---- Start By Abhsihek for keeping search statistics
		$cityID = $_SESSION['cityID'];
		$categoryID=$_SESSION['categoryID'];
		$minPrice=$this->data['Listing']['minprice'];
		$maxPrice=$this->data['Listing']['maxprice'];
		$neighborhoodArray=array();
		$languageArray=array();
		$typeArray=array();
		$filtersUsedArray=array();
		if(isset($this->data['Listing']['neighborhood']))
		{
		    foreach($this->data['Listing']['neighborhood'] as $neighborhood)
			{
			   //$this->Statistics->insertInfo($cityID,$categoryID,$neighborhood,$minPrice,$maxPrice);
			   array_push($neighborhoodArray,$neighborhood);
			}
			
		}
		
		if(isset($this->data['Listing']['language']))
		{
		
		    foreach($this->data['Listing']['language'] as $language)
			{
			   array_push($languageArray,$language);
			}
		
		}
		
		if(isset($this->data['Listing']['type']))
		{
		
		    foreach($this->data['Listing']['type'] as $type)
			{ 
			   array_push($typeArray,$type);
			}
		
		}
		//Now filling all filter's id
		if(isset($this->data['Filterlisting']['filterid']))
		{
		    foreach($this->data['Filterlisting']['filterid'] as $filter)
			{ 
			   array_push($filtersUsedArray,$filter);
			}
		}
		print_r($this->params);
		$this->Statistics->insertInfo($cityID,$categoryID,$neighborhoodArray,$languageArray,$typeArray,$minPrice,$maxPrice,$filtersUsedArray);
		//exit(0);
		
		//---- End By Abhsihek 
		
		
		$this->set('city_id',$cityID);
		
		if($this->data['Usersearch']['flag']==1 && isset($_SESSION['userId']))
			{
				$this->data['Usersearch']['user_id'] = $_SESSION['userId'];
				$this->data['Usersearch']['category_id'] = $_SESSION['categoryID'];
				$this->data['Usersearch']['city_id'] = $_SESSION['cityID'];
				$this->data['Usersearch']['minprice'] = $this->data['Listing']['minprice'];
				$this->data['Usersearch']['maxprice'] = $this->data['Listing']['maxprice'];						
				$this->data['Usersearch']['day'] = $this->data['Usersearch']['emailRemainderDayOfWeek'];
				$this->data['Usersearch']['date'] = $this->data['Usersearch']['emailRemainderDate'];
				$this->data['Usersearch']['time'] = $this->data['Usersearch']['emailRemainderHour'].':'.$this->data['Usersearch']['emailRemainderMinute'].' '.$this->data['Usersearch']['emailRemainderTime'];
				$this->data['Usersearch']['add_date'] = date('Y-m-d');
                                $this->data['Usersearch']['serach_name'] = $this->data['Usersearch']['filterNameValue'];
                                $this->data['Usersearch']['address'] = trim($this->data['Usersearch']['addressValue']);
				$this->Usersearch->save($this->data);
				
				if(isset($this->data['Usersearch']['id']))
				$usersearch_id =$this->data['Usersearch']['id'];
				else
				$usersearch_id = mysql_insert_id();			
				
				
				if(isset($this->data['Listing']['neighborhood']))
					{
						$this->Searchneighborhood->deleteSearchNeighborHood($usersearch_id);
						foreach($this->data['Listing']['neighborhood'] as $value)		
							{				
								$this->Searchneighborhood->saveNeighborHood($value,$usersearch_id);
							}
					}	
				
				if(isset($this->data['Filterlisting']['filterid']))
					{
						$this->Searchfilter->deleteSearchFilter($usersearch_id);
						foreach($this->data['Filterlisting']['filterid'] as $value)		
							{				
								$this->Searchfilter->saveFilter($value,$usersearch_id);
							}
					}	
				
			}
		
		
			/*$search_neighborhood='';
			if(sizeof($neighborhood_data) > 0)
		    $search_neighborhood .= " AND (".implode(" OR ",$neighborhood_data).")";*/
		    
			$search_price = '';
			if($this->data['Listing']['minprice'])
			$search_price .= " AND Listing.price >=".$this->data['Listing']['minprice'];
			
			if($this->data['Listing']['maxprice'])
			$search_price .= " AND Listing.price <=".$this->data['Listing']['maxprice'];
                        
                        $conditionType ="";
                        if(isset($this->data['Listing']['type']))
                                {
                                      $conditionType .=" AND (";  
                                        $view_type_array = $this->data['Listing']['type'];
                                        for($i=0;$i<sizeof($this->data['Listing']['type']);$i++)
                                                {                                                       
                                                        if($this->data['Listing']['type'][$i]=='video')
                                                        {
                                                               $conditionType .="  Listing.video_url!=''"; 
                                                        }
                                                        else if($this->data['Listing']['type'][$i]=='floorplans')
                                                        {
                                                             $conditionType .=" Listing.floorplan!=''";     
                                                        }
                                                        else if($this->data['Listing']['type'][$i]=='pictures')
                                                        {
                                                             $conditionType .=" Imagelisting.file!=''";     
                                                        }
                                                    
                                                    if(sizeof($this->data['Listing']['type'])==$i+1)
                                                       $conditionType .= ")";
                                                    else
                                                        $conditionType .= " OR";     
                                                }
                                               
                                                
                                        
                                }
                        
                      
		//print_r($this->data); exit;
		/*** First Query For result Using Price ***/
		$languages ='';
                
		if(isset($this->data['Listing']['language']))
		{
			$conditionLanguage ='AND (';
                         $count=0;
			foreach($this->data['Listing']['language'] as $value)
				{
                                       if($count>0)
                                       $conditionLanguage .=' OR ';
					//$languages .= "'".$value."',";
                                     $conditionLanguage .=" User.gender like '%,".$value."' or  User.gender like '".$value.",%' or User.gender like '%,".$value.",%' or User.gender =".$value."";
                                     $count++;
				}
                             $conditionLanguage .=")" ;  
			//$languages = substr($languages,0, -1);
		}
		
		
		 $condition = "Listing.active=1 AND is_deleted = '1' AND Listing.category_id = ".$_SESSION['categoryID']." AND Listing.city_id=".$_SESSION['cityID']." $conditionType";	
		if(isset($this->data['Listing']['language']))
		{
		 $condition .= $conditionLanguage;
                }
                 if(isset($this->data['Usersearch']['addressValue']))
                 {
                        $condition .= " AND (streetnumber LIKE '%".trim($this->data['Usersearch']['addressValue'])."%' OR streetname LIKE '%".trim($this->data['Usersearch']['addressValue'])."%' OR state LIKE '%".trim($this->data['Usersearch']['addressValue'])."%' OR CONCAT( streetnumber,' ',streetname ) LIKE '%".trim($this->data['Usersearch']['addressValue'])."%')"; 
                 }
                 
                 
                 
                 //print_r($_SESSION); exit;
		 
		/* if($search_neighborhood!='')
		 $condition = $condition.$search_neighborhood;*/
		 
		 if($search_price!='')
		 $condition =$condition.$search_price;
		//$condition = "Listing.active=1 ";
		
		 	
		
		$order_by = "Listing.price DESC";     
		
		$filelds = array('Listing.id DISTINCT','User.email','User.id','Listing.price','Listing.created','Listing.availabledate','Listing.user_id','Listing.streetname','Listing.streetnumber','Listing.price_request');
		
                //$this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
                
		$listing_detail = $this->Listing->allListing($condition, $filelds, $order_by, NULL, NULL);
		
                //print_r($listing_detail); exit;
		
		/*** First Query end For result Using Price ***/
		
		$result_array = array();
		
		
		if(isset($this->data['Listing']['neighborhood']))
		{
			for($i=0; $i<sizeof($listing_detail); $i++)
				{
				$neighborhood_data = array();
				
				for($j=0; $j<sizeof($listing_detail[$i]['Neighborhoodlisting']); $j++)
						{
							array_push($neighborhood_data,$listing_detail[$i]['Neighborhoodlisting'][$j]['neighborhood_id']);
						}
					
					
					if(sizeof($neighborhood_data)>0)
						{
							if($this->checkTwoNeighborhood($this->data['Listing']['neighborhood'],$neighborhood_data))
							array_push($result_array,$listing_detail[$i]);
						}						
				
				}
				$result_array;		
		}
		else
		$result_array = $listing_detail;	
		
				
		$result_array_filter = array();
		$result_neighborhood = array();
		$result_listing = array();
		
		if(isset($this->data['Filterlisting']['filterid']))
		{
			for($i=0; $i<sizeof($result_array); $i++)
				{
					$filter_array = array();
					for($j=0; $j<sizeof($result_array[$i]['Filterlisting']); $j++)
						{
							array_push($filter_array,$result_array[$i]['Filterlisting'][$j]['filter_id']);
						}
						
					if(sizeof($filter_array)>0)
						{
							if($this->checkTwoArray($this->data['Filterlisting']['filterid'],$filter_array))
							array_push($result_array_filter,$result_array[$i]);
						}
				}
				
			foreach($result_array_filter as $neighborhood)
			{
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{						
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					}
				array_push($result_neighborhood,$neighborhood_list);				
			}	
			//$this->set('neighborhoodDetail',$result_neighborhood);		
			$result_listing = $result_array_filter;		
			
		}
		else	
		{
		
		//print_r($result_array);exit;		
		foreach($result_array as $neighborhood)
			{
			
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{	
								
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						//print_r($neighborhoodDetail);
						//exit;
                                                
						if(isset($neighborhoodDetail[0]['Neighborhood']['neighborhood_name']))
						{
						  $neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
						}  
					}
					
					
				array_push($result_neighborhood,$neighborhood_list);				
			}
			
			
		$result_listing = $result_array;
		
		}
		
		
			
                $this->set('totalPage',$this->frontPaginationTotalPage(sizeof($result_listing),LIMIT));
                $this->set('page',$page);
                $this->set('link','results');
			
		$this->Session->write('listingDetail',$result_listing);		
		$this->Session->write('listingNeighborhood',$result_neighborhood);
		
		if(isset($this->data['Listing']['type'])) 
		$view_type_array = $this->data['Listing']['type'];
		else
		$view_type_array = array();
                
		
		$this->Session->write('user_view_type',$view_type_array);
		
		$show_listing_array = array();
		$show_neighborhood_array = array();
		for($i=0;$i<LIMIT;$i++)
		{
			if(!empty($result_listing[$i]))
			{
				array_push($show_listing_array,$result_listing[$i]);
				array_push($show_neighborhood_array,$result_neighborhood[$i]);
			}
		}
		
		
		$this->set('listingDetail',$show_listing_array);
		$this->set('neighborhoodDetail',$show_neighborhood_array);	
		$this->set('indexContent',$this->translateContent());	
		$this->set('indexContent1',$this->translateContent1());	 			
		$this->set('advertise',$this->siteAdd());
  	}
	
    function myinfo($user_id=NULL,$page=1)
 	{
		if(empty($user_id))
		$this->redirect('/');
		
		$this->layout = "before_userlogin";
                $this->pageTitle = 'Mlsupdate';	
		
		$this->cityData('city','no');
		
		
		$condition = "Listing.user_id = $user_id AND Listing.is_deleted = 1 AND Listing.active=1";		
		
		$order_by = "Listing.id desc";  		 		
			
		
		$filelds = array('Listing.id DISTINCT','User.email','User.id','Listing.price','Listing.created','Listing.availabledate','Listing.user_id','Listing.streetname','Listing.streetnumber','Listing.price_request');
		
		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, 5, $page);	
		//print_r($listingDetail);exit;
		if(empty($listingDetail))
		$this->redirect('/');
		
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{						
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
                        
                $this->set('user_id',$user_id);    
                $this->User->id = $user_id;
                $this->set('userdata',$this->User->read());
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);	
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		$this->set('advertise',$this->siteAdd());
                $this->set('userid',$user_id);
		
		 
	}
        
         function myalllisting($user_id=NULL,$page=1)
 	{
		
		if(empty($user_id))
		$this->redirect('/');
		
		$this->layout = "before_userlogin";
        $this->pageTitle = 'Mlsupdate';	
		
		$this->cityData('city','no');
		
		
		$condition = "user_id = ".$user_id." AND is_deleted = 1 AND Listing.active=1";		
		
		$order_by = "Listing.id desc";   
		  
			
			//$this->set('totalPage',$this->frontPagination($condition,LIMIT));
			$this->set('page',$page);
			$this->set('link','myalllisting/'.$user_id);
		
		//$filelds = array('Listing.id DISTINCT','Listing.price_request','Listing.price','Listing.created','Listing.streetnumber','Listing.streetname','City.city_name','Category.category_name','Listing.active','Listing.sold','Listing_counter.counter','Listing.availabledate');
		$filelds = array('Listing.id DISTINCT','User.email','User.id','Listing.price','Listing.created','Listing.availabledate','Listing.user_id','Listing.streetname','Listing.streetnumber','Listing.price_request');
		$this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
		$listingDetail = $this->Listing->allListing($condition, $filelds, $order_by, LIMIT, $page);	
		//print_r($listingDetail);exit;
		
		if(empty($listingDetail))
		$this->redirect('/');
		
                
				$this->Listing->unbindModel(array('hasOne' => array('Neighborhoodlisting','Imagelisting')));
                $all_record = $this->Listing->allListing($condition, $filelds, $order_by, NULL, $page);
                        
                $totalPage = $this->frontPaginationTotalPage(sizeof($all_record),LIMIT);
                        
                $this->set('totalPage',$totalPage);
		
		$neighborhood_data = array();
		
		foreach($listingDetail as $neighborhood)
			{
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{						
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					}
				array_push($neighborhood_data,$neighborhood_list);				
			}
			
		
		$this->set('listingDetail',$listingDetail);	
		$this->set('neighborhoodDetail',$neighborhood_data);	
		$this->set('indexContent',$this->translateContent());
		$this->set('indexContent1',$this->translateContent1());
		$this->set('advertise',$this->siteAdd());			
		
		 
	}
	
	function listingdetail()
 	{
		$this->layout = "before_userlogin";
                $this->pageTitle = 'Mlsupdate';			
		
		$this->set('urlkey','search');		
		
	 	$condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));	 			
		
		$cityID = $_SESSION['cityID'];
		
		$this->set('city_id',$cityID);		
	 $this->set('indexContent',$this->translateContent());	
	 $this->set('advertise',$this->siteAdd());	
 		
  	}
	
function ajax_login() 
  {
  			$this->layout = "ajax";
			$this->pageTitle = 'Mlsupdate123';	
			//$this->render('change_city', 'ajax');
			//echo 'hello';
			Configure::write('debug', '0');
			$message = '';
			//print_r($this->data);
			if(!empty($this->data))
		 	{
					 //pr($this->data);
					 $username=$this->data['User']['username'];
					 $password=$this->data['User']['password'];
					 
					 $condition = "username='$username' AND isblocked='0' AND isdelete='0' AND password=md5('$password')";	 
					 $filelds = array('id','username','email','fname','lname','mlsID');			 
					
					 $user_details=$this->User->userLogin($condition, $filelds);
					//print_r($user_details);
					
					 if($user_details)
					 {	
					 				
						if($this->User->userUpdate($user_details[0]['User']['id']))
						{
							$this->Session->write('userId',$user_details[0]['User']['id']);
							$this->Session->write('userName',$user_details[0]['User']['username']);
							$this->Session->write('userEmail',$user_details[0]['User']['email']);
							$this->Session->write('fname',$user_details[0]['User']['fname']);
							$this->Session->write('lname',$user_details[0]['User']['lname']);
							
							if(!empty($user_details[0]['User']['mlsID']))
							$this->Session->write('broker','1');
						}	
						
						 $message = '1';			
					
					  }
					  else
					  {
					  	 $message='2';
					  }
			  
			 }
			 else{
			     $message = '3';	
			 }
			//echo $message;exit;
			$this->set('message',$message);
			// $this->render('ajax_login','Ajax');	
 }
 
 
  function checkTwoArray($array1,$array2)
			{
				
				for($i=0;$i<sizeof($array1);$i++)
					{
						$check=false;
						
							for($j=0;$j<sizeof($array2);$j++)
								{								
									if($array2[$j]==$array1[$i])
										{											
											$check=true;	
											break;
										}
															
								}
								
								if($check==true)
								break;
						
					}
					
					return $check;
			}
			
		function checkTwoNeighborhood($array1,$array2)
			{
				
				for($i=0;$i<sizeof($array1);$i++)
					{
						$check=false;
						
							for($j=0;$j<sizeof($array2);$j++)
								{								
									if($array2[$j]==$array1[$i])
										{											
											$check=true;	
											break;
										}
															
								}
								
								if($check==true)
								break;
						
					}
					
					return $check;
			}
			
			
	function editsearchparameter()
		{
			$this->checkUser();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Mlsupdate';			
			
			$this->set('urlkey','search');	
						
			$condition = "isdelete='0' and isblocked= 0";	
					
			$filelds = array('city_name','id');		
				
			$order_by = "id asc";     
			
			$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));
			
		$condition = "";
		
		$filelds = array('language','id');
		
		$order_by = "id asc";     
		$languagedata = $this->Language->findAllLanguageDetail($condition, $filelds, $order_by, NULL, NULL);
		
		if(!empty($languagedata) && $this->checkLang() != 'en')
		{
		 $languagedata = $this->changeLang('1',"Language","language",$languagedata);
		} 
		$this->set('languagedata',$languagedata);
			
			$cityID = 1;
			
			//$this->set('city_id',$cityID);	
				
			
			$condition = "active='y' AND user_id = ".$_SESSION['userId'];	
					
			$filelds = array('serach_name','add_date','id');		
				
			$order_by = "id desc";     
			
			$this->set('user_search',$this->Usersearch->allUserSearch($condition, $filelds, $order_by, NULL, NULL));	 			
			$this->set('indexContent',$this->translateContent());		
			$this->set('advertise',$this->siteAdd());
		
			
		}
		
	function deletesearchparameter($id = NULL)
		{
			$this->checkUser();
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Mlsupdate';
						
			$this->Usersearch->deleteUserSearch($id);	
			$this->set('advertise',$this->siteAdd());		
			$this->redirect('/editsearch');			
		
			
		}
	
		
	function searchresult($page=1,$searchValue = NULL)
		{
		//$this->chk_controller();exit;
		//print_r($this->data);
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Mlsupdate';
			$this->pageTitle = 'Mlsupdate';
			
			$this->set('urlkey','search');	
			
			$search_val = '';
			if(isset($_REQUEST['searchValue']))
			{
				$this->params['form']['searchValue'] = $_REQUEST['searchValue'];
				$this->Session->write('searchValue',$_REQUEST['searchValue']);
				$search_val = $_REQUEST['searchValue'];
			}
			
		$this->set('searchValue',$search_val);	
		
	 	$condition = "isdelete='0' AND isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));	 			
		
		$cityID = 1;
		if(isset($_SESSION['cityID']))
		$cityID = $_SESSION['cityID'];
		
		//$this->set('city_id',$cityID);
		
		
		
		//$condition ='LEFT JOIN `neighborhoodlistings` AS `Neighborhoodlisting` ON (`Neighborhoodlisting`.`listing_id` = `Listing`.`id`)';
		$condition1 ='';
                $condition = '';
			if(isset($this->params['form']['searchValue']))
			{
				
				//if(is_numeric(trim($this->params['form']['searchValue'])))
				// $condition = "Listing.active=1 AND (Listing.id=".trim($this->params['form']['searchValue']).")" ;	
				// else
				//{
				  	$city_detail = $this->City->findBycity_name(trim($this->params['form']['searchValue']));
				 	if($city_detail)
					{
						$condition1 .=" OR Listing.city_id = ".$city_detail['City']['id']."";
					}
					
					$category_detail = $this->Category->findBycategory_name(trim($this->params['form']['searchValue']));
				 	if($category_detail)
					{
						$condition1 .=" OR Listing.category_id = ".$category_detail['Category']['id']."";
					}
					
					$neighborhood_detail = $this->Neighborhood->findByneighborhood_name(trim($this->params['form']['searchValue']));
				 	if($neighborhood_detail)
					{
						$condition1 .=" OR Neighborhoodlisting.neighborhood_id = ".$neighborhood_detail['Neighborhood']['id']."";
					}
                                        
                                        if(is_numeric(trim($this->params['form']['searchValue'])))
                                        {
                                             $condition .=  "Listing.id='".trim($this->params['form']['searchValue'])."' OR ";  
                                        }
					
				 $condition = "Listing.active=1 AND Listing.is_deleted=1 AND ( $condition streetnumber LIKE '%".trim($this->params['form']['searchValue'])."%' OR streetname LIKE '%".trim($this->params['form']['searchValue'])."%' OR state LIKE '%".trim($this->params['form']['searchValue'])."%' OR phone LIKE '%".trim($this->params['form']['searchValue'])."%' OR Listing.email LIKE '%".trim($this->params['form']['searchValue'])."%' OR CONCAT( streetnumber,' ',streetname ) LIKE '%".trim($this->params['form']['searchValue'])."%' $condition1 )" ;		 
				//}
				
			//echo $condition; 
			
			$this->set('page',$page);
			$this->set('link','searchresult');
			$this->set('searchParameter',$this->params['form']['searchValue']);
			
			$order_by = "Listing.price ASC";     
			
			$filelds = array('Listing.id DISTINCT','User.email','User.id','Listing.price','Listing.created','Listing.availabledate','Listing.streetnumber','Listing.streetname','City.city_name','Listing.price_request','Listing.zip');
			
			$listing_detail = $this->Listing->allListing($condition, $filelds, $order_by, LIMIT, $page);
                        
                        $all_record = $this->Listing->allListing($condition, $filelds, $order_by, NULL, $page);
                        
                        $totalPage = $this->frontPaginationTotalPage(sizeof($all_record),LIMIT);
                        
                        $this->set('totalPage',$totalPage);
			
			//print_r($listing_detail); exit;
					
			$result_neighborhood =array();
			
			foreach($listing_detail as $neighborhood)
			{
			
				$neighborhood_list ='';
				foreach($neighborhood['Neighborhoodlisting'] as $neighborhood_detail)
					{	
								
						$condition = "Neighborhood.id = ".$neighborhood_detail['neighborhood_id']."";			
						$filelds = array('Neighborhood.neighborhood_name');			
						$order_by = "Neighborhood.id asc";	 
						$neighborhoodDetail = $this->Neighborhood->allNeighborhood($condition, $filelds, $order_by, NULL, NULL);	
						if($neighborhoodDetail)
						$neighborhood_list .=$neighborhoodDetail[0]['Neighborhood']['neighborhood_name'].', ';
					}
					
					
				array_push($result_neighborhood,$neighborhood_list);				
			}
			
			//print_r($listing_detail);exit;
			$this->set('listingDetail',$listing_detail);
			$this->set('neighborhoodDetail',$result_neighborhood);	
			
		}
		 $this->set('indexContent',$this->translateContent());	
		 $this->set('indexContent1',$this->translateContent1());  
		 $this->set('advertise',$this->siteAdd());			
			$this->render('result');		
		}
		
		
	function frontPagination($criteria,$show)
		{
			
			$count = $this->Listing->findCount($criteria,0);
			$pageCount = ceil($count / $show );
			
			return $pageCount;
			
		}
		
	function frontPaginationTotalPage($total_record,$show)
		{
			
			
			$pageCount = ceil($total_record / $show );
			
			return $pageCount;
			
		}
		
	function results($page=1)
		{
                        
			//print_r($_SESSION);
			$this->layout = "before_userlogin";
			$this->pageTitle = 'Mlsupdate';			
			$this->checkUserSession();	
                        	
			$this->set('urlkey','search');	
			
			if($page==NULL || $page==0)	
			$page=1;
		
	 	$condition = "isdelete='0' AND isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$this->set('citydata',$this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL));	 			
		
		$cityID = !empty($_SESSION['cityID'])?$_SESSION['cityID']:'';
		
		$this->set('city_id',$cityID);
					
				
			$show_listing_array = array();
			$show_neighborhood_array = array();	
				
			if(isset($_SESSION['listingDetail']) && isset($_SESSION['listingNeighborhood']))
				{
					
					$this->set('totalPage',$this->frontPaginationTotalPage(sizeof($_SESSION['listingDetail']),LIMIT));
					$this->set('page',$page);
					$this->set('link','results');	
					
					/*if($page>1)
					$no_of_record = ($limit*$page);
					else
					$no_of_record = ($limit*$page);	*/
				
					for($i=LIMIT*($page-1);$i<LIMIT*$page;$i++)
					{
						if(!empty($_SESSION['listingDetail'][$i]))
						{
							array_push($show_listing_array,$_SESSION['listingDetail'][$i]);
							array_push($show_neighborhood_array,$_SESSION['listingNeighborhood'][$i]);
						}
					}
                                        $this->set('listingDetail',$show_listing_array);
			$this->set('neighborhoodDetail',$show_neighborhood_array);	
					
				} else
                                $this->redirect('/');
			//$this->Session->write('listingDetail',$result_listing);
			//$this->Session->write('listingNeighborhood',$result_neighborhood);
			
					
			
			$this->set('indexContent',$this->translateContent());	
		        $this->set('indexContent1',$this->translateContent1());			
			$this->set('advertise',$this->siteAdd());
			$this->render('result');	
			
		}
	function ajax_autocomplete()
	{
		 $strSearchVal = $_REQUEST['searchValue'];
		 $this->layout = "ajax";
		 Configure::write('debug', '0');
		 //$this->set('searchValue',$strSearchVal);
		 $filelds = array("concat( streetnumber, ' ', streetname ) AS address");
		 $this->set('posts',$this->Listing->findAll("streetnumber LIKE '".$strSearchVal."%' OR streetname LIKE '".$strSearchVal."%'GROUP BY streetnumber,streetname",$filelds));
	}
	
		
	function currencyconversion($amtVal = 0) 
   {
       // $x = $this->Currency_conversion->CurrencyConverter();
	   //$rates = $this->Currency_conversion->downloadExchangeRates();
	   //pr($rates);
     //$newAmt =  $this->Currency_conversion->convert($_REQUEST['amt'],'USD',$_REQUEST['orderBy']);
	 //echo $this->params['form']['currency'];
	 //echo $amtVal;
   	$newAmt =  $this->Currency_conversion->convert($amtVal,'USD',$this->params['form']['currency']);
	//echo $newAmt;
   	$this->set('price', $newAmt);
	$this->render('price_value', 'ajax');
	 
  }
  function deleteconfirmation($sid=NULL)
	{
		$this->layout = "before_userlogin";
		$this->pageTitle = 'Mlsupdate';
		$this->Usersearch->deleteUserSearch($sid);
				 
	}
}
?>