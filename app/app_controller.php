<?php
/* SVN FILE: $Id: app_controller.php 6305 2008-01-02 02:33:56Z phpnut $ */
/**
 * Short description for file.
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) :  Rapid Development Framework <http://www.cakephp.org/>
 * Copyright 2005-2008, Cake Software Foundation, Inc.
 *								1785 E. Sahara Avenue, Suite 490-204
 *								Las Vegas, Nevada 89104
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright 2005-2008, Cake Software Foundation, Inc.
 * @link				http://www.cakefoundation.org/projects/info/cakephp CakePHP(tm) Project
 * @package			cake
 * @subpackage		cake.cake
 * @since			CakePHP(tm) v 0.2.9
 * @version			$Revision: 6305 $
 * @modifiedby		$LastChangedBy: phpnut $
 * @lastmodified	$Date: 2008-01-01 20:33:56 -0600 (Tue, 01 Jan 2008) $
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */
/**
 * This is a placeholder class.
 * Create the same file in app/app_controller.php
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		cake
 * @subpackage	cake.cake
 */
 
error_reporting(0); 
class AppController extends Controller {
		  
	   var $uses=array('City','User','Advertise','Metatag','Statistics','Listing');
	 

	  public function beforeRender(){
       header("Pragma: no-cache");
	   header("Cache-Control: no-cache");
	   header("Expires: Thu, 01 Dec 1994 16:00:00 GMT");
	    $this_page = (isset($_SERVER['QUERY_STRING']) && ($_SERVER['QUERY_STRING'] != '')) ? ($_SERVER['QUERY_STRING']) : '';
	    $this_page=substr($this_page,4);
		$urlArr=explode("/",$this_page);
		
		
		if($urlArr[0]=="listingdetail")
		{
		    if(isset($urlArr[1]))
			{
				if(isset($urlArr[1]) && !empty($urlArr[1]))
				{
				   $data=$this->Listing->query("Select id from listings WHERE id='{$urlArr[1]}' AND is_deleted=1");
				   if($data)
				   {
					   $date=$this->Statistics->getTime(42.1497,-74.9384);
					   $rs=mysql_query("Select count(*) AS NUM_ROW from property_statistics where date(DATE)=date('{$date}') AND PROPERTY_ID='{$urlArr[1]}'") or die(mysql_error);
					   while($rec=mysql_fetch_array($rs))
					   {
						   if($rec['NUM_ROW']>0){
									$this->Statistics->insertPropertyInfo($urlArr[1]);
						   }
					   //exit(0);
					   }
				   }
				  
				  
				}
				
			}				
		}
		
		if($urlArr[0]=="mobile" && $urlArr[1]=="viewlisting")
		{
		    if(isset($urlArr[2]))
			{
				if(isset($urlArr[2]) && !empty($urlArr[2]))
				{
				  $this->Statistics->insertPropertyInfo($urlArr[2]);
				}
				
			}				
		}
	
    } 
         
         

function checkSession()
    {
		
		$tr=$this->Session->read('admin_id');
        if(empty($tr))
        {
            $this->redirect('/admin/');
        }
		
    }
function checkUserSession()
    {
		
        if(!isset($_SESSION))
        {
            $this->redirect('/');
        }
		
    }	
public function metatagclass()
{
	 $arrMetatag = Metatag::findAllMeta();
	 return $arrMetatag;
}
function checkUser()
    {
		
		$tr=$this->Session->read('userId');
        if(empty($tr))
        {
            $this->redirect('/signin');
        }
		
    }
    
    function checkUserValidSession()
    {
	//print_r($_SESSION);	
        if(!isset($_SESSION['cityID']) || !isset($_SESSION['categoryID']))
        {
            $this->redirect('/');
        }
		
    }
	
function checkBroker()
    {		
        if($this->Session->read('broker'))
        {
          //$this->Session->setFlash('You are not a Broker!');
		  return true;
        }
		else
		{
		 $this->redirect('/');
		} 
    }
function checklang()
{
		$lang = $this->Session->read('lang');
        if(empty($lang))
        {
            $lang = 'en';
        }
		$this->lang = $lang;
	return $lang;	
}	

public function siteAdd()
{

		$condition = "isdelete='0' and isblocked= 0 AND type='s'";
		
		$filelds = array('name','file','url');
		
		$order_by = "rand()"; 	
			
		$advertise = $this->Advertise->findAllAdvertiseDetail($condition,$filelds,$order_by,NULL,NULL); 	 	   
			
		return $advertise;	
}	


public function cityData($urlKey,$select=NULL)
{

		$cityID = 1;
		if(isset($_SESSION['cityID']))
		$cityID = $_SESSION['cityID'];
		
		if(isset($city_id))	
		{	
			$cityID = $city_id;		
			$_SESSION['cityID'] = $city_id;
		}
		$this->set('urlkey',$urlKey);		
		if($select!='no')
                  $this->set('city_id',$cityID);
			
			
		  $condition = "isdelete='0' and isblocked= 0";
		
		$filelds = array('city_name','id');
		
		$order_by = "id asc";     
		
		$citydata = $this->City->findAllCityDetail($condition, $filelds, $order_by, NULL, NULL);	 
		
		/*if($this->checkLang() != 'en')
		{
		 $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}*/
                
                if($this->checkLang() != 'en' && $this->checkLang() != 'zh-TW')
		{
                        $citydata = $this->changeLang('1',"City","city_name",$citydata);
		}
                elseif($this->checkLang() == 'zh-TW')
                {
                        foreach($citydata as $key=>$value)
                        {
                                   $citydata[$key]['City']['city_name'] = $this->changeLang2($citydata[$key]['City']['city_name']); 
                        }
                    
                }
		$this->set('citydata',$citydata);
}	


function translateContent($stat='')
    {
		
		$header_content = "If a contact person tells you the property is no longer available click the appropriate button and we will take the ad down";
		//$this->set('header_content',$header_content);
		$header_content1 = "If we don't have exactly what you are looking for set up an e-mail alert and we'll let you know immediately, just register your e-mail(don't worry we're not spammers) then set up your search and click set up e-mail alerts.";
		//$this->set('header_content1',$header_content1);
		$header_content2 = "If we don't have exactly what you are looking for set up an e-mail alert and we'll let you know immediately, just register your e-mail(don't worry we're not spammers) 
							then set up your search and click set up e-mail alerts.";	
							
		$strContent = "Welcome|Home|Logout|Signup Now?|Forgot Password?|Search.....|".$header_content1."|".$header_content."|Feedback|Contact Us|About Us|Privacy Policy|Terms of Use|Neighborhoods|Search Parameters|".$header_content2."|Price range|Minimum|Maximum|Search|All";
		$strContent1 = "To post ads click here|My Listing|My Events|My Newsletters|Please post a picture of yourself here so it will show up in all of your ads|Save Search Management|My Account|Change Password|Log Out|Post Ads|".$this->User->findContent()."|User Name";
		$strContent2 = "";
		//if(isset($stat) && $stat!='')
		//{
			$strContent2 .= "Username|Email|Password|Confirm Password|First Name|Last Name|Language|Address|State/Province|Town/City|Telephone|Zip|Office Number|Fax Number|Instant Messenger|Skype Account|Facebook URL|MySpace URL|NYS Broker Identification Number (required to send newsletters)";
			$strContent2 .= "|Denotes a required field.|We offer free newsletters, we only require that you put our sponsors in your newsletter. If you do not wish to have our sponsors in your newsletter then we ask for $10 a month and you can send all the e-mails you like. If you know anyone who would like to be in our newsletters please have them contact us at advertisewithus@straightforwardlistings.com";
			$strContent2 .= "|View My Account|Edit My Account|Signup|Cancel|Submit|Edit|Edit Picture|Deactivate|Activate|Delete|Duplicate|Available|View Listing|Click here for suggestion|View Lister Info|floorplan|Map|photos|listing details|Description|Details|Pet Policy|Property no longer available|Click here to change Availablity Status|Account Information|Contact Information|Other Information|Broker Information|Cell Phone|MisInformation|Share With Friend|Print Details|Click here if this listing violates fair housing laws|Click here if listing is no longer available|Add To Newsletter|contact|Contact Name|Broker ID|next|prev|list|Set Email Alert|Videos|Pictures|Floorplans|Select Type|Select which you want to see|Broker Language|Search for a broker who speaks selected language|Lister Info|Current listing|E-Mail Me|View All My Listings|View All Listing|Delete|STEPS|Image|Description|Action|back|Edit Newsletter Content|Title|Select which image(s) you want to send |
E-mail subject|Content|Signature|Newsletter Title|send|broker|Not a broker|update|You can upload multiple JPG files. (Maximum size of 2MB per photo)|Picture|Add More|Title|Start Date|End Date|Start Time|End Time|Location|Street|Unit/Apt|State|Contact Phone|Contact Email|View Event|Edit Event|Create Event|My Event|Listing|Please add | to your adress book so our e-mails won't get trapped in your spam folder";
		//}
		$strContent = $strContent."|".$strContent1."|".$strContent2;
		$strContent_old = $strContent."|".$strContent1."|".$strContent2;

		if($this->checkLang() != 'en')
		{
			$strContent = $this->changeLang('2',$strContent);
		}
		//$indexContent = explode("|",$strContent);
		if($strContent=='')
		$strContent=$strContent_old;
		
		$indexContent = explode("|",trim($strContent));
		return $indexContent;
		
		
    }
    function translatecontent1($stat='')
    {
	 	if($this->params['controller'] == 'listings')
		{
		  $strContent3 = 'Home|Create Listing|select listing type|Listing Details|City|Neighborhood|Street Number|Street Name|Unit/Apt|Zip Code|Not Exact Address|Title|Description|Sq. Ft.|Sq. Meter|Price|Price on request|CLICK HERE IF YOU ARE A BROKER AND YOU ARE WILLING TO  SHARE A COMMISION ON THIS LISTING|Paste the URL from your YouTube video|Availability Date|Upload Floor plan|Upload Images|Upload More|CONTACT|Contact Name|Contact Phone|Text Friendly|Contact Email|Blackberry Friendly|Office Number|Fax Number|Instant Messenger|Skype Account|Facebook URL|MySpace URL|Linked In URL|Other URL|Advanced Filters|PET POLICY|Description|My Listing|Listing Results|ID|Date|Viewing|Edit Listing|Video URL|You can select maximum three neighborhoods';
		}
		if($this->params['controller'] == 'searches')
		{
		  $strContent3 = 'Home|Search|Listing Results|neighborhood|price|No Record(s)|Price-Request Information|Mail To|Subject|Content|Available Date|ID|View Listing';
		}
		if($this->params['controller'] == 'users')
		{
		  $strContent3 = 'Home|Change Password|Old Password|New Password|Confirm Password|Upload Broker|Upload Broker ads|Upload Picture|Remove Picture|Change Picture|My Event|EVENT TITLE|EVENT DATE|EVENT CREATED|ACTION|No Record(s) exists|My Newsletter|Add Mailing Type|MAILING TYPE|ACTION|Add / Send Mail|EMAIL ADDRESS|Click Here To Prepare News Letter|Add E-Mail Address|Check All|Uncheck All|Prepare News Letter|Ads|Listing|Title|Description|Price|Manage Ads|Email-Ads|Content|Edit|Delete|View|Send Invitation|Do you want to delete the Event?|Create Event||Email|Response|Send Invitation|If you want to add contact address or import contact adddress from Yahoo,Google,American Online,Outlook Express etc.|Click Here|Approved Invitation|Reject Invitation|No Response';
		}
		$strContent3_old = $strContent3;
		if($this->checkLang() != 'en')
		{
			$strContent3 = $this->changeLang('2',$strContent3);
		}
		if($strContent3=='')
		$strContent3=$strContent3_old;
		//$indexContent = explode("|",$strContent);		
		$indexContent1 = explode("|",trim($strContent3));
		return $indexContent1;
    }
    function chk_controller()
    {
	 print_r($this->params);
    }
//the public variables are used for PAYPAL
		public $returnUrl="";
		public $cancelUrl="";
		public $notifyUrl="";
		public $amountPay=NULL;
		public $userEmail=NULL;
		public $eventId=NULL;
		public $userId=NULL;
		public $merchantEmail=NULL;
		public $itemName=NULL;
		public $itemNumber=NULL;
		public $itemQuantity=NULL;
		public $invoice=NULL;
		public $custom=NULL;
		public $paypalUrl='https://www.sandbox.paypal.com/cgi-bin/webscr';
		public $opts = array("text" => "", "language_pair" => "en|nl");
   		public $out = "";
		
		function paypal()
			{
				$this->Paypal->paypal_class();
				$this->Paypal->paypal_url = $this->paypalUrl;   // testing paypal url
			
				$this->Paypal->add_field('business', $this->merchantEmail);
				$this->Paypal->add_field('return', $this->returnUrl);
				//$this->Paypal->add_field('cancel_return', $this->cancelUrl);
				$this->Paypal->add_field('notify_url', $this->notifyUrl);
				$this->Paypal->add_field('item_name', $this->itemName);
				$this->Paypal->add_field('amount', $this->amountPay);
				$this->Paypal->add_field('quantity', $this->itemQuantity);
				$this->Paypal->add_field('custom', $this->custom);
				$this->Paypal->add_field('invoice',$this->invoice);       
				$this->Paypal->add_field('cbt','Return to Xoimages.com to complete your purchase');
				$this->Paypal->submit_paypal_post(); // submit the fields to paypal
			}
	function getCity()
	{
		$arrCity = $this->User->fetchcityfn();
	 	$this->set('arrCity',$arrCity);
	}
    public function setOpts($opts) {
        if($opts["text"] != "") $this->opts["text"] = $opts["text"];
        if($opts["language_pair"] != "") $this->opts["language_pair"] = $opts["language_pair"];
    }
public function asian_lang($currLang)
{
	$arrLang = array('0'=>'ja','1'=>'ko','2'=>'zh-CN','3'=>'zh-TW','4'=>'ru');
	if(in_array($currLang,$arrLang))
	{
		return true;
	}
	else
	{
		return false;
	}
}
   /* public function translate() {
        $this->out = "";
        $google_translator_url = "http://translate.google.com/translate_t?langpair=".urlencode($this->opts["language_pair"])."&amp;";
        $google_translator_data = "text=".urlencode($this->opts["text"]);
        $gphtml = $this->postPage(array("url" => $google_translator_url, "data" => $google_translator_data));
        $out = substr($gphtml, strpos($gphtml, "<div id=result_box dir=\"ltr\">"));
        $out = substr($out, 29);
        $out = substr($out, 0, strpos($out, "</div>"));
		//print_r(utf8_encode($out));
		if($this->asian_lang($this->lang))
		{
        	//echo 'success';exit;
			$this->out = $out;
		}
		else
		{
			$this->out = utf8_encode($out);
		}	
		//$file_path= "a.php";
		//$fp=fopen($file_path,"w") or die("could not open");
		//fwrite($fp,$this->out)or die("could not write");
        return $this->out;
    } */
	
	public function translate() {
        $this->out = "";
        $google_translator_url = "http://translate.google.com/translate_t?langpair=".urlencode($this->opts["language_pair"])."&amp;";
        $google_translator_data = "text=".urlencode($this->opts["text"]);
        $gphtml = $this->postPage(array("url" => $google_translator_url, "data" => $google_translator_data));
        //echo htmlspecialchars($gphtml);
		// New Implementation of google translator by (Abhishek).
		$returnHtml = array();
		$returnRawHtml = $gphtml;	
		preg_match_all("/<div id=utranscell(.*)\<\/div\>/Uis",$returnRawHtml,$returnHtml,PREG_PATTERN_ORDER);
		if (isset($returnHtml[0][0])) 
		{
          $out = strip_tags($returnHtml[0][0]);
		}
		else
		{
		$out="Failed to translate ... please contact your system administrator";
		}
		
	    // exit(0);
		
		// Blocking the old code
		//$out = substr($gphtml, strpos($gphtml, "<div id=result_box dir=\"ltr\">"));
        //$out = substr($out, 29);
        //$out = substr($out, 0, strpos($out, "</div>"));
		
		//echo htmlspecialchars($out);
	    //exit(0);
		//print_r(utf8_encode($out));
		if($this->asian_lang($this->lang))
		{
        	//echo 'success';exit;
			$this->out = $out;
		}
		else
		{
			$this->out = utf8_encode($out);
		}	
		//$file_path= "a.php";
		//$fp=fopen($file_path,"w") or die("could not open");
		//fwrite($fp,$this->out)or die("could not write");
		
        return $this->out;
    }

    // post form data to a given url using curl libs
    public function postPage($opts) {
        $html = "";
        if($opts["url"] != "" && $opts["data"] != "") {
            $ch = curl_init($opts["url"]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $opts["data"]);
            $html = curl_exec($ch);
            if(curl_errno($ch)) $html = "";
            curl_close ($ch);
        }
        return $html;
    }
	public function changeLang($flag,$fieldName2='',$fieldName3='',$keywordsArr=array()) {
		$strLang = '';
		$lang = $this->checkLang();
		$original_array = $keywordsArr;
		if($flag==1)
		{
			for($count = 0;$count < sizeof($keywordsArr);$count++)
			{
				$strLang .= $keywordsArr[$count][$fieldName2][$fieldName3]."|";
			}
			$strLang = substr($strLang,0,-1);
                        
			$this->setOpts(array("text" =>stripslashes($strLang), "language_pair" => "en|".$lang.""));
			$this->translate();
			$strLangNew = explode("|",str_ireplace("<!-- span-->","",trim(html_entity_decode($this->out))));
                        //print_r($strLangNew);
			for($count = 0;$count < sizeof($strLangNew);$count++)
			{
				$keywordsArr[$count][$fieldName2][$fieldName3] = $strLangNew[$count];
			}
			if(!empty($this->out))
			return $keywordsArr;
			else
			return $original_array;
		}
		if($flag==2)
		{
			$strLang = $fieldName2;
			$this->setOpts(array("text" =>stripslashes($strLang), "language_pair" => "en|".$lang.""));
			$this->translate();
			return trim(html_entity_decode($this->out));
		}	
	}
        
        
        public function changeLang2($keywords) {
		//$strLang = '';
		$lang = $this->checkLang();
		
			/*for($count = 0;$count < sizeof($keywordsArr);$count++)
			{
				$strLang .= $keywordsArr[$count][$fieldName2][$fieldName3]."|";
			}
			$strLang = substr($strLang,0,-1);*/
			$this->setOpts(array("text" =>stripslashes($keywords), "language_pair" => "en|".$lang.""));
			$this->translate();
			return trim(html_entity_decode($this->out));
                        //print_r($strLangNew);
			/*for($count = 0;$count < sizeof($strLangNew);$count++)
			{
				$keywordsArr[$count][$fieldName2][$fieldName3] = $strLangNew[$count];
			}
			return $keywordsArr;*/
		
			
	}		
}
?>